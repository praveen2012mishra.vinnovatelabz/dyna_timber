// import Dashboard from "views/Dashboard/Dashboard.jsx";
// import ErrorPage from "views/Pages/ErrorPage.jsx";
// import LockScreenPage from "views/Pages/LockScreenPage.jsx";
// import LoginPage from "views/Pages/LoginPage.jsx";
// import RegisterPage from "views/Pages/RegisterPage.jsx";
// import UserProfile from "views/Pages/UserProfile.jsx";
// import MachinesPage from "views/Machines/MachinesPage";
// import ProductionDataPage from "views/ProductionData/ProductionDataPage";

import { lazy } from 'react';
import { PieChart, VerifiedUser } from '@material-ui/icons';
import FinishedInventoryReportPage from 'views/Reports/FinishedInventoryReport/FinishedInventoryReportPage';
import ReleaseReportPage from 'views/Reports/ReleaseReport/ReleaseReportPage';
import DrumModePage from 'views/InputPages/DrumMode/DrumModePage';
import DrumPerformancePage from 'views/Reports/DrumResults/DrumPerformance/DrumPerformancePage';
import DrumMissedChargesPage from 'views/Reports/DrumResults/DrumMissedCharges/DrumMissedChargesPage';
import DrumOutOfSequencePage from 'views/Reports/DrumResults/DrumOutOfSequence/DrumOutOfSequencePage';
import DrumLoadingReportPage from 'views/Reports/DrumLoadingReport/DrumLoadingReportPage';
import TreatmentChargeTimesPage from '../src/views/Reports/TreatmentChargeTimes/TreatmentChargeTimesPage';
import FeedstockInventorySKUReportPage from 'views/Reports/FeedstockInventory/FeedstockInventorySKUReport/FeedstockInventorySKUReportPage';
import FeedstockInventoryOrderedSizeReportPage from 'views/Reports/FeedstockInventory/FeedstockInventoryOrderedSizeReport/FeedstockInventoryOrderedSizeReportPage';
import DrumSchedulePage from 'views/InputPages/DrumSchedule/DrumSchedulePage';
import NonDrumSchedulePage from 'views/InputPages/NonDrumSchedule/NonDrumSchedulePage';
import MYOBInvoicesPage from 'views/Reports/MYOBInvoicesReport/MYOBInvoicesPage';

const ErrorPage = lazy(() => import('views/Pages/ErrorPage.jsx'));
const LockScreenPage = lazy(() => import('views/Pages/LockScreenPage.jsx'));
const LoginPage = lazy(() => import('views/Pages/LoginPage.jsx'));
const RegisterPage = lazy(() => import('views/Pages/RegisterPage.jsx'));
const UserProfile = lazy(() => import('views/Pages/UserProfile.jsx'));
const Profiles = lazy(() => import('views/Pages/Users/Users.jsx'));
const InventorySKUMasterPage = lazy(() =>
	import('views/InputPages/InventorySKUMaster/InventorySKUMasterPage')
);
const MachinesPage = lazy(() =>
	import('views/InputPages/Machines/MachinesPage')
);
const MaterialsSKUMasterPage = lazy(() =>
	import('views/InputPages/MaterialsSKUMaster/MaterialsSKUMasterPage')
);
const InventoryStocktakePage = lazy(() =>
	import('views/InputPages/FinishedInventoryStocktake/InventoryStocktakePage')
);
const RawMaterialInventoryStocktakePage = lazy(() =>
	import(
		'views/InputPages/RawMaterialInventoryStocktake/RawMaterialInventoryStocktakePage'
	)
);
const ProductionDataPage = lazy(() =>
	import('views/InputPages/ProductionData/ProductionDataPage')
);
const Dashboard = lazy(() => import('views/Dashboard/Dashboard.jsx'));
const MaximumDemandAnalasysPage = lazy(() =>
	import('views/Reports/MaximumDemandAnalasys/MaximumDemandAnalasysPage')
);
const ProductionHoursWorkedPage = lazy(() =>
	import('views/InputPages/ProductionHoursWorked/ProductionHoursWorkedPage')
);
const InvoicesFilterPage = lazy(() =>
	import('views/InputPages/InvoiceFilter/InvoiceFilterPage')
);

let dashRoutes = [
	{
		path: '/dashboard',
		name: 'Dashboard',
		icon: 'dashboard',
		component: Dashboard,
		layout: '/admin',
		inSidebar: true,
		access: ['ADMIN', 'FULL']
	},
	{
		collapse: true,
		name: 'Input Pages',
		icon: 'content_paste',
		state: 'pageCollapse',
		inSidebar: true,
		access: ['ADMIN', 'FULL', 'PRODUCTION'],
		views: [
			{
				path: '/Machines',
				name: 'Machines & Capacities',
				mini: 'MC',
				component: MachinesPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/treatmentChargeTimes',
				name: 'Treatment Charge Times',
				mini: 'TCT',
				component: TreatmentChargeTimesPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/MaterialsSKUMaster',
				name: 'Materials SKU Master',
				mini: 'MM',
				component: MaterialsSKUMasterPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/InventorySKUMaster',
				name: 'Inventory SKU Master',
				mini: 'IM',
				component: InventorySKUMasterPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/FinishedInventoryStocktake',
				name: 'Finished Inventory Stocktake',
				mini: 'FIS',
				component: InventoryStocktakePage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/RawMaterialInventoryStocktake',
				name: 'Raw Material Stocktake',
				mini: 'RMS',
				component: RawMaterialInventoryStocktakePage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/ProductionData',
				name: 'Production Data',
				mini: 'PD',
				component: ProductionDataPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL', 'PRODUCTION']
			},

			{
				path: '/ProductionHoursWorked',
				name: 'Production Hours Worked',
				mini: 'PHW',
				component: ProductionHoursWorkedPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL', 'PRODUCTION']
			},
			// {
			// 	path: '/DrumMode',
			// 	name: 'Drum Mode',
			// 	mini: 'DM',
			// 	component: DrumModePage,
			// 	layout: '/admin',
			// 	inSidebar: true,
			// 	access: ['ADMIN', 'FULL']
			// },
			{
				path: '/InvoicesFilter',
				name: 'Invoices Filter',
				mini: 'IF',
				component: InvoicesFilterPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/login-page',
				name: 'Login Page',
				mini: 'L',
				component: LoginPage,
				layout: '/auth'
			},
			{
				path: '/register-page',
				name: 'Register Page',
				mini: 'R',
				component: RegisterPage,
				layout: '/auth'
			},
			{
				path: '/lock-screen-page',
				name: 'Lock Screen Page',
				mini: 'LS',
				component: LockScreenPage,
				layout: '/auth'
			},
			{
				path: '/user-page',
				name: 'User Profile',
				mini: 'UP',
				component: UserProfile,
				layout: '/admin'
			},
			{
				path: '/error-page',
				name: 'Error Page',
				mini: 'E',
				component: ErrorPage,
				layout: '/auth'
			}
		]
	},
	{
		path: '/reports',
		name: 'Reports',
		icon: PieChart,
		state: 'reportsCollapse',
		inSidebar: true,
		collapse: true,
		access: ['ADMIN', 'FULL'],
		views: [
			{
				path: '/finishedInventoryReport',
				name: 'Finished Inventory',
				mini: 'FI',
				component: FinishedInventoryReportPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},

			{
				path: '/maximumDemandAnalasys',
				name: 'Max Demand Analasys',
				mini: 'MDA',
				component: MaximumDemandAnalasysPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/releaseReport',
				name: 'Release Report',
				mini: 'RR',
				component: ReleaseReportPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/drumLoadingReport',
				name: 'Drum Loading Report',
				mini: 'DLR',
				component: DrumLoadingReportPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/drumScheduleReport',
				name: 'Drum Schedule Report',
				mini: 'DSR',
				component: DrumSchedulePage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/nonDrumScheduleReport',
				name: 'Non Drum Schedule Report',
				mini: 'NDSR',
				component: NonDrumSchedulePage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			{
				path: '/MYOBSales',
				name: 'MYOB Sales Data',
				mini: 'SD',
				component: MYOBInvoicesPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			},
			// {
			// 	path: '/DrumResults',
			// 	name: 'Drum Results',
			// 	mini: 'DR',
			// 	state: 'drumResultsCollapse',
			// 	inSidebar: true,
			// 	collapse: true,
			// 	access: ['ADMIN', 'FULL'],
			// 	views: [
			// 		{
			// 			path: '/drumPerformance',
			// 			name: 'Drum Performance',
			// 			mini: 'DP',
			// 			component: DrumPerformancePage,
			// 			layout: '/admin',
			// 			inSidebar: true,
			// 			access: ['ADMIN', 'FULL']
			// 		},
			// 		{
			// 			path: '/drumMissedCharges',
			// 			name: 'Drum Missed Charges',
			// 			mini: 'DMC',
			// 			component: DrumMissedChargesPage,
			// 			layout: '/admin',
			// 			inSidebar: true,
			// 			access: ['ADMIN', 'FULL']
			// 		},
			// 		{
			// 			path: '/drumOutOfSequence',
			// 			name: 'Drum Out of Sequence',
			// 			mini: 'DOS',
			// 			component: DrumOutOfSequencePage,
			// 			layout: '/admin',
			// 			inSidebar: true,
			// 			access: ['ADMIN', 'FULL']
			// 		}
			// 	]
			// },
			{
				path: '/feedstockInventoryOrderedSize',
				name: 'Feed. Inv. Ordered Size',
				mini: 'FIO',
				component: FeedstockInventoryOrderedSizeReportPage,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN', 'FULL']
			}
			// {
			// 	path: '/FeedstockInventoryReports',
			// 	name: 'Feedstock Inventory',
			// 	mini: 'FI',
			// 	state: 'feedstockInventoryCollapse',
			// 	inSidebar: true,
			// 	collapse: true,
			// 	access: ['ADMIN', 'FULL'],
			// 	views: [
			// 		// {
			// 		// 	path: '/feedstockInventorySKU',
			// 		// 	name: 'Feedstock Inventory SKU',
			// 		// 	mini: 'FIS',
			// 		// 	component: FeedstockInventorySKUReportPage,
			// 		// 	layout: '/admin',
			// 		// 	inSidebar: true,
			// 		// 	access: ['ADMIN', 'FULL']
			// 		// },
			// 		{
			// 			path: '/feedstockInventoryOrderedSize',
			// 			name: 'Feed. Inv. Ordered Size',
			// 			mini: 'FIO',
			// 			component: FeedstockInventoryOrderedSizeReportPage,
			// 			layout: '/admin',
			// 			inSidebar: true,
			// 			access: ['ADMIN', 'FULL']
			// 		}
			// 	]
			// }
		]
	},
	{
		path: '/users',
		name: 'Users',
		icon: VerifiedUser,
		state: 'usersCollapse',
		inSidebar: true,
		collapse: true,
		access: ['ADMIN'],
		views: [
			{
				path: '/users',
				name: 'User List',
				mini: 'USR',
				component: Profiles,
				layout: '/admin',
				inSidebar: true,
				access: ['ADMIN']
			}
		]
	}
];
export default dashRoutes;
