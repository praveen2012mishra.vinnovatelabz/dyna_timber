import {ApiClient} from './../client';

class FeedstockInventorySKUApiClient extends ApiClient {}
const FeedstockInventorySKUClient = new FeedstockInventorySKUApiClient(null, '/feedstockInventory/SKU');
export {FeedstockInventorySKUClient};