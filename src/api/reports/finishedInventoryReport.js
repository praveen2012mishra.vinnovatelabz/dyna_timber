import {ApiClient} from 'api/client';

class FinishedInventoryReportClient extends ApiClient {
}

export default new FinishedInventoryReportClient(null, '/inventoryReport');