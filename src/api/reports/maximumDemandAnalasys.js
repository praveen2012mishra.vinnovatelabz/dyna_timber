import { ApiClient } from "api/client";

class MaximumDemandAnalasysClient extends ApiClient {}

export default new MaximumDemandAnalasysClient(null, "/maximumDemandAnalasys");
