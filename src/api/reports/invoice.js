import {ApiClient} from './../client';

const InvoiceClient = new ApiClient(null, '/invoice');

export default InvoiceClient;