import {ApiClient} from 'api/client';

class ReleaseReportClient extends ApiClient {
}

export default new ReleaseReportClient(null, '/releaseReport');