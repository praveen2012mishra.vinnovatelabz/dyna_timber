import {ApiClient} from './../client';

class FeedstockInventoryOrderedSizeApiClient extends ApiClient {}
const FeedstockInventoryOrderedSizeClient = new FeedstockInventoryOrderedSizeApiClient(null, '/feedstockInventory/orderedSize');
export {FeedstockInventoryOrderedSizeClient};