import { ApiClient } from "./client";

class InventoryStocktakeClient extends ApiClient {
  newStocktake() {
    return this.get(`${this.resourceName}/${"newStocktake"}`);
  }
}

export default new InventoryStocktakeClient(null, "/inventoryStocktake");
