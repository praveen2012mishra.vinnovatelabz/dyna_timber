import {ApiClient} from './client';

class AuthClient extends ApiClient {

    authenticate(data) {
        return this.post(this.resourceName, data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        })
    }

}

export default new AuthClient(null, '/authenticate');