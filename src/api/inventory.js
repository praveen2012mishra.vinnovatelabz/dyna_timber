import {ApiClient} from './client';

class InventoryClient extends ApiClient {

    getSkus() {
        return this.all(`${this.resourceName}/${"sku"}`)
    }

}

export default new InventoryClient(null, '/inventory');