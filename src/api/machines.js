import {ApiClient} from './client';

const MachinesClient = new ApiClient(null, '/machine');

export default MachinesClient;