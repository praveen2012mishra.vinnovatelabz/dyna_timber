import {ApiClient} from './client';

class DrumMissedChargesApiClient extends ApiClient {}
const DrumMissedChargesClient = new DrumMissedChargesApiClient(null, '/drumResults/missedCharges');
export {DrumMissedChargesClient};