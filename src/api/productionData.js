import { ApiClient } from './client';

const ProductionDataClient = new ApiClient(null, '/productionData');
export default ProductionDataClient;
