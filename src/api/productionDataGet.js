import { ApiClient } from './client';

const ProductionDataGetClient = new ApiClient(null, '/productionData/get');
export default ProductionDataGetClient;
