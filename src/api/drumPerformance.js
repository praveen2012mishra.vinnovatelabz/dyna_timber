import {ApiClient} from './client';

class DrumPerformanceApiClient extends ApiClient {}
const DrumPerformanceClient = new DrumPerformanceApiClient(null, '/drumResults/performance');
export {DrumPerformanceClient};