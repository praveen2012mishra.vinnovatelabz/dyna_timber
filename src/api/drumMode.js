import {ApiClient} from './client';

const DrumModeClient = new ApiClient(null, '/drumMode');

export default DrumModeClient;