import {ApiClient} from './client';

class InvoiceFilterClient extends ApiClient {
  getDistinctShipvia() {
    return this.all(`${this.resourceName}/${"shipVia"}`);
  }

  getDistinctSalesperson() {
    return this.all(`${this.resourceName}/${"salesPerson"}`);
  }
}

export default new InvoiceFilterClient(null, '/invoiceFilter');