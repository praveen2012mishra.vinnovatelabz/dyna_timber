import {ApiClient} from './client';

class UsersClient extends ApiClient {

}

export default new UsersClient(null, '/user');