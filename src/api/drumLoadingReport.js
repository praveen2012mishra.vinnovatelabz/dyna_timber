import {ApiClient} from './client';

class DrumLoadingReportApiClient extends ApiClient {}
const DrumLoadingReportClient = new DrumLoadingReportApiClient(null, '/drumLoadingReport');
export {DrumLoadingReportClient};