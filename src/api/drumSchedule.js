// import {ApiClient} from './client';
// //debugger;
// const DrumScheduleClient = new ApiClient(null, '/drumSchedule');

// export default DrumScheduleClient;

import { ApiClient } from "./client";

class DrumScheduleClient extends ApiClient {
  newStocktake() {
    return this.get(`${this.resourceName}/${"newStocktake"}`);
  }
}

export default new DrumScheduleClient(null, "/drumSchedule");
