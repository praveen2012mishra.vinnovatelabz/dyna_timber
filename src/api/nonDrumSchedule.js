import { ApiClient } from "./client";

class NonDrumScheduleClient extends ApiClient {
  saveMachineTime(machineTime) {
    return this.save(machineTime, `${this.resourceName}/${"machineTime"}`);
  }
}

// const NonDrumScheduleClient = new ApiClient(null, '/nonDrumSchedule');

export default new NonDrumScheduleClient(null, "/nonDrumSchedule");
