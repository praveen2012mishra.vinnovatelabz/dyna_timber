import {ApiClient} from './client';

class DrumOutOfSequenceApiClient extends ApiClient {}
const DrumOutOfSequenceClient = new DrumOutOfSequenceApiClient(null, '/drumResults/outOfSequence');
export {DrumOutOfSequenceClient};