import axios from 'axios';
import { runInAction } from 'mobx';
import rootStore from 'stores/RootStore';
import moment from "moment";

const getFromDate = () => {
  var startdate = moment();
  startdate = startdate.subtract(30, "days");
  startdate = startdate.format("YYYY-MM-DD");
  let store1 = JSON.parse(localStorage.getItem("fromDate")) || {
    fromDate: {},
  };
  return typeof getFromDate() === "object" ? startdate : store1.fromDate
};

const getToDate = () => {
  let toDate1 = moment(new Date()).format("YYYY-MM-DD");
  let store2 = JSON.parse(localStorage.getItem("toDate")) || {
    toDate: {},
  };
  return typeof getToDate() === "object" ? toDate1 : store2.toDate;
};

// to be removed
function randomNumber(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

const initDummyData = response => {
	if (
		response.config.url ===
		'http://operations.dynagroup.com.au/dyna/api/drumLoadingReport'
	) {
		let newData = [];
		if (Array.isArray(response.data)) {
			response.data.forEach(oldData => {
				newData.push({
					...oldData,
					act: randomNumber(1, 5),
					actPlusWip: randomNumber(1, 5),
					weekStartTargetInv: randomNumber(1, 101)
				});
			});
			let res = Object.assign({}, response);
			res.data = newData;
			return res;
		}
	}
	return false;
};

/**
 * Create a new Axios client instance
 * @see https://github.kcom/mzabriskie/axios#creating-an-instance
 */
const getClient = (baseUrl = null, token = null) => {
	const options = {
		baseURL:
		//'http://operations.dynagroup.com.au/dyna/api'	
		//'http://cfd888fb983b.ngrok.io/api'
		'http://157.245.49.114:8080/dyna/api'
			 /* baseUrl === null
				? process.env.REACT_APP_API_URL
				: baseUrl */
	};

	console.log('Token', token);
	let coars_header=`Access-Control-Allow-Origin`
	
	if (token) {
		options.headers = {
			Authorization: `Bearer ${token}`,
			//[coars_header]: 'http://operations.dynagroup.com.au/'
		};
	}

	const client = axios.create(options);

	// Add a request interceptor
	client.interceptors.request.use(
		requestConfig => requestConfig,
		requestError => {
			console.error(requestError);
			return Promise.reject(requestError);
		}
	);

	// Add a response interceptor
	client.interceptors.response.use(
		response => {
			//console.log(response);
			let dd = initDummyData(response);
			if (dd) {
				return dd;
			}

			return response;
		},
		error => {
			if(error.response){
				const status = error.response.status;
				if (status >= 500) {
					console.error(error);
					console.log(error);
				}
				if (status < 500 && status >= 400) {
					// console.log("Redirecting to login because of 403 1", rootStore);
					// window.localStorage.removeItem("token");
					// runInAction(() => {
					//   if (rootStore) {
					//     console.log("Redirecting to login because of 403");
					//     rootStore.auth.token = null;
					//     rootStore.routingStore.push("/auth/login-page");
					//   }
					// });
				}
			}
			

			return Promise.reject(error);
		}
	);
	return client;
};

class ApiClient {
	constructor(baseUrl = null, resourceName = null) {
		this.getClient = () =>
			getClient(baseUrl, rootStore ? rootStore.auth.token : null);
		this.resourceName = resourceName;
	}

	get(url, conf = {}) {		
		return this.getClient()
			.get(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}

	delete(url, conf = {}) {
		console.log('Deleting', url);
		return this.getClient()
			.delete(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}

	head(url, conf = {}) {
		return this.getClient()
			.head(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}

	options(url, conf = {}) {
		return this.getClient()
			.options(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}

	post(url, data = {}, conf = {}) {
		return this.getClient()
			.post(url, data, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}

	groupPost(url, data = [], conf = {}){
		//on();
		console.log('saving data post method', data);
		return this.getClient()
		.post(url, data, conf)
		.then(response =>{ Promise.resolve(response)/* ;off() */})
		.catch(error => {Promise.reject(error)/* ;off() */});
	}

	groupSave(data, resource = this.resourceName){
		 return this.groupPost(resource/* ,on,off */, data);
	}

	put(url, data = {}, conf = {}) {
		console.log(data,'------------------------------->');
		
		return this.getClient()
			.put(url, data, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}

	patch(url, data = {}, conf = {}) {
		return this.getClient()
			.patch(url, data, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}

	all(resource = this.resourceName) {
		let result = this.get(resource);
		console.log('get path url',resource);
		// console.log("getting data", result);
		return result;
	}

	deleteList(items, resource = this.resourceName) {
		//console.log('Deleting', items);
		return this.delete(`${resource}?ids=${items.join()}`);
	}

	deleteListPost(items, resource = this.resourceName) {
		//console.log('Deleting', items);
		items.forEach((ele)=>{
			console.log('Deleting multiple row', ele);
			return this.delete(`${resource}/${ele}`);
		})
		//return this.delete(`${resource}`, { data: { ids: items } });
	}

	deleteOne(id, resource = this.resourceName) {
		console.log('Deleting single row',id);
		return this.delete(`${resource}/${id}`);
	}

	one(id, resource = this.resourceName) {
		return this.get(`${resource}/${id}`);
	}

	update(data, resource = this.resourceName) {
		console.log('saving data put method', data);
		return this.put(`${resource}/${data.id}`, data);
	}

	save(data, resource = this.resourceName) {
		console.log('saving data post method', data);
		return this.post(resource, data);
	}
	
}

export { ApiClient };

/**
 * Base HTTP Client
 */
export default {
	// Provide request methods with the default base_url
	get(url, conf = {}) {
		return getClient()
			.get(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	},

	delete(url, conf = {}) {
		return getClient()
			.delete(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	},

	head(url, conf = {}) {
		return getClient()
			.head(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	},

	options(url, conf = {}) {
		return getClient()
			.options(url, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	},

	post(url, data = {}, conf = {}) {
		return getClient()
			.post(url, data, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	},

	put(url, data = {}, conf = {}) {
		return getClient()
			.put(url, data, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	},

	patch(url, data = {}, conf = {}) {
		return getClient()
			.patch(url, data, conf)
			.then(response => Promise.resolve(response))
			.catch(error => Promise.reject(error));
	}
};
