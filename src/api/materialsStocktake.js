import { ApiClient } from './client';

class MaterialStocktakeClient extends ApiClient {
	getWeeks() {
		return this.all(`${this.resourceName}/${'week'}`);
	}

	updateWeek(week) {
		return this.update(week, `${this.resourceName}/${'week'}`);
	}

	saveWeek(week) {
		return this.save(week, `${this.resourceName}/${'week'}`);
	}
	newStocktake() {
		return this.get(`${this.resourceName}/${"newStocktake"}`);
	  }
}

export default new MaterialStocktakeClient(null, '/materialStocktake');
