import {ApiClient} from './client';

const ProductionHoursWorkedClient = new ApiClient(null, '/productionHoursWorked');

export default ProductionHoursWorkedClient;