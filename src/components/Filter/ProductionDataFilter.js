import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './color-filter.scss';
import { get } from 'lodash';

export default class ProductionDataFilter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			black: true,
			green: true,
			yellow: true,
			red: true,
			white: true
		};

		this.valueGetter = this.props.valueGetter;
	}

	isFilterActive() {
		const { black, green, yellow, red, white } = this.state;
		return (
			black !== undefined ||
			green !== undefined ||
			yellow !== undefined ||
			red !== undefined ||
			white !== undefined
		);
	}

	doesFilterPass(params) {
		const { black, green, yellow, red, white } = this.state;
        console.log(params);
		let weekStartActPlusWIP;
		let weekStartActCol;
		let currentActPlusWIP;
		let currentActCol;
		let store = JSON.parse(sessionStorage.getItem('weekStartActPlusWIP')) || {};
		weekStartActPlusWIP = parseInt(store[params.data.inventory.sku]);

		let store1 = JSON.parse(sessionStorage.getItem('weekStartActCol')) || {};
		weekStartActCol = parseInt(store1[params.data.inventory.sku]);

		let store2 = JSON.parse(sessionStorage.getItem('currentActPlusWIP')) || {};
		currentActPlusWIP = parseInt(store2[params.data.inventory.sku]);

		let store3 = JSON.parse(sessionStorage.getItem('currentActCol')) || {};
		currentActCol = parseInt(store3[params.data.inventory.sku]);
		// console.log(
		// 	params.data.inventory.sku,
		// 	this.props.colDef.field,
		// 	weekStartActPlusWIP,
		// 	weekStartActCol,
		// 	currentActPlusWIP,
		// 	currentActCol
		// );

		if (
			black &&
			((parseFloat(weekStartActPlusWIP) > 150 &&
				this.props.colDef.field === 'weekStartActPlusWIP') ||
				(parseFloat(weekStartActCol) > 150 &&
					this.props.colDef.field === 'weekStartActCol') ||
				(parseFloat(currentActPlusWIP) > 150 &&
					this.props.colDef.field === 'currentActPlusWIP') ||
				(parseFloat(currentActCol) > 150 &&
					this.props.colDef.field === 'currentActCol'))
		) {
			return true;
		} else if (
			green &&
			((weekStartActPlusWIP <= 150 &&
				weekStartActPlusWIP > 100 &&
				this.props.colDef.field === 'weekStartActPlusWIP') ||
				(weekStartActCol <= 150 &&
					weekStartActCol > 100 &&
					this.props.colDef.field === 'weekStartActCol') ||
				(currentActPlusWIP <= 150 &&
					currentActPlusWIP > 100 &&
					this.props.colDef.field === 'currentActPlusWIP') ||
				(currentActCol <= 150 &&
					currentActCol > 100 &&
					this.props.colDef.field === 'currentActCol'))
		) {
			return true;
		} else if (
			yellow &&
			((weekStartActPlusWIP <= 100 &&
				weekStartActPlusWIP > 50 &&
				this.props.colDef.field === 'weekStartActPlusWIP') ||
				(weekStartActCol <= 100 &&
					weekStartActCol > 50 &&
					this.props.colDef.field === 'weekStartActCol') ||
				(currentActPlusWIP <= 100 &&
					currentActPlusWIP > 50 &&
					this.props.colDef.field === 'currentActPlusWIP') ||
				(currentActCol <= 100 &&
					currentActCol > 50 &&
					this.props.colDef.field === 'currentActCol'))
		) {
			return true;
		} else if (
			red &&
			((parseFloat(weekStartActPlusWIP) <= 50 &&
				parseFloat(weekStartActPlusWIP) >= 0 &&
				this.props.colDef.field === 'weekStartActPlusWIP') ||
				(parseFloat(weekStartActCol) <= 50 &&
					weekStartActCol >= 0 &&
					this.props.colDef.field === 'weekStartActCol') ||
				(parseFloat(currentActPlusWIP) <= 50 &&
					parseFloat(currentActPlusWIP) >= 0 &&
					this.props.colDef.field === 'currentActPlusWIP') ||
				(parseFloat(currentActCol) <= 50 &&
					parseFloat(currentActCol) >= 0 &&
					this.props.colDef.field === 'currentActCol'))
		) {
			return true;
		} else if (
			white &&
			((parseFloat(weekStartActPlusWIP) < 0 &&
				this.props.colDef.field === 'weekStartActPlusWIP') ||
				(parseFloat(weekStartActCol) < 0 &&
					this.props.colDef.field === 'weekStartActCol') ||
				(parseFloat(currentActPlusWIP) < 0 &&
					this.props.colDef.field === 'currentActPlusWIP') ||
				(parseFloat(currentActCol) < 0 &&
					this.props.colDef.field === 'currentActCol'))
		) {
			return true;
		} else {
			return false;
		}
	}

	afterGuiAttached(params) {
		this.focus();
	}

	focus() {
		window.setTimeout(() => {
			let container = ReactDOM.findDOMNode(this.refs.input);
			if (container) {
				container.focus();
			}
		});
	}

	checkboxChange = (type, event) => {
		this.setState(
			{
				[type]: event.target.checked
			},
			() => {
				this.props.filterChangedCallback();
			}
		);
	};

	selectAll = event => {
		this.setState(
			{
				black: event.target.checked,
				green: event.target.checked,
				yellow: event.target.checked,
				red: event.target.checked,
				white: event.target.checked
			},
			() => {
				this.props.filterChangedCallback();
			}
		);
	};

	render() {
		const { black, green, yellow, red, white } = this.state;
		return (
			<Fragment>
				{/* <label className="container ">
					<input
						type="checkbox"
						checked={black && green && yellow && red && white}
						onChange={this.selectAll}
					/>
					<span className="checkmark"></span>
					<div className="block">(Select All)</div>
				</label>
				<hr className="filter-hr" />
				<label className="container ">
					<input
						type="checkbox"
						checked={Boolean(white)}
						onChange={e => this.checkboxChange('white', e)}
					/>
					<span className="checkmark"></span>
					<div style={{ backgroundColor: 'white' }} className="block"></div>
				</label>
				<label className="container ">
					<input
						type="checkbox"
						checked={Boolean(black)}
						onChange={e => this.checkboxChange('black', e)}
					/>
					<span className="checkmark"></span>
					<div style={{ backgroundColor: 'black' }} className="block"></div>
				</label>
				<label className="container">
					<input
						type="checkbox"
						checked={Boolean(green)}
						onChange={e => this.checkboxChange('green', e)}
					/>
					<span className="checkmark"></span>
					<div style={{ backgroundColor: 'green' }} className="block"></div>
				</label>
				<label className="container">
					<input
						type="checkbox"
						checked={Boolean(yellow)}
						onChange={e => this.checkboxChange('yellow', e)}
					/>
					<span className="checkmark"></span>
					<div style={{ backgroundColor: 'yellow' }} className="block"></div>
				</label>
				<label className="container">
					<input
						type="checkbox"
						checked={Boolean(red)}
						onChange={e => this.checkboxChange('red', e)}
					/>
					<span className="checkmark"></span>
					<div style={{ backgroundColor: 'red' }} className="block"></div>
				</label> */}
			</Fragment>
		);
	}
}
