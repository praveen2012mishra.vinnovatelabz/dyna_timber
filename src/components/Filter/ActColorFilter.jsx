import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './color-filter.scss';

export default class ActColorFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      black: true,
      green: true,
      yellow: true,
      red: true
    };

    this.valueGetter = this.props.valueGetter;
  }

  isFilterActive() {
    const { black, green, yellow, red } = this.state;
    return (
      black !== undefined ||
      green !== undefined ||
      yellow !== undefined ||
      red !== undefined
    );
  }

  doesFilterPass(params) {    
    const { black, green, yellow, red } = this.state;
    if (black && params.data.act === 1) {
      return true;
    } else if (green && params.data.act === 2) {
      return true;
    } else if (yellow && params.data.act === 3) {
      return true;
    } else if (red && params.data.act === 4) {
      return true;
    } else {
      return false;
    }
  }

  afterGuiAttached(params) {
    this.focus();
  }

  focus() {
    window.setTimeout(() => {
      let container = ReactDOM.findDOMNode(this.refs.input);
      if (container) {
        container.focus();
      }
    });
  }

  checkboxChange = (type, event) => {
    this.setState(
      {
        [type]: event.target.checked
      },
      () => {
        this.props.filterChangedCallback();
      }
    );
  };

  selectAll = event => {
    this.setState(
      {
        black: event.target.checked,
        green: event.target.checked,
        yellow: event.target.checked,
        red: event.target.checked
      },
      () => {
        this.props.filterChangedCallback();
      }
    );
  };

  render() {
    const { black, green, yellow, red } = this.state;
    return (
      <Fragment>
        <label className="container ">
          <input
            type="checkbox"
            checked={black && green && yellow && red}
            onChange={this.selectAll}
          />
          <span className="checkmark"></span>
          <div className="block">(Select All)</div>
        </label>
        <hr className="filter-hr" />
        <label className="container ">
          <input
            type="checkbox"
            checked={Boolean(black)}
            onChange={e => this.checkboxChange('black', e)}
          />
          <span className="checkmark"></span>
          <div style={{ backgroundColor: 'black' }} className="block"></div>
        </label>
        <label className="container">
          <input
            type="checkbox"
            checked={Boolean(green)}
            onChange={e => this.checkboxChange('green', e)}
          />
          <span className="checkmark"></span>
          <div style={{ backgroundColor: 'green' }} className="block"></div>
        </label>
        <label className="container">
          <input
            type="checkbox"
            checked={Boolean(yellow)}
            onChange={e => this.checkboxChange('yellow', e)}
          />
          <span className="checkmark"></span>
          <div style={{ backgroundColor: 'yellow' }} className="block"></div>
        </label>
        <label className="container">
          <input
            type="checkbox"
            checked={Boolean(red)}
            onChange={e => this.checkboxChange('red', e)}
          />
          <span className="checkmark"></span>
          <div style={{ backgroundColor: 'red' }} className="block"></div>
        </label>
      </Fragment>
    );
  }
}
