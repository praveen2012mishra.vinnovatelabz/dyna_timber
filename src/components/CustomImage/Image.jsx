import React from "react";
import PropTypes from "prop-types";

// mterial-ui components
import withStyles from "@material-ui/core/styles/withStyles";

// const style = {
//   clearfix: {
//     "&:after,&:before": {
//       display: "table",
//       content: '" "'
//     },
//     "&:after": {
//       clear: "both"
//     }
//   }
// };

export default function CustomImage({ ...props }) {
  const { src,alt,className } = props;
  //return <div className={classes.clearfix} />;
  return <img src={`${src}`} alt={`${alt}`} className={`${className}`}></img>
}

CustomImage.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    className: PropTypes.object
};

//export default withStyles(style)(CustomImage);