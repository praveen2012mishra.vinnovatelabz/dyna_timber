import React, { Component, Fragment } from 'react';
import { withSnackbar } from 'notistack';
import { inject, observer } from 'mobx-react';
import { autorun } from 'mobx';
import Button from '@material-ui/core/Button';

class Notifier extends Component {
	displayed = [];

	storeDisplayed = id => {
		this.displayed = [...this.displayed, id];
	};

	componentDidMount() {
		const action = key => (
			<Fragment>
				<Button
					color="secondary"
					onClick={() => {
						this.props.closeSnackbar(key);
					}}
				>
					{'Dismiss'}
				</Button>
			</Fragment>
		);

		const defaultOptions = {
			action,
			anchorOrigin: {
				vertical: 'bottom',
				horizontal: 'right'
			}
		};

		autorun(() => {
			const { notifications = [] } = this.props.notifications;

			notifications.forEach(notification => {
				// Do nothing if snackbar is already displayed
				if (this.displayed.includes(notification.key)) return;
				// Display snackbar using notistack
				this.props.enqueueSnackbar(
					notification.message,
					notification.options || defaultOptions
				);
				// Keep track of snackbars that we've displayed
				this.storeDisplayed(notification.key);
				// Dispatch action to remove snackbar from mobx store
				this.props.notifications.removeSnackbar(notification.key);
			});
		});
	}

	render() {
		return null;
	}
}

export default withSnackbar(inject('notifications')(observer(Notifier)));
