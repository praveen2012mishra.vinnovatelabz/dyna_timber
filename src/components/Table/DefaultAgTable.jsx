import React, { useState } from "react";
import PropTypes, { string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import RegularButton from "../CustomButtons/Button";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import DialogTitle from "@material-ui/core/DialogTitle";

import Dialog from "@material-ui/core/Dialog";
import PersonIcon from "@material-ui/icons/Person";
import AddIcon from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography";
import { blue } from "@material-ui/core/colors";
import Paper from "@material-ui/core/Paper";
import { AgGridReact } from "ag-grid-react";
import ExportToExcelPanel from "components/Table/ExportToExcelPanel";
import PrintPanel from "components/Table/PrintPanel";
import { action, flow, observable } from "mobx";
import DrumScheduleClient from "../../api/drumSchedule";
import { inject, observer } from "mobx-react";
import { autorun } from "mobx";
import CircularProgress from "@material-ui/core/CircularProgress";
import NumericEditor from "utils/grid/NumericEditor";
import logo from "../../assets/img/dyna-group-print-logo.png";
import ActPlusWipColorFilter from "components/Filter/ActPlusWipColorFilter";
import ActColorFilter from "components/Filter/ActColorFilter";
import PartialMatchFilter from "components/Filter/partialMatchFilter";
import ProductionDataFilter from "components/Filter/ProductionDataFilter";
import extendedFormsStyle from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.jsx";
import CustomTable from "./Table";
import CustomImage from "components/CustomImage/Image";
import DrsTransferProductionClient from "api/dsrTransferProduction";
//import DrumScheduleClient from "api/drumSchedule";
import "assets/scss/ag-grid-theme.scss";
import ProductionDataGetClient from "api/productionDataGet";
let selectedRowsDsr = [];
let selectedColumnsDsr = [];

function textWidth(text, fontProp) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (data, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };

  store[componentName] = {
    ...store[componentName],
  };

  if (data.length > 0 && store) {
    let keyArray = Object.keys(data[0]);
    for (let i = 0; i < keyArray.length; i++) {
      let getWidth = Object.assign([]);
      data.forEach((ele) => {
        getWidth.push(textWidth(keyArray[i], "normal 12px Roboto"));
        //console.log(ele[keyArray[i]]);
        getWidth.push(textWidth(ele[keyArray[i]], "normal 12px Roboto"));
      });

      console.log(
        store[componentName][keyArray[i]] != Math.max(...getWidth),
        store[componentName][keyArray[i]],
        Math.max(...getWidth),
        store[componentName] === {},
        store[componentName],
        store
      );

      if (store[componentName][keyArray[i]] != Math.max(...getWidth)) {
        //console.log(store[componentName][keyArray[i]]);
        //console.log('==============>');
      } else if (store[componentName][keyArray[i]]) {
        //console.log(getWidth,Math.max( ...getWidth ));
        //console.log('---------------->');

        store[componentName] = {
          ...store[componentName],
          [keyArray[i]]: Math.max(...getWidth) + 20,
        };
        //console.log(store[componentName][keyArray[i]],Math.max( ...getWidth ));

        sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
      }
      //console.log(store[componentName]);
    }
  }
};

const styles = (theme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto",
  },
  table: {
    height: "700px",
    width: "100%",
  },
});

function getFirstDayWeek() {
  let curr = new Date();
  let first = curr.getDate() - curr.getDay();
  let firstday = new Date(curr.setDate(first));
  return (
    firstday.getDate() +
    "/" +
    (firstday.getMonth() + 1) +
    "/" +
    firstday.getFullYear().toString().substring(2)
  );
}

function getNextDay() {
  let firstday = new Date();

  return (
    firstday.getDate() +
    "/" +
    (firstday.getMonth() + 1) +
    "/" +
    firstday.getFullYear().toString().substring(2)
  );
}

const getRefreshState = () => {
  //debugger;
  let number;
  let store = JSON.parse(sessionStorage.getItem("getRefreshState")) || {
    number: 0,
  };
  if (store[number] === 0) {
    console.log("==============================");
    store[number] = 1;
    sessionStorage.setItem("getRefreshState", JSON.stringify(store));
    window.location.reload();
  }
};

const getPageStatus = (page, componentName, defaultStore) => {
  //debugger
  let getPageNumber;
  let store1 = JSON.parse(sessionStorage.getItem("getHttpStatus")) || {
    ["ProductionDataTable"]: {},
  };
  let store = JSON.parse(sessionStorage.getItem("setPage")) || {
    ["ProductionDataTable"]: {},
  };
  //debugger
  if (
    store1["ProductionDataTable"].id === 1 &&
    page >= 1 &&
    Object.keys(store1["ProductionDataTable"]).length != 0
  ) {
    getPageNumber = store["ProductionDataTable"].pageNumber;
    sessionStorage.removeItem("getHttpStatus");
    return getPageNumber;
  }
  if (store1["ProductionDataTable"].id === 0 && page >= 1) {
    console.log(store1);
    let store = JSON.parse(sessionStorage.getItem("setPage")) || {
      ["ProductionDataTable"]: {},
    };
    let page = {
      limit: 100,
      pageNumber: 1,
    };
    store["ProductionDataTable"] = {
      ...store["ProductionDataTable"],
      ...page,
    };
    sessionStorage.setItem("setPage", JSON.stringify(store));
    defaultStore.fetchAll();
    sessionStorage.removeItem("getHttpStatus");
    getPageNumber = 1;
    return getPageNumber;
  } else {
    getPageNumber = page;
    return getPageNumber;
  }
};

// eslint-disable-next-line
let drumScheduleArray;
//@inject('drumSchedule')
//@inject("productionData")
@observer
class DefaultAgTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rowValue: {},
      buttonState: false,
      submitResult: "",
      selectedRowsTable: [],
      deleteStatus: false,
      number: 0,
      page: 1,
    };
    //getPageNumber=this.state.page;
    autorun(() => {
      // let { data } = this.defaultStore ? this.defaultStore : this.props.store;
      let data = this.defaultStore ? this.defaultStore : this.props.store;
      //console.log(data,'------------------------------------>')
      if (this.gridApi) {
        // if (loading) {
        //     this.gridApi.showLoadingOverlay();
        // } else {
        //     this.gridApi.hideOverlay();
        // }
      }
    });
  }

  getPaginationStatus = (status) => {
    //debugger
    let store2 = JSON.parse(sessionStorage.getItem("setPage")) || {
      [this.componentName]: {},
    };
    let store = JSON.parse(sessionStorage.getItem("setPage")) || {
      [this.componentName]: {},
    };
    //debugger
    if (status === "Next") {
      if (store2[this.componentName].pageNumber > 1) {
        this.setState({ page: store2[this.componentName].pageNumber + 1 });
        let page = {
          limit: 100,
          pageNumber: store2[this.componentName].pageNumber + 1,
        };
        store[this.componentName] = {
          ...store[this.componentName],
          ...page,
        };
      } else {
        this.setState({ page: this.state.page + 1 });
        let page = {
          limit: 100,
          pageNumber: this.state.page + 1,
        };
        store[this.componentName] = {
          ...store[this.componentName],
          ...page,
        };
      }
      sessionStorage.setItem("setPage", JSON.stringify(store));
      this.defaultStore.fetchAll();
    }
    if (status === "Previous" || this.state.page === 0) {
      let store2 = JSON.parse(sessionStorage.getItem("setPage")) || {
        [this.componentName]: {},
      };
      let store = JSON.parse(sessionStorage.getItem("setPage")) || {
        [this.componentName]: {},
      };
      if (store2[this.componentName].pageNumber > 1) {
        this.setState({ page: store2[this.componentName].pageNumber - 1 });
        let page = {
          limit: 100,
          pageNumber: store2[this.componentName].pageNumber - 1,
        };
        store[this.componentName] = {
          ...store[this.componentName],
          ...page,
        };
      } else {
        this.setState({ page: this.state.page - 1 });
        let page = {
          limit: 100,
          pageNumber: this.state.page - 1,
        };
        store[this.componentName] = {
          ...store[this.componentName],
          ...page,
        };
      }
      sessionStorage.setItem("setPage", JSON.stringify(store));
      this.defaultStore.fetchAll();
      //this.defaultStore.fetchAll();
    }
  };

  getPageInput = (event) => {
    let updatePageNumber=parseInt(event.target.value);
    let store = JSON.parse(sessionStorage.getItem("setPage")) || {
      [this.componentName]: {},
    };
    let page = {
      limit: 100,
      pageNumber: updatePageNumber,
    };
    store[this.componentName] = {
      ...store[this.componentName],
      ...page,
    };
    this.setState({page:updatePageNumber})
    sessionStorage.setItem("setPage", JSON.stringify(store));    
    //getPageStatus(updatePageNumber, this.componentName, this.defaultStore);
    console.log("test==========================>", updatePageNumber);
    this.defaultStore.fetchAll();
  };

  dsrSubmitStatus = (value) => {
    if (value === "Submit") {
      let valueToPD = Object.assign([], selectedRowsDsr);
      let newValueToPD = valueToPD.map((ele) => {
        //delete ele.id;
        return {
          date: ele.date,
          packsPerCh: ele.packsPerCh,
          product: ele.product,
          productionLine: ele.productionLine,
          sku: ele.sku,
          time: ele.time,
          timeIn: ele.timeIn,
        };
      });
      //console.log(newValueToPD);
      DrsTransferProductionClient.groupSave(
        newValueToPD /* ,loaderOnHandler,loaderOffHandler */
      );
      this.setState({
        submitResult: "Data transfer from DSR to PD is successfully completed",
        deleteStatus: true,
        buttonState: true,
      });
    }
    //debugger
    else if (value === "Cancel" && this.state.deleteStatus) {
      console.log(value, this.state.deleteStatus);

      this.setState({
        submitResult: "Data transfer from DSR to PD is successfully completed",
        deleteStatus: false,
        buttonState: false,
      });
      window.location.reload();
    } else if (value === "Delete") {
      //debugger;
      let valueToPD = Object.assign([], selectedRowsDsr);
      valueToPD.forEach((ele) => {
        let deleteSelected = flow(function* () {
          console.log(ele.id);
          yield DrumScheduleClient.deleteOne(ele.id);
        });
        deleteSelected();
        // console.log(ele);
        // DrumScheduleClient.deleteOne(ele.id);
      });

      this.setState({
        submitResult: "Data transfer from DSR to PD is successfully completed",
        deleteStatus: false,
        buttonState: false,
      });
      //this.props.drumSchedule.fetchAll();
      setTimeout(function () {
        window.location.reload();
      }, 4000);
    } else {
      this.setState({
        submitResult: "Data transfer from DSR to PD is cancelled",
        deleteStatus: false,
        buttonState: false,
      });
      //window.location.reload();
    }
  };

  handleClickOpen = () => {
    this.setState({ buttonState: true, submitResult: "" });
  };

  handleClose = (value) => {
    console.log(value);
    this.setState({
      // buttonState: false,
      submitResult: "Transfer data from DSR to PD is success.",
    });
  };

  printHeader = "Report";
  columns = [];
  gridOptions = {};

  printData = (header) => (
    <div id="first-print">
      <div className="logo-container">
        {/*  <img src={logo} alt="Dyna Group" /> */}
        <CustomImage src={logo} alt={"Dyna Group"} />
      </div>
      <div className="header-container">
        <div className="main-header">
          <div className="component-name">
            <h3>{header}</h3>
          </div>
          <div className="week-start-date">
            <div>Week Start Date</div>
            <div className="date">{getFirstDayWeek()}</div>
          </div>
        </div>
        <div className="inventory-color-legend">
          <div className="inventory">
            <div className="header">Inventory colour Legend</div>
            <div className="black indicator">over 150% of target</div>
            <div className="green indicator">100%-150% of target</div>
            <div className="yellow indicator">50%-100% of target</div>
            <div className="red indicator">0%-50% of target</div>
            <div className="white indicator">stock of non-stock product</div>
          </div>

          <div className="updated">
            <div>updated </div> <div className="date">{getNextDay()}</div>
          </div>
        </div>
      </div>
    </div>
  );

  defaultColumnDef = {
    enableRowGroup: true,
    enablePivot: true,
    sortable: true,
    filter: true,
    resizable: true,
  };

  statusBar = {
    statusPanels: [
      {
        statusPanel: "agTotalRowCountComponent",
        align: "left",
      },
      { statusPanel: "agFilteredRowCountComponent" },
      { statusPanel: "agSelectedRowCountComponent" },
      { statusPanel: "agAggregationComponent" },
    ],
  };

  sideBar = {
    toolPanels: [
      {
        id: "columns",
        labelDefault: "Columns",
        labelKey: "columns",
        iconKey: "columns",
        toolPanel: "agColumnsToolPanel",
      },
      {
        id: "filters",
        labelDefault: "Filters",
        labelKey: "filters",
        iconKey: "filter",
        toolPanel: "agFiltersToolPanel",
      },
      {
        id: "exportToExcel",
        labelDefault: "Excel Export",
        labelKey: "exportToExcel",
        iconKey: "expanded",
        toolPanel: "exportToExcelPanel",
      },
      {
        id: "print",
        labelDefault: "Print",
        labelKey: "print",
        iconKey: "print",
        toolPanel: "printPanel",
      },
    ],
  };

  rowDeselection = false;
  componentName = "";
  groupDefaultExpanded = -1;

  onRowSelected = (evt, selectedRows) => {
    if (evt.data) {
      const showColumnArray = [
        "comments",
        "completedOnTime",
        "competedInSequence",
        "tableIndex",
      ];
      let selectedRows = this.gridApi.getSelectedRows();
      //console.log(selectedRows);

      let selectedRowsArray;
      showColumnArray.forEach((i) => {
        selectedRowsArray = selectedRows.map((j) => {
          delete j[i];
          return j;
        });
      });
      //console.log(selectedRows,'--------------------->');
      selectedRowsDsr = Object.assign([], selectedRowsArray);
      selectedColumnsDsr = Object.keys(evt.data);
      this.setState({ selectedRowsTable: selectedRowsDsr });
      selectedRowsDsr.length === 0
        ? this.setState({
            submitResult: "",
            selectedRowsTable: selectedRowsDsr,
          })
        : this.setState({ selectedRowsTable: selectedRowsDsr });
    }
  };

  onSelectionChanged = (evt, selectItems, selectItem) => {
    var selectedRows = this.gridApi.getSelectedRows();
    selectedRowsDsr = Object.assign([], selectedRows);
    selectItem(null);
    if (selectItem) {
      if (selectedRows.length === 1) {
        selectItem(selectedRows[selectedRows.length - 1]);
      }
    }

    if (selectItems) selectItems(selectedRows);
  };

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.onRowDataChange();
    this.filterModifiedCallback();
    //console.log('======================>');
  };

  onSortChanged() {}

  onColumnVisible = (params, componentName) => {
    let printWidth = document.querySelector(".ag-header-container");
    let widthHeader = printWidth.style.width;
    console.log(widthHeader);
    if (componentName.length) {
      let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
        [componentName]: {},
      };
      if (Array.isArray(params.columns)) {
        params.columns.forEach((col) => {
          store[componentName] = {
            ...store[componentName],
            [col.userProvidedColDef.field]: !col.visible,
          };
        });
      }
      localStorage.setItem("hiddenColumns", JSON.stringify(store));
    }
  };

  onFirstDataRendered = (evt, componentName) => {
    let { number } = this.state;
    //let number;
    let store = JSON.parse(sessionStorage.getItem("getRefreshState")) || {};
    //debugger;
    store[componentName] = {
      ...store[componentName],
    };
    if (!store[componentName].number) {
      //console.log("==============================");
      store[componentName] = {
        ...store[componentName],
        number: 1,
      };
      sessionStorage.setItem("getRefreshState", JSON.stringify(store));
      window.location.reload();
    }
  };

  onColumnResized = (event, componentName) => {
    let maxWidth;
    if (event.column && componentName && event.column.userProvidedColDef) {
      //console.log(this.props.store.data,componentName,'=============>',event);
      let field = event.column.userProvidedColDef["field"];
      let widthResizeColumn = event.column["actualWidth"];
      //let columnFieldWidth=textWidth(field, "bold 13px Verdana")
      //let columnFieldValueWidth=getWidthColumnValue(this.props.store.data,field)
      let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
        [componentName]: {},
      };
      store[componentName] = {
        ...store[componentName],
        [field]: widthResizeColumn,
      };
      sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
    }
  };

  onFilterChanged() {
    // filterActive = this.gridApi.isAnyFilterPresent();
    // var suppressRowDrag = sortActive || filterActive;
    // console.log(
    // 	'sortActive = ' +
    // 		sortActive +
    // 		', filterActive = ' +
    // 		filterActive +
    // 		', allowRowDrag = ' +
    // 		suppressRowDrag
    // );
    // this.gridApi.setSuppressRowDrag(suppressRowDrag);
  }

  filterModifiedCallback = () => {
    //console.log('filterModifiedCallback');
  };

  onRowDataChange = () => {
    // this.gridApi.onRowDataChange()
    const allColumnIds = [];
    if (!this.gridColumnApi) return;
    this.gridColumnApi.getAllColumns().forEach(function (column) {
      allColumnIds.push(column.colId);
    });
    //console.log("Columns", allColumnIds);
    this.gridColumnApi.autoSizeColumns(allColumnIds);
    // this.gridApi.onRowDataChange();
  };

  onCellValueChanged = (event) => {
    console.log("Cell changed event: ", event);
  };

  render() {
    let { data, selectItem, selectItems, loading } = this.defaultStore
      ? this.defaultStore
      : this.props.store;
    //console.log(getPageStatus());
    let store = JSON.parse(sessionStorage.getItem("setPage")) || {
      ["ProductionDataTable"]: {},
    };
    let getPageStatusNumber = store["ProductionDataTable"].pageNumber;
    //console.log(getPageStatusNumber);
    return loading ? (
      <CircularProgress />
    ) : (
      <div>
        <Paper
          style={{
            width: "100%",
            overflowX: "auto",
          }}
          elevation={0}
        >
          {this.printData(this.printHeader)}
          <div
            className={"ag-theme-balham grid-class"}
            style={{
              height: "700px",
              width: "100%",
            }}
          >
            <AgGridReact
              //pagination={true}
              //onPaginationChanged={this.onPaginationChanged}
              onFilterChanged={}
              onColumnResized={(evt) =>
                this.onColumnResized(evt, this.componentName)
              }
              onFirstDataRendered={(evt) => {
                this.onFirstDataRendered(evt, this.componentName);
              }}
              //sortingOrder={this.state.sortingOrder}
              //onSortChanged={this.onSortChanged.bind(this)}
              //onFilterChanged={this.onFilterChanged.bind(this)}
              // onRowDragEnd={event => {
              // 	this.onRowDragEnd.bind(this);
              // 	//console.log(event, '---------------->');
              // }}
              onRowDragEnd={
                this.componentName === "DrumScheduleTable"
                  ? this.onRowDragEnd.bind(this)
                  : null
              }
              enterMovesDownAfterEdit={true}
              enterMovesDown={true}
              enableGroupEdit={true}
              onGridReady={this.onGridReady}
              onRowDataChanged={this.onRowDataChange}
              // onRowValueChanged={this.onRowValueChanged}
              onColumnVisible={(evt) =>
                this.onColumnVisible(evt, this.componentName)
              }
              onCellValueChanged={this.onCellValueChanged}
              columnDefs={this.columns}
              defaultColDef={this.defaultColumnDef}
              rowData={data}
              onRowSelected={(evt) =>
                this.onRowSelected(evt, this.gridApi.getSelectedRows())
              }
              onSelectionChanged={(evt) =>
                this.onSelectionChanged(evt, selectItems, selectItem)
              }
              groupSelectsChildren={true}
              // suppressRowClickSelection={true}
              columnVisible={this.onFilterChanged}
              rowSelection={this.rowSelection}
              rowDeselection={this.rowDeselection}
              rowMultiSelectWithClick={this.rowMultiSelectWithClick}
              enableRangeSelection={true}
              enableCharts={true}
              sideBar={this.sideBar}
              statusBar={this.statusBar}
              frameworkComponents={{
                exportToExcelPanel: ExportToExcelPanel,
                printPanel: PrintPanel,
                numericEditor: NumericEditor,
                actPlusWipColorFilter: ActPlusWipColorFilter,
                actColorFilter: ActColorFilter,
                partialMatchFilter: PartialMatchFilter,
                productionDataFilter:ProductionDataFilter
              }}
              onCellEditingStopped={(param) => {
                const refreshParams = {
                  force: true,
                };
                param.api.refreshCells(refreshParams);
              }}
              groupDefaultExpanded={this.groupDefaultExpanded}
              suppressColumnVirtualisation={true}
              overlayLoadingTemplate={
                '<span class="ag-overlay-loading-center">Please wait while your rows are loading</span>'
              }
              rowDataChangeDetectionStrategy="NoCheck" //this could be a problem
              {...this.gridOptions}
            />
          </div>
        </Paper>
        {this.componentName === "ProductionDataTable" && (
          <div className="container dsr-button-container button-group">
            <div className="btn dsr-button mr-4 page-status">
              Page
              <input
                className="input-page"
                type="text"
                id="pageNumber"
                placeholder={`${getPageStatus(
                  this.state.page,
                  this.componentName,
                  this.defaultStore
                )}`}
                onKeyPress={(e) => {
                  e.key == "Enter" ? this.getPageInput(e) : "";
                }}
                //name=
                //value=
              ></input>
              {/* <span>
                &nbsp;
                {getPageStatus(
                  this.state.page,
                  this.componentName,
                  this.defaultStore
                )}
              </span> */}
            </div>

            {getPageStatusNumber > 1 && (
              <button
                type="button"
                className="btn dsr-button"
                onClick={() => this.getPaginationStatus("Previous")}
              >
                Previous
              </button>
            )}
            <button
              type="button"
              className="btn dsr-button mr-4"
              onClick={() => this.getPaginationStatus("Next")}
            >
              Next
            </button>
          </div>
        )}
        {selectedRowsDsr.length > 0 &&
          this.componentName === "DrumScheduleTable" && (
            <div>
              {/* <Typography variant="subtitle1"> {this.state.submitResult}
        </Typography> */}
              {/* {end} */}
              <div className="dsr-box">
                <div className="dsr-result">{this.state.submitResult}</div>
                <div className="dsr-button-selection">
                  <Button
                    /* color={"info"} */ /* round */ onClick={
                      this.handleClickOpen
                    }
                  >
                    Transfer Data from DSR to PD
                  </Button>
                </div>
              </div>

              <SimpleDialog
                selectedValue={this.state.submitResult}
                click={(value) => {
                  this.dsrSubmitStatus(value);
                }}
                open={this.state.buttonState}
                onClose={this.handleClose}
                deleteStatus={this.state.deleteStatus}
              />
            </div>
          )}
        <div>
          {this.state.deleteStatus && (
            <div>
              <SimpleDialogDelete
                selectedValue={this.state.submitResult}
                click={(value) => {
                  this.dsrSubmitStatus(value);
                }}
                open={this.state.buttonState}
                onClose={this.handleClose}
                deleteStatus={this.state.deleteStatus}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default DefaultAgTable;

const useStyles = makeStyles({
  root: {
    margin: 0,
    paddingTop: "5",
    //width:'1500'
  },
  avatar: {
    backgroundColor: "white",
    color: "gray",
    textAlign: "center",
    fontSize: "18px",
    fontWeight: "300",
    lineHeight: "30px",
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  },
  loaderRoot: {
    display: "flex",
    position: "absolute",
    left: "48%",
    bottom: "40%",
  },
});

function SimpleDialog(props) {
  const classes = useStyles();
  const { classese } = props;
  const [buttonState, setButtonState] = useState(false);
  const { onClose, selectedValue, open, click, deleteStatus } = props;
  let selectedRowsArray = selectedRowsDsr.map((ele) => {
    return Object.values(ele);
  });

  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleListItemClick = (value) => {
    onClose(value);
    click(value);
    //console.log(value);
    if (value === "Submit") {
      setButtonState(true);
      //debugger
    } else {
      setButtonState(false);
    }
  };

  const handleDeleteItemClick = (value) => {
    setButtonState(false);
    click(value);
  };

  return (
    <Dialog
      //onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      className={classes.root}
      fullWidth={true}
      maxWidth={"md"}
      maxHeight={"100vh"}
    >
      {selectedRowsDsr.length > 0 && (
        <div>
          {!buttonState && !deleteStatus && (
            <div>
              <DialogTitle id="simple-dialog-title" className={classes.avatar}>
                Selected DSR rows
              </DialogTitle>

              <List>
                <div className="dsr-table">
                  {/* {loader && (
                <div className={classes.loaderRoot}>
                  <CircularProgress color="#000" />
                </div>
              )} */}
                  <CustomTable
                    tableHead={selectedColumnsDsr}
                    tableData={selectedRowsArray}
                    customClassesForCells={[0, 5, 6]}
                    customHeadCellClasses={[
                      classes.center,
                      classes.right,
                      classes.right,
                    ]}
                    // 0 is for classes.center, 5 is for classes.right, 6 is for classes.right
                    customHeadClassesForCells={[0, 5, 6]}
                  />
                </div>
              </List>
              <div className="container dsr-button-container button-group">
                <button
                  type="button"
                  className="btn dsr-button mr-4"
                  onClick={() => handleListItemClick("Submit")}
                >
                  Submit
                </button>
                <button
                  type="button"
                  className="btn dsr-button-danger"
                  onClick={() => handleListItemClick("Cancel")}
                >
                  Cancel
                </button>
              </div>
              {/* <div>
          {this.state.deleteStatus && (
            <div>
              {console.log(this.state.deleteStatus, "--------------->")}
              <SimpleDialogDelete
                selectedValue={this.state.submitResult}
                click={(value) => {
                  this.dsrSubmitStatus(value);
                }}
                open={this.state.buttonState}
                onClose={this.handleClose}
                deleteStatus={this.state.deleteStatus}
              />
            </div>
          )}
        </div> */}
            </div>
          )}
        </div>
      )}
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
};

function SimpleDialogDelete(props) {
  const classes = useStyles();
  const { classese } = props;
  const [buttonState, setButtonState] = useState(false);
  const { onClose, selectedValue, open, click, deleteStatus } = props;
  let selectedRowsArray = selectedRowsDsr.map((ele) => {
    return Object.values(ele);
  });

  //debugger;
  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleDeleteItemClick = (value) => {
    //console.log(value);

    setButtonState(false);
    click(value);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      className={classes.root}
      fullWidth={true}
      maxWidth={"md"}
      maxHeight={"100vh"}
    >
      {/* {deleteStatus && ( */}
      <div>
        <DialogTitle id="simple-dialog-title" className={classes.avatar}>
          Delete the selected rows from DSR
        </DialogTitle>

        <List>
          <div className="dsr-table">
            {/* {loader && (
    <div className={classes.loaderRoot}>
      <CircularProgress color="#000" />
    </div>
  )} */}
            <CustomTable
              tableHead={selectedColumnsDsr}
              tableData={selectedRowsArray}
              customClassesForCells={[0, 5, 6]}
              customHeadCellClasses={[
                classes.center,
                classes.right,
                classes.right,
              ]}
              // 0 is for classes.center, 5 is for classes.right, 6 is for classes.right
              customHeadClassesForCells={[0, 5, 6]}
            />
          </div>
        </List>
        <div className="container dsr-button-container button-group">
          <button
            type="button"
            className="btn dsr-button mr-4"
            onClick={() => handleDeleteItemClick("Delete")}
          >
            Delete
          </button>
          <button
            type="button"
            className="btn dsr-button-danger"
            onClick={() => handleDeleteItemClick("Cancel")}
          >
            Cancel
          </button>
        </div>
      </div>
      {/* )} */}
    </Dialog>
  );
}

SimpleDialogDelete.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
};
