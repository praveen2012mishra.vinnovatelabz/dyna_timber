// import React, { Component } from 'react';

// let cssPagedMedia = (function() {
// 	var style = document.createElement('style');
// 	document.head.appendChild(style);
// 	return function(rule) {
// 		style.innerHTML = rule;
// 	};
// })();

// cssPagedMedia.size = function(size) {
// 	cssPagedMedia('@page {size: ' + size + '}');
// };

// export default class PrintPanel extends Component {
// 	mediaQuery = (printWidth, printHeader) => {
// 		let widthBody = printWidth.style.width.match(/(\d+)/g).map(Number)[0];
// 		let previousElement = document.querySelector('.ag-root');
// 		let headerWidth = printHeader.style.width;

// 		headerWidth = Number(headerWidth.replace(/px$/, ''));
// 		headerWidth = headerWidth.toString();
// 		headerWidth = headerWidth.replace('px', '');
// 		headerWidth = parseInt(headerWidth, 10);

// 		previousElement.classList.add('ag-size-scale');
// 		//let printPage = document.querySelector('.ag-layout-print');
// 		let printPage = document.querySelector('.ag-size-scale');
// 		var box = document.querySelector('.ag-header-container');
// 		let width = box.style.width;
// 		width = Number(width.replace(/px$/, ''));
// 		let widths = width.toString();
// 		widths = widths.replace('px', '');
// 		width = parseInt(widths, 10);
// 		let zoom = width / 842;
// 		let zoomHeader = headerWidth / 842;
// 		//console.log(headerWidth, zoomHeader, 'header', zoom);
// 		if (width < 603) {
// 			//A4 Page Size
// 			if (zoom < 1) {
// 				zoom = 2.5 - zoom;
// 			}
// 			printPage.style.zoom = `${zoom}`;
// 			cssPagedMedia.size('11.7in 16.5in');
// 			printHeader.style.zoom = `.7`;
// 		} else if (width > 600 && width < 841) {
// 			//A4 Page Size
// 			if (zoom < 1) {
// 				zoom = zoom + 0.15;
// 			}
// 			console.log(width, zoom, 'inner');
// 			cssPagedMedia.size('11.7in 16.5in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `.67`;
// 		} else if (width > 840 && width < 1000) {
// 			//A4 Page Size
// 			if (zoom > 1) {
// 				zoom = 1;
// 			}
// 			//console.log(width, zoom, 'inner');
// 			cssPagedMedia.size('11.7in 16.5in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `.66`;
// 		} else if (width > 1000 && width < 1300) {
// 			//A3 Page Size
// 			if (zoom > 1) {
// 				zoom = 1 / zoom;
// 			}
// 			cssPagedMedia.size('11.7in 16.5in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `.66`;
// 		} else if (width > 1300 && width < 1500) {
// 			//A3 Page Size
// 			if (zoom > 1) {
// 				zoom = 1 / zoom;
// 			}
// 			cssPagedMedia.size('11.7in 16.5in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `.63`;
// 		} else if (width > 1500 && width < 1730) {
// 			//A3 Page Size
// 			if (zoom > 1) {
// 				zoom = 0.8;
// 			}
// 			cssPagedMedia.size('11.7in 16.5in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `.57`;
// 		} else if (width > 1730 && width < 2000) {
// 			//A3 Page Size
// 			if (zoom > 1) {
// 				zoom = 1 / zoom;
// 			}
// 			cssPagedMedia.size('11.7in 16.5in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `.5`;
// 		}
// 		let zoomLandscape = width / 1584;
// 		let zoomHeaderLandscape = headerWidth / 1584;
// 		//console.log(width, zoomLandscape, 'outer', zoomHeaderLandscape);
// 		if (width > 2000 && width < 2300) {
// 			//A3 Page Size Landscape
// 			if (zoomLandscape > 1) {
// 				zoom = 0.6;
// 			}
// 			cssPagedMedia.size('16.5in 11.7in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `${zoom}`;
// 		} else if (width > 2300 && width < 2700) {
// 			//A3 Page Size Landscape
// 			if (zoomLandscape > 1) {
// 				zoom = 0.5;
// 			}
// 			cssPagedMedia.size('16.5in 11.7in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `${zoom}`;
// 		} else if (width > 2700 && width < 3200) {
// 			//A3 Page Size Landscape
// 			if (zoomLandscape > 1) {
// 				zoom = 0.425;
// 			}
// 			cssPagedMedia.size('16.5in 11.7in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `${zoom}`;
// 		} else if (width > 3200 && width < 4000) {
// 			//A3 Page Size Landscape
// 			if (zoomLandscape > 1) {
// 				zoom = 0.35;
// 			}
// 			cssPagedMedia.size('16.5in 11.7in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `${zoom}`;
// 		} else if (width > 4000 && width < 5000) {
// 			//A3 Page Size Landscape
// 			if (zoomLandscape > 1) {
// 				zoom = 0.31;
// 			}
// 			cssPagedMedia.size('16.5in 11.7in');
// 			printPage.style.zoom = `${zoom}`;
// 			printHeader.style.zoom = `${zoom}`;
// 		}
// 	};
// 	print = () => {
// 		var eGridDiv = document.querySelector('.grid-class');
// 		eGridDiv.style.width = '100vw';
// 		eGridDiv.style.height = '';
// 		var gridApi = this.props.api;
// 		gridApi.setDomLayout('print');

// 		let printWidth = document.querySelector('.ag-header-container');
// 		let printHeader = document.getElementById('first-print');

// 		printHeader.style.width = printWidth.style.width;
// 		this.mediaQuery(printWidth, printHeader);

// 		setTimeout(function() {
// 			// eslint-disable-next-line no-restricted-globals

// 			print();
// 			let printPage = document.querySelector('.ag-layout-print');
// 			window.location.reload();
// 			gridApi.setDomLayout(null);
// 		}, 3000);
// 	};

// 	render() {
// 		return (
// 			<div style={{ margin: '10px' }}>
// 				<div style={{ margin: '5px', textAlign: 'right' }}>
// 					<label>
// 						<button
// 							onClick={() => this.print()}
// 							type="submit"
// 							style={{ height: '25px', cursor: 'pointer' }}
// 						>
// 							Print
// 						</button>
// 					</label>
// 				</div>
// 			</div>
// 		);
// 	}
// }

import React, { Component } from 'react';

let cssPagedMedia = (function() {
	var style = document.createElement('style');
	document.head.appendChild(style);
	return function(rule) {
		style.innerHTML = rule;
	};
})();

cssPagedMedia.size = function(size) {
	cssPagedMedia('@page {size: ' + size + '}');
};

export default class PrintPanel extends Component {
	mediaQuery = (printWidth, printHeader) => {
		let widthBody = printWidth.style.width.match(/(\d+)/g).map(Number)[0];
		let previousElement = document.querySelector('.ag-root');
		let headerWidth = printHeader.style.width;
		let printWidthPrint=document.querySelector('.print-width');
		printWidthPrint.classList.add('correct-print-width');
		let correctPrintWidth=document.querySelector('.correct-print-width');
		correctPrintWidth.style.cssText = 'float:none !important';
		//style.cssText = 'display:inline !important';
		correctPrintWidth.style.cssText = 'overflow:unset !important';
		headerWidth = Number(headerWidth.replace(/px$/, ''));
		headerWidth = headerWidth.toString();
		headerWidth = headerWidth.replace('px', '');
		headerWidth = parseInt(headerWidth, 10);

		previousElement.classList.add('ag-size-scale');
		//let printPage = document.querySelector('.ag-layout-print');
		let printPage = document.querySelector('.ag-size-scale');
		var box = document.querySelector('.ag-header-container');
		let width = box.style.width;
		width = Number(width.replace(/px$/, ''));
		let widths = width.toString();
		widths = widths.replace('px', '');
		width = parseInt(widths, 10);
		if (width < 2000) {
			let zoom = 715 / width;
			let zoomHeader = 600 / headerWidth;
			printPage.style.zoom = `${zoom}`;
			cssPagedMedia.size('8.3in 11.7in');
			printHeader.style.zoom = `${zoomHeader}`;
		}
		console.log(headerWidth);
		if (width > 2000) {
			let zoom = 1025 / width;
			let zoomHeader = 950 / headerWidth;
			printPage.style.zoom = `${zoom}`;
			cssPagedMedia.size('11.7in 8.3in');
			printHeader.style.zoom = `${zoomHeader}`;
		}
	};
	print = () => {
		var eGridDiv = document.querySelector('.grid-class');
		eGridDiv.style.width = '100vw';
		eGridDiv.style.height = '';
		var gridApi = this.props.api;
		gridApi.setDomLayout('print');
		let printWidth = document.querySelector('.ag-header-container');
		let printHeader = document.getElementById('first-print');
		printHeader.style.width = '1545px';
		this.mediaQuery(printWidth, printHeader);
		setTimeout(function() {
			window.print();
			window.location.reload();
			gridApi.setDomLayout(null);
		}, 3000);
	};

	render() {
		return (
			<div style={{ margin: '10px' }}>
				<div style={{ margin: '5px', textAlign: 'right' }}>
					<label>
						<button
							onClick={() => this.print()}
							type="submit"
							style={{ height: '25px', cursor: 'pointer' }}
						>
							Print
						</button>
					</label>
				</div>
			</div>
		);
	}
}
