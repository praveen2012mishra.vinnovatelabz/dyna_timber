import React, {Component} from 'react';
import {Field, Formik} from "formik";

export default class ExportToExcelPanel extends Component {

    export = (values) => {
        this.props.api.exportDataAsExcel({
            fileName: values.fileName,
            sheetName: values.sheetName,
            exportMode: values.exportMode
        })
    };

    render() {
        return (
            <div style={{margin: "10px"}}>
                <Formik initialValues={{exportMode: "xlsx", sheetName: "sheet1", fileName: "dynaExport"}}
                        onSubmit={(values) => {
                            this.export(values)
                        }}
                        render={({
                                     handleSubmit,
                                 }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="item details">
                                    <label>
                                        File Name:
                                        <Field type="text" name="fileName" style={{width: "100%"}}/>
                                    </label>
                                    <br/>
                                    <label>
                                        Sheet Name:
                                        <Field type="text" name="sheetName" maxLength="31" style={{width: "100%"}}/>
                                    </label>
                                    <br/>
                                    <label>
                                        Export Mode:
                                        <Field component="select" name="exportMode">
                                            <option value="xlsx">.xlsx</option>
                                            <option value="xml">.xml</option>
                                        </Field>
                                    </label>
                                </div>
                                <br/>
                                <div style={{margin: "5px", textAlign: "right"}}>
                                    <label>
                                        <button type="submit" style={{height: "25px", cursor: "pointer"}}>
                                            Export to Excel
                                        </button>
                                    </label>
                                </div>
                            </form>
                        )}/>
            </div>
        );
    }
};