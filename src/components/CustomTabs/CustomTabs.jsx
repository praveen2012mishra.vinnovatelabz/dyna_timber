import React from 'react';
// nodejs library that concatenates classes
import classNames from 'classnames';
// nodejs library to set properties for components
import PropTypes from 'prop-types';
// material-ui components
import withStyles from '@material-ui/core/styles/withStyles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
// core components
import Card from 'components/Card/Card.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';

import customTabsStyle from 'assets/jss/material-dashboard-pro-react/components/customTabsStyle.jsx';
import Box from '@material-ui/core/Box';
import Button from 'components/CustomButtons/Button';

class CustomTabs extends React.Component {
  render() {
    const {
      classes,
      headerColor,
      plainTabs,
      tabs,
      title,
      rtlActive,
      customActions,
      value,
      handleChange
    } = this.props;
    const cardTitle = classNames({
      [classes.cardTitle]: true,
      [classes.cardTitleRTL]: rtlActive
    });
    return (
      <Card plain={plainTabs}>
        <CardHeader
          color={headerColor}
          plain={plainTabs}
          className="card-header"
        >
          {title !== undefined ? (
            <div className={cardTitle}>{title}</div>
          ) : null}
          <Box display="flex">
            <Box display="flex" flexGrow={1}>
              <Tabs
                value={value}
                onChange={(event, value) => handleChange(value)}
                classes={{
                  root: classes.tabsRoot,
                  indicator: classes.displayNone
                }}
              >
                {tabs.map((prop, key) => {
                  var icon = {};
                  if (prop.tabIcon) {
                    icon = {
                      icon: <prop.tabIcon />
                    };
                  }
                  return (
                    <Tab
                      classes={{
                        root: classes.tabRootButton,
                        selected: classes.tabSelected,
                        wrapper: classes.tabWrapper
                      }}
                      key={key}
                      label={prop.tabName}
                      {...icon}
                    />
                  );
                })}
              </Tabs>
            </Box>
            <Box display="flex">
              {customActions
                ? customActions.map((action, index) => (
                    <Button key={index} justIcon round color="info">
                      {action}
                    </Button>
                  ))
                : null}
            </Box>
          </Box>
        </CardHeader>
        <CardBody>
          {tabs.map((prop, key) => {
            if (key === value) {
              return <div key={key}>{prop.tabContent}</div>;
            }
            return null;
          })}
        </CardBody>
      </Card>
    );
  }
}

CustomTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  headerColor: PropTypes.oneOf([
    'warning',
    'success',
    'danger',
    'info',
    'primary',
    'rose'
  ]),
  title: PropTypes.string,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      tabName: PropTypes.string.isRequired,
      tabIcon: PropTypes.object,
      tabContent: PropTypes.node.isRequired
    })
  ),
  rtlActive: PropTypes.bool,
  plainTabs: PropTypes.bool
};

export default withStyles(customTabsStyle)(CustomTabs);
