import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import Select from "react-select";
import { emphasize, makeStyles, useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import NoSsr from "@material-ui/core/NoSsr";
import CustomInput from "components/CustomInput/CustomInput";
import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Chip from "@material-ui/core/Chip";
import MenuItem from "@material-ui/core/MenuItem";
import CancelIcon from "@material-ui/icons/Cancel";
import extendedFormsStyle from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.jsx";

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      style={{
        padding: "6px"
      }}
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      No results OR loading initial list from the server
      {/* {props.children} */}
    </Typography>
  );
}

// NoOptionsMessage.propTypes = {
//   /**
//    * The children to be rendered.
//    */
//   children: PropTypes.node,
//   /**
//    * Props to be passed on to the wrapper.
//    */
//   innerProps: PropTypes.object.isRequired,
//   selectProps: PropTypes.object.isRequired
// };

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

// inputComponent.propTypes = {
//   inputRef: PropTypes.oneOfType([
//     PropTypes.func,
//     PropTypes.shape({
//       current: PropTypes.any.isRequired
//     })
//   ])
// };

function Control(props) {
  const {
    children,
    innerProps,
    innerRef,
    selectProps: { classes, TextFieldProps }
  } = props;

  return (
    <CustomInput
      labelText={TextFieldProps.label}
      formControlProps={{
        fullWidth: true
      }}
      labelProps={{
        shrink: true
      }}
      inputProps={{
        inputComponent,
        inputProps: {
          children,
          ref: innerRef,
          ...innerProps
        }
      }}
      {...TextFieldProps}
    ></CustomInput>
    // <TextField
    //   fullWidth
    //   className={classes}
    //   InputProps={{
    //     inputComponent,
    //     inputProps: {
    //       //   style: { marginBottom: "15px" },
    //       ref: innerRef,
    //       children,
    //       ...innerProps
    //     }
    //   }}
    //   {...TextFieldProps}
    // />
  );
}

// Control.propTypes = {
//   /**
//    * Children to render.
//    */
//   children: PropTypes.node,
//   /**
//    * The mouse down event and the innerRef to pass down to the controller element.
//    */
//   innerProps: PropTypes.shape({
//     onMouseDown: PropTypes.func.isRequired
//   }).isRequired,
//   innerRef: PropTypes.oneOfType([
//     PropTypes.oneOf([null]),
//     PropTypes.func,
//     PropTypes.shape({
//       current: PropTypes.any.isRequired
//     })
//   ]).isRequired,
//   selectProps: PropTypes.object.isRequired
// };

function Option(props) {
  return (
    <MenuItem
      ref={props.innerRef}
      selected={props.isFocused}
      component="div"
      className={props.selectProps.classes.selectMenuItem}
      //   style={{
      //     fontWeight: props.isSelected ? 500 : 400
      //   }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

// Option.propTypes = {
//   /**
//    * The children to be rendered.
//    */
//   children: PropTypes.node,
//   /**
//    * props passed to the wrapping element for the group.
//    */
//   innerProps: PropTypes.shape({
//     id: PropTypes.string.isRequired,
//     key: PropTypes.string.isRequired,
//     onClick: PropTypes.func.isRequired,
//     onMouseMove: PropTypes.func.isRequired,
//     onMouseOver: PropTypes.func.isRequired,
//     tabIndex: PropTypes.number.isRequired
//   }).isRequired,
//   /**
//    * Inner ref to DOM Node
//    */
//   innerRef: PropTypes.oneOfType([
//     PropTypes.oneOf([null]),
//     PropTypes.func,
//     PropTypes.shape({
//       current: PropTypes.any.isRequired
//     })
//   ]).isRequired,
//   /**
//    * Whether the option is focused.
//    */
//   isFocused: PropTypes.bool.isRequired,
//   /**
//    * Whether the option is selected.
//    */
//   isSelected: PropTypes.bool.isRequired
// };

function Placeholder(props) {
  const { selectProps, innerProps = {}, children } = props;
  return (
    <Typography
      //   color="textSecondary"
      className={selectProps.classes.selectLabel}
      {...innerProps}
    >
      {children}
    </Typography>
  );
}

// Placeholder.propTypes = {
//   /**
//    * The children to be rendered.
//    */
//   children: PropTypes.node,
//   /**
//    * props passed to the wrapping element for the group.
//    */
//   innerProps: PropTypes.object,
//   selectProps: PropTypes.object.isRequired
// };

function SingleValue(props) {
  return (
    <Typography
      className={props.selectProps.classes.selectLabel}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

// SingleValue.propTypes = {
//   /**
//    * The children to be rendered.
//    */
//   children: PropTypes.node,
//   /**
//    * Props passed to the wrapping element for the group.
//    */
//   innerProps: PropTypes.any.isRequired,
//   selectProps: PropTypes.object.isRequired
// };

function ValueContainer(props) {
  return (
    <div className={props.selectProps.classes.valueContainer}>
      {props.children}
    </div>
  );
}

// ValueContainer.propTypes = {
//   /**
//    * The children to be rendered.
//    */
//   children: PropTypes.node,
//   selectProps: PropTypes.object.isRequired
// };

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={clsx(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

// MultiValue.propTypes = {
//   children: PropTypes.node,
//   isFocused: PropTypes.bool.isRequired,
//   removeProps: PropTypes.shape({
//     onClick: PropTypes.func.isRequired,
//     onMouseDown: PropTypes.func.isRequired,
//     onTouchEnd: PropTypes.func.isRequired
//   }).isRequired,
//   selectProps: PropTypes.object.isRequired
// };

function Menu(props) {
  return (
    <Paper
      square
      className={props.selectProps.classes.selectPaper}
      {...props.innerProps}
    >
      {props.children}
    </Paper>
  );
}

// Menu.propTypes = {
//   /**
//    * The children to be rendered.
//    */
//   children: PropTypes.element.isRequired,
//   /**
//    * Props to be passed to the menu wrapper.
//    */
//   innerProps: PropTypes.object.isRequired,
//   selectProps: PropTypes.object.isRequired
// };

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer,
  DropdownIndicator: null
};

const Autocomplete = ({
  classes,
  placeholder,
  suggestions,
  label,
  value,
  onChange
}) => {
  const theme = useTheme();
  const [single, setSingle] = React.useState(null);
  const [multi, setMulti] = React.useState(null);

  function handleChangeSingle(value) {
    setSingle(value);
  }

  function handleChangeMulti(value) {
    setMulti(value);
  }

  const selectStyles = {
    input: base => ({
      ...base,
      paddingBottom: "0px!important",
      paddingTop: "0px!important",
      margin: "0px!important",
      color: theme.palette.text.primary,
      "& input": {
        font: "inherit"
      }
    })
  };

  return (
    <Select
      classes={classes}
      styles={selectStyles}
      TextFieldProps={{
        label: label || "Select",
        InputLabelProps: {
          htmlFor: "react-select-single",
          shrink: true
        }
      }}
      placeholder={placeholder}
      options={suggestions}
      components={components}
      //   value={single}
      //   onChange={handleChangeSingle}
      value={value}
      onChange={e => {
        // handleChangeSingle(e);
        onChange(e);
      }}
    />
  );
};

export default withStyles(extendedFormsStyle)(Autocomplete);
