import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { Redirect, Route, Router, Switch } from 'react-router-dom';
import * as mobx from 'mobx';
import { computed } from 'mobx';
import { Provider } from 'mobx-react';
import rootStore from './stores/RootStore';
import { SnackbarProvider } from 'notistack';
import 'ag-grid-enterprise';
import 'ag-grid-enterprise/chartsModule';
import Admin from './layouts/Admin.jsx';
import Auth from './layouts/Auth.jsx';
import 'assets/scss/material-dashboard-pro-react.scss?v=1.7.0';
import 'assets/scss/ag-grid-theme.scss';

const history = createBrowserHistory();
const routingStore = new RouterStore();
const syncedHistory = syncHistoryWithStore(history, routingStore);
rootStore.routingStore = routingStore;

// mobx.configure({ enforceActions: "observed" }); // don't allow state modifications outside actions
// eslint-disable-next-line
//for autocompletion fields
rootStore.inventory.fetchAll();
rootStore.materials.fetchAll();
rootStore.machines.fetchAll();

ReactDOM.render(
  <Provider root={rootStore} {...rootStore}>
    <SnackbarProvider
      maxSnack={3}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right'
      }}
    >
      <Router history={syncedHistory}>
        <Switch>
          <Route path="/auth" component={Auth} />
          <Route
            path="/admin"
            render={props =>
              computed(() => rootStore.auth.token !== null).get() ? (
                <Admin {...props} />
              ) : (
                <Redirect to="/auth/login-page" />
              )
            }
          />
          <Redirect from="/" to="/admin/dashboard" />
        </Switch>
      </Router>
    </SnackbarProvider>
  </Provider>,
  document.getElementById('root')
);
