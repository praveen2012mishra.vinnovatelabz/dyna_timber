import { action, flow, observable } from "mobx";
import MachinesClient from "api/machines";

export default class MachinesStore {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedTab = 0;

  fetchAll = flow(function*(resetData = false) {
    if (resetData) {
      this.data = [];
    }
    this.loading = true;
    try {
      const data = yield MachinesClient.all();

      this.data = data.data;
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @action
  selectItem = item => {
    console.log(item);
    this.selectedItem = item;
  };

  @action
  selectTab = tab => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}
