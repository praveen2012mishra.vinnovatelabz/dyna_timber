import InventoryStore from "stores/InventoryStore";
import AuthStore from "stores/AuthStore";
import NotificationsStore from "stores/NotificationsStore";
import MachinesStore from "stores/MachinesStore";
import MaterialsStore from "stores/MaterialsStore";
import ProductionDataStore from "stores/ProductionDataStore";
import InventoryStocktakeStore from "stores/InventoryStocktakeStore";
import MaterialStocktakeStore from "stores/MaterialStocktakeStore";
import FinishedInventoryReport from "stores/reports/FinishedInventoryReportStore";
import ReleaseReportStore from "stores/reports/ReleaseReportStore";
import DrumModeStore from "stores/DrumModeStore";
import DrumPerformanceStore from "stores/DrumPerformanceStore";
import DrumMissedChargesStore from "stores/DrumMissedChargesStore";
import DrumOutOfSequenceStore from "stores/DrumOutOfSequenceStore";
import DrumLoadingReportStore from "stores/DrumLoadingReportStore";
import FeedstockInventorySKUStore from "stores/reports/FeedstockInventorySKUStore";
import FeedstockInventoryOrderedSizeStore from "stores/reports/FeedstockInventoryOrderedSizeStore";
import DrumScheduleStore from "stores/DrumScheduleStore";
import NonDrumScheduleStore from "stores/NonDrumScheduleStore";
import InvoiceStore from "stores/reports/InvoiceStore";
import UsersStore from "stores/UsersStore";
import MaximumDemandAnalasysStore from "stores/reports/MaximumDemandAnalasysStore";
import ProductionHoursWorkedStore from "stores/ProductionHoursWorkedStore";
import InvoiceFilterStore from "stores/InvoiceFilterStore";
import TreatmentChargeStore from './TreatmentChargeStore';
import DashboardPdStore from "./DashboardPdStore";
class RootStore {
  constructor() {
    this.auth = new AuthStore(this);
    this.dashboardPd=new DashboardPdStore(this);
    this.inventory = new InventoryStore(this);
    this.notifications = new NotificationsStore(this);
    this.machines = new MachinesStore(this);
    this.materials = new MaterialsStore(this);
    this.productionData = new ProductionDataStore(this);
    this.inventoryStocktake = new InventoryStocktakeStore(this);
    this.materialsStocktake = new MaterialStocktakeStore(this);
    this.finishedInventoryReport = new FinishedInventoryReport(this);
    this.releaseReport = new ReleaseReportStore(this);
    this.drumMode = new DrumModeStore(this);
    this.drumPerformance = new DrumPerformanceStore(this);
    this.drumMissedCharges = new DrumMissedChargesStore(this);
    this.drumOutOfSequence = new DrumOutOfSequenceStore(this);
    this.drumLoadingReport = new DrumLoadingReportStore(this);
    this.feedstockInventorySKU = new FeedstockInventorySKUStore(this);
    this.feedstockInventoryOrderedSize = new FeedstockInventoryOrderedSizeStore(
      this
    );
    this.drumSchedule = new DrumScheduleStore(this);
    this.nonDrumSchedule = new NonDrumScheduleStore(this);
    this.invoices = new InvoiceStore(this);
    this.users = new UsersStore(this);
    this.maximumDemandAnalasys = new MaximumDemandAnalasysStore(this);
    this.productionHoursWorked = new ProductionHoursWorkedStore(this);
    this.invoiceFilter = new InvoiceFilterStore(this);
    this.treatmentChargeTimes = new TreatmentChargeStore(this);
  }
}

export default new RootStore();
