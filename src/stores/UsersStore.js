import {action, flow, observable} from "mobx";
import UsersClient from "api/users";

export default class UsersStore {
    @observable data = [];
    @observable loading = false;
    @observable selectedItem = null;
    @observable selectedTab = 0;
    @observable me = null;

    fetchAll = flow(function* () {
        this.data = [];
        this.loading = true;
        try {
            const data = yield UsersClient.all();

            this.data = data.data;
        } catch (error) {
            console.error(error)
        } finally {
            this.loading = false
        }
    }).bind(this);

    fetchMe = flow(function* (id = null) {
        this.me = null;
        this.loading = true;
        try {
            const data = yield UsersClient.one(id !== null ? id : this.root.auth.id);

            this.me = data.data;
        } catch (error) {
            console.error(error)
        } finally {
            this.loading = false
        }
    }).bind(this);

    constructor(rootStore) {
        this.root = rootStore
    }

    @action
    selectItem = (item) => {
        this.selectedItem = item;
    };

    @action
    selectTab = (tab) => {
        this.selectedTab = tab;
    };

    @action
    selectItemsTab = () => {
        this.selectedTab = 0;
    };

    @action
    selectNewTab = () => {
        this.selectedTab = 1;
    };

    @action
    selectEditTab = () => {
        this.selectedTab = 2;
    };
}