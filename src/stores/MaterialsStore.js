// import { action, flow, observable } from "mobx";
// import MaterialsClient from "api/materials";

// export default class MaterialsStore {
//   @observable data = [];
//   @observable loading = false;
//   @observable selectedItem = null;
//   @observable selectedItems = [];
//   @observable selectedTab = 0;

//   fetchAll = flow(function*(resetData = false) {
//     if (resetData) {
//       this.data = [];
//     }
//     this.loading = true;
//     try {
//       const data = yield MaterialsClient.all();

//       this.data = data.data;
//     } catch (error) {
//       console.error(error);
//     } finally {
//       this.loading = false;
//     }
//   }).bind(this);

//   constructor(rootStore) {
//     this.root = rootStore;
//   }

//   @action
//   selectItem = item => {
//     this.selectedItem = item;
//   };

//   @action
//   selectItems = items => {
//     this.selectedItems = items;
//   };

//   @action
//   selectTab = tab => {
//     this.selectedTab = tab;
//   };

//   @action
//   selectItemsTab = () => {
//     this.selectedTab = 0;
//   };

//   @action
//   selectNewTab = () => {
//     this.selectedTab = 1;
//   };

//   @action
//   selectEditTab = () => {
//     this.selectedTab = 2;
//   };
// }
import { action, flow, observable } from "mobx";
import MaterialsClient from "api/materials";

const newUpdateArray = (item, currentArray) => {
  let newArray = currentArray.map((ele) => {
    if (item.id === ele.id) {
      // ele.m3PerPack=item.m3PerPack;
      // ele.feedStock1RecPct=item.feedStock1RecPct;
      return ele;
    } else {
      return ele;
    }
  });
  return newArray;
};

export default class MaterialsStore {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedTab = 0;

  fetchAll = flow(function*(resetData = false) {
    if (resetData) {
      //debugger
      this.data = [];
    }
    this.data = [];
    this.loading = true;
    try {
      const data = yield MaterialsClient.all();

      this.data = data.data;
      //window.location.reload();
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);
  
  constructor(rootStore) {
    this.root = rootStore;
    //this.fetchAll();
  }

  

  @observable setUpdateData = [];
  @observable getUpdateObject = {};
  @action
  setUpdateObject = (item) => {
    this.getUpdateObject = Object.assign({}, item);
    this.setUpdateData = this.getUpdateObject.id
      ? Object.assign([], newUpdateArray(item, this.data))
      : [];
    let newArray =
      this.setUpdateData.length > 0 ? this.setUpdateData : this.data;
    //debugger
      this.data = Object.assign([], newArray);
  };

  @action
  selectItem = (item) => {
    //debugger
    this.selectedItem = item;
  };

  @action
  selectTab = (tab) => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}
