import { action, flow, observable } from "mobx";
import ProductionDataClient from "api/productionData";
import ProductionDataGetClient from 'api/productionDataGet'


export default class ProductionDataStore {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedItems = [];
  @observable selectedTab = 0;
  @observable deleteEnabled = true;

  fetchAll = flow(function* () {
    this.data = [];
    this.loading = true;
    try {
      let store = JSON.parse(sessionStorage.getItem("setPage")) || {
        ["ProductionDataTable"]: {},
      };
      //console.log(Object.keys(store.ProductionDataTable).length);
      if (
        Object.keys(store.ProductionDataTable).length > 0
      ) {
      const data = yield ProductionDataGetClient.save(store.ProductionDataTable);
      this.data = data.data;
      //console.log(data);
      //this.data = newArray;
      }
      if(
      Object.keys(store.ProductionDataTable).length === 0){
        let page={
          limit:100,
          pageNumber:1
        }
        const data = yield ProductionDataGetClient.save(page);
      this.data = data.data;
      }
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  deleteSelected = flow(function* () {
    try {
      if (this.selectedItem)
        yield ProductionDataClient.deleteOne(this.selectedItem.id);
      else if (this.selectedItems && this.selectedItems.length > 1)
        yield ProductionDataClient.deleteList(
          this.selectedItems.map((item) => item.id)
        );
    } catch (error) {
      console.error(error);
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }
  @action
  getPaginationItem = (item) => {
    console.log(item);
    //this.data =Object.assign([],item)
  };


  @action
  selectItem = (item) => {
    this.selectedItem = item;
  };

  @action
  selectItems = (items) => {
    console.log("Selecting itemS ", items);
    this.selectedItems = items;
  };

  @action
  selectTab = (tab) => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}
