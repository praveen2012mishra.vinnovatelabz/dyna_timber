import { action, flow, observable } from "mobx";
import DrumScheduleClient from "api/drumSchedule";

export default class DrumScheduleStore {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedItems = [];
  @observable selectedTab = 0;
  @observable deleteEnabled = true;

  fetchAll = flow(function*() {
    //debugger;
    this.data = [];
    this.loading = true;
    try {
      const data = yield DrumScheduleClient.all();
      //console.log(data.data, '------------------>DrumScheduleClient');

      this.data = data.data;
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  deleteSelected = flow(function*() {
    try {
      if (this.selectedItem) {
        console.log(this.selectedItem);
        yield DrumScheduleClient.deleteOne(this.selectedItem.id);
      } else if (this.selectedItems && this.selectedItems.length > 1) {
        yield DrumScheduleClient.deleteListPost(
          this.selectedItems.map(item => {
            //console.log(item.id, item);
            return item.id;
          })
        );
      }
    } catch (error) {
      console.error(error);
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @action
  selectItem = item => {
    console.log('Selecting item ', item);
    this.selectedItem = item;
  };

  @action
  selectItems = items => {
    console.log('Selecting item ', items);
    this.selectedItems = items;
  };

  @action
  selectTab = tab => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}


//end
// deleteSelected = flow(function*() {
//       yield DrumScheduleClient.deleteOne(this.selectedItem.id);
//    }).bind(this);