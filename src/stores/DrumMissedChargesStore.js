import {action, flow, observable} from "mobx";
import {DrumMissedChargesClient} from "api/drumMissedCharges";

export default class DrumMissedChargesStore {
    @observable data = [];
    @observable loading = false;
    @observable selectedItem = null;
    @observable selectedTab = 0;

    fetchAll = flow(function* () {
        this.data = [];
        this.loading = true;
        try {
            const data = yield DrumMissedChargesClient.all();

            this.data = data.data;
        } catch (error) {
            console.error(error)
        } finally {
            this.loading = false
        }
    }).bind(this);

    constructor(rootStore) {
        this.root = rootStore
    }

    @action
    selectItem = (item) => {
        this.selectedItem = item;
    };

    @action
    selectTab = (tab) => {
        this.selectedTab = tab;
    };

    @action
    selectItemsTab = () => {
        this.selectedTab = 0;
    };

    @action
    selectNewTab = () => {
        this.selectedTab = 1;
    };

    @action
    selectEditTab = () => {
        this.selectedTab = 2;
    };
}