import { computed, flow, observable } from 'mobx';
import AuthClient from 'api/auth';
import jwt_decode from 'jwt-decode';

export default class AuthStore {
	@observable token = null;
	@observable loading = false;

	login = flow(function*(loginInfo) {
		this.loading = true;
		try {
			const data = yield AuthClient.authenticate(
				new URLSearchParams(loginInfo).toString()
			);
			const auth = data.headers.authorization;
			console.log('Login data', auth);
			const token = auth ? auth.substring(7) : null;
			if (token != null) {
				window.localStorage.setItem('token', token);
			}
			this.token = token;
		} catch (error) {
			console.error(error);
		} finally {
			this.loading = false;
		}
	}).bind(this);

	logout = flow(function*() {
		window.localStorage.removeItem('token');
		window.localStorage.removeItem('hiddenColumns');
		window.sessionStorage.clear();
		this.token = null;
		this.root.routingStore.push('/auth/login-page');
	}).bind(this);

	constructor(rootStore) {
		this.root = rootStore;
		this.token = window.localStorage.getItem('token');
	}

	@computed
	get role() {
		if (this.token == null) return null;
		return jwt_decode(this.token)['rol'][0];
	}

	@computed
	get id() {
		if (this.token == null) return null;
		return jwt_decode(this.token)['sub'];
	}
}
