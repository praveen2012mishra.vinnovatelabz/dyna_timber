import { action, flow, observable } from 'mobx';
import TreatmentChargeClient from '../api/treatementChargeTimes';

const objTreatmentTable = [
	{ chargeID: 'CCA H3', chargeTimeMinutes: 35, chargeOverTime: 13 },
	{ chargeID: 'CCA H4', chargeTimeMinutes: 44, chargeOverTime: 13 }
];

export default class TreatmentChargeStore {
	@observable data = [];
	@observable loading = false;
	@observable selectedItem = null;
	@observable selectedTab = 0;

	fetchAll = flow(function*() {
		this.data = [];
		this.loading = true;

		try {
			const data = yield TreatmentChargeClient.all();
			this.data = data.data;
			//console.log(objTreatmentTable, '----------------------->');
			//this.data = objTreatmentTable;
		} catch (error) {
			console.error(error);
		} finally {
			this.loading = false;
		}
	}).bind(this);

	constructor(rootStore) {
		this.root = rootStore;
	}

	@action
	selectItem = item => {
		this.selectedItem = item;
	};

	@action
	selectTab = tab => {
		this.selectedTab = tab;
	};

	@action
	selectItemsTab = () => {
		this.selectedTab = 0;
	};

	@action
	selectNewTab = () => {
		this.selectedTab = 1;
	};

	@action
	selectEditTab = () => {
		this.selectedTab = 2;
	};
}
