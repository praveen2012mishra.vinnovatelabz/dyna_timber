import { action, flow, observable } from "mobx";
import InvoiceFilterClient from "api/invoiceFilter";

export default class InvoiceFilterStore {
  @observable data = [];
  @observable shipVia = [];
  @observable salesPerson = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedTab = 0;
  @observable deleteEnabled = true;

  fetchAll = flow(function*() {
    this.data = null;
    this.loading = true;
    try {
      let data = yield InvoiceFilterClient.all();
      this.data = data.data;

      data = yield InvoiceFilterClient.getDistinctShipvia();
      this.shipVia = data.data;

      data = yield InvoiceFilterClient.getDistinctSalesperson();
      this.salesPerson = data.data;

      console.log("ShipVia", this.shipVia);
      console.log("SalesPerson", this.salesPerson);
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  deleteSelected = flow(function*() {
    try {
      if (this.selectedItem)
        yield InvoiceFilterClient.deleteOne(this.selectedItem.id);
      else if (this.selectedItems && this.selectedItems.length > 1)
        yield InvoiceFilterClient.deleteList(
          this.selectedItems.map(item => item.id)
        );
    } catch (error) {
      console.error(error);
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @action
  selectItem = item => {
    this.selectedItem = item;
  };

  @action
  selectTab = tab => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}
