// import { action, flow, observable } from "mobx";
// import InventoryClient from "api/inventory";

// export default class InventoryStore {
//   @observable data = [];
//   @observable skus = [];
//   @observable loading = false;
//   @observable selectedItem = null;
//   @observable selectedTab = 0;

//   fetchAll = flow(function*(resetData = false) {
//     if (resetData) {
//       this.data = [];
//     }
//     this.loading = true;
//     try {
//       const data = yield InventoryClient.all();

//       this.data = data.data;
//     } catch (error) {
//       console.error(error);
//     } finally {
//       this.loading = false;
//     }
//   }).bind(this);

//   fetchAllSkus = flow(function*() {
//     this.skus = [];
//     try {
//       const data = yield InventoryClient.getSkus();

//       this.skus = data.data;
//       console.log("skus", this.skus);
//     } catch (error) {
//       console.error(error);
//     }
//   }).bind(this);

//   constructor(rootStore) {
//     this.root = rootStore;
//   }

//   @action
//   selectItem = item => {
//     this.selectedItem = item;
//   };

//   @action
//   selectItems = items => {
//     this.selectedItems = items;
//   };

//   @action
//   selectTab = tab => {
//     this.selectedTab = tab;
//   };

//   @action
//   selectItemsTab = () => {
//     this.selectedTab = 0;
//   };

//   @action
//   selectNewTab = () => {
//     this.selectedTab = 1;
//   };

//   @action
//   selectEditTab = () => {
//     this.selectedTab = 2;
//   };
// }
import { action, flow, observable } from "mobx";
import InventoryClient from "api/inventory";

const newUpdateArray = (item, currentArray) => {
  let newArray = currentArray.map((ele) => {
    if (item.id === ele.id) {
      ele.m3PerPack=item.m3PerPack;
      ele.feedStock1RecPct=item.feedStock1RecPct;
      ele.feedStock2RecPct=item.feedStock2RecPct;
      ele.usageM3Last30Days=item.usageM3Last30Days;
      ele.usagePacksLast30Days=item.usagePacksLast30Days;
      ele.usageDollarsLast30Days=item.usageDollarsLast30Days;
      ele.numPacksBuffer=item.numPacksBuffer;
      ele.m3Buffer=item.m3Buffer;
      ele.rawMaterialPull=item.rawMaterialPull;
      ele.b1=item.b1;
      ele.b2=item.b2;
      ele.b3=item.b3;
      ele.usage=item.usage;
      ele.usage=item.usage;
      return ele;
    } else {
      return ele;
    }
  });
  return newArray;
};

export default class InventoryStore {
  @observable data = [];
  @observable skus = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedTab = 0;
 

  fetchAll = flow(function*(resetData = false) {
    if (resetData) {
      this.data = [];
    }
    this.loading = true;
    try {
      const data = yield InventoryClient.all();

      this.data = data.data;
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  fetchAllSkus = flow(function*() {
    this.skus = [];
    try {
      const data = yield InventoryClient.getSkus();

      this.skus = data.data;
      console.log("skus", this.skus);
    } catch (error) {
      console.error(error);
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @observable setUpdateData = [];
  @observable getUpdateObject = {};
  @action
  setUpdateObject = (item) => {
    this.getUpdateObject = Object.assign({}, item);
    this.setUpdateData = this.getUpdateObject.id
      ? Object.assign([], newUpdateArray(item, this.data))
      : [];
    let newArray =
      this.setUpdateData.length > 0 ? this.setUpdateData : this.data;
    this.data = Object.assign([], newArray);
  };

  @action
  selectItem = item => {
    this.selectedItem = item;
  };

  @action
  selectTab = tab => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}

