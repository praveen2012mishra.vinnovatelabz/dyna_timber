import {action, flow, observable} from "mobx";
import InvoiceClient from "api/reports/invoice";

export default class InvoiceStore {
    @observable data = null;
    @observable loading = false;
    @observable selectedItem = null;
    @observable selectedTab = 0;

    fetchAll = flow(function* () {
        this.data = null;
        this.loading = true;
        try {
            const data = yield InvoiceClient.all();

            this.data = data.data;
        } catch (error) {
            console.error(error)
        } finally {
            this.loading = false
        }
    }).bind(this);

    constructor(rootStore) {
        this.root = rootStore
    }

    @action
    selectItem = (item) => {
        this.selectedItem = item;
    };

    @action
    selectTab = (tab) => {
        this.selectedTab = tab;
    };

    @action
    selectItemsTab = () => {
        this.selectedTab = 0;
    };

    @action
    selectNewTab = () => {
        this.selectedTab = 1;
    };

    @action
    selectEditTab = () => {
        this.selectedTab = 2;
    };
}