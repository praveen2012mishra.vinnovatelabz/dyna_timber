import { action, flow, observable } from 'mobx';
import FinishedInventoryReportClient from 'api/reports/finishedInventoryReport';

export default class FinishedInventoryReport {
	@observable data = [];
	@observable loading = false;
	@observable selectedItem = null;
	@observable selectedTab = 0;

	fetchAll = flow(function*() {
		this.data = [];
		this.loading = true;
		try {
			const data = yield FinishedInventoryReportClient.all();

			this.data = data.data;
		} catch (error) {
			console.error(error);
		} finally {
			this.loading = false;
		}
	}).bind(this);

	constructor(rootStore) {
		this.root = rootStore;
	}
	@observable FinishedInventoryTargetValue = {};
	@action
	setTargetValueFinishedInventory = item => {
		this.FinishedInventoryTargetValue = Object.assign(
			{},
			this.FinishedInventoryTargetValue,
			item
		);
	};
	@action
	getTargetValueFinishedInventory = item => {
		return this.FinishedInventoryTargetValue;
	};
	@action
	selectItem = item => {
		this.selectedItem = item;
	};

	@action
	selectTab = tab => {
		this.selectedTab = tab;
	};

	@action
	selectItemsTab = () => {
		this.selectedTab = 0;
	};
}
