// import {action, flow, observable} from "mobx";
// import {FeedstockInventoryOrderedSizeClient} from "api/reports/feedstockInventoryOrderedSize";

// export default class FeedstockInventoryOrderedSizeStore {
//     @observable data = [];
//     @observable loading = false;
//     @observable selectedItem = null;
//     @observable selectedTab = 0;

//     fetchAll = flow(function* () {
//         this.data = [];
//         this.loading = true;
//         try {
//             const data = yield FeedstockInventoryOrderedSizeClient.all();

//             this.data = data.data;
//         } catch (error) {
//             console.error(error)
//         } finally {
//             this.loading = false
//         }
//     }).bind(this);

//     constructor(rootStore) {
//         this.root = rootStore
//     }

//     @action
//     selectItem = (item) => {
//         this.selectedItem = item;
//     };

//     @action
//     selectTab = (tab) => {
//         this.selectedTab = tab;
//     };

//     @action
//     selectItemsTab = () => {
//         this.selectedTab = 0;
//     };

// }

import {action, flow, observable} from "mobx";
import {FeedstockInventoryOrderedSizeClient} from "api/reports/feedstockInventoryOrderedSize";

const newUpdateArray = (item, currentArray) => {
    let newArray = currentArray.map((ele) => {
        
      if (item.material.id === ele.material.id) {
        //console.log(ele,'------------>',item);
        ele.totalToOrder=item.totalToOrder;
        ele.total=item.total;
        ele.difference=item.difference;
        return ele;
      } else {
        return ele;
      }
    });
    return newArray;
  };

const getDataChange=(data)=>{
   let newArray=data.map((ele)=>{
     //console.log(ele);
     if(!ele.fio){
      ele.fio=Object.assign({});
      ele.fio.orderFrequency=0;
      ele.fio.totalOnOrder=0;
      ele.fio.hyne=0;
      ele.fio.imbil=0;
      ele.fio.akd=0;
      ele.fio.atp=0;
      ele.fio.wade=0;
      return ele;
     }
     else return ele;
   })
   return newArray;
}

export default class FeedstockInventoryOrderedSizeStore {
    @observable data = [];
    @observable loading = false;
    @observable selectedItem = null;
    @observable selectedTab = 0;

    fetchAll = flow(function* () {
        this.data = [];
        this.loading = true;
        try {
            const data = yield FeedstockInventoryOrderedSizeClient.all();
            let newData=getDataChange(data.data);
            this.data = Object.assign([],newData);
        } catch (error) {
            console.error(error)
        } finally {
            this.loading = false
        }
    }).bind(this);

    constructor(rootStore) {
        this.root = rootStore
    }

    @observable setUpdateData = [];
    @observable getUpdateObject = {};
    @action
    setUpdateObject = (item) => {
        //console.log('------------>',item);
      this.getUpdateObject = Object.assign({}, item);
      this.setUpdateData = this.getUpdateObject.fio.id
        ? Object.assign([], newUpdateArray(item, this.data))
        : [];
      let newArray =
        this.setUpdateData.length > 0 ? this.setUpdateData : this.data;
      this.data = Object.assign([], newArray);
    };

    @action
    selectItem = (item) => {
        this.selectedItem = item;
    };

    @action
    selectTab = (tab) => {
        this.selectedTab = tab;
    };

    @action
    selectItemsTab = () => {
        this.selectedTab = 0;
    };

}