import {action, flow, observable} from "mobx";
import {FeedstockInventorySKUClient} from "api/reports/feedstockInventorySKU";

export default class FeedstockInventorySKUStore {
    @observable data = [];
    @observable loading = false;
    @observable selectedItem = null;
    @observable selectedTab = 0;

    fetchAll = flow(function* () {
        this.data = [];
        this.loading = true;
        try {
            const data = yield FeedstockInventorySKUClient.all();

            this.data = data.data;
        } catch (error) {
            console.error(error)
        } finally {
            this.loading = false
        }
    }).bind(this);

    constructor(rootStore) {
        this.root = rootStore
    }

    @action
    selectItem = (item) => {
        this.selectedItem = item;
    };

    @action
    selectTab = (tab) => {
        this.selectedTab = tab;
    };

    @action
    selectItemsTab = () => {
        this.selectedTab = 0;
    };

}