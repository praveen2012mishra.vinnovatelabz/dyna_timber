import { action, flow, observable } from "mobx";
import MaximumDemandAnalasysClient from "api/reports/maximumDemandAnalasys";

export default class MaximumDemandAnalasysStore {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedTab = 0;

  fetchAll = flow(function*() {
    this.data = [];
    this.loading = true;
    try {
      const data = yield MaximumDemandAnalasysClient.all();

      this.data = data.data;
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @action
  selectItem = item => {
    this.selectedItem = item;
  };

  @action
  selectTab = tab => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };
}
