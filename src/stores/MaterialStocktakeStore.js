import { action, flow, observable } from "mobx";
import MaterialStocktakeClient from "api/materialsStocktake";

const newUpdateArray = (item, currentArray) => {
  let newArray = currentArray.map((ele) => {
    if (item.id === ele.id) {
      //console.log(item,ele,'================>');
      ele.week0To1M3=item.week0To1M3;
      ele.week2To3M3=item.week2To3M3;
      ele.week4To5M3=item.week4To5M3;
      ele.week6M3 =item.week6M3 ;
      ele.totalM3 =item.totalM3 ;
      return ele;
    } else {
      return ele;
    }
  });
  return newArray;
};

const setPrecision = (val, precision) => val.toFixed(precision);

const getPrecision=(data)=>{
  console.log(data);
  let newArray=data.map((item)=>{
    item.m3PerPack=setPrecision(item.m3PerPack,1);
    item.endOfSectionM2=setPrecision(item.endOfSectionM2,5);
    item.week5To6M3=setPrecision(item.week5To6M3,1);
    item.week3To4M3=setPrecision(item.week3To4M3,1);
    item.week1To2M3=setPrecision(item.week1To2M3,1);
    item.totalM3=setPrecision(item.totalM3,1);
    return item
  })
  return newArray
}

export default class MaterialStocktakeStore {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedTab = 0;

  fetchAll = flow(function*() {
    this.data = [];
    this.loading = true;
    try {
      const data = yield MaterialStocktakeClient.all();

      this.data = data.data;
      this.data=getPrecision(data.data);
      //console.log("Material stocktake data", this.data);
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @observable setUpdateData = [];
  @observable getUpdateObject = {};
  @action
  setUpdateObject = (item) => {
    this.getUpdateObject = Object.assign({}, item);
    this.setUpdateData = this.getUpdateObject.id
      ? Object.assign([], newUpdateArray(item, this.data))
      : [];
    let newArray =
      this.setUpdateData.length > 0 ? this.setUpdateData : this.data;
    this.data = Object.assign([], newArray);
  };

  @action
  selectItem = (item) => {
    console.log('Selecting item ', item);
    this.selectedItem = item;
  };

  @action
  selectTab = (tab) => {
    //console.log('Selecting item ', items);
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}
