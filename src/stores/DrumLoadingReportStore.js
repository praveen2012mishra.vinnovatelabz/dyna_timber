// import { action, flow, observable } from 'mobx';
// import { DrumLoadingReportClient } from 'api/drumLoadingReport';

// export default class DrumLoadingReportStore {
// 	@observable data = [];
// 	@observable loading = false;
// 	@observable selectedItem = null;
// 	@observable selectedTab = 0;

// 	fetchAll = flow(function*(deletePrevious = true) {
// 		this.data = [];
// 		this.loading = true;
// 		try {
// 			const data = yield DrumLoadingReportClient.all();

// 			this.data.replace(data.data);
// 		} catch (error) {
// 			console.error(error);
// 		} finally {
// 			this.loading = false;
// 		}
// 	}).bind(this);

// 	constructor(rootStore) {
// 		this.root = rootStore;
// 	}
// 	@observable drumLoadingTargetValue = {};
// 	@action
// 	setTargetValueDrumLoading = item => {
// 		this.drumLoadingTargetValue = Object.assign(
// 			{},
// 			this.drumLoadingTargetValue,
// 			item
// 		);
// 	};
// 	@action
// 	getTargetValueDrumLoading = item => {
// 		return this.drumLoadingTargetValue;
// 	};
// 	@action
// 	selectedColumn = item => {};
// 	@action
// 	selectItem = item => {
// 		this.selectedItem = item;
// 	};

// 	@action
// 	selectTab = tab => {
// 		this.selectedTab = tab;
// 	};

// 	@action
// 	selectItemsTab = () => {
// 		this.selectedTab = 0;
// 	};

// 	@action
// 	selectNewTab = () => {
// 		this.selectedTab = 1;
// 	};

// 	@action
// 	selectEditTab = () => {
// 		this.selectedTab = 2;
// 	};
// }
import { action, flow, observable } from 'mobx';
import { DrumLoadingReportClient } from 'api/drumLoadingReport';

const newUpdateArray=(item,currentArray)=>{
	
	let newArray=currentArray.map((ele)=>{
		
		//console.log(item,ele);
		if(item.id===ele.inventory.id){
			//debugger
			ele.currentAct=1;
			console.log(item,ele,'===============>');
			return ele
		}
		else{
			return ele
		}
	})
	return newArray;
	
}

export default class DrumLoadingReportStore {
	@observable data = [];
	@observable loading = false;
	@observable selectedItem = null;
	@observable selectedTab = 0;

	fetchAll = flow(function*(deletePrevious = true) {
		this.data = [];
		this.loading = true;
		try {
			const data = yield DrumLoadingReportClient.all();

			this.data.replace(data.data);
		} catch (error) {
			console.error(error);
		} finally {
			this.loading = false;
		}
	}).bind(this);

	constructor(rootStore) {
		this.root = rootStore;
	}
	@observable setUpdateData=[]
    @observable getUpdateObject={}
	@action 
	setUpdateObject=item=>{
	   this.getUpdateObject=Object.assign({},item);
	   this.setUpdateData=this.getUpdateObject.id?Object.assign([],newUpdateArray(item,this.data)):[];
	   let newArray=this.setUpdateData.length>0?this.setUpdateData:this.data;
	   this.data=Object.assign([],newArray);
	}

	@action
	selectedColumn = item => {};
	@action
	selectItem = item => {
		this.selectedItem = item;
	};

	@action
	selectTab = tab => {
		this.selectedTab = tab;
	};

	@action
	selectItemsTab = () => {
		this.selectedTab = 0;
	};

	@action
	selectNewTab = () => {
		this.selectedTab = 1;
	};

	@action
	selectEditTab = () => {
		this.selectedTab = 2;
	};
}
