import {action, observable} from 'mobx';

export default class NotificationsStore {
    @observable notifications = [];

    constructor(rootStore) {
        this.root = rootStore
    }

    @action
    enqueueSnackbar(note) {
        this.notifications.push({
            key: new Date().getTime() + Math.random(),
            ...note,
        });
    }

    @action
    removeSnackbar(note) {
        this.notifications.splice(this.notifications.indexOf(note), 1);
    }
}