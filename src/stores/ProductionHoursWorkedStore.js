import { action, flow, observable } from "mobx";
import ProductionHoursWorkedClient from "api/productionHoursWorked";

export default class ProductionHoursWorked {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedItems = [];
  @observable selectedTab = 0;
  @observable deleteEnabled = true;

  fetchAll = flow(function*() {
    this.data = [];
    this.loading = true;
    try {
      const data = yield ProductionHoursWorkedClient.all();

      this.data = data.data;
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  deleteSelected = flow(function*() {
    try {
      if (this.selectedItem)
        yield ProductionHoursWorkedClient.deleteOne(this.selectedItem.id);
      else if (this.selectedItems && this.selectedItems.length > 1)
        yield ProductionHoursWorkedClient.deleteList(
          this.selectedItems.map(item => item.id)
        );
    } catch (error) {
      console.error(error);
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @action
  selectItem = item => {
    this.selectedItem = item;
  };

  @action
  selectItems = items => {
    this.selectedItems = items;
  };

  @action
  selectTab = tab => {
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}
