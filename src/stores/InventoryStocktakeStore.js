import { action, flow, observable } from "mobx";
import InventoryStocktakeClient from "api/inventoryStocktake";

const newUpdateArray = (item, currentArray) => {
  let newArray = currentArray.map((ele) => {
    if (item.id === ele.id) {
      return ele;
    } else {
      return ele;
    }
  });
  return newArray;
};

export default class InventoryStocktakeStore {
  @observable data = [];
  @observable loading = false;
  @observable selectedItem = null;
  @observable selectedItems = [];
  @observable selectedTab = 0;
  @observable deleteEnabled = true;
 
  fetchAll = flow(function*() {
    this.data = [];
    this.loading = true;
    try {
      const data = yield InventoryStocktakeClient.all();

      this.data = data.data;
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }).bind(this);

  deleteSelected = flow(function*() {
    
    try {
      if (this.selectedItem){
        console.log(this.selectedItem);
        yield InventoryStocktakeClient.deleteOne(this.selectedItem.id);
        }
      else if (this.selectedItems && this.selectedItems.length > 1)
       { yield InventoryStocktakeClient.deleteListPost(
          this.selectedItems.map(item => {
            console.log(item.inventory.id,item);
            return item.inventory.id
          })
        );
        
      }
    } catch (error) {
      console.error(error);
    }
  }).bind(this);

  constructor(rootStore) {
    this.root = rootStore;
  }

  @observable setUpdateData = [];
  @observable getUpdateObject = {};
  @action
  setUpdateObject = (item) => {
    this.getUpdateObject = Object.assign({}, item);
    this.setUpdateData = this.getUpdateObject.id
      ? Object.assign([], newUpdateArray(item, this.data))
      : [];
    let newArray =
      this.setUpdateData.length > 0 ? this.setUpdateData : this.data;
    this.data = Object.assign([], newArray);
  };

  @action
  selectItem = item => {
    console.log('Selecting item ', item);
    this.selectedItem = item;
  };

  @action
  selectItems = items => {
    console.log('Selecting item ', items);
    this.selectedItems = items;
  };

  @action
  selectTab = tab => {
    console.log(tab);
    this.selectedTab = tab;
  };

  @action
  selectItemsTab = () => {
    this.selectedTab = 0;
  };

  @action
  selectNewTab = () => {
    this.selectedTab = 1;
  };

  @action
  selectEditTab = () => {
    this.selectedTab = 2;
  };
}
