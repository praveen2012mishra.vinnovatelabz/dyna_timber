import React from "react";
import DefaultPage from "views/DefaultPage";
import {inject, observer} from "mobx-react";
import UsersTable from "views/Pages/Users/UsersTable";
import UsersForm from "views/Pages/Users/UsersForm";

@inject("users")
@observer
class Users extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.users;
        this.components = {
            table: UsersTable,
            edit: UsersForm
        };
    }

}

export default Users;
