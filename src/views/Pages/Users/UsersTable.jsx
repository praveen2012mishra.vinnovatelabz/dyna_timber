import DefaultAgTable from "components/Table/DefaultAgTable";

export default class UsersTable extends DefaultAgTable {

    columns = [
        {headerName: 'Full Name', field: 'fullName'},
        {headerName: 'Username', field: 'username'},
        {headerName: 'Role', field: 'role'},
        {headerName: 'Enabled?', field: 'enabled'},
        {headerName: 'Email', field: 'email'},
    ];

}
