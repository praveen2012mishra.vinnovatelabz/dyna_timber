import React from 'react'
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import {Field, Formik} from "formik";
import {inject, observer} from "mobx-react";
import UsersClient from "api/users";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import withStyles from "@material-ui/core/styles/withStyles";
import extendedFormsStyle from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";

@inject("notifications")
@observer
class UsersForm extends React.Component {

    render() {
        const {notifications, store, classes} = this.props;

        return (
            <Formik
                enableReinitialize
                initialValues={this.props.initialData ? this.props.initialData : {isEnabled: true}}
                onSubmit={async (values, actions) => {
                    try {
                        if (values.id)
                            await UsersClient.update(values);
                        else
                            await UsersClient.save(values);
                        notifications.enqueueSnackbar({message: 'Saved.'});
                        store.fetchAll();
                        store.selectItem(null);
                        store.selectItemsTab()
                    } catch (err) {
                        console.error(err);
                        notifications.enqueueSnackbar({message: 'Saving failed.', options: {variant: 'error'}});
                    } finally {
                        actions.setSubmitting(false)
                    }
                }}
                render={({
                             handleSubmit,
                             setFieldValue,
                             isSubmitting,
                         }) => (
                    <form onSubmit={handleSubmit}>
                        <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="fullName" render={({field}) => (
                                            <CustomInput
                                                labelText="Full Name"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="username" render={({field}) => (
                                            <CustomInput
                                                labelText="Username"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="password" render={({field}) => (
                                            <CustomInput
                                                labelText="Password"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "password",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                </GridContainer>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="email" render={({field}) => (
                                            <CustomInput
                                                labelText="Email"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="role" render={({field}) => (
                                            <FormControl
                                                fullWidth
                                                className={classes.selectFormControl}
                                            >
                                                <InputLabel
                                                    htmlFor="role"
                                                    className={classes.selectLabel}
                                                >
                                                    Choose Code
                                                </InputLabel>
                                                <Select
                                                    MenuProps={{
                                                        className: classes.selectMenu
                                                    }}
                                                    classes={{
                                                        select: classes.select
                                                    }}
                                                    value={field.value || ''}
                                                    onChange={(item) => {
                                                        setFieldValue("role", item.target.value)
                                                    }}
                                                    inputProps={{
                                                        id: "role"
                                                    }}
                                                >
                                                    <MenuItem
                                                        disabled
                                                        classes={{
                                                            root: classes.selectMenuItem
                                                        }}
                                                    >
                                                        Choose Role
                                                    </MenuItem>
                                                    <MenuItem key={1}
                                                              classes={{
                                                                  root: classes.selectMenuItem,
                                                                  selected: classes.selectMenuItemSelected
                                                              }}
                                                              value={"ADMIN"}
                                                    >
                                                        ADMIN
                                                    </MenuItem>
                                                    <MenuItem key={2}
                                                              classes={{
                                                                  root: classes.selectMenuItem,
                                                                  selected: classes.selectMenuItemSelected
                                                              }}
                                                              value={"FULL"}
                                                    >
                                                        FULL CONTROLL
                                                    </MenuItem>
                                                    <MenuItem key={3}
                                                              classes={{
                                                                  root: classes.selectMenuItem,
                                                                  selected: classes.selectMenuItemSelected
                                                              }}
                                                              value={"PRODUCTION"}
                                                    >
                                                        PRODUCTION DATA
                                                    </MenuItem>
                                                </Select>
                                            </FormControl>
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="enabled" render={({field}) => (
                                            <div className={classes.switch}>
                                                <InputLabel/>
                                                <FormControlLabel
                                                    control={
                                                        <Switch
                                                            checked={field.value}
                                                            onChange={(item) => setFieldValue("enabled", item.target.checked)}
                                                            // classes={{
                                                            //     switchBase: classes.switchBase,
                                                            //     checked: classes.switchChecked,
                                                            //     icon: classes.switchIcon,
                                                            //     iconChecked: classes.switchIconChecked,
                                                            //     bar: classes.switchBar
                                                            // }}
                                                        />
                                                    }
                                                    classes={{
                                                        label: classes.label
                                                    }}
                                                    label="Account Enabled?"
                                                />
                                            </div>
                                        )}/>
                                    </GridItem>
                                </GridContainer>


                                <Button color="primary" type="submit"
                                        disabled={isSubmitting}>{this.props.initialData ? "Update Item" : "Save Item"}</Button>

                            </GridItem>

                        </GridContainer>
                    </form>
                )}/>)

    }

}

export default withStyles(extendedFormsStyle)(UsersForm);