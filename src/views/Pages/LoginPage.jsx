import React from "react";
import PropTypes from "prop-types";
import jwt_decode from 'jwt-decode';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import loginPageStyle from "assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx";
import {Field, Formik} from "formik";
import {inject, observer} from "mobx-react";

// import LockOutline from "@material-ui/icons/LockOutline";

@inject("notifications", "auth", "routingStore", "users")
@observer
class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        // we use this to make the card to appear after the page has been rendered
        this.state = {
            cardAnimaton: "cardHidden"
        };
    }

    componentDidMount() {
        // we add a hidden class to the card and after 700 ms we delete it and the transition appears
        this.timeOutFunction = setTimeout(
            function () {
                this.setState({cardAnimaton: ""});
            }.bind(this),
            700
        );
    }

    componentWillUnmount() {
        clearTimeout(this.timeOutFunction);
        this.timeOutFunction = null;
    }

    render() {
        const {classes, notifications, auth, routingStore, users} = this.props;
        return (
            <div className={classes.container}>
                <GridContainer justify="center">
                    <GridItem xs={12} sm={6} md={4}>
                        <Formik
                            enableReinitialize
                            initialValues={{}}
                            onSubmit={async (values, actions) => {
                                try {
                                    console.log("Notification sent", values);
                                    let token = await auth.login(values);
                                    const decoded = jwt_decode(auth.token);
                                    console.log(decoded);
                                    await users.fetchMe(decoded.sub);
                                    console.log(users.me);
                                    routingStore.push("/admin/dashboard");
                                } catch (err) {
                                    console.error(err);
                                    notifications.enqueueSnackbar({
                                        message: 'Login failed.',
                                        options: {variant: 'error'}
                                    });
                                } finally {
                                    actions.setSubmitting(false)
                                }
                            }}
                            render={({
                                         handleSubmit,
                                         isSubmitting,
                                     }) => (
                                <form onSubmit={handleSubmit}>
                                    <Card login className={classes[this.state.cardAnimaton]}>
                                        <CardHeader
                                            className={`${classes.cardHeader} ${classes.textCenter}`}
                                            color="info"
                                        >
                                            <h2 className={classes.cardTitle}>Log in</h2>
                                        </CardHeader>
                                        <CardBody>
                                            <Field name="username" render={({field}) => (
                                                <CustomInput
                                                    labelText="Username..."
                                                    formControlProps={{
                                                        fullWidth: true
                                                    }}
                                                    inputProps={{
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <Email className={classes.inputAdornmentIcon}/>
                                                            </InputAdornment>
                                                        ),
                                                        ...field,
                                                        value: field.value || ''
                                                    }}
                                                />
                                            )}
                                            />
                                            <Field name="password" render={({field}) => (
                                                <CustomInput
                                                    labelText="Password"
                                                    formControlProps={{
                                                        fullWidth: true
                                                    }}
                                                    inputProps={{
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <Icon className={classes.inputAdornmentIcon}>
                                                                    lock_outline
                                                                </Icon>
                                                            </InputAdornment>
                                                        ),
                                                        type: "password",
                                                        ...field,
                                                        value: field.value || ''
                                                    }}
                                                />
                                            )}
                                            />
                                        </CardBody>
                                        <CardFooter className={classes.justifyContentCenter}>
                                            <Button color="info" simple size="lg" block type="submit"
                                                    disabled={isSubmitting}>
                                                Let's Go
                                            </Button>
                                        </CardFooter>
                                    </Card>
                                </form>
                            )}/>
                    </GridItem>
                </GridContainer>
            </div>
        );
    }
}

LoginPage.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(loginPageStyle)(LoginPage);
