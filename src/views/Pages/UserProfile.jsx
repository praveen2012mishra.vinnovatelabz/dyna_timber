import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import PermIdentity from "@material-ui/icons/PermIdentity";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Clearfix from "components/Clearfix/Clearfix.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import userProfileStyles from "assets/jss/material-dashboard-pro-react/views/userProfileStyles.jsx";
import avatar from "assets/img/faces/empty-profile.png";
import {Field, Formik} from "formik";
import UsersClient from "api/users";
import {inject, observer} from "mobx-react";
import CircularProgress from "@material-ui/core/CircularProgress";
import CustomImage from "components/CustomImage/Image";

@inject("notifications", "users", "auth")
@observer
class UserProfile extends React.Component {

    constructor(props) {
        super(props)

        this.props.users.fetchMe()
    }

    render() {
        const {classes, notifications, users} = this.props;

        return (
            users.me === null ? <CircularProgress/> :
                <div>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={8}>
                            <Card>
                                <CardHeader color="rose" icon>
                                    <CardIcon color="rose">
                                        <PermIdentity/>
                                    </CardIcon>
                                    <h4 className={classes.cardIconTitle}>
                                        Edit Profile
                                    </h4>
                                </CardHeader>
                                <CardBody>
                                    <Formik
                                        enableReinitialize
                                        initialValues={this.props.initialData ? this.props.initialData : {...users.me}}
                                        onSubmit={async (values, actions) => {
                                            try {
                                                await UsersClient.update(values);
                                                notifications.enqueueSnackbar({message: 'Saved.'});
                                            } catch (err) {
                                                console.error(err);
                                                notifications.enqueueSnackbar({
                                                    message: 'Saving failed.',
                                                    options: {variant: 'error'}
                                                });
                                            } finally {
                                                actions.setSubmitting(false)
                                            }
                                        }}
                                        render={({
                                                     handleSubmit,
                                                     setFieldValue,
                                                     isSubmitting,
                                                 }) => (
                                            <form onSubmit={handleSubmit}>
                                                <GridContainer>
                                                    <GridItem xs={12} sm={12} md={12}>
                                                        <Field name="fullName" render={({field}) => (
                                                            <CustomInput
                                                                labelText="Full Name"
                                                                formControlProps={{
                                                                    fullWidth: true
                                                                }}
                                                                inputProps={{
                                                                    ...field,
                                                                    value: field.value || ''
                                                                }}
                                                            />
                                                        )}/>
                                                    </GridItem>
                                                </GridContainer>
                                                <GridContainer>
                                                    <GridItem xs={12} sm={12} md={6}>
                                                        <Field name="username" render={({field}) => (
                                                            <CustomInput
                                                                labelText="Username"
                                                                formControlProps={{
                                                                    fullWidth: true
                                                                }}
                                                                inputProps={{
                                                                    disabled: true,
                                                                    ...field,
                                                                    value: field.value || ''
                                                                }}
                                                            />
                                                        )}/>
                                                    </GridItem>
                                                    <GridItem xs={12} sm={12} md={6}>
                                                        <Field name="password" render={({field}) => (
                                                            <CustomInput
                                                                labelText="Password"
                                                                formControlProps={{
                                                                    fullWidth: true
                                                                }}
                                                                inputProps={{
                                                                    type: "password",
                                                                    ...field,
                                                                    value: field.value || ''
                                                                }}
                                                            />
                                                        )}/>
                                                    </GridItem>
                                                </GridContainer>
                                                <Button color="rose" type="submit"
                                                        className={classes.updateProfileButton}
                                                        disabled={isSubmitting}>
                                                    Update Profile
                                                </Button>
                                                <Clearfix/>
                                            </form>
                                        )}/>
                                </CardBody>
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={12} md={4}>
                            <Card profile>
                                <CardAvatar profile>
                                    <a href="#avatar" onClick={e => e.preventDefault()}>
                                        {/* <img src={avatar} alt="..."/> */}
                                        <CustomImage src={avatar} alt={'...'} />
                                    </a>
                                </CardAvatar>
                                <CardBody profile>
                                    <h6 className={classes.cardCategory}>Name</h6>
                                    <h4 className={classes.cardTitle}>{users.me.fullName}</h4>
                                    {/*<p className={classes.description}>*/}
                                    {/*    Don't be scared of the truth because we need to restart the*/}
                                    {/*    human foundation in truth And I love you like Kanye loves Kanye*/}
                                    {/*    I love Rick Owens’ bed design but the back is...*/}
                                    {/*</p>*/}
                                    {/*<Button color="rose" round>*/}
                                    {/*    Follow*/}
                                    {/*</Button>*/}
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
        )
    }

}

export default withStyles(userProfileStyles)(UserProfile);
