import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";
import Check from "@material-ui/icons/Check";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";

import registerPageStyle from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import CardHeader from "components/Card/CardHeader";

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: [],
            cardAnimaton: "cardHidden"
        };
        this.handleToggle = this.handleToggle.bind(this);
    }

    componentDidMount() {
        // we add a hidden class to the card and after 700 ms we delete it and the transition appears
        this.timeOutFunction = setTimeout(
            function () {
                this.setState({cardAnimaton: ""});
            }.bind(this),
            700
        );
    }

    componentWillUnmount() {
        clearTimeout(this.timeOutFunction);
        this.timeOutFunction = null;
    }

    handleToggle(value) {
        const {checked} = this.state;
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        this.setState({
            checked: newChecked
        });
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.container}>
                <GridContainer justify="center">
                    <GridItem xs={12} sm={6} md={4}>
                        <Card className={classes.cardSignup}>
                            <CardHeader className={`${classes.cardHeader} ${classes.textCenter}`}
                                        color="info">
                                <h2 className={classes.cardTitle}>Register</h2>
                            </CardHeader>
                            <CardBody>
                                <form className={classes.form}>
                                    <CustomInput
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            startAdornment: (
                                                <InputAdornment
                                                    position="start"
                                                    className={classes.inputAdornment}
                                                >
                                                    <Face className={classes.inputAdornmentIcon}/>
                                                </InputAdornment>
                                            ),
                                            placeholder: "Full Name..."
                                        }}
                                    />
                                    <CustomInput
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            startAdornment: (
                                                <InputAdornment
                                                    position="start"
                                                    className={classes.inputAdornment}
                                                >
                                                    <Email className={classes.inputAdornmentIcon}/>
                                                </InputAdornment>
                                            ),
                                            placeholder: "Email..."
                                        }}
                                    />
                                    <CustomInput
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            startAdornment: (
                                                <InputAdornment
                                                    position="start"
                                                    className={classes.inputAdornment}
                                                >
                                                    <Icon className={classes.inputAdornmentIcon}>
                                                        lock_outline
                                                    </Icon>
                                                </InputAdornment>
                                            ),
                                            placeholder: "Password..."
                                        }}
                                    />
                                    <FormControlLabel
                                        classes={{
                                            root: classes.checkboxLabelControl,
                                            label: classes.checkboxLabel,
                                        }}
                                        control={
                                            <Checkbox
                                                tabIndex={-1}
                                                onClick={() => this.handleToggle(1)}
                                                checkedIcon={
                                                    <Check className={classes.checkedIcon}/>
                                                }
                                                icon={<Check className={classes.uncheckedIcon}/>}
                                                classes={{
                                                    checked: classes.checked,
                                                    root: classes.checkRoot
                                                }}
                                            />
                                        }
                                        label={
                                            <span>
                            I agree to the{" "}
                                                <a href="#pablo">terms and conditions</a>.
                          </span>
                                        }
                                    />
                                    <div className={classes.center}>
                                        <Button color="info" size="lg" simple>
                                            Get started
                                        </Button>
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>
            </div>
        );
    }
}

RegisterPage.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(registerPageStyle)(RegisterPage);
