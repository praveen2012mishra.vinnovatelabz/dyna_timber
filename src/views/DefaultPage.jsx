import React from 'react';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import CustomTabs from 'components/CustomTabs/CustomTabs';
import { Edit, PlusOne, TableChart } from '@material-ui/icons';
import Refresh from '@material-ui/icons/Refresh';
import Delete from '@material-ui/icons/Delete';
import { observable } from 'mobx';

class DefaultPage extends React.Component {
  aditionalActions = [];
  @observable initialDataNew = null;

  noNewTab = false;

  componentDidMount() {
    this.defaultStore.fetchAll();
  }

  getTabs = (components, selectedItem) => {
    let TableTabComponent = components.table;
    let EditTabComponent = components.edit;

    let tabs = [];
    if (TableTabComponent != null) {
      tabs.push({
        tabName: 'Items',
        tabButton: 'Items',
        tabIcon: TableChart,
        tabContent: <TableTabComponent store={this.defaultStore} />
      });
    }

    if (EditTabComponent != null && !this.noNewTab) {
      tabs.push({
        tabName: 'New Item',
        tabButton: 'New Item',
        tabIcon: PlusOne,
        tabContent: (
          <EditTabComponent
            initialData={this.initialDataNew}
            store={this.defaultStore}
          />
        )
      });
    }

    if (EditTabComponent && selectedItem !== null) {
      tabs.push({
        tabName: 'Edit Item',
        tabButton: 'Edit Item',
        tabIcon: Edit,
        tabContent: (
          <EditTabComponent
            initialData={selectedItem}
            store={this.defaultStore}
          />
        )
      });
    }
    if (components.additionalTabs) {
      tabs.concant(components.additionalTabs);
    }

    return tabs;
  };

  render() {
    if (!this.defaultStore) return null;
    const {
      selectedTab,
      selectedItem,
      selectedItems,
      selectTab,
      deleteEnabled
    } = this.defaultStore;
    const tabs = this.getTabs(this.components, selectedItem);
    // if(selectedTab > tabs.length - 1) {
    //     selectTab(0)
    // }
    let customActions = [
      <Refresh onClick={() => this.defaultStore.fetchAll()} />
    ];
    if (
      deleteEnabled &&
      (selectedItem || (selectedItems && selectedItems.length > 0))
    ) {
      customActions.push(
        <Delete
          onClick={async () => {
            await this.defaultStore.deleteSelected();
            await this.defaultStore.fetchAll();
          }}
        />
      );
    }
    customActions.push(...this.aditionalActions);

    return (
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={12}>
          {/*<Card>*/}
          {/*    <CardBody>*/}

          <CustomTabs
            title=""
            headerColor="info"
            color="info"
            value={selectedTab}
            handleChange={selectTab}
            tabs={tabs}
            customActions={customActions}
          />
          {/*    </CardBody>*/}
          {/*</Card>*/}
        </GridItem>
      </GridContainer>
    );
  }
}

export default DefaultPage;
