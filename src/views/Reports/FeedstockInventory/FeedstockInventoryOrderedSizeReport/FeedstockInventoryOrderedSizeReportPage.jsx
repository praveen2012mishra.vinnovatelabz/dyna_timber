import React from "react";
import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import FeedstockInventoryOrderedSizeTable
    from "views/Reports/FeedstockInventory/FeedstockInventoryOrderedSizeReport/FeedstockInventoryOrderedSizeTable";

@inject("feedstockInventoryOrderedSize")
@observer
export default class FeedstockInventoryOrderedSizeReportPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.feedstockInventoryOrderedSize;
        this.components = {
            table: FeedstockInventoryOrderedSizeTable,
            edit: null
        };
    }
}

