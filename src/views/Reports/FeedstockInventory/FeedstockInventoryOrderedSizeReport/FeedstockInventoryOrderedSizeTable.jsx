// import DefaultAgTable from "components/Table/DefaultAgTable";
// import MaterialsClient from "../../../../api/materials";
// import * as _ from "lodash";
// import { get } from "lodash";

// //set decimal precision
// const setPrecision = (val, precision) => val.toFixed(precision);

// //calculate targetCalculation
// const targetCalculation = (params) => {
//   let dryTime = cellRendererAddDryTime(params);
//   let replenishmentTime = cellRendererReplenishTime(params);
//   let reliability = cellRendererReliability(params);
//   let usage = params.data.usage;
//   let target = ((dryTime + replenishmentTime) * usage * 100) / reliability;
 
//   if (params.data.target !== Math.floor(target) && params.data.target !== 0) {
//     return Math.floor(target);
//   }
//   if (params.data.target === Math.floor(target) || params.data.target === 0) {
//     return params.data.target;
//   }
// };

// //set color w.r.t to value
// function numberToColor(val) {
//   if (parseFloat(val) > 150) {
//     return "black";
//   } else if (parseFloat(val) > 100 && parseFloat(val) <= 150) {
//     return "green";
//   } else if (parseFloat(val) > 50 && parseFloat(val) <= 100) {
//     return "yellow";
//   } else if (parseFloat(val) >= 0 && parseFloat(val) <= 50) {
//     return "red";
//   } else if (parseFloat(val) < 0) {
//     return "#fff";
//   }
// }

// //hide column
// function isColumnHidden(field) {
//   let store = JSON.parse(sessionStorage.getItem("hiddenColumns")) || {
//     FeedstockInventoryOrderedSizeTable: {},
//   };
//   if (store.FeedstockInventoryOrderedSizeTable) {
//     return store.FeedstockInventoryOrderedSizeTable[field] || false;
//   }
//   return false;
// }


// //give custom width to column
// const isColumnWidth = (field, componentName) => {
// 	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
// 	  [componentName]: {},
// 	};
// 	if (store[componentName]) {
// 	  let width = store[componentName][field];
// 	  width = parseInt(width, 10);
// 	  width = width+45;
// 	  //console.log(width,field,'============>');
// 	  return width;
// 	}
//   };
  
//   //take text width
//   function textWidth(text, fontProp, type) {
// 	var tag = document.createElement("div");
// 	tag.style.position = "absolute";
// 	tag.style.left = "-999em";
// 	tag.style.whiteSpace = "nowrap";
// 	tag.style.font = fontProp;
// 	text=text?text.toString():text;
// 	tag.innerHTML = text;
  
// 	document.body.appendChild(tag);
  
// 	var result = tag.clientWidth;
// 	//console.log(result,'====>');
// 	result = result;
// 	//console.log(result);
  
// 	document.body.removeChild(tag);
  
// 	return result;
//   }
  
//   const getWidthColumnValue = (params, componentName, field,headerName) => {
// 	let data = params.data;
// 	//console.log(data,componentName,field);
// 	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
// 	  [componentName]: {},
// 	};
  
// 	store[componentName] = {
// 	  ...store[componentName],
// 	};
// 	let storeWidth;
// 	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
// 	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
//   //console.log(fieldWidth,valueWidth,field);
// 	let arrayWidth = Object.assign([]);
// 	arrayWidth.push(fieldWidth);
// 	arrayWidth.push(valueWidth);
// 	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
// 	if (Object.keys(store[componentName]).length === 0) {
// 	  let finalWidth = Math.max(...arrayWidth);
// 	  store[componentName] = {
// 		...store[componentName],
// 		[field]: finalWidth,
// 	  };
// 	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
// 	} else if (!store[componentName][field]) {
// 	  let finalWidth = Math.max(...arrayWidth);
// 	  store[componentName] = {
// 		...store[componentName],
// 		[field]: finalWidth,
// 	  };
// 	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
// 	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
// 	  arrayWidth.push(store[componentName][field]);
// 	  let finalWidth = Math.max(...arrayWidth);
// 	  store[componentName] = {
// 		...store[componentName],
// 		[field]: finalWidth,
// 	  };
// 	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
// 	} else {
// 	  //console.log("No change in width resize");
// 	}
//   };

// const onCellValueChangedAddDryTime = (params, gridApi) => {
//   let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
//   store = {
//     ...store,
//     [get(params, "data.material.id")]: params.newValue,
//   };
//   sessionStorage.setItem("dryTime", JSON.stringify(store));
//   params.data.material.dryTime = params.newValue;
//   updateReliability(Object.assign({}, params.data.material));
//   gridApi.refreshCells({ force: true });
// };

// const updateAddDryTime = (dryTime) => {
//   //console.log("------------------------------->", dryTime);
//   MaterialsClient.update(dryTime)
//     .then((res) => {})
//     .catch((err) => console.log("error while updating inventory \n", err));
// };

// const valueGetterAddDryTime = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
//   //console.log(parseInt(store[params.data.material.id]));

//   if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
//     return parseInt(store[params.data.material.id]);
//   }
//   return params.data.material.dryTime;
// };

// const valueSetterAddDryTime = (params) => {
//   if (parseInt(params.newValue) === parseInt(params.data.material.dryTime))
//     return false;
//   let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
//   store = {
//     ...store,
//     [get(params, "data.material.id")]: params.newValue,
//   };
//   sessionStorage.setItem("dryTime", JSON.stringify(store));
//   params.data.material.dryTime = params.newValue;
//   return true;
// };

// const cellRendererAddDryTime = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
//   if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
//     return parseInt(store[[get(params, "data.material.id")]]);
//   }
//   return params.data.material.dryTime;
// };

// const onCellValueChangedReplenishTime = (params, gridApi) => {
//   let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
//   store = {
//     ...store,
//     [get(params, "data.material.id")]: params.newValue,
//   };
//   sessionStorage.setItem("replenishmentTime", JSON.stringify(store));
//   params.data.material.replenishmentTime = params.newValue;
//   updateReplenishTime(Object.assign({}, params.data.material));
//   gridApi.refreshCells({ force: true });
// };

// const updateReplenishTime = (replenishmentTime) => {
//   //console.log("------------------------------->", replenishTime);
//   MaterialsClient.update(replenishmentTime)
//     .then((res) => {})
//     .catch((err) => console.log("error while updating inventory \n", err));
// };

// const valueGetterReplenishTime = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
//   //console.log(parseInt(store[params.data.material.id]));

//   if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
//     return parseInt(store[params.data.material.id]);
//   }
//   return params.data.material.replenishmentTime;
// };

// const valueSetterReplenishTime = (params) => {
//   if (
//     parseInt(params.newValue) ===
//     parseInt(params.data.material.replenishmentTime)
//   )
//     return false;
//   let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
//   store = {
//     ...store,
//     [get(params, "data.material.id")]: params.newValue,
//   };
//   sessionStorage.setItem("replenishmentTime", JSON.stringify(store));
//   params.data.material.replenishmentTime = params.newValue;
//   return true;
// };

// const cellRendererReplenishTime = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
//   if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
//     return parseInt(store[[get(params, "data.material.id")]]);
//   }
//   return params.data.material.replenishmentTime;
// };

// const onCellValueChangedReliability = (params, gridApi) => {
//   let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
//   store = {
//     ...store,
//     [get(params, "data.material.id")]: params.newValue,
//   };
//   sessionStorage.setItem("reliability", JSON.stringify(store));
//   params.data.material.reliability = params.newValue;
//   updateReliability(Object.assign({}, params.data.material));
//   gridApi.refreshCells({ force: true });
// };

// const updateReliability = (reliability) => {
//   //console.log("------------------------------->", reliability);
//   MaterialsClient.update(reliability)
//     .then((res) => {})
//     .catch((err) => console.log("error while updating inventory \n", err));
// };

// const valueGetterReliability = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
//   //console.log(parseInt(store[params.data.material.id]));

//   if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
//     return parseInt(store[params.data.material.id]);
//   }
//   return params.data.material.reliability;
// };

// const valueSetterReliability = (params) => {
//   if (parseInt(params.newValue) === parseInt(params.data.material.reliability))
//     return false;
//   let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
//   store = {
//     ...store,
//     [get(params, "data.material.id")]: params.newValue,
//   };
//   sessionStorage.setItem("reliability", JSON.stringify(store));
//   params.data.material.reliability = params.newValue;
//   return true;
// };

// const cellRendererReliability = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
//   if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
//     return parseInt(store[[get(params, "data.material.id")]]);
//   }
//   return params.data.material.reliability;
// };

// export default class FeedstockInventoryOrderedSizeTable extends DefaultAgTable {
//   columns = [
//     {
//       headerName: "SKU of Ordersize",
//       valueGetter: (params) => {
//         getWidthColumnValue(params, this.componentName, "material.orderedSize1","SKU of Ordersize");
//         return params.data.material.orderedSize1;
//       },
//       hide: isColumnHidden("material.orderedSize1"),
//       suppressSizeToFit: true,
//       width: isColumnWidth(
//         "material.orderedSize1",
//         "FeedstockInventoryOrderedSizeTable"
//       ),
//       field: "material.orderedSize1",
//     },
//     {
//       headerName: "Category",
//       valueGetter: (params) => {
//         getWidthColumnValue(params, this.componentName, "material.category",'Category');
//         return params.data.material.category;
//       },
//       hide: isColumnHidden("category"),
//       suppressSizeToFit: true,
//       width: isColumnWidth("category", "FeedstockInventoryOrderedSizeTable"),
//       field: "category",
//     },

//     {
//       headerName: "Replenishment Time",
//       suppressSizeToFit: true,
//       width: isColumnWidth("replenishmentTime", "FeedstockInventoryOrderedSizeTable"),
//       hide: isColumnHidden("replenishmentTime"),
//       editable: true,
//       // type: "numericColumn",
//       cellEditor: "numericEditor",
//       onCellValueChanged: (params) => {
//         return onCellValueChangedReplenishTime(params, this.gridApi);
//       },
//       valueGetter: (params) => {
//         getWidthColumnValue(params, this.componentName, "replenishmentTime","Replenishment Time");
//         return valueGetterReplenishTime(params);
//       },
//       valueSetter: (params) => {
//         return valueSetterReplenishTime(params);
//       },
//       cellRenderer: (params) => {
//         return cellRendererReplenishTime(params);
//       },
//     },
//     {
//       headerName: "Dry time",
//       field: "dryTime",
//       width: isColumnWidth("dryTime", "FeedstockInventoryOrderedSizeTable"),
//       suppressSizeToFit: true,
//       hide: isColumnHidden("dryTime"),
//       editable: true,
//       // type: "numericColumn",
//       cellEditor: "numericEditor",
//       onCellValueChanged: (params) => {
//         return onCellValueChangedAddDryTime(params, this.gridApi);
//       },
//       valueGetter: (params) => {
//         getWidthColumnValue(params, this.componentName, "dryTime","Dry time");
//         return valueGetterAddDryTime(params);
//       },
//       valueSetter: (params) => {
//         return valueSetterAddDryTime(params);
//       },
//       cellRenderer: (params) => {
//         return cellRendererAddDryTime(params);
//       },
//     },

//     {
//       headerName: "Reliability",
//       field: "reliability",
//       width: isColumnWidth("reliability", "FeedstockInventoryOrderedSizeTable"),
//       suppressSizeToFit: true,
//       hide: isColumnHidden("reliability"),
//       editable: true,
//       // type: "numericColumn",
//       cellEditor: "numericEditor",
//       onCellValueChanged: (params) => {
//         return onCellValueChangedReliability(params, this.gridApi);
//       },
//       valueGetter: (params) => {
//         getWidthColumnValue(params, this.componentName, "reliability","Reliability");
//         return valueGetterReliability(params);
//       },
//       valueSetter: (params) => {
//         return valueSetterReliability(params);
//       },
//       cellRenderer: (params) => {
//         return cellRendererReliability(params);
//       },
//     },
//     {
//       headerName: "Usage",
//       field: "usage",
//       suppressSizeToFit: true,
//       width: isColumnWidth("usage", "FeedstockInventoryOrderedSizeTable"),
//       hide: isColumnHidden("usage"),
//       cellRenderer: (params) =>{
//         getWidthColumnValue(params, this.componentName, "usage","Usage");
//         return params.data.usage},
//     },
//     {
//       headerName: "Total Inventory",
//       children: [
//         {
//           headerName: "Target",
//           field: "target",
//           suppressSizeToFit: true,          
//           valueGetter: (params) => {
//             getWidthColumnValue(params, this.componentName, "target","Target");
//             return targetCalculation(params)},
//           width: isColumnWidth("target", "FeedstockInventoryOrderedSizeTable"),
//           hide: isColumnHidden("target"),
//         },
//         {
//           headerName: "Act.",
//           field: "act",
//           hide: isColumnHidden("act"),
//           suppressSizeToFit: true,
//           cellRenderer: (params) =>{
//             getWidthColumnValue(params, this.componentName, "act","Act.");
//             return params.data.act},
//           width: isColumnWidth("act", "FeedstockInventoryOrderedSizeTable"),
//         },
//         {
//           headerName: "Colour",
//           field: "displayColor",
//           suppressSizeToFit: true,
//           width: 87,
//           // isColumnWidth(
//           //   "displayColor ",
//           //   "FeedstockInventoryOrderedSizeTable"
//           // ),
//           hide: isColumnHidden("displayColor "),
//           cellStyle: function (params) {
//             let color = numberToColor(params.data.displayColor);
//             return {
//               "background-color": color,
//             };
//           },
//           valueGetter: function (params) {
//             return "";
//           },
//         },
//       ],
//       marryChildren: true,
//       hide: isColumnHidden("Total Inventory"),
//     },
//   ];

//   constructor(props) {
//     super(props);
//     //this.defaultStore = this.props.drumLoadingReport;
//     this.componentName = "FeedstockInventoryOrderedSizeTable";
//     this.printHeader = "Feedstock Inventory Ordered Size Table";
//   }

//   onGridReady = (params) => {
//     this.gridApi = params.api;
//     this.gridApi.sizeColumnsToFit();
//   };
// }


import DefaultAgTable from "components/Table/DefaultAgTable";
import MaterialsClient from "../../../../api/materials";
import * as _ from "lodash";
import { get } from "lodash";
import { inject, observer } from "mobx-react";
import {FeedstockInventoryOrderedSizeClient} from "api/reports/feedstockInventoryOrderedSize";
//set decimal precision
const setPrecision = (val, precision) => val.toFixed(precision);

//calculate targetCalculation
const targetCalculation = (params) => {
  let dryTime = cellRendererAddDryTime(params);
  let replenishmentTime = cellRendererReplenishTime(params);
  let reliability = cellRendererReliability(params);
  let usage = params.data.usage;
  let target = ((dryTime + replenishmentTime) * usage * 100) / reliability;

  if (params.data.target !== Math.floor(target) && params.data.target !== 0) {
    return Math.floor(target);
  }
  if (params.data.target === Math.floor(target) || params.data.target === 0) {
    return params.data.target;
  }
};

//set color w.r.t to value
function numberToColor(val) {
  if (parseFloat(val) > 150) {
    return "black";
  } else if (parseFloat(val) > 100 && parseFloat(val) <= 150) {
    return "green";
  } else if (parseFloat(val) > 50 && parseFloat(val) <= 100) {
    return "yellow";
  } else if (parseFloat(val) >= 0 && parseFloat(val) <= 50) {
    return "red";
  } else if (parseFloat(val) < 0) {
    return "#fff";
  }
}

//hide column
function isColumnHidden(field) {
  let store = JSON.parse(sessionStorage.getItem("hiddenColumns")) || {
    FeedstockInventoryOrderedSizeTable: {},
  };
  if (store.FeedstockInventoryOrderedSizeTable) {
    return store.FeedstockInventoryOrderedSizeTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    let width = store[componentName][field];
    width = parseInt(width, 10);
    width = width + 45;
    //console.log(width,field,'============>');
    return width;
  }
};

//take text width
function textWidth(text, fontProp, type) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  text = text ? text.toString() : text;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;
  //console.log(result,'====>');
  result = result;
  //console.log(result);

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (params, componentName, field, headerName) => {
  let data = params.data;
  //console.log(data,componentName,field);
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };

  store[componentName] = {
    ...store[componentName],
  };
  let storeWidth;
  let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
  let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
  let arrayWidth = Object.assign([]);
  arrayWidth.push(fieldWidth);
  arrayWidth.push(valueWidth);
  //console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);

  if (Object.keys(store[componentName]).length === 0) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (!store[componentName][field]) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (store[componentName][field] != Math.max(...arrayWidth)) {
    arrayWidth.push(store[componentName][field]);
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else {
    //console.log("No change in width resize");
  }
};

const onCellValueChangedAddDryTime = (params, gridApi) => {
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  store = {
    ...store,
    [get(params, "data.material.id")]: params.newValue,
  };
  sessionStorage.setItem("dryTime", JSON.stringify(store));
  params.data.material.dryTime = params.newValue;
  updateReliability(Object.assign({}, params.data.material));
  gridApi.refreshCells({ force: true });
};

const updateAddDryTime = (dryTime) => {
  //console.log("------------------------------->", dryTime);
  MaterialsClient.update(dryTime)
    .then((res) => {})
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterAddDryTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  //console.log(parseInt(store[params.data.material.id]));

  if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
    return parseInt(store[params.data.material.id]);
  }
  return params.data.material.dryTime;
};

const valueSetterAddDryTime = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.material.dryTime))
    return false;
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  store = {
    ...store,
    [get(params, "data.material.id")]: params.newValue,
  };
  sessionStorage.setItem("dryTime", JSON.stringify(store));
  params.data.material.dryTime = params.newValue;
  return true;
};

const cellRendererAddDryTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
    return parseInt(store[[get(params, "data.material.id")]]);
  }
  return params.data.material.dryTime;
};

const onCellValueChangedReplenishTime = (params, gridApi) => {
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  store = {
    ...store,
    [get(params, "data.material.id")]: params.newValue,
  };
  sessionStorage.setItem("replenishmentTime", JSON.stringify(store));
  params.data.material.replenishmentTime = params.newValue;
  updateReplenishTime(Object.assign({}, params.data.material));
  gridApi.refreshCells({ force: true });
};

const updateReplenishTime = (replenishmentTime) => {
  //console.log("------------------------------->", replenishTime);
  MaterialsClient.update(replenishmentTime)
    .then((res) => {})
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterReplenishTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  //console.log(parseInt(store[params.data.material.id]));

  if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
    return parseInt(store[params.data.material.id]);
  }
  return params.data.material.replenishmentTime;
};

const valueSetterReplenishTime = (params) => {
  if (
    parseInt(params.newValue) ===
    parseInt(params.data.material.replenishmentTime)
  )
    return false;
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  store = {
    ...store,
    [get(params, "data.material.id")]: params.newValue,
  };
  sessionStorage.setItem("replenishmentTime", JSON.stringify(store));
  params.data.material.replenishmentTime = params.newValue;
  return true;
};

const cellRendererReplenishTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
    return parseInt(store[[get(params, "data.material.id")]]);
  }
  return params.data.material.replenishmentTime;
};

const onCellValueChangedReliability = (params, gridApi) => {
  let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
  store = {
    ...store,
    [get(params, "data.material.id")]: params.newValue,
  };
  sessionStorage.setItem("reliability", JSON.stringify(store));
  params.data.material.reliability = params.newValue;
  updateReliability(Object.assign({}, params.data.material));
  gridApi.refreshCells({ force: true });
};

const updateReliability = (reliability) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(reliability)
    .then((res) => {})
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterReliability = (params) => {
  let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
  //console.log(parseInt(store[params.data.material.id]));

  if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
    return parseInt(store[params.data.material.id]);
  }
  return params.data.material.reliability;
};

const valueSetterReliability = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.material.reliability))
    return false;
  let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
  store = {
    ...store,
    [get(params, "data.material.id")]: params.newValue,
  };
  sessionStorage.setItem("reliability", JSON.stringify(store));
  params.data.material.reliability = params.newValue;
  return true;
};

const cellRendererReliability = (params) => {
  let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.material.id")]]))) {
    return parseInt(store[[get(params, "data.material.id")]]);
  }
  return params.data.material.reliability;
};

const onCellValueChangedorderFrequency = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("orderFrequency")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("orderFrequency", JSON.stringify(store));
  //params.data.fio=Object.assign({})
  params.data.fio.orderFrequency = params.newValue;
  params.data.fio.orderedSize1=params.data.material.orderedSize1
  updateorderFrequency(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateorderFrequency = (orderFrequency, props) => {
  //console.log("------------------------------->", reliability);
  FeedstockInventoryOrderedSizeClient.save(orderFrequency)
    .then((res) => {
      //console.log(orderFrequency,"------------------------------->", res.data);
      //debugger
      props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterorderFrequency = (params) => {
  let store = JSON.parse(sessionStorage.getItem("orderFrequency")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  //console.log(params.data);
  //debugger
  return params.data.fio?params.data.fio.orderFrequency:0;
};

const valueSetterorderFrequency = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.fio?params.data.fio.orderFrequency:0))
    return false;
  let store = JSON.parse(sessionStorage.getItem("orderFrequency")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("orderFrequency", JSON.stringify(store));
  params.data.fio.orderFrequency = params.newValue;
  return true;
};

const cellRendererorderFrequency = (params) => {
  let store = JSON.parse(sessionStorage.getItem("orderFrequency")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.fio?params.data.fio.orderFrequency:0;
};

const onCellValueChangedtotalOnOrder = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("totalOnOrder")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("totalOnOrder", JSON.stringify(store));
  params.data.fio.totalOnOrder = params.newValue;
  params.data.fio.orderedSize1=params.data.material.orderedSize1;
  updatetotalOnOrder(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatetotalOnOrder = (totalOnOrder, props) => {
  //console.log("------------------------------->", reliability);
  FeedstockInventoryOrderedSizeClient.save(totalOnOrder)
    .then((res) => {
      props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGettertotalOnOrder = (params) => {
  let store = JSON.parse(sessionStorage.getItem("totalOnOrder")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.fio?params.data.fio.totalOnOrder:0;
};

const valueSettertotalOnOrder = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.fio.totalOnOrder))
    return false;
  let store = JSON.parse(sessionStorage.getItem("totalOnOrder")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("totalOnOrder", JSON.stringify(store));
  params.data.fio.totalOnOrder = params.newValue;
  return true;
};

const cellRenderertotalOnOrder = (params) => {
  let store = JSON.parse(sessionStorage.getItem("totalOnOrder")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.fio?params.data.fio.totalOnOrder:0;
};

const onCellValueChangedhyne = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("hyne")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("hyne", JSON.stringify(store));
  params.data.fio.hyne = params.newValue;
  params.data.fio.orderedSize1=params.data.material.orderedSize1;
  updatehyne(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatehyne = (hyne, props) => {
  console.log("------------------------------->", hyne);
  FeedstockInventoryOrderedSizeClient.save(hyne)
    .then((res) => {
      props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterhyne = (params) => {
  let store = JSON.parse(sessionStorage.getItem("hyne")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.fio?params.data.fio.hyne:0;
};

const valueSetterhyne = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.fio.hyne)) return false;
  let store = JSON.parse(sessionStorage.getItem("hyne")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("hyne", JSON.stringify(store));
  params.data.fio.hyne = params.newValue;
  return true;
};

const cellRendererhyne = (params) => {
  let store = JSON.parse(sessionStorage.getItem("hyne")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.fio?params.data.fio.hyne:0;
};

const onCellValueChangedimbil = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("imbil")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("imbil", JSON.stringify(store));
  params.data.fio.imbil = params.newValue;
  params.data.fio.orderedSize1=params.data.material.orderedSize1
  updateimbil(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateimbil = (imbil, props) => {
  //console.log("------------------------------->", reliability);
  FeedstockInventoryOrderedSizeClient.save(imbil)
    .then((res) => {
      props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterimbil = (params) => {
  let store = JSON.parse(sessionStorage.getItem("imbil")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.fio?params.data.fio.imbil:0;
};

const valueSetterimbil = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.fio.imbil)) return false;
  let store = JSON.parse(sessionStorage.getItem("imbil")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("imbil", JSON.stringify(store));
  params.data.fio.imbil = params.newValue;
  return true;
};

const cellRendererimbil = (params) => {
  let store = JSON.parse(sessionStorage.getItem("imbil")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.fio?params.data.fio.imbil:0;
};

const onCellValueChangedakd = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("akd")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("akd", JSON.stringify(store));
  params.data.fio.akd = params.newValue;
  params.data.fio.orderedSize1=params.data.material.orderedSize1
  updateakd(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateakd = (akd, props) => {
  //console.log("------------------------------->", reliability);
  FeedstockInventoryOrderedSizeClient.save(akd)
    .then((res) => {
      props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterakd = (params) => {
  let store = JSON.parse(sessionStorage.getItem("akd")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.fio?params.data.fio.akd:0;
};

const valueSetterakd = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.fio.akd)) return false;
  let store = JSON.parse(sessionStorage.getItem("akd")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("akd", JSON.stringify(store));
  params.data.fio.akd = params.newValue;
  return true;
};

const cellRendererakd = (params) => {
  let store = JSON.parse(sessionStorage.getItem("akd")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.fio?params.data.fio.akd:0;
};

const onCellValueChangedatp = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("atp")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("atp", JSON.stringify(store));
  params.data.fio.atp = params.newValue;
  params.data.fio.orderedSize1=params.data.material.orderedSize1;
  updateatp(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateatp = (atp, props) => {
  //console.log("------------------------------->", reliability);
  FeedstockInventoryOrderedSizeClient.save(atp)
    .then((res) => {
      props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetteratp = (params) => {
  let store = JSON.parse(sessionStorage.getItem("atp")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.fio?params.data.fio.atp:0;
};

const valueSetteratp = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.fio.atp)) return false;
  let store = JSON.parse(sessionStorage.getItem("atp")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("atp", JSON.stringify(store));
  params.data.fio.atp = params.newValue;
  return true;
};

const cellRendereratp = (params) => {
  let store = JSON.parse(sessionStorage.getItem("atp")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.fio?params.data.fio.atp:0;
};

const onCellValueChangedwade = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("wade")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("wade", JSON.stringify(store));
  params.data.fio.wade = params.newValue;
  params.data.fio.orderedSize1=params.data.material.orderedSize1;
  updatewade(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatewade = (wade, props) => {
  //console.log("------------------------------->", reliability);
  FeedstockInventoryOrderedSizeClient.save(wade)
    .then((res) => {
      props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterwade = (params) => {
  let store = JSON.parse(sessionStorage.getItem("wade")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.fio?params.data.fio.wade:0;
};

const valueSetterwade = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.fio.wade)) return false;
  let store = JSON.parse(sessionStorage.getItem("wade")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("wade", JSON.stringify(store));
  params.data.fio.wade = params.newValue;
  return true;
};

const cellRendererwade = (params) => {
  let store = JSON.parse(sessionStorage.getItem("wade")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.fio?params.data.fio.wade:0;
};

// const onCellValueChangedtotal = (params, gridApi, props) => {
//   let store = JSON.parse(sessionStorage.getItem("total")) || {};
//   store = {
//     ...store,
//     [get(params, "data.id")]: params.newValue,
//   };
//   sessionStorage.setItem("total", JSON.stringify(store));
//   params.data.total = params.newValue;
//   updatetotal(Object.assign({}, params.data), props);
//   gridApi.refreshCells({ force: true });
// };

// const updatetotal = (total, props) => {
//   //console.log("------------------------------->", reliability);
//   FioNewApiClient.update(total)
//     .then((res) => {
//       props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
//     })
//     .catch((err) => console.log("error while updating inventory \n", err));
// };

// const valueGettertotal = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("total")) || {};
//   //console.log(parseInt(store[params.data.id]));

//   if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
//     return parseInt(store[params.data.id]);
//   }
//   return params.data.total;
// };

// const valueSettertotal = (params) => {
//   if (parseInt(params.newValue) === parseInt(params.data.total)) return false;
//   let store = JSON.parse(sessionStorage.getItem("total")) || {};
//   store = {
//     ...store,
//     [get(params, "data.id")]: params.newValue,
//   };
//   sessionStorage.setItem("total", JSON.stringify(store));
//   params.data.total = params.newValue;
//   return true;
// };

// const cellRenderertotal = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("total")) || {};
//   if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
//     return parseInt(store[[get(params, "data.id")]]);
//   }
//   return params.data.total;
// };

// const onCellValueChangeddifference = (params, gridApi, props) => {
//   let store = JSON.parse(sessionStorage.getItem("difference")) || {};
//   store = {
//     ...store,
//     [get(params, "data.id")]: params.newValue,
//   };
//   sessionStorage.setItem("difference", JSON.stringify(store));
//   params.data.difference = params.newValue;
//   updatedifference(Object.assign({}, params.data), props);
//   gridApi.refreshCells({ force: true });
// };

// const updatedifference = (difference, props) => {
//   //console.log("------------------------------->", reliability);
//   FioNewApiClient.update(difference)
//     .then((res) => {
//       props.feedstockInventoryOrderedSize.setUpdateObject(res.data);
//     })
//     .catch((err) => console.log("error while updating inventory \n", err));
// };

// const valueGetterdifference = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("difference")) || {};
//   //console.log(parseInt(store[params.data.id]));

//   if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
//     return parseInt(store[params.data.id]);
//   }
//   return params.data.difference;
// };

// const valueSetterdifference = (params) => {
//   if (parseInt(params.newValue) === parseInt(params.data.difference))
//     return false;
//   let store = JSON.parse(sessionStorage.getItem("difference")) || {};
//   store = {
//     ...store,
//     [get(params, "data.id")]: params.newValue,
//   };
//   sessionStorage.setItem("difference", JSON.stringify(store));
//   params.data.difference = params.newValue;
//   return true;
// };

// const cellRendererdifference = (params) => {
//   let store = JSON.parse(sessionStorage.getItem("difference")) || {};
//   if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
//     return parseInt(store[[get(params, "data.id")]]);
//   }
//   return params.data.difference;
// };

@inject("feedstockInventoryOrderedSize")
@observer
export default class FeedstockInventoryOrderedSizeTable extends DefaultAgTable {
  columns = [
    {
      headerName: "SKU of Ordersize",
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "material.orderedSize1",
          "SKU of Ordersize"
        );
        return params.data.material.orderedSize1;
      },
      hide: isColumnHidden("material.orderedSize1"),
      suppressSizeToFit: true,
      width: isColumnWidth(
        "material.orderedSize1",
        "FeedstockInventoryOrderedSizeTable"
      ),
      field: "material.orderedSize1",
    },
    {
      headerName: "Category",
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "material.category",
          "Category"
        );
        return params.data.material.category;
      },
      hide: isColumnHidden("category"),
      suppressSizeToFit: true,
      width: isColumnWidth("category", "FeedstockInventoryOrderedSizeTable"),
      field: "category",
    },

    {
      headerName: "Replenishment Time",
      suppressSizeToFit: true,
      width: isColumnWidth(
        "replenishmentTime",
        "FeedstockInventoryOrderedSizeTable"
      ),
      hide: isColumnHidden("replenishmentTime"),
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedReplenishTime(params, this.gridApi);
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "replenishmentTime",
          "Replenishment Time"
        );
        return valueGetterReplenishTime(params);
      },
      valueSetter: (params) => {
        return valueSetterReplenishTime(params);
      },
      cellRenderer: (params) => {
        return cellRendererReplenishTime(params);
      },
    },
    {
      headerName: "Dry time",
      field: "dryTime",
      width: isColumnWidth("dryTime", "FeedstockInventoryOrderedSizeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("dryTime"),
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedAddDryTime(params, this.gridApi);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "dryTime", "Dry time");
        return valueGetterAddDryTime(params);
      },
      valueSetter: (params) => {
        return valueSetterAddDryTime(params);
      },
      cellRenderer: (params) => {
        return cellRendererAddDryTime(params);
      },
    },

    {
      headerName: "Reliability",
      field: "reliability",
      width: isColumnWidth("reliability", "FeedstockInventoryOrderedSizeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("reliability"),
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedReliability(params, this.gridApi);
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "reliability",
          "Reliability"
        );
        return valueGetterReliability(params);
      },
      valueSetter: (params) => {
        return valueSetterReliability(params);
      },
      cellRenderer: (params) => {
        return cellRendererReliability(params);
      },
    },
    {
      headerName: "Usage",
      field: "usage",
      suppressSizeToFit: true,
      width: isColumnWidth("usage", "FeedstockInventoryOrderedSizeTable"),
      hide: isColumnHidden("usage"),
      cellRenderer: (params) => {
        getWidthColumnValue(params, this.componentName, "usage", "Usage");
        return params.data.usage;
      },
    },
    {
      headerName: "Total Inventory",
      children: [
        {
          headerName: "Target",
          field: "target",
          suppressSizeToFit: true,
          valueGetter: (params) => {
            getWidthColumnValue(params, this.componentName, "target", "Target");
            return targetCalculation(params);
          },
          width: isColumnWidth("target", "FeedstockInventoryOrderedSizeTable"),
          hide: isColumnHidden("target"),
        },
        {
          headerName: "Act.",
          field: "act",
          hide: isColumnHidden("act"),
          suppressSizeToFit: true,
          cellRenderer: (params) => {
            getWidthColumnValue(params, this.componentName, "act", "Act.");
            return params.data.act;
          },
          width: isColumnWidth("act", "FeedstockInventoryOrderedSizeTable"),
        },
        {
          headerName: "Colour",
          field: "displayColor",
          suppressSizeToFit: true,
          width: 87,
          // isColumnWidth(
          //   "displayColor ",
          //   "FeedstockInventoryOrderedSizeTable"
          // ),
          hide: isColumnHidden("displayColor "),
          cellStyle: function (params) {
            let color = numberToColor(params.data.displayColor);
            return {
              "background-color": color,
            };
          },
          valueGetter: function (params) {
            return "";
          },
        },
      ],
      marryChildren: true,
      hide: isColumnHidden("Total Inventory"),
    },
    {
      headerName: "Order Frequency",
      field: "orderFrequency",
      hide: isColumnHidden("orderFrequency"),
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedorderFrequency(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "orderFrequency",
          "Order Frequency"
        );
        //console.log(params.data,'---------->');
        return valueGetterorderFrequency(params);
      },
      valueSetter: (params) => {
        return valueSetterorderFrequency(params);
      },
      cellRenderer: (params) => {
        return cellRendererorderFrequency(params);
      },
      width: isColumnWidth(
        "orderFrequency",
        "FeedstockInventoryOrderedSizeTable"
      ),
    },
    {
      headerName: "Total on Order",
      field: "totalOnOrder",
      hide: isColumnHidden("totalOnOrder"),
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedtotalOnOrder(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "totalOnOrder",
          "Total to Order"
        );
        return valueGettertotalOnOrder(params);
      },
      valueSetter: (params) => {
        return valueSettertotalOnOrder(params);
      },
      cellRenderer: (params) => {
        return cellRenderertotalOnOrder(params);
      },
      width: 121,
      // isColumnWidth(
      //   "totalOnOrder",
      //   "FeedstockInventoryOrderedSizeTable"
      // ),
    },
    {
      headerName: "Hyne",
      field: "hyne",
      hide: isColumnHidden("hyne"),
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedhyne(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "hyne", "hyne");
        return valueGetterhyne(params);
      },
      valueSetter: (params) => {
        return valueSetterhyne(params);
      },
      cellRenderer: (params) => {
        return cellRendererhyne(params);
      },
      width: isColumnWidth("hyne", "FeedstockInventoryOrderedSizeTable"),
    },
    {
      headerName: "Imbil",
      field: "imbil",
      hide: isColumnHidden("imbil"),
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedimbil(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "imbil", "imbil");
        return valueGetterimbil(params);
      },
      valueSetter: (params) => {
        return valueSetterimbil(params);
      },
      cellRenderer: (params) => {
        return cellRendererimbil(params);
      },
      width: isColumnWidth("imbil", "FeedstockInventoryOrderedSizeTable"),
    },
    {
      headerName: "AKD",
      field: "akd",
      hide: isColumnHidden("akd"),
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedakd(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "akd", "akd");
        return valueGetterakd(params);
      },
      valueSetter: (params) => {
        return valueSetterakd(params);
      },
      cellRenderer: (params) => {
        return cellRendererakd(params);
      },
      width: isColumnWidth("akd", "FeedstockInventoryOrderedSizeTable"),
    },
    {
      headerName: "ATP",
      field: "atp",
      hide: isColumnHidden("atp"),
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedatp(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "atp", "atp");
        return valueGetteratp(params);
      },
      valueSetter: (params) => {
        return valueSetteratp(params);
      },
      cellRenderer: (params) => {
        return cellRendereratp(params);
      },
      width: isColumnWidth("atp", "FeedstockInventoryOrderedSizeTable"),
    },
    {
      headerName: "Wade",
      field: "wade",
      hide: isColumnHidden("wade"),
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedwade(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "wade", "wade");
        return valueGetterwade(params);
      },
      valueSetter: (params) => {
        return valueSetterwade(params);
      },
      cellRenderer: (params) => {
        return cellRendererwade(params);
      },
      width: isColumnWidth("wade", "FeedstockInventoryOrderedSizeTable"),
    },
    {
      headerName: "Total",
      field: "total",
      hide: isColumnHidden("total"),
      suppressSizeToFit: true,
      // editable: true,
      // // type: "numericColumn",
      // cellEditor: "numericEditor",
      // onCellValueChanged: (params) => {
      //   return onCellValueChangedtotal(params, this.gridApi, this.props);
      // },
      valueGetter: (params) => {

        getWidthColumnValue(params, this.componentName, "total", "total");
        return params.data.total;
      },
      // valueSetter: (params) => {
      //   return valueSettertotal(params);
      // },
      // cellRenderer: (params) => {
      //   return cellRenderertotal(params);
      // },
      width: isColumnWidth("total", "FeedstockInventoryOrderedSizeTable"),
    },
    {
      headerName: "Difference",
      field: "difference",
      hide: isColumnHidden("difference"),
      suppressSizeToFit: true,
      // editable: true,
      // // type: "numericColumn",
      // cellEditor: "numericEditor",
      valueSetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "difference",
          "difference"
        );
        return params.data.difference;
      },
      // onCellValueChanged: (params) => {
      //   return onCellValueChangeddifference(params, this.gridApi, this.props);
      // },
      // valueGetter: (params) => {
      //   getWidthColumnValue(
      //     params,
      //     this.componentName,
      //     "difference",
      //     "difference"
      //   );
      //   return valueGetterdifference(params);
      // },

      // cellRenderer: (params) => {
      //   return cellRendererdifference(params);
      // },
      width: 100,//isColumnWidth("difference", "FeedstockInventoryOrderedSizeTable"),
    },
    {
      headerName: "Total to Order",
      field: "totalToOrder",
      hide: isColumnHidden("totalToOrder"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "totalToOrder",
          "totalToOrder"
        );
        return params.data.totalToOrder;
      },
      width: 120,
      // isColumnWidth(
      //   "totalToOrder",
      //   "FeedstockInventoryOrderedSizeTable"
      // ),
    },
  ];

  constructor(props) {
    super(props);
    //this.defaultStore = this.props.drumLoadingReport;
    this.componentName = "FeedstockInventoryOrderedSizeTable";
    this.printHeader = "Feedstock Inventory Ordered Size Table";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}
