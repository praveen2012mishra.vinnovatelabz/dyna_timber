import DefaultAgTable from 'components/Table/DefaultAgTable';

function isColumnHidden(field) {
	let store = JSON.parse(localStorage.getItem('hiddenColumns')) || {
		FeedstockInventorySKUTable: {}
	};
	if (store.FeedstockInventorySKUTable) {
		return store.FeedstockInventorySKUTable[field] || false;
	}
	return false;
}

const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem('isColumnWidth')) || {
		[componentName]: {}
	};
	if (store[componentName]) {
		let width = store[componentName][field];
		width = parseInt(width, 10);
		return width;
	}
	else{
		return 300;
	}
};

export default class FeedstockInventorySKUTable extends DefaultAgTable {
	columns = [
		{
			headerName: 'Code',
			suppressSizeToFit: true,
			width: isColumnWidth('code', 'FeedstockInventorySKUTable'),
			valueGetter: params => {
				return params.data.material.code;
			},
			hide: isColumnHidden('code'),
			field: 'code'
		},
		{
			headerName: 'Min % of Category',
			suppressSizeToFit: true,
			width: isColumnWidth('minPctOfCategory1', 'FeedstockInventorySKUTable'),
			valueGetter: params => {
				return params.data.material.minPctOfCategory1;
			},
			hide: isColumnHidden('minPctOfCategory1'),
			field: 'minPctOfCategory1'
		},

		{
			headerName: 'Total Inventory',
			children: [
				{
					headerName: 'Target',
					suppressSizeToFit: true,
					width: isColumnWidth('target', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.target;
					},
					field: 'target',
					hide: isColumnHidden('target')
				},
				{
					headerName: 'Act.',
					field: 'act',
					suppressSizeToFit: true,
					width: isColumnWidth('act', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.act;
					},
					hide: isColumnHidden('act')
				},
				{
					headerName: 'Col.',
					field: 'color',
					suppressSizeToFit: true,
					width: isColumnWidth('color', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.color;
					},
					hide: isColumnHidden('color')
				}
			],
			marryChildren: true,
			hide: isColumnHidden('Total Inventory'),
			field: 'Total Inventory'
		},
		{
			headerName: 'Incoming - In yard drying',
			children: [
				{
					headerName: 'RTG',
					field: 'rtg',
					suppressSizeToFit: true,
					width: isColumnWidth('rtg', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.rtg;
					},
					hide: isColumnHidden('rtg')
				},
				{
					headerName: 'Wk2',
					field: 'wk2',
					suppressSizeToFit: true,
					width: isColumnWidth('wk2', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.wk2;
					},
					hide: isColumnHidden('wk2')
				},
				{
					headerName: 'Wk3',
					field: 'wk3',
					suppressSizeToFit: true,
					width: isColumnWidth('wk3', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.wk3;
					},
					hide: isColumnHidden('wk3')
				},
				{
					headerName: 'Wk4',
					field: 'wk4',
					suppressSizeToFit: true,
					width: isColumnWidth('wk4', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.wk4;
					},
					hide: isColumnHidden('wk4')
				},
				{
					headerName: 'Wk5',
					field: 'wk5',
					suppressSizeToFit: true,
					width: isColumnWidth('wk5', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.wk5;
					},
					hide: isColumnHidden('wk5')
				},
				{
					headerName: 'Wk6',
					field: 'wk6',
					suppressSizeToFit: true,
					width: isColumnWidth('wk6', 'FeedstockInventorySKUTable'),
					valueGetter: params => {
						return params.data.material.wk6;
					},
					hide: isColumnHidden('wk6')
				}
			],
			marryChildren: true
		},
		{
			headerName: 'To Replenish',
			field: 'toReplenish',
			suppressSizeToFit: true,
			width: isColumnWidth('toReplenish', 'FeedstockInventorySKUTable'),
			valueGetter: params => {
				return params.data.material.toReplenish;
			},
			hide: isColumnHidden('toReplenish')
		}
	];

	constructor(props) {
		super(props);
		//this.defaultStore = this.props.drumLoadingReport;
		this.componentName = 'FeedstockInventorySKUTable';
		this.printHeader = 'Feedstock Inventory SKU Table';
	}

	onGridReady = params => {
		this.gridApi = params.api;
		this.gridApi.sizeColumnsToFit();
	};
}
