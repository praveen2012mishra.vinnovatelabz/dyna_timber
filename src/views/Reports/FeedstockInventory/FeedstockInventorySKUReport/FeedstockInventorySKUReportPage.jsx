import React from "react";
import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import FeedstockInventorySKUTable
    from "views/Reports/FeedstockInventory/FeedstockInventorySKUReport/FeedstockInventorySKUTable";

@inject("feedstockInventorySKU")
@observer
export default class FeedstockInventorySKUReportPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.feedstockInventorySKU;
        this.components = {
            table: FeedstockInventorySKUTable,
            edit: null
        };
    }
}

