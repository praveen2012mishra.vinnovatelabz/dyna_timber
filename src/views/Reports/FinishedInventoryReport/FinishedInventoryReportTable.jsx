import React from "react";
import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";
import { get } from "lodash";
import { FastField } from "formik";
import InventoryClient from "api/inventory";

let targetValueObject = {};

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+45;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

//color selection
function numberToColor(val) {
  if (parseFloat(val) > 150) {
    return "black";
  } else if (parseFloat(val) > 100 && parseFloat(val) <= 150) {
    return "green";
  } else if (parseFloat(val) > 50 && parseFloat(val) <= 100) {
    return "yellow";
  } else if (parseFloat(val) >= 0 && parseFloat(val) <= 50) {
    return "red";
  } else if (parseFloat(val) < 0) {
    return "#fff";
  }
}


//edit feature
const onCellValueChangedBuffer = (params) => {
  let store = JSON.parse(sessionStorage.getItem("bufferAdjustment")) || {};
  store = {
    ...store,
    [get(params, "data.inventory.id")]: params.newValue,
  };
  sessionStorage.setItem("bufferAdjustment", JSON.stringify(store));
  params.data.inventory.bufferAdjustment = params.newValue;
  updateBufferAdjustment(Object.assign({}, params.data.inventory));
  //this.gridApi.refreshCells({ force: true });
};

const cellStyleWeekStartActPlusWip = (params) => {
  let openAct = params.data.weekStartAct;
  let openWIP = params.data.weekStartWIP;
  let targetInventory = getNewTargetInventory(params);
  let actWIPCol = (openAct + openWIP) / targetInventory;
  actWIPCol = Math.round(actWIPCol * 100);
  let store = JSON.parse(sessionStorage.getItem("weekStartActPlusWIP")) || {};
  store = {
    ...store,
    [params.data.inventory.sku]: actWIPCol,
  };
  sessionStorage.setItem("weekStartActPlusWIP", JSON.stringify(store));
  params.data.weekStartActPlusWIP = actWIPCol;
  return actWIPCol;
};

const cellStyleCurrentActPlusWIP = (params) => {
  let currentAct = params.data.currentAct;
  let currentWip = params.data.currentWIP;
  let targetInventory = getNewTargetInventory(params);
  let actWIPCol = (currentAct + currentWip) / targetInventory;
  actWIPCol = Math.round(actWIPCol * 100);
  let store = JSON.parse(sessionStorage.getItem("currentActPlusWIP")) || {};
  store = {
    ...store,
    [params.data.inventory.sku]: actWIPCol,
  };
  sessionStorage.setItem("currentActPlusWIP", JSON.stringify(store));
  params.data.currentActPlusWIP = actWIPCol;
  //console.log(actWIPCol, 'actCol', sku);
  return actWIPCol;
};

const cellStyleWeekStartActCol = (params) => {
  let openAct = params.data.weekStartAct;
  let targetInventory = getNewTargetInventory(params);
  let actCol = openAct / targetInventory;
  actCol = Math.round(actCol * 100);

  let store = JSON.parse(sessionStorage.getItem("weekStartActCol")) || {};
  store = {
    ...store,
    [params.data.inventory.sku]: actCol,
  };
  sessionStorage.setItem("weekStartActCol", JSON.stringify(store));
  params.data.weekStartActCol = actCol;
  return actCol;
};

const cellStyleCurrentActCol = (params) => {
  let targetInventory = getNewTargetInventory(params);
  let currentAct = params.data.currentAct;
  let actCol = currentAct / targetInventory;
  actCol = Math.round(actCol * 100);
  let store = JSON.parse(sessionStorage.getItem("currentActCol")) || {};
  store = {
    ...store,
    [params.data.inventory.sku]: actCol,
  };
  sessionStorage.setItem("currentActCol", JSON.stringify(store));
  params.data.currentActPlusWIP = actCol;
  return actCol;
};

const valueGetterBuffer = (params) => {
  let store = JSON.parse(sessionStorage.getItem("bufferAdjustment")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.inventory.id")]]))) {
    return parseInt(store[[get(params, "data.inventory.id")]]);
  }
  return params.data.inventory.bufferAdjustment;
};

const valueSetterBuffer = (params) => {
  if (
    parseInt(params.newValue) ===
    parseInt(params.data.inventory.bufferAdjustment)
  )
    return false;
  let store = JSON.parse(sessionStorage.getItem("bufferAdjustment")) || {};
  store = {
    ...store,
    [get(params, "data.inventory.id")]: params.newValue,
  };
  sessionStorage.setItem("bufferAdjustment", JSON.stringify(store));
  params.data.inventory.bufferAdjustment = params.newValue;

  return true;
};

const cellRendererBuffer = (params) => {
  let store = JSON.parse(sessionStorage.getItem("bufferAdjustment")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.inventory.id")]]))) {
    return parseInt(store[[get(params, "data.inventory.id")]]);
  }

  return params.data.inventory.bufferAdjustment;
};

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    FinishedInventoryReportTable: {},
  };
  if (store.FinishedInventoryReportTable) {
    return store.FinishedInventoryReportTable[field] || false;
  }
  return false;
}

//set precision
const setPrecision = (val, precision) => val.toFixed(precision);

const updateBufferAdjustment = (inventory) => {
  InventoryClient.update(inventory)
    .then((res) => {})
    .catch((err) => console.log("error while updating inventory \n", err));
};

const getNewTargetInventory = (params, props) => {
  let buffAdj = params.data.inventory.bufferAdjustment;
  let targetValue;
  if (buffAdj && buffAdj > 0) {
    targetValue = Math.round((params.data.currentTargetInv / 100) * buffAdj);
    //console.log(params.data.inventory.sku,targetValue);
    return targetValue;
  } else {
    targetValue = params.data.currentTargetInv;
    //console.log(params.data.inventory.sku,targetValue,'--------------------->');
    return targetValue;
  }
};

//table column
@inject("finishedInventoryReport")
@observer
export default class FinishedInventoryReportTable extends DefaultAgTable {
  columns = [
    {
      headerName: "SKU",
      field: 'sku',
      sort: "asc",
      width: isColumnWidth("sku", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("sku"),
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "inventory.sku","SKU");
        return params.data.inventory.sku;
      }, 
    },
    {
      width: isColumnWidth("productName", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Product Name",
      field: "productName",
      //sort: 'asc',
      hide: isColumnHidden("productName"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.productName","Product Name");
        return get(params, "data.inventory.productName");
      },
    },
    {
      width: isColumnWidth("inventory.stock", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Stock",
      field: "inventory.stock",
      hide: isColumnHidden("inventory.stock"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.stock","Stock");
        return get(params, "data.inventory.stock");
      },
    },
    {
      width: isColumnWidth("inventory.pcsPerPack", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Pcs/Pack",
      field: "inventory.pcsPerPack",
      hide: isColumnHidden("inventory.pcsPerPack"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.pcsPerPack","Pcs/Pack");
        return get(params, "data.inventory.pcsPerPack");
      },
    },
    {
      width: isColumnWidth("inventory.m3PerPack", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "m3/pck",
      field: "inventory.m3PerPack",
      hide: isColumnHidden("inventory.m3PerPack"),
      valueGetter: (params) =>{
        getWidthColumnValue(params, this.componentName, "inventory.m3PerPack","m3/pck");
        return params.data.inventory.m3PerPack
          ? +setPrecision(params.data.inventory.m3PerPack, 2)
          : params.value},
    },
    {
      width: isColumnWidth("inventory.feedStock1Machine", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Machine",
      field: "inventory.feedStock1Machine",
      hide: isColumnHidden("inventory.feedStock1Machine"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.feedStock1Machine","Machine");
        return get(params, "data.inventory.feedStock1Machine");
      },
    },
    {
      width: isColumnWidth("inventory.lastProcess", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Last Process",
      field: "inventory.lastProcess",
      hide: isColumnHidden("inventory.lastProcess"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.lastProcess","Last Process");
        return get(params, "data.inventory.lastProcess");
      },
    },
    {
      width: isColumnWidth("pdnLeadTimeDays", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Pdn lead time (days)",
      field: "pdnLeadTimeDays",
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "pdnLeadTimeDays","Pdn lead time (days)");
        return params.data.pdnLeadTimeDays;
      }, 
      hide: isColumnHidden("pdnLeadTimeDays"),
    },
    {
      width: isColumnWidth("ordInterval", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Ord. Interval",
      field: "ordInterval",
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "ordInterval","Ord. Interval");
        return params.data.ordInterval;
      }, 
      hide: isColumnHidden("ordInterval"),
    },
    {
      width: isColumnWidth("maxUseOver30Days", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Maximum use over 30 day period",
      field: "maxUseOver30Days",
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "maxUseOver30Days","Maximum use over 30 day period");
        return params.data.maxUseOver30Days;
      }, 
      hide: isColumnHidden("maxUseOver30Days"),
    },
    {
      width: isColumnWidth(
        "avgDailyUseLast30DaysOpening",
        "FinishedInventoryReportTable"
      ),
      suppressSizeToFit: true,
      headerName: "Avg. daily use last 30 days opening",
      field: "avgDailyUseLast30DaysOpening",
      hide: isColumnHidden("avgDailyUseLast30DaysOpening"),
      valueGetter: (params) =>{
        getWidthColumnValue(params, this.componentName, "avgDailyUseLast30DaysOpening","Avg. daily use last 30 days opening");
        return params.data.avgDailyUseLast30DaysOpening},
    },
    {
      width: isColumnWidth("reliability", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Reliability%",
      field: "reliability",
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "reliability","Reliability%");
        return params.data.reliability;
      }, 
      hide: isColumnHidden("reliability"),
    },
    {
      width: isColumnWidth(
        "inventory.bufferAdjustment",
        "FinishedInventoryReportTable"
      ),
      suppressSizeToFit: true,
      headerName: "Buffer adjusment",
      editable: true,
      //sort: 'asc',
      type: "numericColumn",
      field: "inventory.bufferAdjustment",
      hide: isColumnHidden("inventory.bufferAdjustment"),
      onCellValueChanged: (params) => {
        return onCellValueChangedBuffer(params);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.bufferAdjustment","Buffer adjusment");
        return valueGetterBuffer(params);
      },
      valueSetter: (params) => {
        return valueSetterBuffer(params);
      },
      cellRenderer: (params) => {
        return cellRendererBuffer(params);
      },
    },
    {
      width: isColumnWidth("sku", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Week Start",
      hide: isColumnHidden("Week Start"),
      children: [
        {
          width: isColumnWidth(
            "weekStartTargetInv",
            "FinishedInventoryReportTable"
          ),
          suppressSizeToFit: true,
          headerName: "Target Inv.",
          hide: isColumnHidden("weekStartTargetInv"),
          field: "weekStartTargetInv",
          valueGetter: (params) => {
            getWidthColumnValue(params, this.componentName, "weekStartTargetInv","Target Inv.");
            return getNewTargetInventory(params);
          },
        },
        {
          width: isColumnWidth("weekStartAct", "FinishedInventoryReportTable"),
          suppressSizeToFit: true,
          headerName: "Open Act.",
          field: "weekStartAct",
          hide: isColumnHidden("weekStartAct"),
          valueGetter: (params) => {
            getWidthColumnValue(params, this.componentName, "weekStartAct","Open Act.");
            return params.data.weekStartAct;
          },
        },
        {
          width: isColumnWidth("weekStartWIP", "FinishedInventoryReportTable"),
          suppressSizeToFit: true,
          headerName: "Open. WIP",
          field: "weekStartWIP",
          hide: isColumnHidden("weekStartWIP"),
          valueGetter: (params) => {
            getWidthColumnValue(params, this.componentName, "weekStartWIP","Open. WIP");
            return params.data.weekStartWIP;
          },
        },
        {
          width: 135,
          // isColumnWidth(
          //   "actPlusWipColorFilter",
          //   "FinishedInventoryReportTable"
          // ),
          suppressSizeToFit: true,
          headerName: "Act. + WIP col.",
          field: "weekStartActPlusWIP",
          hide: isColumnHidden("weekStartActPlusWIP"),
          filter: "actPlusWipColorFilter",
          filterParams: {
            field: "weekStartActPlusWIP",
          },
          cellStyle: function (params) {
            let actWIPCol = cellStyleWeekStartActPlusWip(params);
            let color = numberToColor(actWIPCol);
            return {
              "background-color": color,
            };
          },
          cellRenderer:params=>"",
        },
        {
          width: 96,
          // isColumnWidth(
          //   "weekStartActCol",
          //   "FinishedInventoryReportTable"
          // ),
          suppressSizeToFit: true,
          headerName: "Act. Col.",
          field: "weekStartActCol",
          hide: isColumnHidden("weekStartActCol"),
          filter: "actPlusWipColorFilter",
          filterParams: {
            field: "weekStartActCol",
          },
          cellStyle: function (params) {
            let actCol = cellStyleWeekStartActCol(params);
            let color = numberToColor(actCol);

            return {
              "background-color": color,
            };
          },
          cellRenderer:params=>"",
          
        },
      ],
      marryChildren: true,
    },
    {
      width: isColumnWidth("currentStatus", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Current Status",
      field: "currentStatus",
      hide: isColumnHidden("currentStatus"),
      children: [
        {
          width: isColumnWidth(
            "currentTargetInv",
            "FinishedInventoryReportTable"
          ),
          suppressSizeToFit: true,
          headerName: "Target Inv.",
          field: "currentTargetInv",
          hide: isColumnHidden("currentTargetInv"),
          valueGetter: (params) => {
            getWidthColumnValue(params, this.componentName, "currentTargetInv","Target Inv.");
            return getNewTargetInventory(params, this.props);
          },
        },
        {
          width: isColumnWidth("currentAct", "FinishedInventoryReportTable"),
          suppressSizeToFit: true,
          headerName: "Current Act.",
          field: "currentAct",
          hide: isColumnHidden("currentAct"),
          valueGetter: (params) =>{
            getWidthColumnValue(params, this.componentName, "currentAct","Current Act.");
            return params.data.currentAct},
        },
        {
          width: isColumnWidth("currentWIP", "FinishedInventoryReportTable"),
          suppressSizeToFit: true,
          headerName: "Current WIP",
          field: "currentWIP",
          hide: isColumnHidden("currentWIP"),
          valueGetter: (params) =>{
            getWidthColumnValue(params, this.componentName, "currentWIP","Current WIP");
            return params.data.currentWIP},
        },
        {
          width:133 ,
          // isColumnWidth(
          //   "currentActPlusWIP",
          //   "FinishedInventoryReportTable"
          // ),
          suppressSizeToFit: true,
          headerName: "Act. + WIP col.",
          field: "currentActPlusWIP",
          filter: "actPlusWipColorFilter",
          hide: isColumnHidden("currentActPlusWIP"),
          filterParams: {
            field: "currentActPlusWIP",
          },
          cellStyle: function (params) {
            let actWIPCol = cellStyleCurrentActPlusWIP(params);
            let color = numberToColor(actWIPCol);
            return {
              "background-color": color,
            };
          },
          cellRenderer:params=>"",
        },
        {
          width: 97,
          //isColumnWidth("currentActCol", "FinishedInventoryReportTable"),
          suppressSizeToFit: true,
          headerName: "Act. Col.",
          field: "currentActCol",
          filter: "actPlusWipColorFilter",
          filterParams: {
            field: "currentActCol",
          },
          cellRenderer:params=>"",
          hide: isColumnHidden("currentActCol"),
          cellStyle: function (params) {
            let actCol = cellStyleCurrentActCol(params);
            let color = numberToColor(actCol);
            //console.log(actCol, 'actCol', sku);

            return {
              "background-color": color,
            };
          },
        },
      ],
      marryChildren: true,
    },
    {
      width: isColumnWidth("movement", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Movement",
      field: "movement",
      hide: isColumnHidden("movement"),
      children: [
        {
          width: isColumnWidth(
            "movementActPlusWIP",
            "FinishedInventoryReportTable"
          ),
          suppressSizeToFit: true,
          headerName: "Act. + WIP",
          field: "movementActPlusWIP",
          hide: isColumnHidden("movementActPlusWIP"),
          valueGetter: (params) =>{
            getWidthColumnValue(params, this.componentName, "movementActPlusWIP","Act. + WIP");
            return params.value},
        },
        {
          width: isColumnWidth("movementAct", "FinishedInventoryReportTable"),
          suppressSizeToFit: true,
          headerName: "Act",
          field: "movementAct",
          hide: isColumnHidden("movementAct"),
          cellRenderer: (params) =>{
            getWidthColumnValue(params, this.componentName, "movementAct","Act");
            return params.value },
        },
        {
          // width: isColumnWidth(
          //   "movementColourChangeActPlusWip",
          //   "FinishedInventoryReportTable"
          // ),
          width:190,
          suppressSizeToFit: true,
          headerName: "Colour Change Act. + WIP",
          field: "movementColourChangeActPlusWip",
          hide: isColumnHidden("movementColourChangeActPlusWip"),
          cellStyle: function (params) {
            let color = params.value == true ? "red" : "green";
            return {
              color: color,
              "font-weight": "bold",
            };
          },
          cellRenderer: function (params) {
            //getWidthColumnValue(params, this.componentName, "movementColourChangeActPlusWip");
            return params.value == true ? "⇩" : "⇧";
          },
        },
        {
          // width: isColumnWidth(
          //   "movementColourChangeAct",
          //   "FinishedInventoryReportTable"
          // ),
          width:150,
          suppressSizeToFit: true,
          headerName: "Colour Change Act.",
          field: "movementColourChangeAct",
          hide: isColumnHidden("movementColourChangeAct"),
          cellStyle: function (params) {
            let color = params.value == true ? "red" : "green";
            return {
              color: color,
              "font-weight": "bold",
            };
          },
          cellRenderer: function (params) {
            //getWidthColumnValue(params, this.componentName, "movementColourChangeAct");
            return params.value == true ? "⇩" : "⇧";
          },
        },
        {
          width: isColumnWidth(
            "movementPdnWTD",
            "FinishedInventoryReportTable"
          ),
          suppressSizeToFit: true,
          headerName: "Pdn WTD",
          field: "movementPdnWTD",
          valueGetter: params => {
            getWidthColumnValue(params, this.componentName, "movementPdnWTD","Pdn WTD");
            return params.data.movementPdnWTD;
          },  
          hide: isColumnHidden("movementPdnWTD"),
        },
        {
          width: isColumnWidth(
            "movementTreatmentWTD",
            "FinishedInventoryReportTable"
          ),
          suppressSizeToFit: true,
          headerName: "Treatment WTD",
          field: "movementTreatmentWTD",
          hide: isColumnHidden("movementTreatmentWTD"),
          cellRenderer: (params) =>{
            getWidthColumnValue(params, this.componentName, "movementTreatmentWTD","Treatment WTD");
            return params.data.movementTreatmentWTD},
        },
        {
          width: isColumnWidth(
            "movementSalesWTD",
            "FinishedInventoryReportTable"
          ),
          suppressSizeToFit: true,
          headerName: "Sales WTD",
          field: "movementSalesWTD",
          hide: isColumnHidden("movementSalesWTD"),
          cellRenderer: (params) =>{
            getWidthColumnValue(params, this.componentName, "movementSalesWTD","Sales WTD");
            return params.data.movementSalesWTD},
        },
      ],
      marryChildren: true,
    },
    {
      width: isColumnWidth("usage", "FinishedInventoryReportTable"),
      suppressSizeToFit: true,
      headerName: "Usage",
      hide: isColumnHidden("usage"),
      field: "usage",
      children: [
        {
          width: isColumnWidth("usageOnOrder", "FinishedInventoryReportTable"),
          suppressSizeToFit: true,
          headerName: "On Order",
          field: "usageOnOrder",
          valueGetter: params => {
            getWidthColumnValue(params, this.componentName, "usageOnOrder","On Order");
            return params.data.usageOnOrder;
          },  
          hide: isColumnHidden("usageOnOrder"),
        },
      ],
      marryChildren: true,
    },
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.finishedInventoryReport;
    this.componentName = "FinishedInventoryReportTable";
    this.printHeader = "Finished Inventory Weekly Report (packs)";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}
