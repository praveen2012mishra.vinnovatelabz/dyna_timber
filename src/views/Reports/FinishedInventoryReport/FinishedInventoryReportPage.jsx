import { inject, observer } from "mobx-react";
import DefaultPage from "views/DefaultPage";
import FinishedInventoryReportTable from "views/Reports/FinishedInventoryReport/FinishedInventoryReportTable";

@inject("finishedInventoryReport")
@observer
export default class FinishedInventoryReportPage extends DefaultPage {
  constructor(props) {
    super(props);
    this.defaultStore = this.props.finishedInventoryReport;
    this.components = {
      table: FinishedInventoryReportTable,
      edit: null
    };
  }
}
