import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";
import moment from "moment";

//hide column
function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    MaximumDemandAnalasysTable: {},
  };
  if (store.MaximumDemandAnalasysTable) {
    return store.MaximumDemandAnalasysTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    let width = store[componentName][field];
    width = parseInt(width, 10);
    width = width + 45;
    //console.log(width,field,'============>');
    return width;
  }
};

//take text width
function textWidth(text, fontProp, type) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  text = text ? text.toString() : text;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;
  //console.log(result,'====>');
  result = result;
  //console.log(result);

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (params, componentName, field, headerName) => {
  let data = params.data;
  //console.log(data,componentName,field);
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };

  store[componentName] = {
    ...store[componentName],
  };
  let storeWidth;
  let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
  let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
  let arrayWidth = Object.assign([]);
  arrayWidth.push(fieldWidth);
  arrayWidth.push(valueWidth);
  //console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);

  if (Object.keys(store[componentName]).length === 0) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (!store[componentName][field]) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (store[componentName][field] != Math.max(...arrayWidth)) {
    arrayWidth.push(store[componentName][field]);
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else {
    //console.log("No change in width resize");
  }
};

@inject("maximumDemandAnalasys")
@observer
export default class MaximumDemandAnalasysTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Date",
      field: "date",
      cellRenderer: (params) => {
        getWidthColumnValue(params, this.componentName, "date", "Date");
        return params.value
          ? new moment(params.value).format("MMMM Do YYYY")
          : "";
      },
      filter: "agDateColumnFilter",
      filterParams: {
        comparator: function(filterLocalDateAtMidnight, cellValue) {
          var dateAsString = cellValue;
          if (dateAsString == null) return 0;

          // In the example application, dates are stored as dd/mm/yyyy
          // We create a Date object for comparison against the filter date
          let dateAsDate = new moment(dateAsString, "YYYY-MM-DD").toDate();
          // Now that both parameters are Date objects, we can compare
          if (dateAsDate < filterLocalDateAtMidnight) {
            return -1;
          } else if (dateAsDate > filterLocalDateAtMidnight) {
            return 1;
          } else {
            return 0;
          }
        },
      },
      hide: isColumnHidden("agDateColumnFilter"),
      suppressSizeToFit: true,
      width: 150,
      //width: isColumnWidth("date", "MaximumDemandAnalasysTable"),
      cellRenderer: (params) => {
        getWidthColumnValue(params, this.componentName, "date");
        return params.value
          ? new moment(params.value).format("MMMM Do YYYY")
          : "";
      },
    },
    {
      headerName: "Total Daily Sale",
      field: "totalDailySale",
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "totalDailySale",
          "Total Daily Sale"
        );
        return params.data.totalDailySale;
      },
      suppressSizeToFit: true,
      width: isColumnWidth("totalDailySale", "MaximumDemandAnalasysTable"),

      hide: isColumnHidden("totalDailySale"),
    },
    {
      headerName: "Sales In Last 5 Days",
      field: "salesInLast5Days",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "salesInLast5Days",
          "Sales In Last 5 Days"
        );
        return params.data.totalDailySale;
      },
      width: isColumnWidth("salesInLast5Days", "MaximumDemandAnalasysTable"),
      hide: isColumnHidden("salesInLast5Days"),
    },
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.maximumDemandAnalasys;
    this.componentName = "MaximumDemandAnalasysTable";
    this.printHeader = "Maximum Demand Analasys Table";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    //this.enableTooltip(true);
  };
}
