import { inject, observer } from 'mobx-react';
import DefaultPage from 'views/DefaultPage';
import MaximumDemandAnalasysTable from 'views/Reports/MaximumDemandAnalasys/MaximumAnalasysDemandTable';

@inject('maximumDemandAnalasys')
@observer
export default class MaximumDemandAnalasysPage extends DefaultPage {
	constructor(props) {
		super(props);
		this.defaultStore = this.props.maximumDemandAnalasys;
		this.components = {
			table: MaximumDemandAnalasysTable,
			edit: null
		};
	}
}
