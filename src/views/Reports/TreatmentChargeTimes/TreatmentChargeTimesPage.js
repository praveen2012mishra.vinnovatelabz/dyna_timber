import { inject, observer } from 'mobx-react';
import DefaultPage from 'views/DefaultPage';
import TreatmentChargeTimesTable from './TreatmentChargeTimesTable';

@inject('treatmentChargeTimes')
@observer
export default class TreatmentChargeTimesPage extends DefaultPage {
	constructor(props) {
		super(props);
		this.defaultStore = this.props.treatmentChargeTimes;
		this.components = {
			table: TreatmentChargeTimesTable,
			edit: null
		};
	}
}
