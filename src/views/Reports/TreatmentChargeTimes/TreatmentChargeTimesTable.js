import { inject, observer } from 'mobx-react';
import DefaultAgTable from '../../../components/Table/DefaultAgTable';
import moment from 'moment';

function isColumnHidden(field) {
	let store = JSON.parse(localStorage.getItem('hiddenColumns')) || {
		TreatmentChargeTimesTable: {}
	};
	if (store.TreatmentChargeTimesTable) {
		return store.TreatmentChargeTimesTable[field] || false;
	}
	return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+50;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

@inject('treatmentChargeTimes')
@observer
export default class TreatmentChargeTimesTable extends DefaultAgTable {
	columns = [
		{
			suppressSizeToFit: true,
			headerName: 'Charge ID',
			field: 'chargeId',
			filter: 'chargeId',
			//width:800,
			hide: isColumnHidden('chargeId'),
			width: isColumnWidth("chargeId", "TreatmentChargeTimesTable"),
			valueGetter: params => {
				//console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
				getWidthColumnValue(params, this.componentName, "chargeId",'Charge ID');
				return params.data.chargeId;
			},
		},
		{
			suppressSizeToFit: true,
			headerName: 'Charge Time Minutes',
			field: 'chargeTime',
			filter: 'chargeTime',
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "chargeTime",'Charge Time Minutes');
				return params.data.chargeTime;
			  },
			hide: isColumnHidden('chargeTime'),
			width: isColumnWidth("chargeTime", "TreatmentChargeTimesTable")
		},
		{
			suppressSizeToFit: true,
			headerName: 'Charge Over Time',
			field: 'chargeOvertime',
			filter: 'chargeOvertime',
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "chargeOvertime",'Charge Over Time');
				return params.data.chargeOvertime;
			  },
			hide: isColumnHidden('chargeOvertime'),
			width: isColumnWidth("chargeOvertime", "TreatmentChargeTimesTable")
		},
		{
			suppressSizeToFit: true,
			headerName: 'Total Charge Time',
			field: 'totalChargeTime',
			filter: 'totalChargeTime',
			hide: isColumnHidden('totalChargeTime'),
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "totalChargeTime",'Total Charge Time');
				return (params.data.chargeOvertime+params.data.chargeTime);
			  },
			width: isColumnWidth("totalChargeTime", "TreatmentChargeTimesTable"),
			
		}
	];

	constructor(props) {
		super(props);
		//debugger;
		this.defaultStore = this.props.treatmentChargeTimes;
		this.componentName = "TreatmentChargeTimesTable";
		this.printHeader = 'Treatment Charge Times Table';
	}

	onGridReady = params => {
		this.gridApi = params.api;
		this.gridApi.sizeColumnsToFit();
		//this.enableTooltip(true);
	};
}
