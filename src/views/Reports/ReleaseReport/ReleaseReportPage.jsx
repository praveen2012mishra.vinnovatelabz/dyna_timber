import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import ReleaseReportTable from "views/Reports/ReleaseReport/ReleaseReportTable";


@inject("releaseReport")
@observer
export default class ReleaseReportPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.releaseReport;
        this.components = {
            table: ReleaseReportTable,
            edit: null
        };
    }
}
