import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";
import * as _ from "lodash";

//hide the column
function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    ReleaseReportTable: {},
  };
  if (store.ReleaseReportTable) {
    return store.ReleaseReportTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+45;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

@inject("releaseReport")
@observer
export default class ReleaseReportTable extends DefaultAgTable {
  columns = [
    {
      headerName: "SKU",
      field: "sku",
      suppressSizeToFit: true,
      width: isColumnWidth("sku", "ReleaseReportTable"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.sku","SKU");
        return _.get(params, "data.inventory.sku");
      },
      hide: isColumnHidden("sku"),
    },
    {
      headerName: "Product Name",
      field: "productName",
      suppressSizeToFit: true,
      width: isColumnWidth("productName", "ReleaseReportTable"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "inventory.productName","Product Name");
        return _.get(params, "data.inventory.productName");
      },
      hide: isColumnHidden("productName"),
    },

    {
      headerName: "# pcks Untreated at week start",
      field: "numberOfPacksUntreatedAtWeekStart",
      suppressSizeToFit: true,
      width: isColumnWidth(
        "numberOfPacksUntreatedAtWeekStart",
        "ReleaseReportTable"
      ),
      hide: isColumnHidden("numberOfPacksUntreatedAtWeekStart"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "numberOfPacksUntreatedAtWeekStart","# pcks Untreated at week start"
        );
        return params.data.numberOfPacksUntreatedAtWeekStart;
      },
    },
    {
      headerName: "# pcks to TOB",
      field: "numberOfPacksToTOB",
      suppressSizeToFit: true,
      width: isColumnWidth("numberOfPacksToTOB", "ReleaseReportTable"),
      hide: isColumnHidden("numberOfPacksToTOB"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "numberOfPacksToTOB","# pcks to TOB");
        return params.data.numberOfPacksToTOB;
      },
    },
    {
      headerName: "Sold",
      field: "sold",
      suppressSizeToFit: true,
      width: isColumnWidth("sold", "ReleaseReportTable"),
      hide: isColumnHidden("sold"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "sold","Sold");
        return params.data.sold;
      },
    },
    {
      headerName: "Total pcks to Release",
      field: "totalPacksToRelease",
      suppressSizeToFit: true,
      width: isColumnWidth("totalPacksToRelease", "ReleaseReportTable"),
      hide: isColumnHidden("totalPacksToRelease"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "totalPacksToRelease","Total pcks to Release");
        return params.data.totalPacksToRelease;
      },
    },
    {
      headerName: "Actual pcks Released",
      field: "actualPacksReleased",
      suppressSizeToFit: true,
      width: isColumnWidth("actualPacksReleased", "ReleaseReportTable"),
      hide: isColumnHidden("actualPacksReleased"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "actualPacksReleased","Actual pcks Released");
        return params.data.actualPacksReleased;
      },
    },
    {
      headerName: "Total pcks to Load",
      field: "totalPacksToLoad",
      suppressSizeToFit: true,
      width: isColumnWidth("totalPacksToLoad", "ReleaseReportTable"),
      hide: isColumnHidden("totalPacksToLoad"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "totalPacksToLoad","Total pcks to Load");
        return params.data.totalPacksToLoad;
      },
    },
    {
      headerName: "Pcks Loaded",
      field: "packsLoaded",
      suppressSizeToFit: true,
      width: isColumnWidth("packsLoaded", "ReleaseReportTable"),
      hide: isColumnHidden("packsLoaded"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "packsLoaded","Pcks Loaded");
        return params.data.packsLoaded;
      },
    },
    {
      headerName: "Remainder pcks to load",
      field: "remainderPacksToLoad",
      suppressSizeToFit: true,
      width: isColumnWidth("remainderPacksToLoad", "ReleaseReportTable"),
      hide: isColumnHidden("remainderPacksToLoad"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "remainderPacksToLoad","Remainder pcks to load");
        return params.data.remainderPacksToLoad;
      },
    },
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.releaseReport;
    this.componentName = "ReleaseReportTable";
    this.printHeader = "Release Report Table";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}
