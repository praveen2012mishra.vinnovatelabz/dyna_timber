import DefaultAgTable from 'components/Table/DefaultAgTable';

function isColumnHidden(field) {
	let store = JSON.parse(localStorage.getItem('hiddenColumns')) || {
		MYOBInvoicesTable: {}
	};
	if (store.MYOBInvoicesTable) {
		return store.MYOBInvoicesTable[field] || false;
	}
	return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+45;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

export default class MYOBInvoicesTable extends DefaultAgTable {
	columns = [
		{
			headerName: 'ID',
			field: 'invoiceId',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "invoiceId",'ID');
				return params.data.invoiceId;
			  },
			width: isColumnWidth('invoiceId', 'MYOBInvoicesTable'),
			hide: isColumnHidden('invoiceId')
		},
		{
			headerName: 'Product',
			field: 'product',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "product",'Product');
				return params.data.product;
			  },
			width: isColumnWidth('product', 'MYOBInvoicesTable'),
			hide: isColumnHidden('product')
		},
		{
			headerName: 'Customer Name',
			field: 'customerName',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "customerName",'Customer Name');
				return params.data.customerName;
			  },
			width: isColumnWidth('customerName', 'MYOBInvoicesTable'),
			hide: isColumnHidden('customerName')
		},
		{
			headerName: 'Transaction Number',
			field: 'transactionNumber',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "transactionNumber",'Transaction Number');
				return params.data.transactionNumber;
			  },
			width: isColumnWidth('transactionNumber', 'MYOBInvoicesTable'),
			hide: isColumnHidden('transactionNumber')
		},
		{
			headerName: 'Transaction Date',
			field: 'transactionDate',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "transactionDate",'Transaction Date');
				return params.data.transactionDate;
			  },
			width: isColumnWidth('transactionDate', 'MYOBInvoicesTable'),
			hide: isColumnHidden('transactionDate')
		},
		{
			headerName: 'Transaction Type',
			field: 'transactionType',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "transactionType",'Transaction Type');
				return params.data.transactionType;
			  },
			width: isColumnWidth('transactionType', 'MYOBInvoicesTable'),
			hide: isColumnHidden('transactionType')
		},
		{
			headerName: 'Transaction Status',
			field: 'transactionStatus',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "transactionStatus",'Transaction Status');
				return params.data.transactionStatus;
			  },
			width: isColumnWidth('transactionStatus', 'MYOBInvoicesTable'),
			hide: isColumnHidden('transactionStatus')
		},
		{
			headerName: 'Item Number',
			field: 'itemNumber',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "itemNumber",'Item Number');
				return params.data.itemNumber;
			  },
			width: isColumnWidth('itemNumber', 'MYOBInvoicesTable'),
			hide: isColumnHidden('itemNumber')
		},
		{
			headerName: 'Account Number',
			field: 'accountNumber',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "accountNumber",'Account Number');
				return params.data.accountNumber;
			  },
			width: isColumnWidth('accountNumber', 'MYOBInvoicesTable'),
			hide: isColumnHidden('accountNumber')
		},
		{
			headerName: 'Line Memo',
			field: 'lineMemo',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "lineMemo",'Line Memo');
				return params.data.lineMemo;
			  },
			width: isColumnWidth('lineMemo', 'MYOBInvoicesTable'),
			hide: isColumnHidden('lineMemo')
		},
		{
			headerName: 'Employee Name',
			field: 'employeeName',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "employeeName",'Employee Name');
				return params.data.employeeName;
			  },
			width: isColumnWidth('employeeName', 'MYOBInvoicesTable'),
			hide: isColumnHidden('employeeName')
		},
		{
			headerName: 'Quantity',
			field: 'qty',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "qty",'Quantity');
				return params.data.qty;
			  },
			width: isColumnWidth('qty', 'MYOBInvoicesTable'),
			hide: isColumnHidden('qty')
		},
		{
			headerName: 'Tax ex. Amount',
			field: 'taxExAmount',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "taxExAmount",'Tax ex. Amount');
				return params.data.taxExAmount;
			  },
			width: isColumnWidth('taxExAmount', 'MYOBInvoicesTable'),
			hide: isColumnHidden('taxExAmount')
		},
		{
			headerName: 'Tax Amount',
			field: 'taxAmount',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "taxAmount",'Tax Amount');
				return params.data.taxAmount;
			  },
			width: isColumnWidth('taxAmount', 'MYOBInvoicesTable'),
			hide: isColumnHidden('taxAmount')
		},
		{
			headerName: 'Promise Date',
			field: 'promiseDate',
			suppressSizeToFit: true,
			valueGetter: (params) => {
				getWidthColumnValue(params, this.componentName, "promiseDate",'Promise Date');
				return params.data.promiseDate;
			  },
			width: isColumnWidth('promiseDate', 'MYOBInvoicesTable'),
			hide: isColumnHidden('promiseDate')
		}
	];

	constructor(props) {
		super(props);
		//this.defaultStore = this.props.drumLoadingReport;
		this.componentName = 'MYOBInvoicesTable';
		this.printHeader = 'MYOB Invoices Table';
	}

	onGridReady = params => {
		this.gridApi = params.api;
		this.gridApi.sizeColumnsToFit();
	};
}
