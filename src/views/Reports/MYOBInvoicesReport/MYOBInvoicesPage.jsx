import React from "react";
import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import MYOBInvoicesTable from "views/Reports/MYOBInvoicesReport/MYOBInvoicesTable";

@inject("invoices")
@observer
export default class MYOBInvoicesPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.invoices;
        this.components = {
            table: MYOBInvoicesTable,
            edit: null
        };
    }
}

