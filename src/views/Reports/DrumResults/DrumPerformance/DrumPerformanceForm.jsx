import React from 'react';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import CustomInput from 'components/CustomInput/CustomInput';
import Button from 'components/CustomButtons/Button';
import { Field, Formik } from 'formik';
import { inject, observer } from 'mobx-react';
import { DrumPerformanceClient } from 'api/drumPerformance';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Datetime from 'react-datetime';
import moment from 'moment';

@inject('notifications', 'drumPerformance')
@observer
export default class DrumPerformanceForm extends React.Component {
	render() {
		const { notifications, drumPerformance } = this.props;

		return (
			<Formik
				enableReinitialize
				initialValues={
					this.props.initialData
						? this.props.initialData
						: { date: moment().format('YYYY-MM-DD') }
				}
				onSubmit={async (values, actions) => {
					try {
						if (values.id) await DrumPerformanceClient.update(values);
						else await DrumPerformanceClient.save(values);
						notifications.enqueueSnackbar({ message: 'Saved.' });
						drumPerformance.fetchAll();
						drumPerformance.selectItem(null);
						drumPerformance.selectItemsTab();
					} catch (err) {
						//console.error(err);
						notifications.enqueueSnackbar({
							message: 'Saving failed.',
							options: { variant: 'error' }
						});
					} finally {
						actions.setSubmitting(false);
					}
				}}
				render={({ handleSubmit, setFieldValue, isSubmitting }) => (
					<form onSubmit={handleSubmit}>
						<GridContainer>
							<GridItem xs={12} sm={12} md={12}>
								<GridContainer>
									<GridItem xs={12} sm={12} md={3}>
										<InputLabel></InputLabel>
										<br />
										<Field
											name="date"
											render={({ field }) => (
												<FormControl fullWidth>
													<Datetime
														value={field.value}
														onChange={item => {
															setFieldValue('date', item.format('YYYY-MM-DD'));
														}}
														dateFormat={'MMMM Do YYYY'}
														timeFormat={false}
													/>
												</FormControl>
											)}
										/>
									</GridItem>
									<GridItem xs={12} sm={12} md={3}>
										<Field
											name="targetCharges"
											render={({ field }) => (
												<CustomInput
													labelText="Target Charges"
													formControlProps={{
														fullWidth: true
													}}
													inputProps={{
														type: 'number',
														...field,
														value: field.value || ''
													}}
												/>
											)}
										/>
									</GridItem>
									<GridItem xs={12} sm={12} md={3}>
										<Field
											name="actualCharges"
											render={({ field }) => (
												<CustomInput
													labelText="Actual Charges"
													formControlProps={{
														fullWidth: true
													}}
													inputProps={{
														type: 'number',
														...field,
														value: field.value || ''
													}}
												/>
											)}
										/>
									</GridItem>
									<GridItem xs={12} sm={12} md={3}>
										<Field
											name="numOutOfSequence"
											render={({ field }) => (
												<CustomInput
													labelText="# charges out of seq."
													formControlProps={{
														fullWidth: true
													}}
													inputProps={{
														type: 'number',
														...field,
														value: field.value || ''
													}}
												/>
											)}
										/>
									</GridItem>
								</GridContainer>

								<Button color="primary" type="submit" disabled={isSubmitting}>
									{this.props.initialData ? 'Update Item' : 'Save Item'}
								</Button>
							</GridItem>
						</GridContainer>
					</form>
				)}
			/>
		);
	}
}
