import React from "react";
import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import DrumPerformanceTable from "views/Reports/DrumResults/DrumPerformance/DrumPerformanceTable";
import DrumPerformanceForm from "views/Reports/DrumResults/DrumPerformance/DrumPerformanceForm";

@inject("drumPerformance")
@observer
export default class DrumPerformancePage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.drumPerformance;
        this.components = {
            table: DrumPerformanceTable,
            edit: DrumPerformanceForm
        };
    }
}

