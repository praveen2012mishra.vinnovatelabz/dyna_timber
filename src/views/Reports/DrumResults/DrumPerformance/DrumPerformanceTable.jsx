import { inject, observer } from 'mobx-react';
import DefaultAgTable from 'components/Table/DefaultAgTable';

function isColumnHidden(field) {
	let store = JSON.parse(localStorage.getItem('hiddenColumns')) || {
		DrumPerformanceTable: {}
	};
	if (store.DrumPerformanceTable) {
		return store.DrumPerformanceTable[field] || false;
	}
	return false;
}

const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem('isColumnWidth')) || {
		[componentName]: {}
	};
	if (store[componentName]) {
		let width = store[componentName][field];
		width = parseInt(width, 10);
		return width;
	}
};

@inject('drumPerformance')
@observer
export default class DrumPerformanceTable extends DefaultAgTable {
	columns = [
		{
			headerName: 'Date',
			field: 'date',
			suppressSizeToFit: true,
			width: isColumnWidth('date', 'DrumPerformanceTable'),
			hide: isColumnHidden('date')
		},
		{
			headerName: 'Target Charges',
			field: 'targetCharges',
			suppressSizeToFit: true,
			width: isColumnWidth('targetCharges', 'DrumPerformanceTable'),
			hide: isColumnHidden('targetCharges')
		},
		{
			headerName: 'Actual Charges',
			field: 'actualCharges',
			suppressSizeToFit: true,
			width: isColumnWidth('actualCharges', 'DrumPerformanceTable'),
			hide: isColumnHidden('actualCharges')
		},
		{
			headerName: '% complete',
			field: 'pctCompleted',
			suppressSizeToFit: true,
			width: isColumnWidth('pctCompleted', 'DrumPerformanceTable'),
			hide: isColumnHidden('pctCompleted')
		},
		{
			headerName: '# charges out of seq.',
			field: 'numOutOfSequence',
			suppressSizeToFit: true,
			width: isColumnWidth('numOutOfSequence', 'DrumPerformanceTable'),
			hide: isColumnHidden('numOutOfSequence')
		},
		{
			headerName: '% sequence',
			field: 'pctSequence',
			suppressSizeToFit: true,
			width: isColumnWidth('pctSequence', 'DrumPerformanceTable'),
			hide: isColumnHidden('pctSequence')
		}
	];

	constructor(props) {
		super(props);
		this.defaultStore = this.props.store;
		this.componentName = 'DrumPerformanceTable';
		this.printHeader = 'Drum Performance Table';
	}

	onGridReady = params => {
		this.gridApi = params.api;
		this.gridApi.sizeColumnsToFit();
	};
}
