import React from 'react'
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import {Field, Formik} from "formik";
import {inject, observer} from "mobx-react";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Datetime from "react-datetime";
import moment from "moment";
import {DrumOutOfSequenceClient} from "api/drumOutOfSequence";

@inject("notifications")
@observer
export default class DrumOutOfSequenceForm extends React.Component {

    render() {
        const {notifications, store} = this.props;

        return (
            <Formik
                enableReinitialize
                initialValues={this.props.initialData ? this.props.initialData : {date: moment().format("YYYY-MM-DD")}}
                onSubmit={async (values, actions) => {
                    try {
                        if (values.id)
                            await DrumOutOfSequenceClient.update(values);
                        else
                            await DrumOutOfSequenceClient.save(values);
                        notifications.enqueueSnackbar({message: 'Saved.'});
                        store.fetchAll();
                        store.selectItem(null);
                        store.selectItemsTab()
                    } catch (err) {
                        console.error(err);
                        notifications.enqueueSnackbar({message: 'Saving failed.', options: {variant: 'error'}});
                    } finally {
                        actions.setSubmitting(false)
                    }
                }}
                render={({
                             handleSubmit,
                             setFieldValue,
                             isSubmitting,
                         }) => (
                    <form onSubmit={handleSubmit}>
                        <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <InputLabel>
                                        </InputLabel>
                                        <br/>
                                        <Field name="date" render={({field}) => (
                                            <FormControl fullWidth>
                                                <Datetime
                                                    value={field.value}
                                                    onChange={(item) => {
                                                        setFieldValue("date", item.format("YYYY-MM-DD"))
                                                    }}
                                                    dateFormat={"MMMM Do YYYY"}
                                                    timeFormat={false}
                                                />
                                            </FormControl>
                                        )}
                                        />
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <Field name="stockReleaseIssues" render={({field}) => (
                                            <CustomInput
                                                labelText="Stock release issues"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                </GridContainer>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="stockJamUpOfPlant" render={({field}) => (
                                            <CustomInput
                                                labelText="Stock Jam-up of plant"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="rmStockout" render={({field}) => (
                                            <CustomInput
                                                labelText="RM Stockout"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="stockStoredIncorectly" render={({field}) => (
                                            <CustomInput
                                                labelText="Stock Stored incorrectly"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                </GridContainer>


                                <Button color="primary" type="submit"
                                        disabled={isSubmitting}>{this.props.initialData ? "Update Item" : "Save Item"}</Button>

                            </GridItem>

                        </GridContainer>
                    </form>
                )}/>)

    }

}