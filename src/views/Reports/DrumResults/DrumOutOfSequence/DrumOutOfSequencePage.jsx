import React from "react";
import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import DrumOutOfSequenceTable from "views/Reports/DrumResults/DrumOutOfSequence/DrumOutOfSequenceTable";
import DrumOutOfSequenceForm from "views/Reports/DrumResults/DrumOutOfSequence/DrumOutOfSequenceForm";

@inject("drumOutOfSequence")
@observer
export default class DrumOutOfSequencePage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.drumOutOfSequence;
        this.components = {
            table: DrumOutOfSequenceTable,
            edit: DrumOutOfSequenceForm
        };
    }
}

