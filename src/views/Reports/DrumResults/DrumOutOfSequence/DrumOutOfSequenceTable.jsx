import DefaultAgTable from 'components/Table/DefaultAgTable';

function isColumnHidden(field) {
	let store = JSON.parse(localStorage.getItem('hiddenColumns')) || {
		DrumOutOfSequenceTable: {}
	};
	if (store.DrumOutOfSequenceTable) {
		return store.DrumOutOfSequenceTable[field] || false;
	}
	return false;
}

const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem('isColumnWidth')) || {
		[componentName]: {}
	};
	if (store[componentName]) {
		let width = store[componentName][field];
		width = parseInt(width, 10);
		return width;
	}
};

export default class DrumOutOfSequenceTable extends DefaultAgTable {
	columns = [
		{
			headerName: 'Date',
			field: 'date',
			suppressSizeToFit: true,
			width: isColumnWidth('date', 'DrumOutOfSequenceTable'),
			hide: isColumnHidden('date')
		},
		{
			headerName: 'Stock release issues',
			field: 'stockReleaseIssues',
			hide: isColumnHidden('stockReleaseIssues')
		},
		{
			headerName: 'Stock Jam-up of plant',
			suppressSizeToFit: true,
			width: isColumnWidth('stockJamUpOfPlant', 'DrumOutOfSequenceTable'),
			field: 'stockJamUpOfPlant',
			hide: isColumnHidden('stockJamUpOfPlant')
		},
		{
			headerName: 'RM Stockout',
			suppressSizeToFit: true,
			width: isColumnWidth('rmStockout', 'DrumOutOfSequenceTable'),
			field: 'rmStockout',
			hide: isColumnHidden('rmStockout')
		},
		{
			headerName: 'Stock Stored incorrectly',
			field: 'stockStoredIncorectly',
			suppressSizeToFit: true,
			width: isColumnWidth('stockStoredIncorectly', 'DrumOutOfSequenceTable'),
			hide: isColumnHidden('stockStoredIncorectly')
		}
	];

	constructor(props) {
		super(props);
		//this.defaultStore = this.props.drumLoadingReport;
		this.componentName = 'DrumOutOfSequenceTable';
		this.printHeader = 'Drum Out Of Sequence Table';
	}

	onGridReady = params => {
		this.gridApi = params.api;
		this.gridApi.sizeColumnsToFit();
	};
}
