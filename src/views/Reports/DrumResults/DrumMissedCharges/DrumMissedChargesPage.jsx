import React from 'react';
import { inject, observer } from 'mobx-react';
import DefaultPage from 'views/DefaultPage';
import DrumMissedChargesTable from 'views/Reports/DrumResults/DrumMissedCharges/DrumMissedChargesTable';
import DrumMissedChargesForm from 'views/Reports/DrumResults/DrumMissedCharges/DrumMissedChargesForm';

@inject('drumMissedCharges')
@observer
export default class DrumMissedChargesPage extends DefaultPage {
	constructor(props) {
		super(props);
		this.defaultStore = this.props.drumMissedCharges;
		this.components = {
			table: DrumMissedChargesTable,
			edit: DrumMissedChargesForm
		};
	}
}
