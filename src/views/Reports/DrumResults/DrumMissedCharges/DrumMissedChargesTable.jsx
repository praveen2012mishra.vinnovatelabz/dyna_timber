import DefaultAgTable from 'components/Table/DefaultAgTable';

function isColumnHidden(field) {
	let store = JSON.parse(localStorage.getItem('hiddenColumns')) || {
		DrumMissedChargesTable: {}
	};
	if (store.DrumMissedChargesTable) {
		return store.DrumMissedChargesTable[field] || false;
	}
	return false;
}

const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem('isColumnWidth')) || {
		[componentName]: {}
	};
	if (store[componentName]) {
		let width = store[componentName][field];
		width = parseInt(width, 10);
		return width;
	} else {
		return 200;
	}
};

export default class DrumMissedChargesTable extends DefaultAgTable {
	columns = [
		{
			headerName: 'Date',
			field: 'date',
			hide: isColumnHidden('date'),
			width: isColumnWidth('date', 'DrumMissedChargesTable'),
			valueGetter: params => {
				return params.data.date;
			}
		},
		{
			headerName: 'Other',
			field: 'other',
			hide: isColumnHidden('other'),
			width: isColumnWidth('other', 'DrumMissedChargesTable'),
			valueGetter: params => {
				return params.data.other;
			}
		},
		{
			headerName: 'Interruptions from others',
			field: 'interruptionsFromOthers',
			hide: isColumnHidden('interruptionsFromOthers'),
			width: isColumnWidth('interruptionsFromOthers', 'DrumMissedChargesTable'),
			valueGetter: params => {
				return params.data.interruptionsFromOthers;
			}
		},
		{
			headerName: 'Flow Meter',
			field: 'flowMeter',
			hide: isColumnHidden('flowMeter'),
			width: isColumnWidth('flowMeter', 'DrumMissedChargesTable'),
			valueGetter: params => {
				return params.data.flowMeter;
			}
		},
		{
			headerName: 'Cleaning Tmt Plant',
			field: 'cleaningTmtPlant',
			hide: isColumnHidden('cleaningTmtPlant'),
			width: isColumnWidth('cleaningTmtPlant', 'DrumMissedChargesTable'),
			valueGetter: params => {
				return params.data.cleaningTmtPlant;
			}
		}
	];
	constructor(props) {
		super(props);
		//this.defaultStore = this.props.drumLoadingReport;
		this.componentName = 'DrumMissedChargesTable';
		this.printHeader = 'Drum Missed Charges Table';
	}

	onGridReady = params => {
		this.gridApi = params.api;
		this.gridApi.sizeColumnsToFit();
	};
}
