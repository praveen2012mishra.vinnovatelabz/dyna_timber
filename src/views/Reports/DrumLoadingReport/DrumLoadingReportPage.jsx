import React from 'react';
import { inject, observer } from 'mobx-react';
import DefaultPage from 'views/DefaultPage';
import DrumLoadingReportTable from 'views/Reports/DrumLoadingReport/DrumLoadingReportTable';

@inject('drumLoadingReport')
@observer
export default class DrumLoadingReportPage extends DefaultPage {
	constructor(props) {
		super(props);
		this.defaultStore = this.props.drumLoadingReport;
		this.components = {
			table: DrumLoadingReportTable,
			edit: null
		};
	}
}
