import React from "react";
import { inject, observer } from "mobx-react";
import * as _ from "lodash";
import { get } from "lodash";
import DefaultAgTable from "components/Table/DefaultAgTable";
import { DrumLoadingReportClient } from "../../../api/drumLoadingReport";
import InventoryClient from "../../../api/inventory";

function numberToColor(val) {
  if (val > 150) {
    return "black";
  } else if (val > 100) {
    return "green";
  } else if (val > 50) {
    return "yellow";
  } else if (val >= 0) {
    return "red";
  } else {
    return "#fff";
  }
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+45;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

const setPrecision = (val, precision) => val.toFixed(precision);

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    DrumLoadingReportTable: {}
  };
  if (store.DrumLoadingReportTable) {
    return store.DrumLoadingReportTable[field] || false;
  }
  return false;
}

const getNewTargetInventory = (params, props) => {
  let sku = params.data.inventory.sku;
  let buffAdj = params.data.inventory.bufferAdjustment;
  let targetValue;
  let newObject = {};
  if (buffAdj && buffAdj > 0) {
    targetValue = Math.round((params.data.currentTargetInv / 100) * buffAdj);
    newObject = { [sku]: targetValue };
    return targetValue;
  } else {
    targetValue = params.data.currentTargetInv;
    newObject = { [sku]: targetValue };
    return targetValue;
  }
};

const getChargeDrumLoad = (params, props) => {
  let currentTargetInv = getNewTargetInventory(params, props);
  let currentAct = params.data.currentAct;
  let currentWIP = params.data.currentWIP;
  let packsPerCharge = params.data.packsPerChge;
  let chargesToLoadInDrum;
  if (packsPerCharge && packsPerCharge > 0) {
    chargesToLoadInDrum =
      (currentTargetInv - currentAct - currentWIP) / packsPerCharge;
    return Math.floor(
      chargesToLoadInDrum ? +setPrecision(chargesToLoadInDrum, 2) : 0
    );
  } else {
    chargesToLoadInDrum = params.data.chargesToLoadInDrum;
    return chargesToLoadInDrum ?+setPrecision(chargesToLoadInDrum, 2):0
  }
};

const yetToLoadInDrum =(params,props)=>{
  let yetToLoad;
  let chargesLoaded=params.data.chargesLoaded
  let chargesToLoadInDrum=getChargeDrumLoad(params,props);
  yetToLoad=chargesToLoadInDrum-chargesLoaded
  //console.log(yetToLoad,chargesToLoadInDrum,chargesLoaded,'------------------>');
  
  return Math.floor(
    yetToLoad ? +setPrecision(yetToLoad, 2) : 0
  );
}

//to be removed
function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

const updatePacksPerChge = (inventory,props) => {
  //console.log("------------------------------->", inventory);
  InventoryClient.update(inventory)
    .then(res => {
      //props.drumLoadingReport.setUpdateObject(res.data);
    })
    .catch(err => console.log("error while updating inventory \n", err));
};

const onCellValueChangedPacksPerChge = (params, gridApi,props) => {
  let store = JSON.parse(sessionStorage.getItem("packsPerChge")) || {};
  store = {
    ...store,
    [get(params, "data.inventory.id")]: params.newValue
  };
  sessionStorage.setItem("packsPerChge", JSON.stringify(store));
  params.data.packsPerChge = params.newValue;
  params.data.inventory.dlrPackPerChg = params.newValue;
  updatePacksPerChge(Object.assign({}, params.data.inventory),props);
  gridApi.refreshCells({ force: true });
};

const valueGetterPacksPerChge = params => {
  let store = JSON.parse(sessionStorage.getItem("packsPerChge")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.inventory.id")]]))) {
    return parseInt(store[[get(params, "data.inventory.id")]]);
  }
  return params.data.packsPerChge;
};

const valueSetterPacksPerChge = params => {
  if (parseInt(params.newValue) === parseInt(params.data.packsPerChge))
    return false;
  let store = JSON.parse(sessionStorage.getItem("packsPerChge")) || {};
  store = {
    ...store,
    [get(params, "data.inventory.id")]: params.newValue
  };
  sessionStorage.setItem("packsPerChge", JSON.stringify(store));
  params.data.packsPerChge = params.newValue;
  return true;
};

const cellRendererPacksPerChge = params => {
  let store = JSON.parse(sessionStorage.getItem("packsPerChge")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.inventory.id")]]))) {
    return parseInt(store[[get(params, "data.inventory.id")]]);
  }

  return params.data.packsPerChge;
};

let targetValueObject = {};
@inject("drumLoadingReport")
@observer
export default class DrumLoadingReportTable extends DefaultAgTable {
  columns = [
    {
      headerName: "SKU",
      sort: "asc",
      field: "sku",      
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "inventory.sku","SKU");
        return params.data.inventory.sku;
      },      
      width: isColumnWidth("sku", "DrumLoadingReportTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("sku")
    },
    {
      suppressSizeToFit: true,
      headerName: "Product Name",
      width: isColumnWidth("productName", "DrumLoadingReportTable"),
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "inventory.productName","Product Name");
        return params.data.inventory.productName;
      },
      field: "inventory.productName",
      hide: isColumnHidden("inventory.productName")
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("inventory.length", "DrumLoadingReportTable"),
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "inventory.length","Length");
        return params.data.inventory.length;
      },
      headerName: "Length",
      field: "inventory.length",
      hide: isColumnHidden("inventory.length")
    },

    {
      suppressSizeToFit: true,
      width: isColumnWidth("packsPerChge", "DrumLoadingReportTable"),
      headerName: "Pcks/chge",
      type: "numericColumn",
      field: "packsPerChge",
      hide: isColumnHidden("packsPerChge"),
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: params => {
        return onCellValueChangedPacksPerChge(params, this.gridApi,this.props);
      },
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "packsPerChge","Pcks/chge");
        return valueGetterPacksPerChge(params);
      },
      valueSetter: params => {
        return valueSetterPacksPerChge(params);
      },
      cellRenderer: params => {
        return cellRendererPacksPerChge(params);
      }
    },

    {
      suppressSizeToFit: true,
      width: isColumnWidth("currentTargetInv", "DrumLoadingReportTable"),
      headerName: "Target Inv.",
      hide: isColumnHidden("currentTargetInv"),
      field: "currentTargetInv",
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "currentTargetInv","Target Inv.");
        return getNewTargetInventory(params, this.props);
      }
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("currentAct", "DrumLoadingReportTable"),
      headerName: "Current Act.",
      field: "currentAct",
      hide: isColumnHidden("currentAct"),
      valueGetter: params =>{
        getWidthColumnValue(params, this.componentName, "currentAct","Current Act.");
        return params.data.currentAct ? +setPrecision(params.data.currentAct, 2) : params.data.currentAct}
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("currentWIP", "DrumLoadingReportTable"),
      headerName: "Current WIP",
      field: "currentWIP",
      hide: isColumnHidden("currentWIP"),
      valueGetter: params =>{
        getWidthColumnValue(params, this.componentName, "currentWIP","Current WIP");
        return params.data.currentWIP ? +setPrecision(params.data.currentWIP, 2) : params.data.currentWIP}
    },
    {
      suppressSizeToFit: true,
      width:131 ,
      //isColumnWidth("currentActPlusWIP", "DrumLoadingReportTable"),
      headerName: "Act. + WIP col.",
      field: "currentActPlusWIP",
      filter: "actPlusWipColorFilter",
      filterParams: {
        field: "currentActPlusWIP"
      },
      hide: isColumnHidden("currentActPlusWIP"),
      cellRenderer: function(params) {
        //getWidthColumnValue(params, this.componentName, "currentActPlusWIP");
        return "";
      },
      cellStyle: function(params) {
        let currentTargetInv = getNewTargetInventory(params);
        let currentAct = params.data.currentAct;
        let currentWIP = params.data.currentWIP;
        let actWIPCol = (currentAct + currentWIP) / currentTargetInv;
        actWIPCol = Math.round(actWIPCol * 100);
        let color = numberToColor(actWIPCol);
        return {
          "background-color": color
        };
      },
      filter: "actPlusWipColorFilter"
    },
    {
      suppressSizeToFit: true,
      width: 95,
      //isColumnWidth("currentActCol", "DrumLoadingReportTable"),
      headerName: "Act. Col.",
      field: "currentActCol",
      filter: "actPlusWipColorFilter",
      hide: isColumnHidden("currentActCol"),
      filterParams: {
        field: "currentActCol"
      },
      cellRenderer: function(params) {
        //getWidthColumnValue(params, this.componentName, "currentActCol");
        return "";
      },
      cellStyle: function(params) {
        let currentAct = params.data.currentAct;
        let currentTargetInv = getNewTargetInventory(params);
        let actWIPCol = currentAct / currentTargetInv;
        actWIPCol = Math.round(actWIPCol * 100);
        let color = numberToColor(actWIPCol);
        return {
          "background-color": color
        };
      },
      
      filter: "actColorFilter"
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("chargesToLoadInDrum", "DrumLoadingReportTable"),
      headerName: "Charges To load in Drum",
      field: "chargesToLoadInDrum",
      hide: isColumnHidden("chargesToLoadInDrum"),
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "chargesToLoadInDrum","Charges To load in Drum");
        return getChargeDrumLoad(params, this.props);
      }
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("chargesLoaded", "DrumLoadingReportTable"),
      headerName: "Charges loaded",
      field: "chargesLoaded",
      hide: isColumnHidden("chargesLoaded"),
      valueGetter: params =>{ 
        getWidthColumnValue(params, this.componentName, "chargesLoaded","Charges loaded");
        return params.data.chargesLoaded}
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("yetToLoadInDrum", "DrumLoadingReportTable"),
      headerName: "Yet to load in Drum",
      field: "yetToLoadInDrum",
      hide: isColumnHidden("yetToLoadInDrum"),
      valueGetter: params =>{
        getWidthColumnValue(params, this.componentName, "yetToLoadInDrum","Yet to load in Drum");
        return yetToLoadInDrum(params, this.props)
      } /*params.data.yetToLoadInDrum*/
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("scheduledToMake", "DrumLoadingReportTable"),
      headerName: "Scheduled to Make",
      field: "scheduledToMake",
      hide: isColumnHidden("scheduledToMake"),
      valueGetter: params => {
        getWidthColumnValue(params, this.componentName, "scheduledToMake","Scheduled to Make");
        return params.data.scheduledToMake;
      }
    },
    {
      suppressSizeToFit: true,
      width: isColumnWidth("whatToMake", "DrumLoadingReportTable"),
      headerName: "What to Make",
      field: "whatToMake",
      hide: isColumnHidden("whatToMake"),
      valueGetter: params =>{
        getWidthColumnValue(params, this.componentName, "whatToMake","What to Make");
        return params.data.whatToMake ? +setPrecision(params.data.whatToMake, 2) : ""}
    }
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.drumLoadingReport;
    this.componentName = "DrumLoadingReportTable";
    this.printHeader = "Drum Loading Report (packs)";
  }

  onCellValueChanged = async event => {
    const inventory = event.data;
    //await InventoryClient.update(inventory);
    //await DrumLoadingReportClient.update(inventory);
    //this.defaultStore.fetchAll(false);
  };

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}
