import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    MachinesTable: {},
  };
  if (store.MachinesTable) {
    return store.MachinesTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    let width = store[componentName][field];
    width = parseInt(width, 10);
    width = width + 45;
    //console.log(width,field,'============>');
    return width;
  }
};

//take text width
function textWidth(text, fontProp, type) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  text = text ? text.toString() : text;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;
  //console.log(result,'====>');
  result = result;
  //console.log(result);

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (params, componentName, field, headerName) => {
  let data = params.data;
  //console.log(data,componentName,field);
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };

  store[componentName] = {
    ...store[componentName],
  };
  let storeWidth;
  let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
  let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
  let arrayWidth = Object.assign([]);
  arrayWidth.push(fieldWidth);
  arrayWidth.push(valueWidth);
  //console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);

  if (Object.keys(store[componentName]).length === 0) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (!store[componentName][field]) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (store[componentName][field] != Math.max(...arrayWidth)) {
    arrayWidth.push(store[componentName][field]);
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else {
    //console.log("No change in width resize");
  }
};

@inject("machines")
@observer
class MachinesTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Name",
      width: isColumnWidth("name", "MachinesTable"),
      suppressSizeToFit: true,
      field: "name",
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "name","Name");
        return params.data.name;
      },
      hide: isColumnHidden("name"),
    },
    {
      headerName: "Number of Staff",
      field: "numberOfStaff",
      width: isColumnWidth("numberOfStaff", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "numberOfStaff","Number of Staff");
        return params.data.numberOfStaff;
      },
      hide: isColumnHidden("numberOfStaff"),
    },
    {
      headerName: "Standard Hours",
      field: "standardHours",
      width: isColumnWidth("standardHours", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "standardHours","Standard Hours");
        return params.data.standardHours;
      },
      hide: isColumnHidden("standardHours"),
    },
    {
      headerName: "Standard OT Hours",
      field: "standardOTHours",
      width: isColumnWidth("standardOTHours", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "standardOTHours","Standard OT Hours");
        return params.data.standardOTHours;
      },
      hide: isColumnHidden("standardOTHours"),
    },
    {
      headerName: "Beast Mode Hours",
      field: "beastModeHours",
      width: isColumnWidth("beastModeHours", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "beastModeHours","Beast Mode Hours");
        return params.data.beastModeHours;
      },
      hide: isColumnHidden("beastModeHours"),
    },
    {
      headerName: "Benchmark m3/hour",
      field: "benchmarkM3perHours",
      width: isColumnWidth("benchmarkM3perHours", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "benchmarkM3perHours","Benchmark m3/hour");
        return params.data.benchmarkM3perHours;
      },
      hide: isColumnHidden("benchmarkM3perHours"),
    },
    {
      headerName: "M/3 Per Man Hour",
      field: "mPer3PerManHour",
      width: isColumnWidth("mPer3PerManHour", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "mPer3PerManHour","M/3 Per Man Hour");
        return params.data.mPer3PerManHour;
      },
      hide: isColumnHidden("mPer3PerManHour"),
    },
    {
      headerName: "BM Formula",
      field: "bmformula",
      width: isColumnWidth("bmformula", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "bmformula","BM Formula");
        return params.data.bmformula;
      },
      hide: isColumnHidden("bmformula"),
    },
    {
      headerName: "Rope Length Days",
      field: "ropeLengthDays",
      width: isColumnWidth("ropeLengthDays", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "ropeLengthDays","Rope Length Days");
        return params.data.ropeLengthDays;
      },
      hide: isColumnHidden("ropeLengthDays"),
    },
    {
      headerName: "Rope Length Reliability",
      field: "ropeLengthReliability",
      width: isColumnWidth("ropeLengthReliability", "MachinesTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "ropeLengthReliability",
          "Rope Length Reliability"
        );
        return params.data.ropeLengthReliability;
      },
      hide: isColumnHidden("ropeLengthReliability"),
    },
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.machines;
    this.componentName = "MachinesTable";
    this.printHeader = "Machines Table";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}

export default MachinesTable;
