import React from 'react'
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import {Field, Formik} from "formik";
import {inject, observer} from "mobx-react";
import MachinesClient from "api/machines";

@inject("notifications", "machines")
@observer
class MachinesForm extends React.Component {

    render() {
        const {notifications, machines, initialData} = this.props;

        return (
            <Formik
                enableReinitialize
                initialValues={initialData ? initialData : {}}
                onSubmit={async (values, actions) => {
                    try {
                        if (values.id)
                            await MachinesClient.update(values);
                        else
                            await MachinesClient.save(values);
                        notifications.enqueueSnackbar({message: 'Saved.'});
                        machines.fetchAll();
                        machines.selectItem(null);
                        machines.selectItemsTab()
                    } catch (err) {
                        console.error(err);
                        notifications.enqueueSnackbar({message: 'Saving failed.', options: {variant: 'error'}});
                    } finally {
                        actions.setSubmitting(false)
                    }
                }}
                render={({
                             handleSubmit,
                             isSubmitting,
                         }) => (
                    <form onSubmit={handleSubmit}>
                        <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="name" render={({field}) => (
                                            <CustomInput
                                                labelText="Name"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="standardHours" render={({field}) => (
                                            <CustomInput
                                                labelText="Standard Hours"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="standardOTHours" render={({field}) => (
                                            <CustomInput
                                                labelText="Standard OT Hours"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="beastModeHours" render={({field}) => (
                                            <CustomInput
                                                labelText="Beast Mode Hours"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                </GridContainer>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="benchmarkM3perHours" render={({field}) => (
                                            <CustomInput
                                                labelText="Benchmark m3/hours"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="bmformula" render={({field}) => (
                                            <CustomInput
                                                labelText="BM Formula"
                                                formControlProps={{
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="ropeLengthDays" render={({field}) => (
                                            <CustomInput
                                                labelText="Rope Length Days"
                                                formControlProps={{
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={3}>
                                        <Field name="ropeLengthReliability" render={({field}) => (
                                            <CustomInput
                                                labelText="Rope Length Reliability"
                                                formControlProps={{
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                </GridContainer>

                                <Button color="primary" type="submit"
                                        disabled={isSubmitting}>{initialData ? "Update Item" : "Save Item"}</Button>

                            </GridItem>

                        </GridContainer>
                    </form>
                )}/>)

    }

}

export default MachinesForm;