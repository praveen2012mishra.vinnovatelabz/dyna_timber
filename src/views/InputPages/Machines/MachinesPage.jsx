import {inject, observer} from "mobx-react";
import MachinesForm from "views/InputPages/Machines/MachinesForm";
import MachinesTable from "views/InputPages/Machines/MachinesTable";
import DefaultPage from "views/DefaultPage";


@inject("machines")
@observer
export default class MachinesPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.machines;
        this.components = {
            table: MachinesTable,
            edit: MachinesForm
        };
    }
}
