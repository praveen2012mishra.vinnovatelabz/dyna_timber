// import React from "react";
// import GridContainer from "components/Grid/GridContainer";
// import GridItem from "components/Grid/GridItem";
// import CustomInput from "components/CustomInput/CustomInput";
// import Button from "components/CustomButtons/Button";
// import { Field, Formik } from "formik";
// import { inject, observer } from "mobx-react";
// import MaterialsStocktakeClient from "api/materialsStocktake";
// import FormControl from "@material-ui/core/FormControl";
// import InputLabel from "@material-ui/core/InputLabel";
// import Select from "@material-ui/core/Select";
// import MenuItem from "@material-ui/core/MenuItem";
// import withStyles from "@material-ui/core/styles/withStyles";
// import extendedFormsStyle from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.jsx";
// import IntegrationReactSelect from "components/Autocomplete/Autocomplete";
// import * as _ from "lodash";

// const getOrderSize=(ordersizeArray)=>{
//     let arrayOfOrderSize=[]
//     ordersizeArray.forEach((ele) => {
//       arrayOfOrderSize = _.union(arrayOfOrderSize, [ele.label]);
//     });
//     arrayOfOrderSize=arrayOfOrderSize.map((ele)=>{
//       return {label:ele,value:'ele'}
//     })
//     return arrayOfOrderSize
// }

// @inject("notifications", "materials", "materialsStocktake")
// @observer
// class RawMaterialInventoryStocktakeForm extends React.Component {
//   componentDidMount() {
//     this.props.materials.fetchAll();
//   }

//   render() {
//     const {
//       notifications,
//       materialsStocktake,
//       materials,
//       classes
//     } = this.props;

//     const categoryArray = materials.data.map((suggestion) => ({
//       value: suggestion,
//       label: suggestion.category,
//     }));
//     let categorysize=getOrderSize(categoryArray)

//     const materialArray = materials.data.map((suggestion) => ({
//       value: suggestion,
//       label: suggestion.code,
//     }));
//     let materialCodes=getOrderSize(materialArray)

//     // const materialCodes = materials.data
//     //   ? materials.data.map(material => {
//     //       return {
//     //         value: material.code,
//     //         label: material.code
//     //       };
//     //     })
//     //   : [];
//     return (
//       <Formik
//         enableReinitialize
//         initialValues={this.props.initialData ? this.props.initialData : {}}
//         onSubmit={async (values, actions) => {
//           try {

//             if (_.get(values, "code.label")) {
//               values.category = values.category?values.category.label:'';
//               values.code =values.code? values.code.label:'';
//             }
//             if (values.id) await MaterialsStocktakeClient.update(values);
//             else await MaterialsStocktakeClient.save(values);
//             notifications.enqueueSnackbar({ message: "Saved." });
//             materialsStocktake.fetchAll();
//             materialsStocktake.selectItem(null);
//             materialsStocktake.selectItemsTab();
//           } catch (err) {
//             console.error(err);
//             notifications.enqueueSnackbar({
//               message: "Saving failed.",
//               options: { variant: "error" }
//             });
//           } finally {
//             actions.setSubmitting(false);
//           }
//         }}
//         render={({ handleSubmit, isSubmitting, setFieldValue }) => (
//           <form onSubmit={handleSubmit}>
//             <GridContainer>
//               <GridItem xs={12} sm={12} md={12}>
//                 <GridContainer>
//                   <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="code"
//                       render={({ field }) => (
//                         // <FormControl
//                         //   fullWidth
//                         //   className={classes.selectFormControl}
//                         // >
//                           <IntegrationReactSelect
//                             placeholder={`${field.value}`}
//                             label="Code"
//                             inputProps={{
//                               ...field,
//                               value: field.value || "",
//                             }}
//                             suggestions={materialCodes}
//                             value={field.value || ""}
//                             onChange={item => {
//                               console.log("on change", item.value,field.value);
//                               setFieldValue("code", item);
//                             }}
//                           ></IntegrationReactSelect>
//                         //   <CustomInput
//                         //   labelText="Code"
//                         //   suggestions={materialCodes}
//                         //   formControlProps={{
//                         //     fullWidth: true,
//                         //   }}
//                         //   inputProps={{
//                         //     //type: "number",
//                         //     step: "any",
//                         //     ...field,
//                         //     value: field.value || "",
//                         //   }}
//                         // />
//                           // {/* <InputLabel
//                           //                           htmlFor="code"
//                           //                           className={classes.selectLabel}
//                           //                       >
//                           //                           Choose Code
//                           //                       </InputLabel>
//                           //                       <Select
//                           //                           MenuProps={{
//                           //                               className: classes.selectMenu
//                           //                           }}
//                           //                           classes={{
//                           //                               select: classes.select
//                           //                           }}
//                           //                           value={field.value || ''}
//                           //                           onChange={(item) => {
//                           //                               setFieldValue("code", item.target.value)
//                           //                           }}
//                           //                           inputProps={{
//                           //                               id: "code"
//                           //                           }}
//                           //                       >
//                           //                           <MenuItem
//                           //                               disabled
//                           //                               classes={{
//                           //                                   root: classes.selectMenuItem
//                           //                               }}
//                           //                           >
//                           //                               Choose Code
//                           //                           </MenuItem>
//                           //                           {
//                           //                               materials.data.map(material => (
//                           //                                   <MenuItem key={material.code}
//                           //                                             classes={{
//                           //                                                 root: classes.selectMenuItem,
//                           //                                                 selected: classes.selectMenuItemSelected
//                           //                                             }}
//                           //                                             value={material.code}
//                           //                                   >
//                           //                                       {material.code}
//                           //                                   </MenuItem>
//                           //                               ))
//                           //                           }
//                           //                       </Select> */}
//                           // </FormControl>
                       
//                       )}
//                     />
//                   </GridItem>
//                   <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="category"
//                       render={({ field }) => (
//                         // <CustomInput
//                         //   labelText="Category"
//                         //   formControlProps={{
//                         //     fullWidth: true
//                         //   }}
//                         //   inputProps={{
//                         //     //type: "number",
//                         //     step: "any",
//                         //     ...field,
//                         //     value: field.value || ""
//                         //   }}
//                         // />
//                         <IntegrationReactSelect
//                         placeholder={`${field.value}`}
//                         label="Category"
//                         suggestions={categorysize}
//                         inputProps={{
//                           ...field,
//                           value: field.value || "",
//                         }}
//                         value={field.value || {}}
//                         onChange={(item) => {
//                           setFieldValue("category", item);
                         
//                         }}
//                       ></IntegrationReactSelect>
//                       )}
//                     />
//                   </GridItem>
//                 </GridContainer>
//                 <GridContainer>
//                   {/* <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="pcsPerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="Pcs/Pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             step: "any",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem> */}
//                   {/* <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="m3PerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="m3/pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem> */}
//                 </GridContainer>
//                 <GridContainer>               
//                 <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="week0PcsPerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="Week 0 Pcs\Pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             step: "any",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem>
//                   <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="week1PcsPerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="Week 1 Pcs\Pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             step: "any",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem>
//                   <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="week2PcsPerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="Week 2 Pcs\Pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             step: "any",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem>
//                   <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="week3PcsPerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="Week 3 Pcs\Pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             step: "any",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem>
//                   <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="week4PcsPerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="Week 4 Pcs\Pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             step: "any",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem>
//                   <GridItem xs={12} sm={12} md={6}>
//                     <Field
//                       name="week5PcsPerPack"
//                       render={({ field }) => (
//                         <CustomInput
//                           labelText="Week 5 Pcs\Pack"
//                           formControlProps={{
//                             fullWidth: true
//                           }}
//                           inputProps={{
//                             type: "number",
//                             step: "any",
//                             ...field,
//                             value: field.value || ""
//                           }}
//                         />
//                       )}
//                     />
//                   </GridItem>
//                 </GridContainer>
//                 <Button color="primary" type="submit" disabled={isSubmitting}>
//                   {this.props.initialData ? "Update Item" : "Save Item"}
//                 </Button>
//               </GridItem>
//             </GridContainer>
//           </form>
//         )}
//       />
//     );
//   }
// }

// export default withStyles(extendedFormsStyle)(
//   RawMaterialInventoryStocktakeForm
// );

import React from "react";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import { inject, observer } from "mobx-react";
import MaterialsStocktakeClient from "api/materialsStocktake";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import withStyles from "@material-ui/core/styles/withStyles";
import extendedFormsStyle from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.jsx";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";
import * as _ from "lodash";

const getOrderSize=(ordersizeArray)=>{
    let arrayOfOrderSize=[]
    ordersizeArray.forEach((ele) => {
      arrayOfOrderSize = _.union(arrayOfOrderSize, [ele.label]);
    });
    arrayOfOrderSize=arrayOfOrderSize.map((ele)=>{
      return {label:ele,value:'ele'}
    })
    return arrayOfOrderSize
}

@inject("notifications", "materials", "materialsStocktake")
@observer
class RawMaterialInventoryStocktakeForm extends React.Component {
  componentDidMount() {
    this.props.materials.fetchAll();
  }

  render() {
    const {
      notifications,
      materialsStocktake,
      materials,
      classes
    } = this.props;

    const categoryArray = materials.data.map((suggestion) => ({
      value: suggestion,
      label: suggestion.category,
    }));
    let categorysize=getOrderSize(categoryArray)

    const materialArray = materials.data.map((suggestion) => ({
      value: suggestion,
      label: suggestion.code,
    }));
    let materialCodes=getOrderSize(materialArray)

    // const materialCodes = materials.data
    //   ? materials.data.map(material => {
    //       return {
    //         value: material.code,
    //         label: material.code
    //       };
    //     })
    //   : [];
    return (
      <Formik
        enableReinitialize
        initialValues={this.props.initialData ? this.props.initialData : {}}
        onSubmit={async (values, actions) => {
          try {

            if (_.get(values, "code.label")) {
              values.category = values.category?values.category.label:'';
              //values.code =values.code? values.code.label:'';
            }
            if (_.get(values, "code.label")) {
              values.code = values.code.label;
            }
            if (values.id) await MaterialsStocktakeClient.update(values);
            else await MaterialsStocktakeClient.save(values);
            notifications.enqueueSnackbar({ message: "Saved." });
            materialsStocktake.fetchAll();
            materialsStocktake.selectItem(null);
            materialsStocktake.selectItemsTab();
          } catch (err) {
            console.error(err);
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" }
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, isSubmitting, setFieldValue }) => (
          <form onSubmit={handleSubmit}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="code"
                      render={({ field }) => (
                        // <FormControl
                        //   fullWidth
                        //   className={classes.selectFormControl}
                        // >
                          <IntegrationReactSelect
                            placeholder={`${field.value}`}
                            label="Code"
                            // inputProps={{
                            //   ...field,
                            //   value: field.value || "",
                            // }}
                            suggestions={materialArray}
                            value={field.value || ""}
                            onChange={item => {
                              console.log("on change", item.value,field.value);
                              setFieldValue("code", item);
                              setFieldValue("category", {
                                value: item.value,
                                label: item.value.category
                              });
                            }}
                          ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="category"
                      render={({ field }) => (
                        <IntegrationReactSelect
                        placeholder={`${field.value}`}
                        label="Category"
                        value={field.value || {}}
                        onChange={(item) => {
                          setFieldValue("category", item);
                          setFieldValue("code", {
                            value: item.value,
                            label: item.value.code
                          });
                        }}
                      ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer> 
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="week1NumberOfPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Week 1 Number Of Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="week2NumberOfPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Week 2 Number Of Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="week3NumberOfPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Week 3 Number Of Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="week4NumberOfPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Week 4 Number Of Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="week5NumberOfPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Week 5 Number Of Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="week6NumberOfPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Week 6 Number Of Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {this.props.initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}

export default withStyles(extendedFormsStyle)(
  RawMaterialInventoryStocktakeForm
);
