import React from 'react'
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import {Field, Formik} from "formik";
import {inject, observer} from "mobx-react";
import MaterialsStocktakeClient from "api/materialsStocktake";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import withStyles from "@material-ui/core/styles/withStyles";
import extendedFormsStyle from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.jsx";
import Datetime from "react-datetime";
import moment from "moment";

@inject("notifications", "materials", "materialsStocktake")
@observer
class WeekForm extends React.Component {

    render() {
        const {notifications, materialsStocktake} = this.props;
        const initialData = this.props.initialData ? this.props.initialData : {};
        initialData.weekStart = new moment().startOf('isoWeek');

        return (
            <Formik
                enableReinitialize
                initialValues={initialData}
                onSubmit={async (values, actions) => {
                    try {
                        if (values.id)
                            await MaterialsStocktakeClient.updateWeek(values);
                        else
                            //await MaterialsStocktakeClient.saveWeek(values);
                        notifications.enqueueSnackbar({message: 'Saved.'});
                        materialsStocktake.fetchAll();
                        materialsStocktake.selectItem(null);
                        materialsStocktake.selectItemsTab()
                    } catch (err) {
                        console.error(err);
                        notifications.enqueueSnackbar({message: 'Saving failed.', options: {variant: 'error'}});
                    } finally {
                        actions.setSubmitting(false)
                    }
                }}
                render={({
                             handleSubmit,
                             isSubmitting,
                             setFieldValue
                         }) => (
                    <form onSubmit={handleSubmit}>
                        <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <InputLabel>
                                        </InputLabel>
                                        <br/>
                                        <Field name="weekStart" render={({field}) => (
                                            <FormControl fullWidth>
                                                <Datetime
                                                    timeFormat={false}
                                                    isValidDate={(current) => current.day() === 1}
                                                    value={field.value}
                                                    dateFormat={"MMMM Do YYYY"}
                                                    onChange={(item) => {
                                                        setFieldValue("weekStart", item.format("YYYY-MM-DD"))
                                                    }}
                                                />
                                            </FormControl>
                                        )}
                                        />
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <Field name="packs" render={({field}) => (
                                            <CustomInput
                                                labelText="Packs"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}
                                        />
                                    </GridItem>
                                </GridContainer>

                                <Button color="primary" type="submit"
                                        disabled={isSubmitting}>{this.props.initialData ? "Update Item" : "Save Item"}</Button>

                            </GridItem>

                        </GridContainer>
                    </form>
                )}/>)

    }

}

export default withStyles(extendedFormsStyle)(WeekForm);