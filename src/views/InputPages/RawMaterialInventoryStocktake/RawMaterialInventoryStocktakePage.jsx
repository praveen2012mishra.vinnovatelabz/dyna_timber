import {inject, observer} from "mobx-react";
import RawMaterialInventoryStocktakeTable from "views/InputPages/RawMaterialInventoryStocktake/RawMaterialInventoryStocktakeTable";
import RawMaterialInventoryStocktakeForm from "views/InputPages/RawMaterialInventoryStocktake/RawMaterialInventoryStocktakeForm";
import DefaultPage from "views/DefaultPage";
import {Edit, PlusOne, TableChart, AddAlarm} from "@material-ui/icons";
import React from "react";
import ActionIcon from "@material-ui/icons/PlusOne";
import WeekForm from "views/InputPages/RawMaterialInventoryStocktake/WeekForm";
import MaterialStocktakeClient from "api/materialsStocktake";

@inject("materialsStocktake")
@observer
export default class RawMaterialInventoryStocktakePage extends DefaultPage {

    aditionalActions = [
        <ActionIcon
          onClick={async () => {
            await MaterialStocktakeClient.newStocktake();
            this.props.materialsStocktake.fetchAll();
          }}
        />
      ];

    getTabs = (components, selectedItem) => {
        let TableTabComponent = components.table;
        let EditTabComponent = components.edit;

        let tabs = [
            {
                tabName: "Items",
                tabButton: "Items",
                tabIcon: TableChart,
                tabContent: (
                    <TableTabComponent/>
                )
            },
            {
                tabName: "New Item",
                tabButton: "New Item",
                tabIcon: PlusOne,
                tabContent: (
                    <EditTabComponent/>
                )
            }
        ];

        if (selectedItem !== null) {
            tabs.push({
                tabName: "Edit Item",
                tabButton: "Edit Item",
                tabIcon: Edit,
                tabContent:
                    (
                        <EditTabComponent initialData={selectedItem}/>
                    )
            });
            // tabs.push({
            //     tabName: "Add Week",
            //     tabButton: "Add Week",
            //     tabIcon: AddAlarm,
            //     tabContent:
            //         (
            //             <WeekForm initialData={{materialStocktakeId: selectedItem.id}}/>
            //         )
            // })
        }

        return tabs;
    };

    constructor(props) {
        super(props);
        this.defaultStore = this.props.materialsStocktake;
        this.components = {
            table: RawMaterialInventoryStocktakeTable,
            edit: RawMaterialInventoryStocktakeForm
        };
    }

}