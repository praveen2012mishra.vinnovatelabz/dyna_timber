

//new code
import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";
import moment from "moment";
import MaterialStocktakeClient from "../../../api/materialsStocktake";
import * as _ from "lodash";
import { get } from "lodash";
import MaterialStocktakeStore from "../../../api/materialsStocktake";

const onCellValueChangedWeek0NumberOfPacks = (params, gridApi, props) => {
  //debugger
  let store = JSON.parse(sessionStorage.getItem("week0NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week0NumberOfPacks", JSON.stringify(store));
  params.data.week0NumberOfPacks = params.newValue;
  updateWeek0NumberOfPacks(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWeek0NumberOfPacks = (week0NumberOfPacks, props) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeClient.update(week0NumberOfPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWeek0NumberOfPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week0NumberOfPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.week0NumberOfPacks;
};

const valueSetterWeek0NumberOfPacks = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.week0NumberOfPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("week0NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week0NumberOfPacks", JSON.stringify(store));
  params.data.week0NumberOfPacks = params.newValue;
  return true;
};

const cellRendererWeek0NumberOfPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week0NumberOfPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.week0NumberOfPacks;
};

const setPrecision = (val, precision) => val.toFixed(precision);

const onCellValueChangedWeek1PcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("week1NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week1NumberOfPacks", JSON.stringify(store));
  params.data.week1NumberOfPacks = params.newValue;
  updateWeek1PcsPerPack(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWeek1PcsPerPack = (week1NumberOfPacks, props) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeClient.update(week1NumberOfPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWeek1PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week1NumberOfPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.week1NumberOfPacks;
};

const valueSetterWeek1PcsPerPack = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.week1NumberOfPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("week1NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week1NumberOfPacks", JSON.stringify(store));
  params.data.week1NumberOfPacks = params.newValue;
  return true;
};

const cellRendererWeek1PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week1NumberOfPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.week1NumberOfPacks;
};

const onCellValueChangedWeek2PcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("week2NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week2NumberOfPacks", JSON.stringify(store));
  params.data.week2NumberOfPacks = params.newValue;
  updateWeek2PcsPerPack(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWeek2PcsPerPack = (week2NumberOfPacks, props) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeClient.update(week2NumberOfPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWeek2PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week2NumberOfPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.week2NumberOfPacks;
};

const valueSetterWeek2PcsPerPack = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.week2NumberOfPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("week2NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week2NumberOfPacks", JSON.stringify(store));
  params.data.week2NumberOfPacks = params.newValue;
  return true;
};

const cellRendererWeek2PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week2NumberOfPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.week2NumberOfPacks;
};

const onCellValueChangedWeek3PcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("week3NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week3NumberOfPacks", JSON.stringify(store));
  params.data.week3NumberOfPacks = params.newValue;
  updateWeek3PcsPerPack(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWeek3PcsPerPack = (week3NumberOfPacks, props) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeClient.update(week3NumberOfPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWeek3PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week3NumberOfPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.week3NumberOfPacks;
};

const valueSetterWeek3PcsPerPack = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.week3NumberOfPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("week3NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week3NumberOfPacks", JSON.stringify(store));
  params.data.week3NumberOfPacks = params.newValue;
  return true;
};

const cellRendererWeek3PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week3NumberOfPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.week3NumberOfPacks;
};

const onCellValueChangedWeek4PcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("week4NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week4NumberOfPacks", JSON.stringify(store));
  params.data.week4NumberOfPacks = params.newValue;
  updateWeek4PcsPerPack(Object.assign({}, params.data), props, gridApi);
  //console.log(gridApi);
  gridApi.refreshCells({ force: true });
};

const updateWeek4PcsPerPack = (week4NumberOfPacks, props, gridApi) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeClient.update(week4NumberOfPacks)
    .then((res) => {
      //debugger
      // var rowNode = this.gridApi.getRowNode(res.data.id);
      // console.log(rowNode);

      props.materialsStocktake.setUpdateObject(res.data);
      //gridApi.getRowNode(res.data.id).setData(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWeek4PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week4NumberOfPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.week4NumberOfPacks;
};

const valueSetterWeek4PcsPerPack = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.week4NumberOfPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("week4NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week4NumberOfPacks", JSON.stringify(store));
  params.data.week4NumberOfPacks = params.newValue;
  return true;
};

const cellRendererWeek4PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week4NumberOfPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.week4NumberOfPacks;
};

const onCellValueChangedWeek5PcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("week5NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week5NumberOfPacks", JSON.stringify(store));
  params.data.week5NumberOfPacks = params.newValue;
  updateWeek5PcsPerPack(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWeek5PcsPerPack = (week5NumberOfPacks, props) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeClient.update(week5NumberOfPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWeek5PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week5NumberOfPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.week5NumberOfPacks;
};

const valueSetterWeek5PcsPerPack = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.week5NumberOfPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("week5NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week5NumberOfPacks", JSON.stringify(store));
  params.data.week5NumberOfPacks = params.newValue;
  return true;
};

const cellRendererWeek5PcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week5NumberOfPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.week5NumberOfPacks;
};

const onCellValueChangedWeek6NumberOfPacks = (params, gridApi, props) => {
  //debugger
  let store = JSON.parse(sessionStorage.getItem("week6NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week6NumberOfPacks", JSON.stringify(store));
  params.data.week6NumberOfPacks = params.newValue;
  updateWeek6NumberOfPacks(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWeek6NumberOfPacks = (week6NumberOfPacks, props) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeClient.update(week6NumberOfPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWeek6NumberOfPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week6NumberOfPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.week6NumberOfPacks;
};

const valueSetterWeek6NumberOfPacks = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.week6NumberOfPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("week6NumberOfPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("week6NumberOfPacks", JSON.stringify(store));
  params.data.week6NumberOfPacks = params.newValue;
  return true;
};

const cellRendererWeek6NumberOfPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("week6NumberOfPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.week6NumberOfPacks;
};

const onCellValueChangedPcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pcsPerPack", JSON.stringify(store));
  params.data.pcsPerPack = params.newValue;
  updatePcsPerPack(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatePcsPerPack = (pcsPerPack, props) => {
  //console.log("------------------------------->", reliability);
  MaterialStocktakeStore.update(pcsPerPack)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterPcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.pcsPerPack;
};

const getM3PerPack = (params) => {
  //m3/pack = pcs/pack * (L/1000) * (W/1000) * (D/1000)  -> formula in front end after updating Pcs/Pack
  let pcs =
    valueGetterPcsPerPack(params) === params.data.pcsPerPack
      ? params.data.pcsPerPack
      : valueGetterPcsPerPack(params);
  let length = params.data.material.length;
  let width = params.data.material.width;
  let depth = params.data.material.depth;
  let m3Pack = pcs * (length / 1000) * (width / 1000) * (depth / 1000);
  console.log(m3Pack, "====================>");
  return m3Pack;
};

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    RawMaterialInventoryStocktakeTable: {},
  };
  if (store.RawMaterialInventoryStocktakeTable) {
    return store.RawMaterialInventoryStocktakeTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
    width = width+45;
    //debugger
	  console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

@inject("materialsStocktake")
@observer
export default class RawMaterialInventoryStocktakeTable extends DefaultAgTable {
  getColumns = () => {
    let columns = [
      {
        headerName: "Code",
        field: "code",
        suppressSizeToFit: true,
        width: isColumnWidth("code", "RawMaterialInventoryStocktakeTable",),
        //cellRenderer: "agGroupCellRenderer",
        valueGetter: params => {
          //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
          getWidthColumnValue(params, this.componentName, "code","Code");
          return params.data.code;
        },
        hide: isColumnHidden("code"),
      },
      {
        headerName: "Date",
        field: "date",
        valueGetter: (params) =>{
          getWidthColumnValue(params, this.componentName, "date","Date");
          return params.data.date ? new moment(params.data.date).format("MMMM Do YYYY") : new moment(params.data.date).format("MMMM Do YYYY")},
        chartDataType: "excluded",
        filter: "agDateColumnFilter",
        width: 125,/*isColumnWidth("date", "InventoryStocktakeTable"),*/
        suppressSizeToFit: true,
        hide: isColumnHidden("date"),
        filterParams: {
          comparator: function(filterLocalDateAtMidnight, cellValue) {
            var dateAsString = cellValue;
            if (dateAsString == null) return 0;
  
            // In the example application, dates are stored as dd/mm/yyyy
            // We create a Date object for comparison against the filter date
            let dateAsDate = new moment(dateAsString, "YYYY-MM-DD").toDate();
            // Now that both parameters are Date objects, we can compare
            if (dateAsDate < filterLocalDateAtMidnight) {
              return -1;
            } else if (dateAsDate > filterLocalDateAtMidnight) {
              return 1;
            } else {
              return 0;
            }
          },
        },
      },
      {
        headerName: "Pcs/Pack",
        field: "pcsPerPack",
        suppressSizeToFit: true,
        width: isColumnWidth(
          "pcsPerPack",
          "RawMaterialInventoryStocktakeTable"
        ),valueGetter: params => {
          //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
          getWidthColumnValue(params, this.componentName, "pcsPerPack","Pcs/Pack");
          return params.data.pcsPerPack;
        },
        hide: isColumnHidden("pcsPerPack"),
      },
      {
        headerName: "m3/Pack",
        field: "m3PerPack",
        suppressSizeToFit: true,
        width: isColumnWidth("m3PerPack", "RawMaterialInventoryStocktakeTable"),
        hide: isColumnHidden("m3PerPack"),
        valueGetter: (params) => {
          getWidthColumnValue(params, this.componentName, "m3PerPack","m3/Pack");
          return params.data.m3PerPack
            // ? +setPrecision(params.data.m3PerPack, 1)
            // : params.data.m3PerPack;
        },
      },
      {
        headerName: "Category",
        field: "category",
        suppressSizeToFit: true,
        valueGetter: params => {
          //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
          getWidthColumnValue(params, this.componentName, "category","Category");
          return params.data.category;
        },
        width: isColumnWidth("category", "RawMaterialInventoryStocktakeTable"),
        hide: isColumnHidden("category"),
      },
      {
        headerName: "End of Section m2",
        field: "endOfSectionM2",
        suppressSizeToFit: true,
        width: isColumnWidth(
          "endOfSectionM2",
          "RawMaterialInventoryStocktakeTable"
        ),
        suppressSizeToFit: true,
        hide: isColumnHidden("endOfSectionM2"),
        cellRenderer: (params) =>{
          getWidthColumnValue(params, this.componentName, "endOfSectionM2","End of Section m2");
          return params.value }
          //? +setPrecision(params.value, 5) : ""},
      },
    ];

    columns.push(      
      {
        headerName: `Week 6`,
        children: [
          {
            headerName: "Number Of Packs",
            field: "week6NumberOfPacks",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week6NumberOfPacks",
              "RawMaterialInventoryStocktakeTable"
            ),
            editable: true,
            // type: "numericColumn",
            cellEditor: "numericEditor",
            onCellValueChanged: (params) => {
              return onCellValueChangedWeek6NumberOfPacks(
                params,
                this.gridApi,
                this.props
              );
            },
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week6NumberOfPacks","Number Of Packs");
              return valueGetterWeek6NumberOfPacks(params);
            },
            valueSetter: (params) => {
              return valueSetterWeek6NumberOfPacks(params);
            },
            cellRenderer: (params) => {
              return cellRendererWeek6NumberOfPacks(params);
            },
            hide: isColumnHidden("week6NumberOfPacks"),
          },
        ],
      },
      {
        headerName: `Week 5`,
        children: [
          {
            headerName: "Number Of Packs",
            field: "week5NumberOfPacks",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week5NumberOfPacks",
              "RawMaterialInventoryStocktakeTable"
            ),
            editable: true,
            // type: "numericColumn",
            cellEditor: "numericEditor",
            onCellValueChanged: (params) => {
              return onCellValueChangedWeek5PcsPerPack(
                params,
                this.gridApi,
                this.props
              );
            },
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week5NumberOfPacks","Number Of Packs");
              return valueGetterWeek5PcsPerPack(params);
            },
            valueSetter: (params) => {
              return valueSetterWeek5PcsPerPack(params);
            },
            cellRenderer: (params) => {
              return cellRendererWeek5PcsPerPack(params);
            },
            hide: isColumnHidden("week5NumberOfPacks"),
          },
        ],
      },

      {
        headerName: `Week 4`,
        children: [
          {
            headerName: "Number Of Packs",
            field: "week4NumberOfPacks",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week4NumberOfPacks",
              "RawMaterialInventoryStocktakeTable"
            ),
            editable: true,
            // type: "numericColumn",
            cellEditor: "numericEditor",
            onCellValueChanged: (params) => {
              return onCellValueChangedWeek4PcsPerPack(
                params,
                this.gridApi,
                this.props
              );
            },
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week4NumberOfPacks","Number Of Packs");
              return valueGetterWeek4PcsPerPack(params);
            },
            valueSetter: (params) => {
              return valueSetterWeek4PcsPerPack(params);
            },
            cellRenderer: (params) => {
              return cellRendererWeek4PcsPerPack(params);
            },
            hide: isColumnHidden("week4NumberOfPacks"),
          },
        ],
      },
      {
        headerName: `Week 3`,
        children: [
          {
            headerName: "Number Of Packs",
            field: "week3NumberOfPacks",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week3NumberOfPacks",
              "RawMaterialInventoryStocktakeTable"
            ),
            editable: true,
            // type: "numericColumn",
            cellEditor: "numericEditor",
            onCellValueChanged: (params) => {
              return onCellValueChangedWeek3PcsPerPack(
                params,
                this.gridApi,
                this.props
              );
            },
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week3NumberOfPacks","Number Of Packs");
              return valueGetterWeek3PcsPerPack(params);
            },
            valueSetter: (params) => {
              return valueSetterWeek3PcsPerPack(params);
            },
            cellRenderer: (params) => {
              return cellRendererWeek3PcsPerPack(params);
            },
            hide: isColumnHidden("week3NumberOfPacks"),
          },
        ],
      },
      {
        headerName: `Week 2`,
        children: [
          {
            headerName: "Number Of Packs",
            field: "week2NumberOfPacks",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week2NumberOfPacks",
              "RawMaterialInventoryStocktakeTable"
            ),
            editable: true,
            // type: "numericColumn",
            cellEditor: "numericEditor",
            onCellValueChanged: (params) => {
              return onCellValueChangedWeek2PcsPerPack(
                params,
                this.gridApi,
                this.props
              );
            },
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week2NumberOfPacks","Number Of Packs");
              return valueGetterWeek2PcsPerPack(params);
            },
            valueSetter: (params) => {
              return valueSetterWeek2PcsPerPack(params);
            },
            cellRenderer: (params) => {
              return cellRendererWeek2PcsPerPack(params);
            },
            hide: isColumnHidden("week2NumberOfPacks"),
          },
        ],
      },

      {
        headerName: `Week 1`,
        children: [
          {
            headerName: "Number Of Packs",
            field: "week1NumberOfPacks",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week1NumberOfPacks",
              "RawMaterialInventoryStocktakeTable"
            ),
            editable: true,
            // type: "numericColumn",
            cellEditor: "numericEditor",
            onCellValueChanged: (params) => {
              return onCellValueChangedWeek1PcsPerPack(
                params,
                this.gridApi,
                this.props
              );
            },
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week1NumberOfPacks","Number Of Packs");
              return valueGetterWeek1PcsPerPack(params);
            },
            valueSetter: (params) => {
              return valueSetterWeek1PcsPerPack(params);
            },
            cellRenderer: (params) => {
              return cellRendererWeek1PcsPerPack(params);
            },
            
            hide: isColumnHidden("week1NumberOfPacks"),
          },
        ],
      },
    );

    columns.push(
      {
        headerName: `Week 5-6`,
        children: [
          {
            headerName: "M3",
            field: "week5To6M3",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week5To6M3",
              "RawMaterialInventoryStocktakeTable"
            ),
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week5To6M3",`Week 5-6`);
              return params.data.week5To6M3
                // ? +setPrecision(params.data.week5To6M3, 1)
                // : params.data.week5To6M3;
            },
            hide: isColumnHidden("week5To6M3"),
          },
        ],
      },
      {
        headerName: `Week 3-4`,
        children: [
          {
            headerName: "M3",
            field: "week3To4M3",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week3To4M3",
              "RawMaterialInventoryStocktakeTable"
            ),
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "week3To4M3","Week 3-4");
              return params.data.week3To4M3
                // ? +setPrecision(params.data.week3To4M3, 1)
                // : params.data.week3To4M3;
            },
            hide: isColumnHidden("week3To4M3"),
          },
        ],
      },
      {
        headerName: `Week 1-2`,        
        children: [
          {
            headerName: "M3",
            field: "week1To2M3",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "week1To2M3",
              "RawMaterialInventoryStocktakeTable"
            ),
            valueGetter: (params) =>{
              getWidthColumnValue(params, this.componentName, "week1To2M3","Week 1-2");
              return params.data.week1To2M3
                // ? +setPrecision(params.data.week1To2M3, 1)
                // : params.data.week1To2M3
              },
            hide: isColumnHidden("week1To2M3"),
          },
        ],
      },
      
      
           
      {
        headerName: `Week 1-6`,
        children: [
          {
            headerName: "Total M3",
            field: "totalM3",
            suppressSizeToFit: true,
            width: isColumnWidth(
              "totalM3",
              "RawMaterialInventoryStocktakeTable"
            ),
            valueGetter: (params) => {
              getWidthColumnValue(params, this.componentName, "totalM3","Total M3");
              return params.data.totalM3
                // ? +setPrecision(params.data.totalM3, 1)
                // : params.data.totalM3;
            },
            hide: isColumnHidden("totalM3"),
          },
        ],
      },
      
      // {
      //   headerName: "Total Act",
      //   field: "totalAct",
      //   suppressSizeToFit: true,
      //   width: isColumnWidth(
      //     "totalAct",
      //     "RawMaterialInventoryStocktakeTable"
      //   ),
      //   valueGetter: (params) => {
      //     getWidthColumnValue(params, this.componentName, "totalAct");
      //     return params.data.totalAct
      //       ? +setPrecision(params.data.totalAct, 1)
      //       : params.data.totalAct;
      //   },
      //   hide: isColumnHidden("totalAct"),
      // },
      
    );

    return columns;
  };

  columns = this.getColumns();

  

  //rowMultiSelectWithClick = true;
  rowSelection = "multiple";

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };

  onCellValueChanged = async (event) => {
    const inventory = event.data;
    //await InventoryClient.update(inventory);
    //await DrumLoadingReportClient.update(inventory);
    //this.defaultStore.fetchAll(false);
  };

  constructor(props) {
    super(props);
    this.defaultStore = this.props.materialsStocktake;
    this.componentName = "RawMaterialInventoryStocktakeTable";
    this.printHeader = "Raw Material Inventory Stocktake Table";
  }
}
