import { inject, observer } from 'mobx-react';
import DefaultAgTable from 'components/Table/DefaultAgTable';
import moment from 'moment';

function isColumnHidden(field) {
	let store = JSON.parse(localStorage.getItem('hiddenColumns')) || {
		WeekTable: {}
	};
	if (store.WeekTable) {
		return store.WeekTable[field] || false;
	}
	return false;
}

@inject('materialsStocktake')
@observer
export default class WeekTable extends DefaultAgTable {
	columns = [
		{
			headerName: 'Date',
			field: 'weekStart',
			cellRenderer: params =>
				params.value ? new moment(params.value).format('MMMM Do YYYY') : '',
			chartDataType: 'excluded',
			hide: isColumnHidden('weekStart')
		},
		{ headerName: 'Packs', field: 'packs', hide: isColumnHidden('packs') },
		{ headerName: 'm3', field: 'm3', hide: isColumnHidden('m3') },
		{ headerName: 'SKU', field: 'sku', hide: isColumnHidden('sku') },
		{
			headerName: 'Date Ready',
			field: 'dateReady',
			cellRenderer: params =>
				params.value ? new moment(params.value).format('MMMM Do YYYY') : '',
			chartDataType: 'excluded',
			hide: isColumnHidden('dateReady')
		}
	];

	constructor(props) {
		super(props);
		this.defaultStore = this.props.materialsStocktake;
	}
}
