import { inject, observer } from "mobx-react";
import moment from "moment";
import DefaultAgTable from "components/Table/DefaultAgTable";

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    DrumModeTable: {},
  };
  if (store.DrumModeTable) {
    return store.DrumModeTable[field] || false;
  }
  return false;
}

const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
		//debugger
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  return width;
	} else {
	  return 250;
	}
  };

@inject("drumMode")
@observer
export default class DrumModeTable extends DefaultAgTable {
  columns = [
    { headerName: "Name", field: "name", hide: isColumnHidden("name") },
    {
      headerName: "Hrs/day",
      field: "hoursPerDay",
      width: isColumnWidth("hoursPerDay", "DrumModeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("hoursPerDay"),
    },
    {
      headerName: "Hrs/Saturday",
      field: "hoursPerSaturday",
      width: isColumnWidth("hoursPerSaturday", "DrumModeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("hoursPerSaturday"),
    },
    {
      headerName: "Hrs/Wk",
      field: "hoursPerWeek",
      width: isColumnWidth("hoursPerWeek", "DrumModeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("hoursPerWeek"),
    },
    {
      headerName: "Description",
      field: "description",
      width: isColumnWidth("description", "DrumModeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("description"),
    },
    {
      headerName: "#ch./wk",
      field: "numChangesPerWeek",
      width: isColumnWidth("numChangesPerWeek", "DrumModeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("numChangesPerWeek"),
    },
    {
      headerName: "#ch./day",
      field: "numChangesPerDay",
      width: isColumnWidth("numChangesPerDay", "DrumModeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("numChangesPerDay"),
    },
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.drumMode;
    this.componentName = "DrumModeTable";
    this.printHeader = "Drum Mode Table";
  }

  onGridReady = params => {
	this.gridApi = params.api;
	this.gridApi.sizeColumnsToFit();
};
}
