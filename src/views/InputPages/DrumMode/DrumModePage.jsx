import React from "react";
import {inject, observer} from "mobx-react";
import ProductionDataTable from "views/InputPages/ProductionData/ProductionDataTable";
import ProductionDataForm from "views/InputPages/ProductionData/ProductionDataForm";
import DefaultPage from "views/DefaultPage";
import DrumModeTable from "views/InputPages/DrumMode/DrumModeTable";
import DrumModeForm from "views/InputPages/DrumMode/DrumModeForm";

@inject("drumMode")
@observer
export default class DrumModePage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.drumMode;
        this.components = {
            table: DrumModeTable,
            edit: DrumModeForm
        };
    }
}

