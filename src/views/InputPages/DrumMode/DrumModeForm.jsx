import React from 'react'
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import {Field, Formik} from "formik";
import {inject, observer} from "mobx-react";
import DrumModeClient from "api/drumMode";

@inject("notifications", "drumMode")
@observer
export default class DrumModeForm extends React.Component {

    render() {
        const {notifications, drumMode} = this.props;

        return (
            <Formik
                enableReinitialize
                initialValues={this.props.initialData ? this.props.initialData : {}}
                onSubmit={async (values, actions) => {
                    try {
                        if (values.id)
                            await DrumModeClient.update(values);
                        else
                            await DrumModeClient.save(values);
                        notifications.enqueueSnackbar({message: 'Saved.'});
                        drumMode.fetchAll();
                        drumMode.selectItem(null);
                        drumMode.selectItemsTab()
                    } catch (err) {
                        console.error(err);
                        notifications.enqueueSnackbar({message: 'Saving failed.', options: {variant: 'error'}});
                    } finally {
                        actions.setSubmitting(false)
                    }
                }}
                render={({
                             handleSubmit,
                             isSubmitting,
                         }) => (
                    <form onSubmit={handleSubmit}>
                        <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <Field name="name" render={({field}) => (
                                            <CustomInput
                                                labelText="Name"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={6}>
                                        <Field name="description" render={({field}) => (
                                            <CustomInput
                                                labelText="Description"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                </GridContainer>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="hoursPerDay" render={({field}) => (
                                            <CustomInput
                                                labelText="Hrs/day"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}
                                        />
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="hoursPerSaturday" render={({field}) => (
                                            <CustomInput
                                                labelText="Hrs/Saturday"
                                                formControlProps={{
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}
                                        />
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={4}>
                                        <Field name="hoursPerWeek" render={({field}) => (
                                            <CustomInput
                                                labelText="Hrs/Wk"
                                                formControlProps={{
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    type: "number",
                                                    ...field,
                                                    value: field.value || ''
                                                }}
                                            />
                                        )}/>
                                    </GridItem>
                                </GridContainer>

                                <Button color="primary" type="submit"
                                        disabled={isSubmitting}>{this.props.initialData ? "Update Item" : "Save Item"}</Button>

                            </GridItem>

                        </GridContainer>
                    </form>
                )}/>)

    }

}