import { inject, observer } from "mobx-react";
import moment from "moment";
import DefaultAgTable from "components/Table/DefaultAgTable";

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    ProductionDataTable: {},
  };
  if (store.ProductionDataTable) {
    return store.ProductionDataTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+45;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

@inject("productionData")
@observer
export default class ProductionDataTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Product",
      field: "product",
      sort: "asc",
      chartDataType: "category",
      hide: isColumnHidden("product"),
      width: isColumnWidth("product", "ProductionDataTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "product","Product");
        return params.data.product;
      },
    },
    {
      headerName: "SKU",
      field: "sku",
      chartDataType: "category",
      width: isColumnWidth("sku", "ProductionDataTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "sku","SKU");
        return params.data.sku;
      },
      hide: isColumnHidden("sku"),
    },
    {
      headerName: "Date",
      width: 140/*isColumnWidth("date", "ProductionDataTable")*/,
      suppressSizeToFit: true,
      hide: isColumnHidden("date"),
      field: "date",
      cellRenderer: (params) =>{
        getWidthColumnValue(params, this.componentName, "date","Date");
        return params.value ? new moment(params.value).format("MMMM Do YYYY") : ""},
      chartDataType: "excluded",
      filter: "agDateColumnFilter",
      filterParams: {
        comparator: function (filterLocalDateAtMidnight, cellValue) {
          var dateAsString = cellValue;
          if (dateAsString == null) return 0;

          // In the example application, dates are stored as dd/mm/yyyy
          // We create a Date object for comparison against the filter date
          let dateAsDate = new moment(dateAsString, "YYYY-MM-DD").toDate();
          // Now that both parameters are Date objects, we can compare
          if (dateAsDate < filterLocalDateAtMidnight) {
            return -1;
          } else if (dateAsDate > filterLocalDateAtMidnight) {
            return 1;
          } else {
            return 0;
          }
        },
      },
    },
    {
      headerName: "Production Line",
      field: "productionLine",
      chartDataType: "category",
      width: isColumnWidth("productionLine", "ProductionDataTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "productionLine","Production Line");
        return params.data.productionLine;
      },
      hide: isColumnHidden("productionLine"),
    },
    {
      headerName: "m3",
      field: "m3",
      chartDataType: "series",
      width: isColumnWidth("m3", "ProductionDataTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "m3","m3");
        return params.data.m3;
      },
      cellRenderer: (params) => (params.value ? +params.value.toFixed(2) : ""),
      hide: isColumnHidden("m3"),
    },
    {
      headerName: "Number of Packs",
      field: "numberOfPacks",
      chartDataType: "series",
      width: isColumnWidth("numberOfPacks", "ProductionDataTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "numberOfPacks","Number of Packs");
        return params.data.numberOfPacks;
      },
      cellRenderer: (params) => (params.value ? +params.value.toFixed(2) : ""),
      hide: isColumnHidden("numberOfPacks"),
    },

    {
      headerName: "Quantity",
      field: "quantity",
      chartDataType: "series",
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "quantity","Quantity");
        return params.data.quantity;
      },
      width: isColumnWidth("quantity", "ProductionDataTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("quantity"),
    },
  ];

  rowMultiSelectWithClick = true;
  rowSelection = "multiple";

  constructor(props) {
    super(props);
    this.defaultStore = this.props.productionData;
    this.componentName = "ProductionDataTable";
    this.printHeader = "Production Data Table";
  }

  // onPaginationChanged=()=>{
  //   console.log('hi i am praveen brooo');
  // }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}
