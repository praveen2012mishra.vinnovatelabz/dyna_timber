import React from "react";
import { withRouter } from "react-router";
import * as _ from "lodash";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core";
import ProductionDataClient from "api/productionData";
import Datetime from "react-datetime";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import customInputStyle from "assets/jss/material-dashboard-pro-react/components/customInputStyle.jsx";
import moment from "moment";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";

const styles = {
  label: {
    ...customInputStyle.labelRoot
  }
};

let getValue;



@inject(
  "notifications",
  "productionData",
  "inventory",
  "machines",
  "nonDrumSchedule"
)
@observer
class ProductionDataForm extends React.Component {
  async componentDidMount() {
    this.props.inventory.fetchAll();
    this.props.machines.fetchAll();
  }

  getItemStatus(item){
    //debugger
    //console.log(item);
    
    //console.log(item);
    if(item.id){
      let store = JSON.parse(sessionStorage.getItem("getHttpStatus")) || {
        ['ProductionDataTable']: {},
      };
      let id={id:1}
      store['ProductionDataTable'] = {
        ...store['ProductionDataTable'],
        ...id
        };
      sessionStorage.setItem("getHttpStatus", JSON.stringify(store));
      //return item
    }
    
    if(!item.id){
      //debugger
      //console.log(item);
      let store = JSON.parse(sessionStorage.getItem("getHttpStatus")) || {
        ['ProductionDataTable']: {},
      };
      let id={id:0}
      store['ProductionDataTable'] = {
        ...store['ProductionDataTable'],
        ...id
        };
      sessionStorage.setItem("getHttpStatus", JSON.stringify(store));
    }
  }

  render() {
    const {
      notifications,
      productionData,
      inventory,
      machines,
      initialData
    } = this.props;
    //getValue?getItemStatus(getValue):'';
    //getValue?getItemStatus():'';
    console.log("ProductionDataFormInitialData", initialData);
    const productNames = inventory.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.productName
    }));
    const skus = inventory.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.sku
    }));
    //console.log(skus,'==================>skus');
    
    const productionLines = machines.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.name
    }));

    let init;
    if (initialData) {
      init = _.clone(initialData, true);
      if (init.sku && skus) {
        init.sku = skus.find(item => item.label === init.sku);
      }
      if (init.product && productNames) {
        init.product = productNames.find(item => item.label === init.product);
      }
      if (init.productionLine && productionLines) {
        init.productionLine = productionLines.find(
          item => item.label === init.productionLine
        );
      }
    }

    const calculateFields = (sku, setFieldValue, qty) => {
      //console.log("Calculating fields ", sku, setFieldValue, qty);
      // const inv = inventory.data.find(i => i.sku === sku);
      const inv = sku.value;
      if (inv) {
        const m3Val = qty * 0.000000001 * inv.width * inv.depth * inv.length;
        try {
          setFieldValue("m3", m3Val.toFixed(3));
        } catch (err) {
          console.error("Error setting m3", err);
        }
        try {
          setFieldValue("numberOfPacks", (qty / inv.pcsPerPack).toFixed(3));
        } catch (err) {
          setFieldValue("numberOfPacks", Math.round(m3Val));
          console.error("Error setting numberOfPacks", err);
        }
      }
    };
    return (
      <Formik
        enableReinitialize
        initialValues={init ? init : { date: moment().format("YYYY-MM-DD") }}
        onSubmit={async (values, actions) => {
          try {
            //debugger
            getValue=Object.assign({},values);            
            console.log(getValue);
            
            if (_.get(values, "sku.label")) {
              values.sku = values.sku.label;
            }
            if (_.get(values, "product.label")) {
              values.product = values.product.label;
            }
            if (_.get(values, "productionLine.label")) {
              values.productionLine = values.productionLine.label;
            }
            //
            console.log('values before updation or addition  : ', values);
            
            if (values.id) await ProductionDataClient.update(values);
            else await ProductionDataClient.save(values);
            
            notifications.enqueueSnackbar({ message: "Saved." });
            productionData.fetchAll();
            productionData.selectItem(null);
            productionData.selectItemsTab();            
          } catch (err) {
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" }
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, setFieldValue, isSubmitting, values }) => (
          <form onSubmit={handleSubmit}>
            {init && init._createdFrom ? (
              <React.Fragment>
                <h4>Creating From Non Drum Schedule Item:</h4>
                <h6>
                  {`Size : ${init._createdFrom.size}, SKU: ${
                    init._createdFrom.sku
                  }, Date: ${init._createdFrom.date}, Machine: ${
                    init._createdFrom.machine
                  }, M3:
                  ${
                    init._createdFrom.m3 ? init._createdFrom.m3.toFixed(2) : ""
                  }, #packs: ${init._createdFrom.numPacks}, Num. Finished: ${
                    init._createdFrom.numFinished
                  }, Pcs Per Pack: ${
                    init._createdFrom.pcsPerPack
                  }, Raw Material SKU: ${init._createdFrom.rawMaterialSKU}`}
                
                </h6>
              </React.Fragment>
            ) : null}
            <GridContainer>
            {this.getItemStatus(values)}
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="product"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Product Name"
                          label="Product"
                          suggestions={productNames}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("product", item);
                            if (item.value) {
                              setFieldValue("sku", {
                                value: item.value,
                                label: item.value.sku
                              });
                              calculateFields(
                                item.value.sku,
                                setFieldValue,
                                values.quantity
                              );
                            }
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="sku"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Select SKU"
                          label="SKU"
                          suggestions={skus}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("sku", item);
                            if (item.value) {
                              setFieldValue("product", {
                                value: item.value,
                                label: item.value.productName
                              });
                              calculateFields(
                                item.value.sku,
                                setFieldValue,
                                values.quantity
                              );
                            }
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="date"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={field.value}
                            inputProps={{
                              value: new moment(field.value).format(
                                "MMMM Do YYYY"
                              )
                            }}
                            onChange={item => {
                              setFieldValue("date", item.format("YYYY-MM-DD"));
                            }}
                            dateFormat={"MMMM Do YYYY"}
                            timeFormat={false}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="productionLine"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Production Line"
                          label="Production Line"
                          suggestions={productionLines}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("productionLine", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="m3"
                      render={({ field }) => (
                        <CustomInput
                          labelText="m3"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="numberOfPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Number of Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="quantity"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Quantity"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            onChange: event => {
                              const val = event.target.value;
                              console.log(val);
                              setFieldValue("quantity", val);
                              calculateFields(values.sku, setFieldValue, val);
                            },
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                {/* <button
                  type="submit"
                  disabled
                  style={{ display: "none" }}
                  aria-hidden="true"
                ></button> */}
                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {this.props.initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
            
          </form>
        )}
      />
    );
  }
}

export default withStyles(styles)(withRouter(ProductionDataForm));
//getItemStatus(getValue)