import React from 'react';
import queryString from 'query-string';
import { withRouter } from 'react-router';
import * as _ from 'lodash';
import { inject, observer } from 'mobx-react';
import ProductionDataTable from 'views/InputPages/ProductionData/ProductionDataTable';
import ProductionDataForm from 'views/InputPages/ProductionData/ProductionDataForm';
import DefaultPage from 'views/DefaultPage';
import NonDrumScheduleClient from '../../../api/nonDrumSchedule';
import ProductionDataClient from "api/productionData";

@inject('productionData')
@observer
class ProductionDataPage extends DefaultPage {
	async componentDidMount() {
		const id = queryString.parse(_.get(this.props, 'location.search'))
			.nonDrumId;
		console.log('Non Drum ID', id);
		if (id) {
			let nonDrum = (await NonDrumScheduleClient.one(id)).data;
			console.log('NON drum', nonDrum);

			this.initialDataNew = {
				product: nonDrum.size,
				sku: nonDrum.sku,
				date: nonDrum.date,
				productionLine: nonDrum.machine,
				m3: nonDrum.m3 ? nonDrum.m3.toFixed(2) : null,
				numberOfPacks: nonDrum.numPacks,
				quantity: nonDrum.pcsPerPack,
				_createdFrom: nonDrum
			};
			this.props.productionData.selectNewTab();
		}
		else{
            this.props.productionData.fetchAll();
		}
	}

	constructor(props) {
		super(props);
		this.defaultStore = this.props.productionData;
		this.components = {
			table: ProductionDataTable,
			edit: ProductionDataForm
		};
	}
}

export default withRouter(ProductionDataPage);
