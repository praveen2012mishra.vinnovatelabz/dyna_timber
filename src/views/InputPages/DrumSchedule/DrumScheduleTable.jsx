import DefaultAgTable from "components/Table/DefaultAgTable";
import { action, flow, observable } from "mobx";
import DrumScheduleClient from "../../../api/drumSchedule";
import { observer } from "mobx-react";
import { element } from "prop-types";
import { ExpandLessRounded } from "@material-ui/icons";
import moment from "moment";

let treatmentCharge = [
  { chargeShortId: "h3", chargeId: "CCA H3", totalTime: "48" },
  { chargeShortId: "h4", chargeId: "CCA H4", totalTime: "57" }
];

const swapToFromEle = (rowValue, sourceEle, targetEle) => {
  let swapSourceEle = rowValue.find(ele => ele.id === sourceEle.id);
  let swapTargetEle = rowValue.find(ele => ele.id === targetEle.id);
  //debugger
  let swapSourceId = rowValue.indexOf(swapSourceEle);
  let swapTargetId = rowValue.indexOf(swapTargetEle);
  let newArray = Object.assign([], rowValue);
  newArray.splice(swapSourceId, 1);
  newArray.splice(swapTargetId, 0, swapSourceEle);
  // let swapArray= rowValue.map((ele)=>{
  //   if(ele.id===sourceEle.id){
  //     ele=Object.assign({},swapTargetEle)
  //     return ele
  //   }
  //   if(ele.id===targetEle.id){
  //     ele=Object.assign({},swapSourceEle)
  //     return ele
  //   }
  //   else{
  //     return ele
  //   }
  // })
  return newArray;
};

const addTimesMoment = (to, howmany) => {
  let currentTime = moment();
  const [hour, min, sec] = to.split(":");
  currentTime.hour(hour);
  currentTime.minutes(min);
  currentTime.seconds(sec);
  currentTime.add(howmany, "minutes");
  return currentTime.format("HH:mm:ss");
};

const getHours = productId => {
  const foundCharge = treatmentCharge.find(charge =>
    productId.search(new RegExp(charge.chargeShortId, "i")) < 0 ? false : true
  );
  return foundCharge ? foundCharge.totalTime : "0";
};

const addDateMoment = (date, newTime) => {
  let endTime = moment(newTime, "HH:mm:ss");
  let compareTime = moment("00:00:00", "HH:mm:ss");
  let currentDate = moment(date, "YYYY-MM-DD");
  let duration = moment.duration(moment(endTime).diff(compareTime)).asMinutes();
  return duration > 57
    ? currentDate.format("YYYY-MM-DD")
    : moment(currentDate, "YYYY-MM-DD")
        .add(1, "days")
        .format("YYYY-MM-DD");
};

const updateTimeInDate = (sourceEle, targetEle, newArray) => {
  newArray.sort(sortViaDate);
  //console.log(sourceEle, targetEle, newArray);
  var sourceTime = moment(sourceEle.timeIn, "HH:mm:ss").format("HH:mm:ss");
  var targetTime = moment(targetEle.timeIn, "HH:mm:ss").format("HH:mm:ss");
  let sourceDate = moment(sourceEle.date, "YYYY-MM-DD").format("YYYY-MM-DD");
  let targetDate = moment(targetEle.date, "YYYY-MM-DD").format("YYYY-MM-DD");
  let source_date_time = `${sourceDate}T${sourceTime}Z`;
  let target_date_time = `${targetDate}T${targetTime}Z`;
  let updateArray;
  //debugger
  if (source_date_time >= target_date_time) {
    sourceEle.timeIn = targetEle.timeIn;
    sourceEle.date = targetEle.date;
    newArray.unshift(sourceEle, targetEle); //
    updateArray = newArray.map((row, index, arr) => {
      row.timeIn =
        index === 0
          ? row.timeIn
          : addTimesMoment(
              arr[index - 1].timeIn,
              getHours(arr[index - 1].product)
            );

      row.date =
        index === 0 ? row.date : addDateMoment(arr[index - 1].date, row.timeIn);

      return row;
    });

    // updateArray.forEach(ele => {
    //   console.log(ele.sku, ele.timeIn, ele.date);
    // });
  }
  if (source_date_time <= target_date_time) {
    newArray.push(targetEle);
    let ele = newArray[0];
    ele.timeIn = sourceEle.timeIn;
    ele.date = sourceEle.date;
    newArray.push(sourceEle);
    updateArray = newArray.map((row, index, arr) => {
      row.timeIn =
        index === 0
          ? row.timeIn
          : addTimesMoment(
              arr[index - 1].timeIn,
              getHours(arr[index - 1].product)
            );

      row.date =
        index === 0 ? row.date : addDateMoment(arr[index - 1].date, row.timeIn);

      return row;
    });
  }
  //console.log(updateArray);
  return updateArray;
};

const sortViaDate = (d1, d2) => {
  let date1 = new Date(d1.date).getTime();
  let date2 = new Date(d2.date).getTime();
  return date1 > date2 ? 1 : -1;
};

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    DrumScheduleTable: {}
  };
  if (store.DrumScheduleTable) {
    return store.DrumScheduleTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    let width = store[componentName][field];
    width = parseInt(width, 10);
    width = width + 30;
    //console.log(width,field,'============>');
    return width;
  }
};

//take text width
function textWidth(text, fontProp, type) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  text=text?text.toString():text;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;
  //console.log(result,'====>');
  result = result;
  //console.log(result);

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (params, componentName, field) => {
  let data = params.data;
  //console.log(data,componentName,field);
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };

  store[componentName] = {
    ...store[componentName],
  };
  let storeWidth;
  let fieldWidth = textWidth(field, "normal 12px Roboto", "field");
  let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");

  let arrayWidth = Object.assign([]);
  arrayWidth.push(fieldWidth);
  arrayWidth.push(valueWidth);
  if (Object.keys(store[componentName]).length === 0) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (!store[componentName][field]) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (store[componentName][field] != Math.max(...arrayWidth)) {
    arrayWidth.push(store[componentName][field]);
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else {
    //console.log("No change in width resize");
  }
};

let lastOverId;
export default class DrumScheduleTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Date",
      field: "date",
      //rowGroup: true,
      suppressSizeToFit: true,
      hide: isColumnHidden("date"),
      enableRowGroup: true,
      rowDrag: true,
      // valueGetter: (params) => {
      //   getWidthColumnValue(params, this.componentName, "numberOfPacksToTOB");
      //   return params.data.date;
      // },
      //sort: "asc",
      width: isColumnWidth("date", "DrumScheduleTable")
    },
    {
      headerName: "SKU",
      field: "sku",
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "sku");
        return params.data.sku;
      },
      suppressSizeToFit: true,
      width: isColumnWidth("sku", "DrumLoadingReportTable")
    },

    {
      headerName: "Time",
      field: "time",
      suppressSizeToFit: true,
      enableRowGroup: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "time");
        return params.data.time;
      },
      hide: isColumnHidden("time"),
      width: isColumnWidth("time", "DrumScheduleTable")
    },
    {
      headerName: "Time In",
      field: "timeIn",
      enableRowGroup: true,
      suppressSizeToFit: true,
      hide: isColumnHidden("timeIn"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "timeIn");
        return params.data.timeIn;
      },
      //sort: "asc",
      width: isColumnWidth("timeIn", "DrumScheduleTable")
    },

    {
      headerName: "Product",
      field: "product",
      suppressSizeToFit: true,
      enableRowGroup: true,
      hide: isColumnHidden("product"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "product");
        return params.data.product;
      },
      width: isColumnWidth("product", "DrumScheduleTable")
    },
    {
      headerName: "Packs/Charge",
      field: "packsPerCh",
      suppressSizeToFit: true,
      enableRowGroup: true,
      hide: isColumnHidden("packsPerCh"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "packsPerCh");
        return params.data.packsPerCh;
      },
      width: isColumnWidth("packsPerCh", "DrumScheduleTable")
    },
    {
      headerName: "Production Line",
      field: "productionLine",
      enableRowGroup: true,
      hide: isColumnHidden("productionLine"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "productionLine");
        return params.data.productionLine;
      },
      width: isColumnWidth("productionLine", "DrumScheduleTable")
    },
    {
      headerName: "Comments",
      field: "comments",
      hide: isColumnHidden("comments"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "comments");
        return params.data.comments;
      },
      width: isColumnWidth("comments", "DrumScheduleTable")
    },

    {
      headerName: "Completed on time? (if not, why?)",
      field: "completedOnTime",
      enableRowGroup: true,
      hide: isColumnHidden("completedOnTime"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "completedOnTime");
        return params.data.completedOnTime;
      },
      width: isColumnWidth("completedOnTime", "DrumScheduleTable")
    },
    {
      headerName: "Completed in sequence? (if not, why?)",
      field: "competedInSequence",
      enableRowGroup: true,
      hide: isColumnHidden("competedInSequence"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "competedInSequence");
        return params.data.competedInSequence;
      },
      width: isColumnWidth("competedInSequence", "DrumScheduleTable")
    },
    {
      headerName: "Selected rows",
      field: "selectedRows",
      suppressSizeToFit: true,
      hide: isColumnHidden("selectedRows"),
      checkboxSelection: true,
      headerCheckboxSelection: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "selectedRows");
        return params.data.selectedRows;
      },
      width: isColumnWidth("selectedRows", "DrumScheduleTable")
    }
    // {
    //   headerName: "",
    //   field: "",
    //   hide: isColumnHidden(""),
    //   width: isColumnWidth("", "DrumScheduleTable")
    // }
  ];

  constructor(props) {
    super(props); //this.defaultStore = this.props.drumLoadingReport;
    this.componentName = "DrumScheduleTable";
    this.printHeader = "Drum Schedule Table";
    this.state = {
      treatmentCharge: [
        { chargeShortId: "h3", chargeId: "CCA H3", totalTime: "48" },
        { chargeShortId: "h4", chargeId: "CCA H4", totalTime: "57" }
      ]
    };
  }

  sortViaDate = (d1, d2) => {
    let date1 = new Date(d1.date).getTime();
    let date2 = new Date(d2.date).getTime();
    return date1 > date2 ? -1 : 1;
  };
  
  rowMultiSelectWithClick = true;
  rowSelection = "multiple";
  @observable data = [];
  // @observable loading = false;
  // @observable selectedItem = null;
  // @observable selectedTab = 0;
  onGridReady = params => {
    let fetchAll = flow(function*() {
      this.data = [];
      //this.loading = true;
      try {
        let rowValue = yield DrumScheduleClient.all();
        rowValue = Object.assign([], rowValue.data);
        //rowValue.sort(this.sortViaDate);
        console.log(rowValue);
        rowValue.forEach(function(data, index) {
          data.tableIndex = index;
        });
        let store =
          JSON.parse(localStorage.getItem("updateValueAfterEdit")) || {};
        console.log(store.updateValueAfterEdit.length);
        store.updateValueAfterEdit.length > 0
          ? params.api.setRowData(store.updateValueAfterEdit)
          : params.api.setRowData(rowValue);

        store = {
          ...store,
          updateValueAfterEdit: Object.assign([], {})
        };
        localStorage.setItem("updateValueAfterEdit", JSON.stringify(store));

        this.setState({ rowValue: rowValue });
      } catch (error) {
        console.error(error);
      } finally {
        this.loading = false;
      }
    }).bind(this);

    fetchAll();
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };

  tempArray = (sourceEle, targetEle) => {
    var sourceTime = moment(sourceEle.timeIn, "HH:mm:ss").format("HH:mm:ss");
    var targetTime = moment(targetEle.timeIn, "HH:mm:ss").format("HH:mm:ss");
    let sourceDate = moment(sourceEle.date, "YYYY-MM-DD").format("YYYY-MM-DD");
    let targetDate = moment(targetEle.date, "YYYY-MM-DD").format("YYYY-MM-DD");

    let source_date_time = `${sourceDate}T${sourceTime}Z`;
    let target_date_time = `${targetDate}T${targetTime}Z`;
    let rowValue = Object.assign([], this.state.rowValue);
    // debugger
    let newArray = rowValue.filter(ele => {
      let date = moment(ele.date, "YYYY-MM-DD").format("YYYY-MM-DD");
      let time = moment(ele.timeIn, "HH:mm:ss").format("HH:mm:ss");
      //debugger
      let date_time = `${date}T${time}Z`;
      //console.log(source_date_time>=date_time,date_time >=target_date_time);

      if (source_date_time >= target_date_time) {
        return source_date_time > date_time && date_time > target_date_time;
      }
      if (source_date_time <= target_date_time) {
        return source_date_time < date_time && date_time < target_date_time;
      }
    });
    let updateTimeDate = updateTimeInDate(sourceEle, targetEle, newArray);

    return updateTimeDate;
    //this.setState({rowValue:rowValue})
  };

  onRowDragEnd(event) {
    let movingNode = event.node;
    let overNode = event.overNode;
    let rowNeedsToMove = movingNode !== overNode;
    let swapToFromArray;
    let rowValue = Object.assign([], this.state.rowValue);
    //debugger
    if (rowNeedsToMove && overNode) {
      let tempArray = this.tempArray(movingNode.data, overNode.data);
      rowValue.forEach(ele => {
        for (let i = 0; i < tempArray.length; i++) {
          //DrumScheduleClient.update(tempArray[i]);
          if (ele.id === tempArray[i].id) {
            DrumScheduleClient.update(tempArray[i]);
            ele.date = tempArray[i].date;
            ele.timeIn = tempArray[i].timeIn;
            //console.log(ele.date,ele.sku, tempArray[i].timeIn);
          }
        }
      });
      swapToFromArray = swapToFromEle(rowValue, movingNode.data, overNode.data);
      // console.log(swapToFrom,rowValue);
      this.setState(
        () => ({ rowValue: swapToFromArray }),
        () => {
          //console.log(this.state.rowValue);
          this.gridApi.setRowData(swapToFromArray);
          this.gridApi.clearFocusedCell();
        }
      );
    }
    else{
      this.state.rowValue.forEach((ele)=>{
        DrumScheduleClient.update(ele);
      })
      this.gridApi.setRowData(this.state.rowValue);
      this.gridApi.clearFocusedCell();
    }
   
  }
}

// import DefaultAgTable from "components/Table/DefaultAgTable";
// import { action, flow, observable } from "mobx";
// import DrumScheduleClient from "../../../api/drumSchedule";
// import { observer } from "mobx-react";
// import { element } from "prop-types";
// import { ExpandLessRounded } from "@material-ui/icons";
// import moment from "moment";

// function addTimesMoment(to, howmany) {
//   let currentTime = moment();
//   const [hour, min, sec] = to.split(":");
//   currentTime.hour(hour);
//   currentTime.minutes(min);
//   currentTime.seconds(sec);
//   currentTime.add(howmany, "minutes");
//   return currentTime.format("HH:mm:ss");
// }

// const addDateMoment = (date, newTime) => {
//   let endTime = moment(newTime, "HH:mm:ss");
//   let compareTime = moment("00:00:00", "HH:mm:ss");
//   let currentDate = moment(date, "YYYY-MM-DD");
//   let duration = moment.duration(moment(endTime).diff(compareTime)).asMinutes();
//   return duration > 57
//     ? currentDate.format("YYYY-MM-DD")
//     : moment(currentDate, "YYYY-MM-DD")
//         .add(1, "days")
//         .format("YYYY-MM-DD");
// };

// function isColumnHidden(field) {
//   let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
//     DrumScheduleTable: {}
//   };
//   if (store.DrumScheduleTable) {
//     return store.DrumScheduleTable[field] || false;
//   }
//   return false;
// }

// const isColumnWidth = (field, componentName) => {
//   let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
//     [componentName]: {}
//   };
//   if (store[componentName]) {
//     let width = store[componentName][field];
//     width = parseInt(width, 10);
//     return width;
//   }
// };

// let lastOverId;
// export default class DrumScheduleTable extends DefaultAgTable {
//   columns = [
//     {
//       headerName: "Date",
//       field: "date",
//       //rowGroup: true,
//       suppressSizeToFit: true,
//       hide: isColumnHidden("date"),
//       enableRowGroup: true,
//       rowDrag: true,
//       sort: "asc",
//       width: isColumnWidth("date", "DrumScheduleTable")
//     },
//     {
//       headerName: "SKU",
//       field: "sku",
//       suppressSizeToFit: true,
//       width: isColumnWidth("sku", "DrumLoadingReportTable")
//     },
//     {
//       headerName: "Time",
//       field: "time",
//       suppressSizeToFit: true,
//       enableRowGroup: true,
//       hide: isColumnHidden("time"),
//       width: isColumnWidth("time", "DrumScheduleTable")
//     },
//     {
//       headerName: "Product",
//       field: "product",
//       suppressSizeToFit: true,
//       enableRowGroup: true,
//       hide: isColumnHidden("product"),
//       width: isColumnWidth("product", "DrumScheduleTable")
//     },
//     {
//       headerName: "Packs/Charge",
//       field: "packsPerCh",
//       suppressSizeToFit: true,
//       enableRowGroup: true,
//       hide: isColumnHidden("packsPerCh"),
//       width: isColumnWidth("packsPerCh", "DrumScheduleTable")
//     },
//     {
//       headerName: "Production Line",
//       field: "productionLine",
//       enableRowGroup: true,
//       hide: isColumnHidden("productionLine"),
//       width: isColumnWidth("productionLine", "DrumScheduleTable")
//     },
//     {
//       headerName: "Comments",
//       field: "comments",
//       hide: isColumnHidden("comments"),
//       width: isColumnWidth("comments", "DrumScheduleTable")
//     },
//     {
//       headerName: "Time In",
//       field: "timeIn",
//       enableRowGroup: true,
//       hide: isColumnHidden("timeIn"),
//       sort: "asc",
//       width: isColumnWidth("timeIn", "DrumScheduleTable")
//     },
//     {
//       headerName: "Completed on time? (if not, why?)",
//       field: "completedOnTime",
//       enableRowGroup: true,
//       hide: isColumnHidden("completedOnTime"),
//       width: isColumnWidth("completedOnTime", "DrumScheduleTable")
//     },
//     {
//       headerName: "Completed in sequence? (if not, why?)",
//       field: "competedInSequence",
//       enableRowGroup: true,
//       hide: isColumnHidden("competedInSequence"),
//       width: isColumnWidth("competedInSequence", "DrumScheduleTable")
//     },
//     {
//       headerName: "Selected rows",
//       field: "selectedRows",
//       hide: isColumnHidden("selectedRows"),
//       checkboxSelection: true,
//       headerCheckboxSelection: true,
//       width: isColumnWidth("selectedRows", "DrumScheduleTable")
//     },
//     // {
//     //   headerName: "",
//     //   field: "",
//     //   hide: isColumnHidden(""),
//     //   width: isColumnWidth("", "DrumScheduleTable")
//     // }
//   ];

//   constructor(props) {
//     super(props); //this.defaultStore = this.props.drumLoadingReport;
//     this.componentName = "DrumScheduleTable";
//     this.printHeader = "Drum Schedule Table";
//     this.state = {
// treatmentCharge: [
//   { chargeShortId: "h3", chargeId: "CCA H3", totalTime: "48" },
//   { chargeShortId: "h4", chargeId: "CCA H4", totalTime: "57" }
// ]
//     };
//   }

// sortViaDate = (d1, d2) => {
//   let date1 = new Date(d1.date).getTime();
//   let date2 = new Date(d2.date).getTime();
//   return date1 > date2 ? -1 : 1;
// };

//   rowSelection = "multiple";
//   @observable data = [];
//   // @observable loading = false;
//   // @observable selectedItem = null;
//   // @observable selectedTab = 0;
//   onGridReady = params => {
//     let fetchAll = flow(function*() {
//       this.data = [];
//       //this.loading = true;
//       try {
//         let rowValue = yield DrumScheduleClient.all();
//         rowValue = Object.assign([], rowValue.data);
//         rowValue.sort(this.sortViaDate);
//         //console.log(rowValue);
//         rowValue.forEach(function(data, index) {
//           data.tableIndex = index;
//         });
//         params.api.setRowData(rowValue);
//         this.setState({ rowValue: rowValue });
//       } catch (error) {
//         console.error(error);
//       } finally {
//         this.loading = false;
//       }
//     }).bind(this);

//     fetchAll();
//     this.gridApi = params.api;
//     this.gridApi.sizeColumnsToFit();
//   };

//   getHours = productId => {
//     const foundCharge = this.state.treatmentCharge.find(charge =>
//       productId.search(new RegExp(charge.chargeShortId, "i")) < 0 ? false : true
//     );
//     return foundCharge ? foundCharge.totalTime : "0";
//   };

//   scheduleReorder = (sourceId, targetId) => {
//     let newArr = [],
//       start = [],
//       mid = [],
//       end = [];

//     if (sourceId > targetId) {
//       //shift down from target to source -1 , new target = source
//       start = this.state.rowValue.slice(0, targetId);
//       mid = [this.state.rowValue[sourceId]]
//         .concat(this.state.rowValue.slice(targetId, sourceId))
//         .map((row, index, arr) => {
//           row.timeIn =
//             index === 0
//               ? arr[index + 1].timeIn
//               : addTimesMoment(
//                   arr[index - 1].timeIn,
//                   this.getHours(arr[index - 1].product)
//                 );

//           row.date =
//             index === 0
//               ? arr[index + 1].date
//               : addDateMoment(arr[index - 1].date, row.timeIn);
//           return row;
//         });
//       end = this.state.rowValue.slice(sourceId + 1);
//     } else if (sourceId < targetId) {
//       //shift up from target to source + 1 , new target = source

//       start = this.state.rowValue.slice(0, sourceId);
//       mid = this.state.rowValue
//         .slice(sourceId + 1, targetId + 1)
//         .concat([this.state.rowValue[sourceId]])
//         .map((row, index, arr) => {
//           row.timeIn =
//             index === 0
//               ? arr[arr.length - 1].timeIn
//               : addTimesMoment(
//                   arr[index - 1].timeIn,
//                   this.getHours(arr[index - 1].product)
//                 );

//           row.date =
//             index === 0
//               ? arr[index + 1].date
//               : addDateMoment(arr[index - 1].date, row.timeIn);

//           return row;
//         });
//       end = this.state.rowValue.slice(targetId + 1);
//     } else {
//       start = [...this.state.rowValue];
//     }
//     newArr = newArr.concat(start, mid, end);

//     return newArr;
//   };

//   getSourceAndTarget = (sNode, tNode) => {
//     let sourceId = -1,
//       targetId = -1;
//     for (let i = 0; i < this.state.rowValue.length; i++) {
//       if (this.state.rowValue[i].id === sNode.data.id) sourceId = i;
//       if (this.state.rowValue[i].id === tNode.data.id) targetId = i;
//       if (sourceId >= 0 && targetId >= 0) break;
//     }
//     return { sourceId, targetId };
//   };

//   onRowDragEnd(event) {
//     let movingNode = event.node;
//     let overNode = event.overNode;
//     let rowNeedsToMove = movingNode !== overNode;
//     let updatedArray;
//     if (rowNeedsToMove && overNode.data) {
//       const { sourceId, targetId } = this.getSourceAndTarget(
//         movingNode,
//         overNode
//       );

//       /* console.log('moving index : ', sourceId);
// 			console.log('to index : ', targetId); */

//       updatedArray = this.scheduleReorder(sourceId, targetId);

//       updatedArray.forEach(ele => {
//         DrumScheduleClient.update(ele);
//       });
//     }
//     this.setState(
//       () => ({ rowValue: updatedArray }),
//       () => {
//         console.log(this.state.rowValue);
//         this.gridApi.setRowData(updatedArray);
//         this.gridApi.clearFocusedCell();
//       }
//     );
//   }
// }
