import React from "react";
import * as _ from "lodash";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import { inject, observer } from "mobx-react";
import DrumScheduleClient from "api/drumSchedule";
import Datetime from "react-datetime";
import FormControl from "@material-ui/core/FormControl";
import moment from "moment";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";

let skuSelected;

let updateValueAfterEdit;

let getUpdateId;

let newArrayTimeIn;

let previousDate;

let currentDate;

let extendDate;

let selectedDate;

let drumSchedules;

let drumScheduleRows;

let updateRowAfterEdit = values => {  
  updateValueAfterEdit = Object.assign({}, values);
  //console.log(updateValueAfterEdit, drumScheduleRows);
  drumScheduleRows=drumScheduleRows.map((ele)=>{
    return ele.id===updateValueAfterEdit.id?updateValueAfterEdit:ele;
  })
  let store = JSON.parse(localStorage.getItem('updateValueAfterEdit')) || {};	
  //debugger
  if(updateValueAfterEdit.sku){
    store = {
      ...store,
      updateValueAfterEdit: drumScheduleRows
    };
    localStorage.setItem('updateValueAfterEdit', JSON.stringify(store));
  }
  else{
    store = {
      ...store,
      updateValueAfterEdit: Object.assign([],{})
    };
    localStorage.setItem('updateValueAfterEdit', JSON.stringify(store));
  }
  
	
  //console.log(updateValueAfterEdit, drumScheduleRows);
};

const getStatus = (array, date) => {
  let exactDate = array.find(ele => {
    var time1 = moment(currentDate).format("YYYY-MM-DD");
    var time2 = moment(ele.date).format("YYYY-MM-DD");
    return time1 === time2;
  });
  let previousDateObject = array.find(ele => {
    var time1 = moment(currentDate).format("YYYY-MM-DD");
    var time2 = moment(ele.date).format("YYYY-MM-DD");
    return time1 > time2;
  });
  //extendDate=extendDate?extendDate.date:undefined
  let extendDate = array.find(ele => {
    var time1 = moment(currentDate).format("YYYY-MM-DD");
    var time2 = moment(ele.date).format("YYYY-MM-DD");
    return time1 < time2;
  });
  extendDate = extendDate ? extendDate.date : undefined;
  if (array.length === 0) {
    previousDate = true;
  }
  if (extendDate) {
    previousDate = false;
    return date;
  }
  if (exactDate) {
    previousDate = false;
    return date;
  }

  if (selectedDate) {
    previousDate = true;
    currentDate = selectedDate;
    newArrayTimeIn = recentDateArray(drumSchedules, currentDate);
  } else {
    previousDate = true;
    return date;
  }
};

const sortViaDate = (d1, d2) => {
  let date1 = new Date(d1.date).getTime();
  let date2 = new Date(d2.date).getTime();
  return date1 > date2 ? -1 : 1;
};

const minuteTohoure = minut => {
  let num = minut;
  let hours = num / 60;
  let rhours = Math.floor(hours);
  let minutes = (hours - rhours) * 60;
  let rminutes = Math.round(minutes);
  rminutes = rminutes < 10 ? "0" + rminutes : rminutes;
  let timeDsr = "0" + rhours + ":" + rminutes;
  return timeDsr;
};

const onSelectionDate = value => {
  selectedDate = value;
  //debugger
  let dateStatus = recentDateArray(drumSchedules, value);
  //console.log(value, drumSchedules, dateStatus);
  dateStatus ? (newArrayTimeIn = dateStatus) : (previousDate = true);
};

const dateChanged = (date, newTimeIn) => {
  let endTime = moment(newTimeIn, "HH:mm:ss");
  let compareTime = moment("00:00:00", "HH:mm:ss");
  let currentDate = moment(date, "YYYY-MM-DD");
  var duration = moment.duration(moment(endTime).diff(compareTime)).asMinutes();
  if (duration < 57) {
    previousDate = true;
    return moment(currentDate, "YYYY-MM-DD")
      .add(1, "days")
      .format("YYYY-MM-DD");
  } else {
    return currentDate.format("YYYY-MM-DD");
  }
};

const getTimes = getSku => {
  //console.log(typeof getSku,getSku);
  if (getSku && typeof getSku === "object") {
    getSku = getSku.label;
  }

  if (getSku && typeof getSku === "string") {
    let getTime;
    minuteTohoure(48);
    if (getSku.indexOf("H3") !== -1) {
      getTime = minuteTohoure(48);
    } else if (getSku.indexOf("H4") !== -1) {
      getTime = minuteTohoure(57);
    } else {
      getTime = minuteTohoure(0);
    }
    return getTime;
  }
};

const getTimeIn = (timeIn, getSku) => {
  if (getSku && timeIn) {
    let setTime;
    let currentTime = moment();
    const [hour, min, sec] = timeIn.split(":");
    currentTime.hour(hour);
    currentTime.minutes(min);
    currentTime.seconds(sec);
    if (getSku.indexOf("H3") !== -1) {
      setTime = currentTime.add(48, "minutes");
    } else if (getSku.indexOf("H4") !== -1) {
      setTime = currentTime.add(57, "minutes");
    } else {
      setTime = currentTime.add(0, "minutes");
    }
    return setTime.format("HH:mm:ss");
  }
};

const recentDateArray = (array, givenDate) => {
  // debugger
  if (array.length) {
    let recentDate = givenDate;
    previousDate = false;
    let newArray = array.filter(ele => {
      var time1 = moment(recentDate).format("YYYY-MM-DD");
      var time2 = moment(ele.date).format("YYYY-MM-DD");
      //console.log(time1);
      return time1 === time2;
    });
    function sortTimes(arrayOfTimes) {
      return arrayOfTimes.sort((a, b) => {
        const aParts = getNumericParts(a.timeIn);
        const bParts = getNumericParts(b.timeIn);

        // Sorts by hour then minute
        return aParts[0] - bParts[0] || aParts[1] - bParts[1];
      });

      function getNumericParts(time) {
        // accounts formats of 9:15 AM and 09:15:30 but does not handle AM/PM in comparison
        return time
          .split(" ")[0]
          .split(":")
          .map(x => +x);
      }
    }
    let timeArray = [];
    let sortTimeIn = newArray.filter(ele => {
      if (ele.timeIn !== null) {
        timeArray.push({ timeIn: ele.timeIn, sku: ele.sku });
      }
    });

    let sortedtimeArray = sortTimes(timeArray);
    //console.log(sortedtimeArray,'------------------------->',newArray);
    if (sortedtimeArray.length) {
      return getTimeIn(
        sortedtimeArray[sortedtimeArray.length - 1].timeIn,
        sortedtimeArray[sortedtimeArray.length - 1].sku
      );
    }
  } else {
    previousDate = true;
  }
};
let updateDate;
let ArrayTimeIn;
const statusTimeInAndDate = (drumSchedules, getDate) => {
  //debugger
  ArrayTimeIn = recentDateArray(drumSchedules, getDate);
  let endTime = moment(ArrayTimeIn, "HH:mm:ss");
  let compareTime = moment("00:00:00", "HH:mm:ss");
  let duration = moment.duration(moment(endTime).diff(compareTime)).asMinutes();
  //duration=Math.abs(duration)
  //getDate=moment(getDate).format("YYYY-MM-DD");
  updateDate = dateChanged(getDate, ArrayTimeIn);

  //console.log(updateDate,getDate,duration);

  if (updateDate !== getDate && duration < 57) {
    previousDate = true;
    //newArrayTimeIn=recentDateArray(drumSchedules, updateDate);
    //currentDate=dateChanged(updateDate,newArrayTimeIn)
    return statusTimeInAndDate(drumSchedules, updateDate);
  }
  if (updateDate === getDate && duration > 57) {
    //debugger
    previousDate = false;
    newArrayTimeIn = ArrayTimeIn;
    currentDate = updateDate;
    return;
  }
  if (updateDate === getDate && duration === NaN) {
    previousDate = true;
    currentDate = getDate;
    return;
  } else {
    currentDate = getDate;
    previousDate = true;
  }
};

const getEditRow = values => {
  currentDate = values.date;
  getUpdateId = values.id;
  skuSelected = values.sku;
  newArrayTimeIn = values.timeIn;
};

const getNewRow = values => {
  //debugger
  getUpdateId = undefined;
  let todayDate = values.date; /*new Date().toJSON().slice(0, 10);*/
  let getDate = getStatus(drumSchedules, todayDate);

  statusTimeInAndDate(drumSchedules, getDate);
  // console.log(previousDate,todayDate,getDate, skuSelected, newArrayTimeIn,drumSchedules);
};

@inject("notifications", "inventory", "machines", "drumSchedule")
@observer
export default class DrumScheduleForm extends React.Component {
  async componentDidMount() {
    this.props.inventory.fetchAll();
    this.props.machines.fetchAll();
    this.props.drumSchedule.fetchAll();
  }

  render() {
    const {
      notifications,
      store,
      inventory,
      machines,
      initialData,
      drumSchedule
    } = this.props;

    const productNames = inventory.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.productName
    }));
    const skus = inventory.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.sku
    }));
    const productionLines = machines.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.name
    }));

    drumScheduleRows = drumSchedule.data.map(ele => {
      //console.log(ele);
      return {
        id: ele.id,
        date: ele.date,
        time: ele.time,
        packsPerCh: ele.packsPerCh,
        product: ele.product,
        productionLine: ele.productionLine,
        comments: ele.comments,
        timeIn: ele.timeIn,
        completedOnTime: ele.completedOnTime,
        competedInSequence: ele.competedInSequence,
        sku: ele.sku
      };
    });
    drumSchedules = drumSchedule.data.map(ele => {
      return { date: ele.date, timeIn: ele.timeIn, sku: ele.sku };
    });
    drumSchedules.sort(sortViaDate);
    let init;
    if (initialData) {
      init = _.clone(initialData, true);
      if (init.sku && skus) {
        init.sku = skus.find(item => item.label === init.sku);
      }
      if (init.product && productNames) {
        init.product = productNames.find(item => item.label === init.product);
      }
      if (init.productionLine && productionLines) {
        init.productionLine = productionLines.find(
          item => item.label === init.productionLine
        );
      }
    }

    const calculateFields = (sku, setFieldValue, qty) => {
      const inv = sku.value;
      if (inv) {
        const m3Val = qty * 0.000000001 * inv.width * inv.depth * inv.length;
        try {
          setFieldValue("m3", m3Val.toFixed(3));
        } catch (err) {
          console.error("Error setting m3", err);
        }
        try {
          setFieldValue("numberOfPacks", (qty / inv.pcsPerPack).toFixed(3));
        } catch (err) {
          setFieldValue("numberOfPacks", Math.round(m3Val));
          console.error("Error setting numberOfPacks", err);
        }
      }
    };

    return (
      <Formik
        enableReinitialize
        initialValues={
          this.props.initialData
            ? this.props.initialData
            : { date: moment().format("YYYY-MM-DD") }
        }
        onSubmit={async (values, actions) => {
          try {
            let currentSku;
            //debugger
            values.time = getTimes(skuSelected);
            if (_.get(values, "sku.label")) {
              values.sku = values.sku.label;
              currentSku = values.sku.label;

              if (getUpdateId) {
                values.timeIn = newArrayTimeIn;
              }

              if (!previousDate) {
                //debugger
                values.date = dateChanged(currentDate, newArrayTimeIn);
                let newTimeIn = newArrayTimeIn;
                values.timeIn = newTimeIn;
              }
              if (previousDate) {
                //debugger;
                values.date = currentDate;
              }
              if (selectedDate) {
                values.date = selectedDate;
              }
            }
            if (_.get(values, "product.label")) {
              values.product = values.product.label;
            }
            if (_.get(values, "productionLine.label")) {
              values.productionLine = values.productionLine.label;
            }
            delete values.productionLine;

            if (values.id) {
              //debugger
              await DrumScheduleClient.update(values);
              updateRowAfterEdit(values);
              //window.location.reload();
            } else {
              updateRowAfterEdit(undefined);
              await DrumScheduleClient.save(values);
              //window.location.reload();
            }            
            notifications.enqueueSnackbar({ message: "Saved." });
            store.fetchAll();
            store.selectItem(null);
            store.selectItemsTab();
            //updateRowAfterEdit(undefined);
          } catch (err) {
            console.error(err);
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" }
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, setFieldValue, isSubmitting, values }) => (
          <form onSubmit={handleSubmit}>
            {values.id ? getEditRow(values) : getNewRow(values)}
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="date"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={field.value}
                            onChange={item => {
                              //debugger
                              onSelectionDate(item.format("YYYY-MM-DD"));

                              setFieldValue("date", item.format("YYYY-MM-DD"));
                            }}
                            dateFormat={"MMMM Do YYYY"}
                            timeFormat={false}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="sku"
                      render={({ field }) => (
                        <div>
                          <IntegrationReactSelect
                            placeholder={`${field.value}`}
                            label="SKU"
                            suggestions={skus}
                            value={field.value || {}}
                            onChange={item => {
                              //debugger
                              setFieldValue("sku", item);
                              if (item.value) {
                                skuSelected = item.value.sku;
                                //console.log(item,skuSelected,'---------------------->');

                                setFieldValue("product", {
                                  value: item.value,
                                  label: item.value.productName
                                });
                                setFieldValue("productionLine", {
                                  value: item.value,
                                  label: item.value.feedStock1Machine
                                });
                                setFieldValue("time", {
                                  value: item.value,
                                  label: getTimes(skuSelected)
                                });
                                previousDate
                                  ? setFieldValue("timeIn", {
                                      value: item.value,
                                      label: getTimeIn(
                                        newArrayTimeIn,
                                        skuSelected
                                      )
                                    })
                                  : "";
                                calculateFields(
                                  item.value.sku,
                                  setFieldValue,
                                  values.quantity
                                );
                              }
                            }}
                          ></IntegrationReactSelect>
                        </div>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="product"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Product"
                          //suggestions={productNames}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("product", item);
                            if (item.value) {
                              setFieldValue("sku", {
                                value: item.value,
                                label: item.value.sku
                              });
                              calculateFields(
                                item.value.sku,
                                setFieldValue,
                                values.quantity
                              );
                            }
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="productionLine"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Production Line"
                          // suggestions={productionLines}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("productionLine", item);
                            if (item.value) {
                              setFieldValue("sku", {
                                value: item.value,
                                label: item.value.sku
                              });
                            }
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="time"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder={`${getTimes(skuSelected)}`}
                          label="time"
                          //suggestions={productionLines}
                          value={getTimes(skuSelected) || {}}
                          onChange={item => {
                            setFieldValue("time", getTimes(skuSelected));
                            if (item.value) {
                              setFieldValue("sku", {
                                value: getTimes(skuSelected),
                                label: item.value.sku
                              });
                            }
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                    {/* <Field
                      name="time"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={moment(field.value, "HH:mm")}
                            onChange={item => {
                              setFieldValue("time", item.format("HH:mm"));
                            }}
                            dateFormat={false}
                            inputProps={{ placeholder: "Time" }}
                          />
                        </FormControl>
                      )}
                    /> */}
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    {!previousDate ? (
                      <Field
                        name="timeIn"
                        render={({ field }) => (
                          <FormControl fullWidth>
                            <Datetime
                              value={moment(
                                getTimeIn(newArrayTimeIn, "skuSelected"),
                                "HH:mm"
                              )}
                              onChange={item => {
                                if (item.value) {
                                  setFieldValue("sku", {
                                    value: getTimeIn(
                                      newArrayTimeIn,
                                      "skuSelected"
                                    ),
                                    label: item.value.sku
                                  });
                                }
                                //console.log(item);
                                setFieldValue(
                                  "timeIn",
                                  item.format(
                                    "HH:mm"
                                  ) /*.format(
                                    "HH:mm"
                                  )*/
                                  /*getTimeIn(newArrayTimeIn, 'skuSelected')*/
                                );
                              }}
                              dateFormat={false}
                              inputProps={{
                                placeholder: getTimeIn(
                                  newArrayTimeIn,
                                  "skuSelected"
                                )
                              }}
                            />
                          </FormControl>
                        )}
                      />
                    ) : (
                      /* <Field
                      name="timeIn"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder={`${getTimeIn(
                            newArrayTimeIn,
                            skuSelected
                          )}`}
                          label="timeIn"
                          //suggestions={productionLines}
                          value={getTimeIn(newArrayTimeIn, skuSelected) || {}}
                          onChange={item => {
                            setFieldValue(
                              "timeIn",
                              getTimeIn(newArrayTimeIn, skuSelected)
                            );
                            if (item.value) {
                              setFieldValue("sku", {
                                value: getTimeIn(newArrayTimeIn, skuSelected),
                                label: item.value.sku
                              });
                            }
                          }}
                        ></IntegrationReactSelect>
                      )}
                    /> */ <Field
                        name="timeIn"
                        render={({ field }) => (
                          <FormControl fullWidth>
                            <Datetime
                              value={moment(field.value, "HH:mm")}
                              onChange={item => {
                                // debugger
                                setFieldValue("timeIn", item.format("HH:mm"));
                              }}
                              dateFormat={false}
                              inputProps={{ placeholder: "Time In" }}
                            />
                          </FormControl>
                        )}
                      />
                    )}
                    {/* <Field
                      name="timeIn"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={moment(field.value, "HH:mm")}
                            onChange={item => {
                              setFieldValue("timeIn", item.format("HH:mm"));
                            }}
                            dateFormat={false}
                            inputProps={{ placeholder: "Time In" }}
                          />
                        </FormControl>
                      )}
                    /> */}
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="completedOnTime"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Completed on time? (if not, why?)"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="comments"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Comments"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="competedInSequence"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Completed in sequence? (if not, why?)"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>

                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {this.props.initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}
