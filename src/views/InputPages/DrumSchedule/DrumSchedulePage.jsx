import React from "react";
import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import DrumScheduleTable from "views/InputPages/DrumSchedule/DrumScheduleTable";
import DrumScheduleForm from "views/InputPages/DrumSchedule/DrumScheduleForm";
import ActionIcon from "@material-ui/icons/PlusOne";
import DrumScheduleClient from "api/drumSchedule";

@inject("drumSchedule")
@observer
export default class DrumSchedulePage extends DefaultPage {
    
    aditionalActions = [
        <ActionIcon
          onClick={async () => {
            await DrumScheduleClient.newStocktake();
            this.props.drumSchedule.fetchAll();
          }}
        />
      ];
    constructor(props) {
        super(props);
        this.defaultStore = this.props.drumSchedule;
        this.components = {
            table: DrumScheduleTable,
            edit: DrumScheduleForm
        };
    }
}

