import React from "react";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import InputLabel from "@material-ui/core/InputLabel";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik, FieldArray } from "formik";
import { inject, observer } from "mobx-react";
import NonDrumScheduleClient from "api/nonDrumSchedule";
import Datetime from "react-datetime";
import FormControl from "@material-ui/core/FormControl";
import moment from "moment";
import * as _ from "lodash";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";

@inject("notifications", "machines", "inventory", "materials")
@observer
export default class NonDrumScheduleForm extends React.Component {
  submitAction = undefined;

  componentDidMount() {
    this.props.inventory.fetchAll();
    this.props.machines.fetchAll();
    this.props.materials.fetchAll();
  }

  render() {
    const {
      notifications,
      store,
      machines,
      inventory,
      materials,
      initialData
    } = this.props;
    const machineNames = machines.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.name
    }));
    const productNames = inventory.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.productName
    }));
    const skus = inventory.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.sku
    }));
    const materialCodes = materials.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.code
    }));

    let init;
    if (initialData) {
      init = _.clone(initialData, true);
      init.inited = true;
      let obj = {};
      if (init.sku && skus) {
        obj.sku = skus.find(item => item.label === init.sku);
      }
      if (init.size && productNames) {
        obj.size = productNames.find(item => item.label === init.size);
      }
      obj.pcsPerPack = init.pcsPerPack;
      init.items = [obj];
      if (init.machine && machineNames) {
        init.machine = machineNames.find(item => item.label === init.machine);
      }
      if (init.rawMaterialSKU && skus) {
        init.rawMaterialSKU = materialCodes.find(
          item => item.label === init.rawMaterialSKU
        );
      }
    }

    return (
      <Formik
        enableReinitialize
        initialValues={
          init
            ? init
            : {
                timeStart: "09:00",
                timeFinish: "10:00",
                date: moment().format("YYYY-MM-DD"),
                items: [{}]
              }
        }
        onSubmit={async (values, actions) => {
          console.log("Submited values", values);
          try {
            if (_.get(values, "sku.label")) {
              values.sku = values.sku.label;
            }
            if (_.get(values, "rawMaterialSKU.label")) {
              values.rawMaterialSKU = values.rawMaterialSKU.label;
            }
            if (_.get(values, "size.label")) {
              values.size = values.size.label;
            }
            if (_.get(values, "machine.label")) {
              values.machine = values.machine.label;
            }
            if (values.items) {
              for (let i = 0; i < values.items.length; i++) {
                if (_.get(values.items[i], "sku.label")) {
                  values.items[i].sku = values.items[i].sku.label;
                }
                if (_.get(values.items[i], "size.label")) {
                  values.items[i].size = values.items[i].size.label;
                }
              }
            }

            console.log("Submited values finished", values);
            if (values.id) await NonDrumScheduleClient.update(values);
            else {
              if (this.submitAction === 1) {
                values.saveSeparately = false;
              }
              if (this.submitAction === 2) {
                values.saveSeparately = true;
              }
              await NonDrumScheduleClient.save(values);
            }
            notifications.enqueueSnackbar({ message: "Saved." });
            store.fetchAll();
            store.selectItem(null);
            store.selectItemsTab();
          } catch (err) {
            console.error(err);
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" }
            });
          } finally {
            actions.setSubmitting(false);
            this.submitAction = undefined;
          }
        }}
        render={({ handleSubmit, setFieldValue, isSubmitting, values }) => (
          <form onSubmit={handleSubmit}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="date"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={field.value}
                            onChange={item => {
                              setFieldValue("date", item.format("YYYY-MM-DD"));
                            }}
                            dateFormat={"MMMM Do YYYY"}
                            timeFormat={false}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>

                  <GridItem xs={12} sm={12} md={2}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="timeStart"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={moment(field.value, "HH:mm")}
                            onChange={item => {
                              setFieldValue("timeStart", item.format("HH:mm"));
                            }}
                            dateFormat={false}
                            inputProps={{ placeholder: "Time Start" }}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={2}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="timeFinish"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={moment(field.value, "HH:mm")}
                            onChange={item => {
                              setFieldValue("timeFinish", item.format("HH:mm"));
                            }}
                            dateFormat={false}
                            inputProps={{ placeholder: "Time Finish" }}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="customer"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Customer"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={2}>
                    <Field
                      name="numPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="# packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="machine"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Machine"
                          label="Machine"
                          suggestions={machineNames}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("machine", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="rawMaterialSKU"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Raw Material SKU"
                          label="Raw Material SKU"
                          suggestions={materialCodes}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("rawMaterialSKU", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="numFinished"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Num. of Finished SKU Out"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            disabled: values.inited,
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="numDifferentFinished"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Num. of Different Length"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            disabled: values.inited,
                            onChange: item => {
                              const val = item.target.value;
                              setFieldValue("numDifferentFinished", val);
                              let items = [{}];
                              if (val > 1) {
                                for (let i = 1; i < val; i++) {
                                  items.push({});
                                }
                              }
                              console.log(items);
                              setFieldValue("items", items);
                            },
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <FieldArray
                  name="items"
                  render={arrayHelpers => (
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={12}>
                        {values.items.map((item, index) => (
                          <React.Fragment key={index}>
                            {values.items.length > 1 ? (
                              <GridContainer>
                                <GridItem xs={12} sm={12} md={12}>
                                  <h4>Length {index + 1}</h4>
                                </GridItem>
                              </GridContainer>
                            ) : null}
                            <GridContainer>
                              <GridItem xs={12} sm={12} md={4}>
                                <Field
                                  name={`items[${index}].size`}
                                  render={({ field }) => (
                                    <IntegrationReactSelect
                                      placeholder="Size"
                                      label="Size"
                                      suggestions={productNames}
                                      value={field.value || {}}
                                      onChange={item => {
                                        setFieldValue(
                                          `items[${index}].size`,
                                          item
                                        );
                                        if (item.value) {
                                          setFieldValue(`items[${index}].sku`, {
                                            value: item.value,
                                            label: item.value.sku
                                          });
                                          setFieldValue(
                                            `items[${index}].pcsPerPack`,
                                            item.value.pcsPerPack
                                          );
                                        }
                                      }}
                                    ></IntegrationReactSelect>
                                  )}
                                />
                              </GridItem>
                              <GridItem xs={12} sm={12} md={4}>
                                <Field
                                  name={`items[${index}].sku`}
                                  render={({ field }) => (
                                    <IntegrationReactSelect
                                      placeholder="SKU"
                                      label="SKU"
                                      suggestions={skus}
                                      value={field.value || {}}
                                      onChange={item => {
                                        setFieldValue(
                                          `items[${index}].sku`,
                                          item
                                        );
                                        if (item.value) {
                                          setFieldValue(
                                            `items[${index}].size`,
                                            {
                                              value: item.value,
                                              label: item.value.productName
                                            }
                                          );
                                          setFieldValue(
                                            `items[${index}].pcsPerPack`,
                                            item.value.pcsPerPack
                                          );
                                        }
                                      }}
                                    ></IntegrationReactSelect>
                                  )}
                                />
                              </GridItem>
                              <GridItem xs={12} sm={12} md={4}>
                                <Field
                                  name={`items[${index}].pcsPerPack`}
                                  render={({ field }) => (
                                    <CustomInput
                                      labelText="pcs per pack"
                                      formControlProps={{
                                        fullWidth: true
                                      }}
                                      inputProps={{
                                        disabled: true,
                                        type: "number",
                                        ...field,
                                        value: field.value || ""
                                      }}
                                    />
                                  )}
                                />
                              </GridItem>
                            </GridContainer>
                          </React.Fragment>
                        ))}
                      </GridItem>
                    </GridContainer>
                  )}
                />

                <Button
                  color="primary"
                  onClick={e => {
                    this.submitAction = 1;
                    handleSubmit(e);
                  }}
                  disabled={isSubmitting}
                >
                  {this.props.initialData ? "Update Item" : "Save Item"}
                </Button>
                {!this.props.initialData ? (
                  <Button
                    color="primary"
                    onClick={e => {
                      this.submitAction = 2;
                      handleSubmit(e);
                    }}
                    disabled={isSubmitting}
                  >
                    Save On Separate Rows
                  </Button>
                ) : null}
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}
