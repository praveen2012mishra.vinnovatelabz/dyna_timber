import React from "react";
import { inject, observer } from "mobx-react";
import DefaultPage from "views/DefaultPage";
import NonDrumScheduleTable from "views/InputPages/NonDrumSchedule/NonDrumScheduleTable";
import NonDrumScheduleForm from "views/InputPages/NonDrumSchedule/NonDrumScheduleForm";
import ActionIcon from "@material-ui/icons/Launch";

@inject("nonDrumSchedule", "routingStore", "productionData")
@observer
class NonDrumSchedulePage extends DefaultPage {
  aditionalActions = [];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.nonDrumSchedule;
    this.components = {
      table: NonDrumScheduleTable,
      edit: NonDrumScheduleForm
    };
  }

  render() {
    if (!this.defaultStore) return null;
    const { selectedItem } = this.defaultStore;
    const { routingStore, productionData } = this.props;
    if (selectedItem) {
      this.aditionalActions = [
        <ActionIcon
          onClick={async () => {
            let id = selectedItem.id;
            await this.defaultStore.deleteSelected();
            productionData.selectNewTab();
            routingStore.push(`/admin/ProductionData?nonDrumId=${id}`);
          }}
        />
      ];
    } else {
      this.aditionalActions = [];
    }
    return super.render();
  }
}

export default NonDrumSchedulePage;
