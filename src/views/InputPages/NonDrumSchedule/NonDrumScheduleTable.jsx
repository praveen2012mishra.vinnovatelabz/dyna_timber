import DefaultAgTable from "components/Table/DefaultAgTable";
import moment from "moment";
import NonDrumScheduleClient from "api/nonDrumSchedule";
import { inject, observer } from "mobx-react";

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    NonDrumScheduleTable: {},
  };
  if (store.NonDrumScheduleTable) {
    return store.NonDrumScheduleTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    let width = store[componentName][field];
    width = parseInt(width, 10);
    width = width + 45;
    //console.log(width,field,'============>');
    return width;
  }
};

//take text width
function textWidth(text, fontProp, type) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  text = text ? text.toString() : text;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;
  //console.log(result,'====>');
  result = result;
  //console.log(result);

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (params, componentName, field, headerName) => {
  if (params.data) {
    let data = params.data;
    //console.log(data,componentName,field);
    let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
      [componentName]: {},
    };

    store[componentName] = {
      ...store[componentName],
    };
    let storeWidth;
    let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
    let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
    //console.log(fieldWidth,valueWidth,field);
    let arrayWidth = Object.assign([]);
    arrayWidth.push(fieldWidth);
    arrayWidth.push(valueWidth);
    //console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);

    if (Object.keys(store[componentName]).length === 0) {
      let finalWidth = Math.max(...arrayWidth);
      store[componentName] = {
        ...store[componentName],
        [field]: finalWidth,
      };
      sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
    } else if (!store[componentName][field]) {
      let finalWidth = Math.max(...arrayWidth);
      store[componentName] = {
        ...store[componentName],
        [field]: finalWidth,
      };
      sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
    } else if (store[componentName][field] != Math.max(...arrayWidth)) {
      arrayWidth.push(store[componentName][field]);
      let finalWidth = Math.max(...arrayWidth);
      store[componentName] = {
        ...store[componentName],
        [field]: finalWidth,
      };
      sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
    } else {
      //console.log("No change in width resize");
    }
  }
};

@inject("nonDrumSchedule")
@observer
export default class NonDrumScheduleTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Date",
      field: "date",
      suppressSizeToFit: true,
      cellRenderer: (params) => {
        getWidthColumnValue(params, this.componentName, "date", "Date");
        return params.value
          ? new moment(params.value).format("MMMM Do YYYY")
          : "";
      },
      //width: isColumnWidth('date', 'NonDrumScheduleTable'),
      width: 132,
      rowGroup: true,
      filter: "agDateColumnFilter",
      filterParams: {
        comparator: function (filterLocalDateAtMidnight, cellValue) {
          var dateAsString = cellValue;
          if (dateAsString == null) return 0;

          // In the example application, dates are stored as dd/mm/yyyy
          // We create a Date object for comparison against the filter date
          let dateAsDate = new moment(dateAsString, "YYYY-MM-DD").toDate();
          // Now that both parameters are Date objects, we can compare
          if (dateAsDate < filterLocalDateAtMidnight) {
            return -1;
          } else if (dateAsDate > filterLocalDateAtMidnight) {
            return 1;
          } else {
            return 0;
          }
        },
      },
      // valueGetter: (params) => {
      // 	//getWidthColumnValue(params, this.componentName, "date");
      // 	return params.data.date;
      //   },
      hide: isColumnHidden("date"),
    },
    {
      headerName: "Machine",
      field: "machine",
      suppressSizeToFit: true,
      width: isColumnWidth("machine", "NonDrumScheduleTable"),
      rowGroup: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "machine", "Machine");
        if (params.data) {
          return params.data.machine;
        }
      },
      hide: isColumnHidden("machine"),
    },
    {
      headerName: "Time Start",
      field: "timeStart",
      suppressSizeToFit: true,
      width: isColumnWidth("timeStart", "NonDrumScheduleTable"),
      editable: true,
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "timeStart",
          "Time Start"
        );
        if (!params.node.allChildrenCount) {
          try {
            return moment(params.data.timeStart, "HH:mm").format("HH:mm");
          } catch (ex) {
            return params.data.timeStart;
          }
        }
        if (params.node.allChildrenCount > 0 && params.node.leafGroup)
          return moment(
            params.node.allLeafChildren[0].data.machineTimeStart,
            "HH:mm"
          ).format("HH:mm");
        return null;
      },
      valueSetter: async (item) => {
        if (!item.node.allChildrenCount) return;

        await NonDrumScheduleClient.saveMachineTime({
          time:
            item.newValue.split(":").length - 1 === 1
              ? item.newValue + ":00"
              : item.newValue,
          machine: item.node.key,
          date: item.node.parent.key,
        });

        this.props.nonDrumSchedule.fetchAll();

        console.log("Setting", item.node.parent);
      },

      hide: isColumnHidden("timeStart"),
    },
    {
      headerName: "Time Finish",
      field: "timeFinish",
      suppressSizeToFit: true,
      width: isColumnWidth("timeFinish", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "timeFinish",
          "Time Finish"
        );
        if (!params.node.allChildrenCount) {
          try {
            return moment(params.data.timeFinish, "HH:mm").format("HH:mm");
          } catch (ex) {
            return params.data.timeFinish;
          }
        }
        if (params.node.allChildrenCount > 0 && params.node.leafGroup) {
          try {
            let mins = 0;
            params.node.allLeafChildren.forEach(
              (item) => (mins += item.data.minsToProduce)
            );
            let time = moment(
              params.node.allLeafChildren[0].data.machineTimeStart,
              "HH:mm"
            );

            console.log(time, mins);

            time = time.add(mins, "minutes");

            return time.format("HH:mm");
          } catch (ex) {
            return null;
          }
        }
        return null;
      },
      hide: isColumnHidden("timeFinish"),
    },
    {
      headerName: "Raw Material SKU",
      field: "rawMaterialSKU",
      suppressSizeToFit: true,
      width: isColumnWidth("rawMaterialSKU", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "rawMaterialSKU",
          "Raw Material SKU"
        );
        if (params.data) {
          return params.data.rawMaterialSKU;
        }
      },
      hide: isColumnHidden("rawMaterialSKU"),
    },
    {
      headerName: "Size",
      field: "size",
      suppressSizeToFit: true,
      width: isColumnWidth("size", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "size", "Size");
        if (params.data) {
          return params.data.size;
        }
      },
      hide: isColumnHidden("size"),
    },
    {
      headerName: "SKU",
	  field: "sku",
	  suppressSizeToFit: true,
	  width: isColumnWidth("sku", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "sku", "SKU");
        if (params.data) {
          return params.data.sku;
        }
      },
      hide: isColumnHidden("sku"),
    },
    {
      headerName: "Pcs Per Pack",
      field: "pcsPerPack",
      suppressSizeToFit: true,
      width: isColumnWidth("pcsPerPack", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "pcsPerPack",
          "Pcs Per Pack"
        );
        if (params.data) {
          return params.data.pcsPerPack;
        }
      },
      hide: isColumnHidden("pcsPerPack"),
    },
    {
      headerName: "Customer",
      field: "customer",
      suppressSizeToFit: true,
      width: isColumnWidth("customer", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "customer", "Customer");
        if (params.data) {
          return params.data.customer;
        }
      },
      hide: isColumnHidden("customer"),
    },
    {
      headerName: "# Packs",
      field: "numPacks",
      suppressSizeToFit: true,
      width: isColumnWidth("numPacks", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "numPacks", "# Packs");
        if (params.data) {
          return params.data.numPacks;
        }
      },
      hide: isColumnHidden("numPacks"),
    },
    {
      headerName: "Num. Finished",
      field: "numFinished",
      suppressSizeToFit: true,
      width: isColumnWidth("numFinished", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "numFinished",
          "Num. Finished"
        );
        if (params.data) {
          return params.data.numFinished;
        }
      },
      hide: isColumnHidden("numFinished"),
    },
    {
      headerName: "Num. Different Sizes",
      field: "numDifferentFinished",
      suppressSizeToFit: true,
      width: isColumnWidth("numDifferentFinished", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "numDifferentFinished",
          "Num. Different Sizes"
        );
        if (params.data) {
          return params.data.numDifferentFinished;
        }
      },
      hide: isColumnHidden("numDifferentFinished"),
    },
    {
      headerName: "M3",
      field: "m3",
      suppressSizeToFit: true,
      width: isColumnWidth("m3", "NonDrumScheduleTable"),
      cellRenderer: (params) => (params.value ? +params.value.toFixed(2) : ""),
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "m3", "M3");
        if (params.data) {
          return params.data.m3;
        }
      },
      hide: isColumnHidden("m3"),
    },
    {
      headerName: "Minutes to Produce",
      field: "minsToProduce",
      suppressSizeToFit: true,
      width: isColumnWidth("minsToProduce", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "minsToProduce",
          "Minutes to Produce"
        );
        if (params.data) {
          return params.data.minsToProduce;
        }
      },
      hide: isColumnHidden("minsToProduce"),
    },
    {
      headerName: "Pack Number",
      field: "packNumber",
      suppressSizeToFit: true,
      width: isColumnWidth("packNumber", "NonDrumScheduleTable"),
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "packNumber",
          "Pack Number"
        );
        if (params.data) {
          return params.data.packNumber;
        }
      },
      hide: isColumnHidden("packNumber"),
    },
  ];

  rowMultiSelectWithClick = true;
  rowSelection = "multiple";
  constructor(props) {
    super(props);
    this.componentName = "NonDrumScheduleTable";
    this.printHeader = "Non Drum Schedule Table";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}
