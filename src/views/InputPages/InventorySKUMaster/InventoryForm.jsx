import React from "react";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import InventoryClient from "api/inventory";
import { inject, observer } from "mobx-react";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";
import * as _ from "lodash";

const getOrderSize = (ordersizeArray) => {
  let arrayOfOrderSize = [];
  ordersizeArray.forEach((ele) => {
    arrayOfOrderSize = _.union(arrayOfOrderSize, [ele.label]);
  });
  arrayOfOrderSize = arrayOfOrderSize.map((ele) => {
    return { label: ele, value: "ele" };
  });
  return arrayOfOrderSize;
};

@inject("notifications", "inventory","materials")
@observer
class InventoryForm extends React.Component {
  componentDidMount() {
    this.props.inventory.fetchAll();
    this.props.materials.fetchAll();
  }

  render() {
    const { notifications, inventory, materials,initialData } = this.props;
    const feedStock1 = materials.data.map((suggestion) => ({
      value: suggestion,
      label: suggestion.code,
    }));
    let feedStock = getOrderSize(feedStock1);
    const feedStock1Machine = inventory.data.map((suggestion) => ({
        value: suggestion,
        label: suggestion.feedStock1Machine,
      }));
      let machine = getOrderSize(feedStock1Machine);
      machine=machine.filter((ele)=>{
        return ele.label!==null && ele.label!==""
      })
      //console.log(machine,"================>");
      
    return (
      <Formik
        enableReinitialize
        initialValues={initialData ? initialData : {}}
        onSubmit={async (values, actions) => {
          try {
              //console.log(values,'===============>');
              if (_.get(values, "feedStock2Machine.label")) {
                values.feedStock2Machine = values.feedStock2Machine.label;
              }
              if (_.get(values, "feedStock1Machine.label")) {
                values.feedStock1Machine = values.feedStock1Machine.label;
              }
              if (_.get(values, "feedStock1.label")) {
                values.feedStock1 = values.feedStock1.label;
              }
              if (_.get(values, "feedStock2.label")) {
                values.feedStock2 = values.feedStock2.label;
              }
            // values.feedStock2Machine=values.feedStock2Machine?values.feedStock2Machine.label:'';
            // values.feedStock1Machine=values.feedStock1Machine?values.feedStock1Machine.label:'';
            // values.feedStock1=values.feedStock1?values.feedStock1.label:'';
            // values.feedStock2=values.feedStock2?values.feedStock2.label:'';
            if (values.id) await InventoryClient.update(values);
            else await InventoryClient.save(values);
            notifications.enqueueSnackbar({ message: "Saved." });
            inventory.fetchAll();
            inventory.selectItem(null);
            inventory.selectItemsTab();
          } catch (err) {
            console.error(err);
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" },
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, isSubmitting, setFieldValue }) => (
          <form onSubmit={handleSubmit}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="sku"
                      render={({ field }) => (
                        <CustomInput
                          labelText="SKU"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="productName"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Product Name"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="productCategory"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Product Category"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={2}>
                    <Field
                      name="pcsPerPack"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Pcs/Pack"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="stock"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Stock"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="width"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Width (mm)"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="depth"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Depth (mm)"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="length"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Length (mm)"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="feedStock1"
                      render={({ field }) => (
                        // <CustomInput
                        //     labelText="Feedstock1"
                        //     formControlProps={{
                        //         fullWidth: true
                        //     }}
                        //     inputProps={{
                        //         ...field,
                        //         value: field.value || ''
                        //     }}
                        // />
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="feedStock1"
                          suggestions={feedStock}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                          value={field.value || {}}
                          onChange={(item) => {
                            setFieldValue("feedStock1", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="feedStock1Machine"
                      render={({ field }) => (
                        // <CustomInput
                        //   labelText="Machine"
                        //   formControlProps={{
                        //     fullWidth: true,
                        //   }}
                        //   inputProps={{
                        //     ...field,
                        //     value: field.value || "",
                        //   }}
                        // />
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Machine"
                          suggestions={machine}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                          value={field.value || {}}
                          onChange={(item) => {
                            setFieldValue("feedStock1Machine", item);
                           
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={2}>
                    <Field
                      name="feedStock1PcsOut"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Pcs Out"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={2}>
                    <Field
                      name="feedStock1Pct"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Feedstock1 %"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="feedStock2"
                      render={({ field }) => (
                        // <CustomInput
                        //   labelText="Feedstock2"
                        //   formControlProps={{
                        //     fullWidth: true,
                        //   }}
                        //   inputProps={{
                        //     ...field,
                        //     value: field.value || "",
                        //   }}
                        // />
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Feedstock2"
                          suggestions={feedStock}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                          value={field.value || {}}
                          onChange={(item) => {
                            setFieldValue("feedStock2", item);
                           
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="feedStock2Machine"
                      render={({ field }) => (
                        <IntegrationReactSelect
                        placeholder={`${field.value}`}
                        label="Machine"
                        suggestions={machine}
                        inputProps={{
                          ...field,
                          value: field.value || "",
                        }}
                        value={field.value || {}}
                        onChange={(item) => {
                          setFieldValue("feedStock2Machine", item);
                         
                        }}
                      ></IntegrationReactSelect>
                        // <CustomInput
                        //   labelText="Machine"
                        //   formControlProps={{
                        //     fullWidth: true,
                        //   }}
                        //   inputProps={{
                        //     ...field,
                        //     value: field.value || "",
                        //   }}
                        // />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={2}>
                    <Field
                      name="feedStock2PcsOut"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Pcs Out"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={2}>
                    <Field
                      name="feedStock2Pct"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Feedstock2 %"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={12}>
                    <Field
                      name="lastProcess"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Last Process"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>

                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}

export default InventoryForm;
