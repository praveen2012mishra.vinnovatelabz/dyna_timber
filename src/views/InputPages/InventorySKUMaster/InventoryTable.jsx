import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";
import InventoryClient from "../../../api/inventory";
import * as _ from "lodash";
import { get } from "lodash";

//decimal precision
const setPrecision = (val, precision) => val.toFixed(precision);

//hide column
function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    InventoryTable: {},
  };
  if (store.InventoryTable) {
    return store.InventoryTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    let width = store[componentName][field];
    width = parseInt(width, 10);
    width = width + 45;
    //console.log(width,field,'============>');
    return width;
  }
};

//take text width
function textWidth(text, fontProp, type) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  text = text ? text.toString() : text;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;
  //console.log(result,'====>');
  result = result;
  //console.log(result);

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (params, componentName, field, headerName) => {
  let data = params.data;
  //console.log(data,componentName,field);
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };

  store[componentName] = {
    ...store[componentName],
  };
  let storeWidth;
  let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
  let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth, valueWidth, field);
  let arrayWidth = Object.assign([]);
  arrayWidth.push(fieldWidth);
  arrayWidth.push(valueWidth);
  //console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);

  if (Object.keys(store[componentName]).length === 0) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (!store[componentName][field]) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (store[componentName][field] != Math.max(...arrayWidth)) {
    arrayWidth.push(store[componentName][field]);
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else {
    //console.log("No change in width resize");
  }
};

//inline edit feature
const onCellValueChangedWidth = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("widthIM")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("widthIM", JSON.stringify(store));
  params.data.width = params.newValue;
  updateWidth(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWidth = (width, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(width)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWidth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("widthIM")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.width;
};

const valueSetterWidth = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.width)) return false;
  let store = JSON.parse(sessionStorage.getItem("widthIM")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("widthIM", JSON.stringify(store));
  params.data.width = params.newValue;
  return true;
};

const cellRendererWidth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("widthIM")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.width;
};

const onCellValueChangedDepth = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("depthIM")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("depthIM", JSON.stringify(store));
  params.data.depth = params.newValue;
  updateDepth(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateDepth = (depth, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(depth)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterDepth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("depthIM")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.depth;
};

const valueSetterDepth = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.depth)) return false;
  let store = JSON.parse(sessionStorage.getItem("depthIM")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("depthIM", JSON.stringify(store));
  params.data.depth = params.newValue;
  return true;
};

const cellRendererDepth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("depthIM")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.depth;
};

const onCellValueChangedLength = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("lengthIM")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("lengthIM", JSON.stringify(store));
  params.data.length = params.newValue;
  updateLength(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateLength = (length, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(length)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterLength = (params) => {
  let store = JSON.parse(sessionStorage.getItem("lengthIM")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.length;
};

const valueSetterLength = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.length)) return false;
  let store = JSON.parse(sessionStorage.getItem("lengthIM")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("lengthIM", JSON.stringify(store));
  params.data.length = params.newValue;
  return true;
};

const cellRendererLength = (params) => {
  let store = JSON.parse(sessionStorage.getItem("lengthIM")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.length;
};

const onCellValueChangedPcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pcsPerPack", JSON.stringify(store));
  params.data.pcsPerPack = params.newValue;
  updatePcsPerPack(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatePcsPerPack = (pcsPerPack, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(pcsPerPack)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterPcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.pcsPerPack;
};

const valueSetterPcsPerPack = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.pcsPerPack))
    return false;
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pcsPerPack", JSON.stringify(store));
  params.data.pcsPerPack = params.newValue;
  return true;
};

const cellRendererPcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.pcsPerPack;
};

const onCellValueChangedFeedStock1Pct = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1Pct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock1Pct", JSON.stringify(store));
  params.data.feedStock1Pct = params.newValue;
  updateFeedStock1Pct(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateFeedStock1Pct = (feedStock1Pct, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(feedStock1Pct)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterFeedStock1Pct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1Pct")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.feedStock1Pct;
};

const valueSetterFeedStock1Pct = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.feedStock1Pct))
    return false;
  let store = JSON.parse(sessionStorage.getItem("feedStock1Pct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock1Pct", JSON.stringify(store));
  params.data.feedStock1Pct = params.newValue;
  return true;
};

const cellRendererFeedStock1Pct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1Pct")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.feedStock1Pct;
};

const onCellValueChangedFeedStock1PcsOut = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1PcsOut")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock1PcsOut", JSON.stringify(store));
  params.data.feedStock1PcsOut = params.newValue;
  updateFeedStock1PcsOut(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateFeedStock1PcsOut = (feedStock1PcsOut, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(feedStock1PcsOut)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterFeedStock1PcsOut = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1PcsOut")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.feedStock1PcsOut;
};

const valueSetterFeedStock1PcsOut = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.feedStock1PcsOut))
    return false;
  let store = JSON.parse(sessionStorage.getItem("feedStock1PcsOut")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock1PcsOut", JSON.stringify(store));
  params.data.feedStock1PcsOut = params.newValue;
  return true;
};

const cellRendererFeedStock1PcsOut = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1PcsOut")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.feedStock1PcsOut;
};

const onCellValueChangedFeedStock1RecPct = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1RecPct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock1RecPct", JSON.stringify(store));
  params.data.feedStock1RecPct = params.newValue;
  updateFeedStock1RecPct(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateFeedStock1RecPct = (feedStock1RecPct, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(feedStock1RecPct)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterFeedStock1RecPct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1RecPct")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.feedStock1RecPct;
};

const valueSetterFeedStock1RecPct = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.feedStock1RecPct))
    return false;
  let store = JSON.parse(sessionStorage.getItem("feedStock1RecPct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock1RecPct", JSON.stringify(store));
  params.data.feedStock1RecPct = params.newValue;
  return true;
};

const cellRendererFeedStock1RecPct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock1RecPct")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.feedStock1RecPct;
};

const onCellValueChangedFeedStock2Pct = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2Pct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock2Pct", JSON.stringify(store));
  params.data.feedStock2Pct = params.newValue;
  updateFeedStock2Pct(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateFeedStock2Pct = (feedStock2Pct, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(feedStock2Pct)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterFeedStock2Pct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2Pct")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.feedStock2Pct;
};

const valueSetterFeedStock2Pct = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.feedStock2Pct))
    return false;
  let store = JSON.parse(sessionStorage.getItem("feedStock2Pct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock2Pct", JSON.stringify(store));
  params.data.feedStock2Pct = params.newValue;
  return true;
};

const cellRendererFeedStock2Pct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2Pct")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.feedStock2Pct;
};

const onCellValueChangedFeedStock2PcsOut = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2PcsOut")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock2PcsOut", JSON.stringify(store));
  params.data.feedStock2PcsOut = params.newValue;
  updateFeedStock2PcsOut(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateFeedStock2PcsOut = (feedStock2PcsOut, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(feedStock2PcsOut)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterFeedStock2PcsOut = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2PcsOut")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.feedStock2PcsOut;
};

const valueSetterFeedStock2PcsOut = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.feedStock2PcsOut))
    return false;
  let store = JSON.parse(sessionStorage.getItem("feedStock2PcsOut")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock2PcsOut", JSON.stringify(store));
  params.data.feedStock2PcsOut = params.newValue;
  return true;
};

const cellRendererFeedStock2PcsOut = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2PcsOut")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.feedStock2PcsOut;
};

const onCellValueChangedFeedStock2RecPct = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2RecPct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock2RecPct", JSON.stringify(store));
  params.data.feedStock2RecPct = params.newValue;
  updateFeedStock2RecPct(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateFeedStock2RecPct = (feedStock2RecPct, props) => {
  //console.log("------------------------------->", reliability);
  InventoryClient.update(feedStock2RecPct)
    .then((res) => {
      props.inventory.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterFeedStock2RecPct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2RecPct")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.feedStock2RecPct;
};

const valueSetterFeedStock2RecPct = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.feedStock2RecPct))
    return false;
  let store = JSON.parse(sessionStorage.getItem("feedStock2RecPct")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("feedStock2RecPct", JSON.stringify(store));
  params.data.feedStock2RecPct = params.newValue;
  return true;
};

const cellRendererFeedStock2RecPct = (params) => {
  let store = JSON.parse(sessionStorage.getItem("feedStock2RecPct")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.feedStock2RecPct;
};

@inject("inventory")
@observer
class InventoryTable extends DefaultAgTable {
  columns = [
    {
      headerName: "SKU",
      field: "sku",
      suppressSizeToFit: true,
      sort: "asc",
      //width:800,
      width: isColumnWidth("sku", "InventoryTable", "SKU"),
      hide: isColumnHidden("sku"),
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "sku");
        return params.data.sku;
      },
    },
    {
      headerName: "Product Name",
      field: "productName",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(
          params,
          this.componentName,
          "productName",
          "Product Name"
        );
        return params.data.productName;
      },
      width: isColumnWidth("productName", "InventoryTable"),
      hide: isColumnHidden("productName"),
    },
    {
      headerName: "Product Category",
      field: "productCategory",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(
          params,
          this.componentName,
          "productCategory",
          "Product Category"
        );
        return params.data.productCategory;
      },
      width: isColumnWidth("productCategory", "InventoryTable"),
      hide: isColumnHidden("productCategory"),
    },
    {
      headerName: "Stock",
      suppressSizeToFit: true,
      width: isColumnWidth("stock", "InventoryTable"),
      field: "stock",
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "stock", "Stock");
        return params.data.stock;
      },
      hide: isColumnHidden("stock"),
    },
    {
      headerName: "Width",
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedWidth(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "width", "Width");
        return valueGetterWidth(params);
      },
      valueSetter: (params) => {
        return valueSetterWidth(params);
      },
      cellRenderer: (params) => {
        return cellRendererWidth(params);
      },
      width: isColumnWidth("width", "InventoryTable"),
      field: "width",
      hide: isColumnHidden("width"),
    },
    {
      headerName: "Depth",
      suppressSizeToFit: true,
      width: isColumnWidth("depth", "InventoryTable"),
      field: "depth",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedDepth(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "depth", "Depth");
        return valueGetterDepth(params);
      },
      valueSetter: (params) => {
        return valueSetterDepth(params);
      },
      cellRenderer: (params) => {
        return cellRendererDepth(params);
      },
      hide: isColumnHidden("depth"),
    },
    {
      headerName: "Length",
      suppressSizeToFit: true,
      width: isColumnWidth("length", "InventoryTable"),
      field: "length",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedLength(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "length", "Length");
        return valueGetterLength(params);
      },
      valueSetter: (params) => {
        return valueSetterLength(params);
      },
      cellRenderer: (params) => {
        return cellRendererLength(params);
      },
      hide: isColumnHidden("length"),
    },
    {
      headerName: "Pcs/Pack",
      field: "pcsPerPack",
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedPcsPerPack(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "pcsPerPack",
          "Pcs/Pack"
        );
        return valueGetterPcsPerPack(params);
      },
      valueSetter: (params) => {
        return valueSetterPcsPerPack(params);
      },
      cellRenderer: (params) => {
        return cellRendererPcsPerPack(params);
      },
      width: isColumnWidth("pcsPerPack", "InventoryTable"),
      hide: isColumnHidden("pcsPerPack"),
    },
    {
      headerName: "m3/Pack",
      field: "m3PerPack",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "m3PerPack", "m3/Pack");
        return params.data.m3PerPack;
      },
      width: isColumnWidth("m3PerPack", "InventoryTable"),
      hide: isColumnHidden("m3PerPack"),
    },
    {
      headerName: "Feedstock1",
      field: "feedStock1",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock1",
          "Feedstock1"
        );
        return params.data.feedStock1;
      },
      width: isColumnWidth("feedStock1", "InventoryTable"),
      hide: isColumnHidden("feedStock1"),
    },
    {
      headerName: "Pct%",
      field: "feedStock1Pct",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedFeedStock1Pct(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock1Pct",
          "Pct%"
        );
        return valueGetterFeedStock1Pct(params);
      },
      valueSetter: (params) => {
        return valueSetterFeedStock1Pct(params);
      },
      cellRenderer: (params) => {
        return cellRendererFeedStock1Pct(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("feedStock1Pct", "InventoryTable"),
      hide: isColumnHidden("feedStock1Pct"),
    },
    {
      headerName: "Machine",
      field: "feedStock1Machine",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock1Machine",
          "Machine"
        );
        return params.data.feedStock1Machine;
      },
      width: isColumnWidth("feedStock1Machine", "InventoryTable"),
      hide: isColumnHidden("feedStock1Machine"),
    },
    {
      headerName: "Pcs Out",
      field: "feedStock1PcsOut",
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedFeedStock1PcsOut(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock1PcsOut",
          "Pcs Out"
        );
        return valueGetterFeedStock1PcsOut(params);
      },
      valueSetter: (params) => {
        return valueSetterFeedStock1PcsOut(params);
      },
      cellRenderer: (params) => {
        return cellRendererFeedStock1PcsOut(params);
      },
      width: isColumnWidth("feedStock1PcsOut", "InventoryTable"),
      hide: isColumnHidden("feedStock1PcsOut"),
    },
    {
      headerName: "Rec Pct%",
      field: "feedStock1RecPct",
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedFeedStock1RecPct(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock1RecPct",
          "Rec Pct%",
          "Rec Pct%"
        );
        return valueGetterFeedStock1RecPct(params);
      },
      valueSetter: (params) => {
        return valueSetterFeedStock1RecPct(params);
      },
      cellRenderer: (params) => {
        return cellRendererFeedStock1RecPct(params);
      },
      width: isColumnWidth("feedStock1RecPct", "InventoryTable"),
      hide: isColumnHidden("feedStock1RecPct"),
    },

    {
      headerName: "Feedstock2",
      field: "feedStock2",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock2",
          "Feedstock2"
        );
        return params.data.feedStock2;
      },
      width: isColumnWidth("feedStock2", "InventoryTable"),
      hide: isColumnHidden("feedStock2"),
    },
    {
      headerName: "Pct%",
      field: "feedStock2Pct",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedFeedStock2Pct(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock2Pct",
          "Pct%"
        );
        return valueGetterFeedStock2Pct(params);
      },
      valueSetter: (params) => {
        return valueSetterFeedStock2Pct(params);
      },
      cellRenderer: (params) => {
        return cellRendererFeedStock2Pct(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("feedStock2Pct", "InventoryTable"),
      hide: isColumnHidden("feedStock2Pct"),
    },
    {
      headerName: "Machine",
      field: "feedStock2Machine",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock2Machine",
          "Machine"
        );
        return params.data.feedStock2Machine;
      },
      width: isColumnWidth("feedStock2Machine", "InventoryTable"),
      hide: isColumnHidden("feedStock2Machine"),
    },
    {
      headerName: "Pcs Out",
      field: "feedStock2PcsOut",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedFeedStock2PcsOut(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock2PcsOut",
          "Pcs Out"
        );
        return valueGetterFeedStock2PcsOut(params);
      },
      valueSetter: (params) => {
        return valueSetterFeedStock2PcsOut(params);
      },
      cellRenderer: (params) => {
        return cellRendererFeedStock2PcsOut(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("feedStock2PcsOut", "InventoryTable"),
      hide: isColumnHidden("feedStock2PcsOut"),
    },
    {
      headerName: "Rec Pct%",
      field: "feedStock2RecPct",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedFeedStock2RecPct(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "feedStock2RecPct",
          "Rec Pct%"
        );
        return valueGetterFeedStock2RecPct(params);
      },
      valueSetter: (params) => {
        return valueSetterFeedStock2RecPct(params);
      },
      cellRenderer: (params) => {
        return cellRendererFeedStock2RecPct(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("feedStock2RecPct", "InventoryTable"),
      hide: isColumnHidden("feedStock2RecPct"),
    },

    {
      headerName: "Last Process",
      field: "lastProcess",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(
          params,
          this.componentName,
          "lastProcess",
          "Last Process"
        );
        return params.data.lastProcess;
      },
      width: isColumnWidth("lastProcess", "InventoryTable"),
      hide: isColumnHidden("lastProcess"),
    },

    {
      headerName: "Usage",
      field: "usage",
      suppressSizeToFit: true,
      width: isColumnWidth("usage", "InventoryTable"),
      hide: isColumnHidden("usage"),
      valueGetter: (params) => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "usage", "Usage");
        return params.data.usage;
      },
      cellRenderer: (params) => params.data.usage,
    },
  ];

  //rowMultiSelectWithClick = true;
  rowSelection = "multiple";

  constructor(props) {
    super(props);
    this.defaultStore = this.props.inventory;
    this.componentName = "InventoryTable";
    this.printHeader = "Inventory Table";
  }

  onPaginationChanged=()=>{
    console.log('hi i am praveen brooo------------------------>');
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}

export default InventoryTable;
