import {inject, observer} from "mobx-react";
import DefaultPage from "views/DefaultPage";
import InventoryTable from "views/InputPages/InventorySKUMaster/InventoryTable";
import InventoryForm from "views/InputPages/InventorySKUMaster/InventoryForm";

@inject("inventory")
@observer
export default class InventorySKUMasterPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.inventory;
        this.components = {
            table: InventoryTable,
            edit: InventoryForm
        };
    }
}

