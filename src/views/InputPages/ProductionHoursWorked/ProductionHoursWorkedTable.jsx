import { inject, observer } from "mobx-react";
import moment from "moment";
import DefaultAgTable from "components/Table/DefaultAgTable";

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    ProductionHoursWorkedTable: {},
  };
  if (store.ProductionHoursWorkedTable) {
    return store.ProductionHoursWorkedTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+45;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

@inject("productionHoursWorked")
@observer
export default class ProductionHoursWorkedTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Date",
      field: "date",
      hide: isColumnHidden("date"),
      // width: isColumnWidth(
      //   "date",
      //   "ProductionHoursWorkedTable"
      // ),
      width: 154,
      suppressSizeToFit: true,
      cellRenderer: (params) =>{
				getWidthColumnValue(params, this.componentName, "date","Date");
				return params.value ? new moment(params.value).format("MMMM Do YYYY") : ""},
      chartDataType: "excluded",
      filter: "agDateColumnFilter",
      filterParams: {
        comparator: function (filterLocalDateAtMidnight, cellValue) {
          var dateAsString = cellValue;
          if (dateAsString == null) return 0;

          // In the example application, dates are stored as dd/mm/yyyy
          // We create a Date object for comparison against the filter date
          let dateAsDate = new moment(dateAsString, "YYYY-MM-DD").toDate();
          // Now that both parameters are Date objects, we can compare
          if (dateAsDate < filterLocalDateAtMidnight) {
            return -1;
          } else if (dateAsDate > filterLocalDateAtMidnight) {
            return 1;
          } else {
            return 0;
          }
        },
      },
    },
    {
      headerName: "Production Line",
      field: "productionLine",
      width: isColumnWidth(
        "productionLine",
        "ProductionHoursWorkedTable"
      ),
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "productionLine","Production Line");
        return params.data.productionLine;
      },
      suppressSizeToFit: true,
      hide: isColumnHidden("productionLine"),
      chartDataType: "category",
    },
    {
      headerName: "Number of Employees",
      field: "numOfEmployees",
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "numOfEmployees","Number of Employees");
        return params.data.numOfEmployees;
      },
      width: isColumnWidth(
        "numOfEmployees",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      hide: isColumnHidden("numOfEmployees"),
    },
    {
      headerName: "Employees",
      field: "employees",
      width: isColumnWidth(
        "employees",
        "ProductionHoursWorkedTable"
      ),
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "employees","Employees");
        return params.data.employees;
      },
      suppressSizeToFit: true,
      hide: isColumnHidden("employees"),
    },
    {
      headerName: "Time Start",
      field: "timeStart",
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "timeStart","Time Start");
        return params.data.timeStart;
      },
      width: isColumnWidth(
        "timeStart",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      hide: isColumnHidden("timeStart"),
    },
    {
      headerName: "Time Finish",
      field: "timeFinish",
      width: isColumnWidth(
        "timeFinish",
        "ProductionHoursWorkedTable"
      ),
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "timeFinish","Time Finish");
        return params.data.timeFinish;
      },
      suppressSizeToFit: true,
      hide: isColumnHidden("timeFinish"),
    },
    {
      headerName: "Total Minutes for Breaks",
      field: "totalMinutesForBreaks",
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "totalMinutesForBreaks","Total Minutes for Breaks");
        return params.data.totalMinutesForBreaks;
      },
      width: isColumnWidth(
        "totalMinutesForBreaks",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      hide: isColumnHidden("totalMinutesForBreaks"),
    },
    {
      headerName: "Downtime",
      field: "downtime",
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "downtime","Downtime");
        return params.data.downtime;
      },
      width: isColumnWidth(
        "downtime",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      hide: isColumnHidden("downtime"),
    },
    {
      headerName: "Downtime Reason",
      field: "downtimeReason",
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "downtimeReason","Downtime Reason");
        return params.data.downtimeReason;
      },
      width: isColumnWidth(
        "downtimeReason",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      hide: isColumnHidden("downtimeReason"),
    },

    {
      headerName: "Time Operated in Minutes",
      field: "actualTimeOperated",
      valueGetter: params => {
        //console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
        getWidthColumnValue(params, this.componentName, "actualTimeOperated","Time Operated in Minutes");
        return params.data.actualTimeOperated;
      },
      width: isColumnWidth(
        "actualTimeOperated",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      hide: isColumnHidden("actualTimeOperated"),
    },
    {
      headerName: "Total m3 Produced",
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "totalM3Produced","Total m3 Produced");
        try {
          return params.data.totalM3Produced;
        } catch (ex) {
          return null;
        }
      },
      hide: isColumnHidden("totalM3Produced"),
      width: isColumnWidth(
        "totalM3Produced",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      field: "totalM3Produced",
    },
    {
      headerName: "m3/hour",
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "m3perHour","m3/hour");
        try {
          return params.data.m3perHour;
        } catch (ex) {
          return null;
        }
      },
      hide: isColumnHidden("m3perHour"),
      width: isColumnWidth(
        "m3perHour",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      field: "m3perHour",
    },
    {
      headerName: "m3/man-hour",
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "m3perHour","m3/man-hour");
        try {
          //console.log("aaa", params.data.m3manHour);
          return params.data.m3manHour;
        } catch (ex) {
          return null;
        }
      },
      hide: isColumnHidden("m3manHour"),
      width: isColumnWidth(
        "m3manHour",
        "ProductionHoursWorkedTable"
      ),
      suppressSizeToFit: true,
      field: "m3manHour",
    },
  ];

  rowMultiSelectWithClick = true;
  rowSelection = "multiple";

  constructor(props) {
    super(props);
    this.defaultStore = this.props.productionHoursWorked;
    this.componentName = "ProductionHoursWorkedTable";
    this.printHeader = "Production Hours Worked Table";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}
