import React from "react";
import { inject, observer } from "mobx-react";
import ProductionHoursWorkedTable from "./ProductionHoursWorkedTable";
import ProductionHoursWorkedForm from "./ProductionHoursWorkedForm";
import DefaultPage from "views/DefaultPage";

@inject("productionHoursWorked")
@observer
class ProductionHoursWorkedPage extends DefaultPage {
  constructor(props) {
    super(props);
    this.defaultStore = this.props.productionHoursWorked;
    this.components = {
      table: ProductionHoursWorkedTable,
      edit: ProductionHoursWorkedForm
    };
  }
}

export default ProductionHoursWorkedPage;
