import React from "react";
import * as _ from "lodash";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core";
import ProductionHoursWorkedClient from "api/productionHoursWorked";
import Datetime from "react-datetime";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import customInputStyle from "assets/jss/material-dashboard-pro-react/components/customInputStyle.jsx";
import moment from "moment";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";

const styles = {
  label: {
    ...customInputStyle.labelRoot
  }
};

@inject("notifications", "productionHoursWorked", "machines")
@observer
class ProductionHoursWorkedForm extends React.Component {
  async componentDidMount() {
    this.props.machines.fetchAll();
  }

  render() {
    const {
      notifications,
      productionHoursWorked,
      machines,
      initialData
    } = this.props;

    const productionLines = machines.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.name
    }));

    let init;
    if (initialData) {
      init = _.clone(initialData, true);

      if (init.productionLine && productionLines) {
        init.productionLine = productionLines.find(
          item => item.label === init.productionLine
        );
      }
    }

    return (
      <Formik
        enableReinitialize
        initialValues={
          init
            ? init
            : {
                timeStart: "09:00",
                timeFinish: "10:00",
                date: moment().format("YYYY-MM-DD")
              }
        }
        onSubmit={async (values, actions) => {
          try {
            if (_.get(values, "productionLine.label")) {
              values.productionLine = values.productionLine.label;
            }
            if (values.id) await ProductionHoursWorkedClient.update(values);
            else await ProductionHoursWorkedClient.save(values);
            notifications.enqueueSnackbar({ message: "Saved." });
            productionHoursWorked.fetchAll();
            productionHoursWorked.selectItem(null);
            productionHoursWorked.selectItemsTab();
          } catch (err) {
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" }
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, setFieldValue, isSubmitting, values }) => (
          <form onSubmit={handleSubmit}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="date"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={field.value}
                            inputProps={{
                              value: new moment(field.value).format(
                                "MMMM Do YYYY"
                              )
                            }}
                            onChange={item => {
                              setFieldValue("date", item.format("YYYY-MM-DD"));
                            }}
                            dateFormat={"MMMM Do YYYY"}
                            timeFormat={false}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="productionLine"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Production Line"
                          label="Production Line"
                          suggestions={productionLines}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("productionLine", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="numOfEmployees"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Number of Employees"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: 1,
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="employees"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Employees"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="timeStart"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={moment(field.value, "HH:mm")}
                            onChange={item => {
                              setFieldValue("timeStart", item.format("HH:mm"));
                            }}
                            dateFormat={false}
                            inputProps={{ placeholder: "Time Start" }}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="timeFinish"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={moment(field.value, "HH:mm")}
                            onChange={item => {
                              console.log("Time finish", item);
                              setFieldValue("timeFinish", item.format("HH:mm"));
                            }}
                            dateFormat={false}
                            inputProps={{ placeholder: "Time Finish" }}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="totalMinutesForBreaks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Total Minutes for Breaks"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: 1,
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="downtime"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Downtime"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            step: 1,
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="downtimeReason"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Downtime reason"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                {/* <button
                  type="submit"
                  disabled
                  style={{ display: "none" }}
                  aria-hidden="true"
                ></button> */}
                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {this.props.initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}

export default withStyles(styles)(ProductionHoursWorkedForm);
