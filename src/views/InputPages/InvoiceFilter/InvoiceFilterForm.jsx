import React from "react";
import InvoiceFilterClient from "api/invoiceFilter";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import { inject, observer } from "mobx-react";
import * as _ from "lodash";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";

@inject("invoiceFilter", "notifications")
@observer
export default class InvoiceFilterForm extends React.Component {
  render() {
    const { notifications, invoiceFilter } = this.props;

    const types = [
      {
        value: "shippingmethod",
        label: "Shipping Method"
      },
      {
        value: "salesperson",
        label: "Sales Person"
      }
    ];

    const shipViaValues = invoiceFilter.shipVia.map(suggestion => ({
      value: suggestion,
      label: suggestion
    }));

    const salesPersonValues = invoiceFilter.salesPerson.map(suggestion => ({
      value: suggestion,
      label: suggestion
    }));

    return (
      <Formik
        enableReinitialize
        initialValues={
          this.props.initialData
            ? this.props.initialData
            : {
                type: {
                  value: "shippingmethod",
                  label: "Shipping Method"
                }
              }
        }
        onSubmit={async (values, actions) => {
          try {
            if (_.get(values, "type.value")) {
              values.type = values.type.value;
            }
            if (_.get(values, "value.value")) {
              values.value = values.value.value;
            }
            if (values.id) await InvoiceFilterClient.update(values);
            else await InvoiceFilterClient.save(values);
            notifications.enqueueSnackbar({ message: "Saved." });
            invoiceFilter.fetchAll();
            invoiceFilter.selectItem(null);
            invoiceFilter.selectItemsTab();
          } catch (err) {
            console.error(err);
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" }
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, setFieldValue, isSubmitting, values }) => (
          <form onSubmit={handleSubmit}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="type"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Type"
                          label="Type"
                          suggestions={types}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("type", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="value"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Value"
                          label="Value"
                          suggestions={
                            values.type.value == "shippingmethod"
                              ? shipViaValues
                              : salesPersonValues
                          }
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("value", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                </GridContainer>

                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {this.props.initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}
