import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    InvoiceFilterTable: {},
  };
  if (store.InvoiceFilterTable) {
    return store.InvoiceFilterTable[field] || false;
  }
  return false;
}

const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    //debugger
    let width = store[componentName][field];
    width = parseInt(width, 10);
    return width;
  } else {
    return 250;
  }
};

@inject("invoiceFilter")
@observer
class InvoiceFilterTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Type",
      field: "type",
      width: isColumnWidth("type", "InvoiceFilterTable"),
      suppressSizeToFit: true,
      valueGetter: (params) => {
        if (params.data.type == "shippingmethod") return "Shipping Method";
        if (params.data.type == "salesperson") return "Sales Person";
      },
      hide: isColumnHidden("type"),
    },
    {
      headerName: "Value",
      field: "value",
      width: isColumnWidth("value", "InvoiceFilterTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("value"),
    },
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.invoiceFilter;
    this.componentName = 'InvoiceFilterTable';
    this.printHeader = 'Invoice Filter Table';
  }
}

export default InvoiceFilterTable;
