import {inject, observer} from "mobx-react";
import InvoiceFilterForm from "views/InputPages/InvoiceFilter/InvoiceFilterForm";
import InvoiceFilterTable from "views/InputPages/InvoiceFilter/InvoiceFilterTable";
import DefaultPage from "views/DefaultPage";


@inject("invoiceFilter")
@observer
export default class InvoiceFilterPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.invoiceFilter;
        this.components = {
          table: InvoiceFilterTable,
          edit: InvoiceFilterForm
        };
    }
}
