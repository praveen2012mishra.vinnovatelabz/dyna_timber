import {inject, observer} from "mobx-react";
import MaterialsTable from "views/InputPages/MaterialsSKUMaster/MaterialsTable";
import MaterialsForm from "views/InputPages/MaterialsSKUMaster/MaterialsForm";
import DefaultPage from "views/DefaultPage";

@inject("materials")
@observer
export default class MaterialsSKUMasterPage extends DefaultPage {

    constructor(props) {
        super(props);
        this.defaultStore = this.props.materials;
        this.components = {
            table: MaterialsTable,
            edit: MaterialsForm
        };
    }
}
