import React from "react";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import { inject, observer } from "mobx-react";
import MaterialsClient from "api/materials";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";
import { find } from "ag-grid-enterprise/dist/ag-grid-enterprise.noStyle";
import * as _ from "lodash";

const getOrderSize = (ordersizeArray) => {
  let arrayOfOrderSize = [];
  ordersizeArray.forEach((ele) => {
    arrayOfOrderSize = _.union(arrayOfOrderSize, [ele.label]);
  });
  arrayOfOrderSize = arrayOfOrderSize.map((ele) => {
    return { label: ele, value: "ele" };
  });
  return arrayOfOrderSize;
};

@inject("notifications", "materials")
@observer
class MaterialsForm extends React.Component {
  componentDidMount() {
    //debugger
    this.props.materials.fetchAll();
  }

  render() {
    const { notifications, materials } = this.props;
    const ordersizeArray = materials.data.map((suggestion) => ({
      value: suggestion,
      label: suggestion.code,
    }));
    let ordersize = getOrderSize(ordersizeArray);
    const categoryOrdersizeArray = materials.data.map((suggestion) => ({
      value: suggestion,
      label: suggestion.categoryOfOrderedSize,
    }));
    let categoryOrdersize = getOrderSize(categoryOrdersizeArray);
    const categoryArray = materials.data.map((suggestion) => ({
      value: suggestion,
      label: suggestion.category,
    }));
    let categorysize = getOrderSize(categoryArray);

    return (
      <Formik
        enableReinitialize
        initialValues={this.props.initialData ? this.props.initialData : {}}
        onSubmit={async (values, actions) => {
          try {
            if (_.get(values, "category.label")) {
              values.category = values.category.label;
            }
            if (_.get(values, "orderedSize2.label")) {
              values.orderedSize2 = values.orderedSize2.label;
            }
            if (_.get(values, "orderedSize1.label")) {
              values.orderedSize1 = values.orderedSize1.label;
            }
            if (_.get(values, "categoryOfOrderedSize.label")) {
              values.categoryOfOrderedSize = values.categoryOfOrderedSize.label;
            }
            //console.log(values);
            // values.category = values.category ? values.category : "";
            // values.categoryOfOrderedSize = values.categoryOfOrderedSize
            //   ? values.categoryOfOrderedSize.label
            //   : "";
            // values.orderedSize2 = values.orderedSize2
            //   ? values.orderedSize2.label
            //   : null;
            // values.orderedSize1 = values.orderedSize1
            //   ? values.orderedSize1
            //   : null;
            console.log(values);
            if (values.id) await MaterialsClient.update(values);
            else await MaterialsClient.save(values);
            notifications.enqueueSnackbar({ message: "Saved." });
            materials.fetchAll();
            materials.selectItem(null);
            materials.selectItemsTab();
          } catch (err) {
            console.error(err);
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" },
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, setFieldValue, isSubmitting, values }) => (
          <form onSubmit={handleSubmit}>
            {console.log(values)}
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="code"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Code"
                          suggestions={ordersize}
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            //type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="description"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Description"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="category"
                      render={({ field }) => (
                        // <CustomInput
                        //   labelText="Category"
                        //   formControlProps={{
                        //     fullWidth: true,
                        //   }}
                        //   inputProps={{
                        //     ...field,
                        //     value: field.value || "",
                        //   }}
                        // />
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Category"
                          suggestions={categorysize}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                          value={field.value || {}}
                          onChange={(item) => {
                            setFieldValue("category", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="minPctOfCategory1"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Min% of Category1"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="orderedSize1"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Ordered Size1"
                          suggestions={ordersize}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                          value={field.value || {}}
                          onChange={(item) => {
                            console.log(item, field);
                            setFieldValue("orderedSize1", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                    {/* <Field
                      name="orderedSize1"
                      render={({ field }) => (
                        <IntegrationReactSelect
                          placeholder="Ordered Size1"
                          label="Ordered Size1"
                          suggestions={ordersize}
                          value={field.value || {}}
                          onChange={item => {
                            setFieldValue("orderedSize1", item);
                            // if (item.value) {
                            //   setFieldValue("sku", {
                            //     value: item.value,
                            //     label: item.value.sku
                            //   });
                            //   calculateFields(
                            //     item.value.sku,
                            //     setFieldValue,
                            //     values.quantity
                            //   );
                            // }
                          }}
                        ></IntegrationReactSelect>
                      )}
                    /> */}
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="pctOfOrdered1"
                      render={({ field }) => (
                        <CustomInput
                          labelText="% of ordered1"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="minPctOfCategory2"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Min% of Category2"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    {/* <Field
                      name="orderedSize2"
                      render={({ field }) => (
                        // <CustomInput
                        //   labelText="Ordered Size2"
                        //   formControlProps={{
                        //     fullWidth: true,
                        //   }}
                        //   inputProps={{
                        //     ...field,
                        //     value: field.value || "",
                        //   }}
                        // />
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Ordered Size2"
                          suggestions={ordersize}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                          value={field.value || {}}
                          onChange={(item) => {
                            setFieldValue("orderedSize2", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    /> */}
                     <Field
                      name="orderedSize2"
                      render={({ field }) => (
                        // <IntegrationReactSelect
                        //   placeholder={`${field.value}`}
                        //   label="Ordered Size2"
                        //   suggestions={ordersize}
                        //   inputProps={{
                        //     ...field,
                        //     value: field.value || "",
                        //   }}
                        //   value={field.value || {}}
                        //   onChange={(item) => {
                        //     console.log(item, field);
                        //     setFieldValue("orderedSize2", item);
                        //   }}
                        // ></IntegrationReactSelect>
                        <IntegrationReactSelect
                        placeholder={`${field.value}`}
                        label="Ordered Size2"
                        suggestions={ordersize}
                        inputProps={{
                          ...field,
                          value: field.value || "",
                        }}
                        value={field.value || {}}
                        onChange={item => {
                          setFieldValue("orderedSize2", item);
                          
                        }}
                      ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="pctOfOrdered2"
                      render={({ field }) => (
                        <CustomInput
                          labelText="% of ordered2"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="pcsPerPack"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Pcs/Pack"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="categoryOfOrderedSize"
                      render={({ field }) => (
                        // <CustomInput
                        //   labelText="Category of Ordered Size"
                        //   formControlProps={{
                        //     fullWidth: true,
                        //   }}
                        //   inputProps={{
                        //     ...field,
                        //     value: field.value || "",
                        //   }}
                        // />
                        <IntegrationReactSelect
                          placeholder={`${field.value}`}
                          label="Category of Ordered Size"
                          suggestions={categoryOrdersize}
                          inputProps={{
                            ...field,
                            value: field.value || "",
                          }}
                          value={field.value || {}}
                          onChange={(item) => {
                            setFieldValue("categoryOfOrderedSize", item);
                          }}
                        ></IntegrationReactSelect>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="length"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Length"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="width"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Width"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="depth"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Depth"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  {/* <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="endOfSectionM2"
                      render={({ field }) => (
                        <CustomInput
                          labelText="End of Section m2"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem> */}
                  
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="dryTime"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Dry time"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="replenishmentTime"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Repl. time"
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <Field
                      name="reliability"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Reliability"
                          AEWX
                          formControlProps={{
                            fullWidth: true,
                          }}
                          inputProps={{
                            type: "number",
                            step: "any",
                            ...field,
                            value: field.value || "",
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>

                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {this.props.initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}

export default MaterialsForm;
