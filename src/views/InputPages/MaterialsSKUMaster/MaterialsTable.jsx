import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";
import MaterialsClient from "../../../api/materials";
import * as _ from "lodash";
import { get } from "lodash";

function isColumnHidden(field) {
  let store = JSON.parse(sessionStorage.getItem("hiddenColumns")) || {
    MaterialsTable: {},
  };
  if (store.MaterialsTable) {
    return store.MaterialsTable[field] || false;
  }
  return false;
}

const setPrecision = (val, precision) => val.toFixed(precision);

const onCellValueChangedAddDryTime = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("dryTime", JSON.stringify(store));
  params.data.dryTime = params.newValue;
  updateAddDryTime(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateAddDryTime = (dryTime, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(dryTime)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterAddDryTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.dryTime;
};

const valueSetterAddDryTime = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.dryTime)) return false;
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("dryTime", JSON.stringify(store));
  params.data.dryTime = params.newValue;
  return true;
};

const cellRendererAddDryTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("dryTime")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.dryTime;
};

const onCellValueChangedReplenishTime = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("replenishmentTime", JSON.stringify(store));
  params.data.replenishmentTime = params.newValue;
  updateReplenishTime(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateReplenishTime = (replenishmentTime, props) => {
  //console.log("------------------------------->", replenishTime);
  MaterialsClient.update(replenishmentTime)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterReplenishTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.replenishmentTime;
};

const valueSetterReplenishTime = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.replenishmentTime))
    return false;
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("replenishmentTime", JSON.stringify(store));
  params.data.replenishmentTime = params.newValue;
  return true;
};

const cellRendererReplenishTime = (params) => {
  let store = JSON.parse(sessionStorage.getItem("replenishmentTime")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.replenishmentTime;
};

const onCellValueChangedReliability = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("reliability", JSON.stringify(store));
  params.data.reliability = params.newValue;
  updateReliability(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateReliability = (reliability, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(reliability)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterReliability = (params) => {
  let store = JSON.parse(sessionStorage.getItem("reliability")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.reliability;
};

const onCellValueChangedMinPctOfCategory1 = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory1")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("minPctOfCategory1", JSON.stringify(store));
  params.data.minPctOfCategory1 = params.newValue;
  updateMinPctOfCategory1(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateMinPctOfCategory1 = (minPctOfCategory1, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(minPctOfCategory1)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterMinPctOfCategory1 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory1")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.minPctOfCategory1;
};

const onCellValueChangedPctOfOrdered1 = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered1")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pctOfOrdered1", JSON.stringify(store));
  params.data.pctOfOrdered1 = params.newValue;
  updatePctOfOrdered1(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatePctOfOrdered1 = (pctOfOrdered1, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(pctOfOrdered1)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterPctOfOrdered1 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered1")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.pctOfOrdered1;
};

const onCellValueChangedMinPctOfCategory2 = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory2")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("minPctOfCategory2", JSON.stringify(store));
  params.data.minPctOfCategory2 = params.newValue;
  updateMinPctOfCategory2(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateMinPctOfCategory2 = (minPctOfCategory2, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(minPctOfCategory2)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterMinPctOfCategory2 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory2")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.minPctOfCategory2;
};

const onCellValueChangedPctOfOrdered2 = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered2")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pctOfOrdered2", JSON.stringify(store));
  params.data.pctOfOrdered2 = params.newValue;
  updatePctOfOrdered2(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatePctOfOrdered2 = (pctOfOrdered2, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(pctOfOrdered2)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterPctOfOrdered2 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered2")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.pctOfOrdered2;
};

const onCellValueChangedPcsPerPack = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pcsPerPack", JSON.stringify(store));
  params.data.pcsPerPack = params.newValue;
  updatePcsPerPack(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updatePcsPerPack = (pcsPerPack, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(pcsPerPack)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterPcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.pcsPerPack;
};

const onCellValueChangedLength = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("length")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("length", JSON.stringify(store));
  params.data.length = params.newValue;
  updateLength(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateLength = (length, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(length)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterLength = (params) => {
  let store = JSON.parse(sessionStorage.getItem("length")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.length;
};

const onCellValueChangedWidth = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("width")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("width", JSON.stringify(store));
  params.data.width = params.newValue;
  updateWidth(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateWidth = (width, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(width)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterWidth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("width")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.width;
};

const onCellValueChangedDepth = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("depth")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("depth", JSON.stringify(store));
  params.data.depth = params.newValue;
  updateDepth(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateDepth = (depth, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(depth)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterDepth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("depth")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.depth;
};

const onCellValueChangedEndOfSectionM2 = (params, gridApi, props) => {
  let store = JSON.parse(sessionStorage.getItem("endOfSectionM2")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("endOfSectionM2", JSON.stringify(store));
  params.data.endOfSectionM2 = params.newValue;
  updateEndOfSectionM2(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateEndOfSectionM2 = (endOfSectionM2, props) => {
  //console.log("------------------------------->", reliability);
  MaterialsClient.update(endOfSectionM2)
    .then((res) => {
      props.materials.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterEndOfSectionM2 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("endOfSectionM2")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.endOfSectionM2;
};

const valueSetterMinPctOfCategory1 = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.minPctOfCategory1))
    return false;
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory1")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("minPctOfCategory1", JSON.stringify(store));
  params.data.minPctOfCategory1 = params.newValue;
  return true;
};

const cellRendererMinPctOfCategory1 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory1")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.minPctOfCategory1;
};

const valueSetterPctOfOrdered1 = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.pctOfOrdered1))
    return false;
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered1")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pctOfOrdered1", JSON.stringify(store));
  params.data.pctOfOrdered1 = params.newValue;
  return true;
};

const cellRendererPctOfOrdered1 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered1")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.pctOfOrdered1;
};

const valueSetterMinPctOfCategory2 = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.minPctOfCategory2))
    return false;
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory2")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("minPctOfCategory2", JSON.stringify(store));
  params.data.minPctOfCategory2 = params.newValue;
  return true;
};

const cellRendererMinPctOfCategory2 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("minPctOfCategory2")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.minPctOfCategory2;
};

const valueSetterPctOfOrdered2 = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.pctOfOrdered2))
    return false;
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered2")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pctOfOrdered2", JSON.stringify(store));
  params.data.pctOfOrdered2 = params.newValue;
  return true;
};

const cellRendererPctOfOrdered2 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pctOfOrdered2")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.pctOfOrdered2;
};

const valueSetterPcsPerPack = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.pcsPerPack))
    return false;
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("pcsPerPack", JSON.stringify(store));
  params.data.pcsPerPack = params.newValue;
  return true;
};

const cellRendererPcsPerPack = (params) => {
  let store = JSON.parse(sessionStorage.getItem("pcsPerPack")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.pcsPerPack;
};

const valueSetterWidth = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.width)) return false;
  let store = JSON.parse(sessionStorage.getItem("width")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("width", JSON.stringify(store));
  params.data.width = params.newValue;
  return true;
};

const cellRendererWidth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("width")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.width;
};

const valueSetterDepth = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.depth)) return false;
  let store = JSON.parse(sessionStorage.getItem("depth")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("depth", JSON.stringify(store));
  params.data.depth = params.newValue;
  return true;
};

const cellRendererDepth = (params) => {
  let store = JSON.parse(sessionStorage.getItem("depth")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.depth;
};

const valueSetterLength = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.length)) return false;
  let store = JSON.parse(sessionStorage.getItem("length")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("length", JSON.stringify(store));
  params.data.length = params.newValue;
  return true;
};

const cellRendererLength = (params) => {
  let store = JSON.parse(sessionStorage.getItem("length")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.length;
};

const valueSetterEndOfSectionM2 = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.endOfSectionM2))
    return false;
  let store = JSON.parse(sessionStorage.getItem("endOfSectionM2")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("endOfSectionM2", JSON.stringify(store));
  params.data.endOfSectionM2 = params.newValue;
  return true;
};

const cellRendererEndOfSectionM2 = (params) => {
  let store = JSON.parse(sessionStorage.getItem("endOfSectionM2")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }
  return params.data.endOfSectionM2;
};

//give custom width to column
const isColumnWidth = (field, componentName) => {
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };
  if (store[componentName]) {
    let width = store[componentName][field];
    width = parseInt(width, 10);
    width = width + 45;
    //console.log(width,field,'============>');
    return width;
  }
};

//take text width
function textWidth(text, fontProp, type) {
  var tag = document.createElement("div");
  tag.style.position = "absolute";
  tag.style.left = "-999em";
  tag.style.whiteSpace = "nowrap";
  tag.style.font = fontProp;
  text = text ? text.toString() : text;
  tag.innerHTML = text;

  document.body.appendChild(tag);

  var result = tag.clientWidth;
  //console.log(result,'====>');
  result = result;
  //console.log(result);

  document.body.removeChild(tag);

  return result;
}

const getWidthColumnValue = (params, componentName, field, headerName) => {
  let data = params.data;
  //console.log(data,componentName,field);
  let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
    [componentName]: {},
  };

  store[componentName] = {
    ...store[componentName],
  };
  let storeWidth;
  let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
  let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
  let arrayWidth = Object.assign([]);
  arrayWidth.push(fieldWidth);
  arrayWidth.push(valueWidth);
  //console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);

  if (Object.keys(store[componentName]).length === 0) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (!store[componentName][field]) {
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else if (store[componentName][field] != Math.max(...arrayWidth)) {
    arrayWidth.push(store[componentName][field]);
    let finalWidth = Math.max(...arrayWidth);
    store[componentName] = {
      ...store[componentName],
      [field]: finalWidth,
    };
    sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
  } else {
    //console.log("No change in width resize");
  }
};

@inject("materials")
@observer
class MaterialsTable extends DefaultAgTable {
  columns = [
    {
      headerName: "Code",
      field: "code",
      sort: "asc",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "code", "Code");
        return params.data.code;
      },
      width: isColumnWidth("code", "MaterialsTable"),
      hide: isColumnHidden("code"),
    },
    {
      headerName: "Description",
      field: "description",
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "description",
          "Description"
        );
        return params.data.description;
      },
      suppressSizeToFit: true,
      width: isColumnWidth("description", "MaterialsTable"),
      hide: isColumnHidden("description"),
    },
    {
      headerName: "Category",
      field: "category",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "category", "Category");
        return params.data.category;
      },
      width: isColumnWidth("category", "MaterialsTable"),
      hide: isColumnHidden("category"),
    },

    {
      headerName: "Min% of Category1",
      field: "minPctOfCategory1",
      suppressSizeToFit: true,
      width: isColumnWidth("minPctOfCategory1", "MaterialsTable"),
      hide: isColumnHidden("minPctOfCategory1"),
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedMinPctOfCategory1(
          params,
          this.gridApi,
          this.props
        );
      },
      valueSetter: (params) => {
        return valueSetterMinPctOfCategory1(params);
      },
      cellRenderer: (params) => {
        return cellRendererMinPctOfCategory1(params);
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "minPctOfCategory1",
          "Min% of Category1"
        );
        return valueGetterMinPctOfCategory1(params);
      },
    },
    {
      headerName: "Ordered Size1",
      field: "orderedSize1",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "orderedSize1",
          "Ordered Size1"
        );
        return params.data.orderedSize1;
      },
      width: isColumnWidth("orderedSize1", "MaterialsTable"),
      hide: isColumnHidden("orderedSize1"),
    },
    {
      headerName: "% of ordered1",
      field: "pctOfOrdered1",
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedPctOfOrdered1(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "pctOfOrdered1",
          "% of ordered1"
        );
        return valueGetterPctOfOrdered1(params);
      },
      valueSetter: (params) => {
        return valueSetterPctOfOrdered1(params);
      },
      cellRenderer: (params) => {
        return cellRendererPctOfOrdered1(params);
      },
      width: isColumnWidth("pctOfOrdered1", "MaterialsTable"),
      hide: isColumnHidden("pctOfOrdered1"),
    },

    {
      headerName: "Min% of Category2",
      field: "minPctOfCategory2",
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedMinPctOfCategory2(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "minPctOfCategory2",
          "Min% of Category2"
        );
        return valueGetterMinPctOfCategory2(params);
      },
      valueSetter: (params) => {
        return valueSetterMinPctOfCategory2(params);
      },
      cellRenderer: (params) => {
        return cellRendererMinPctOfCategory2(params);
      },
      width: isColumnWidth("minPctOfCategory2", "MaterialsTable"),
      hide: isColumnHidden("minPctOfCategory2"),
    },
    {
      headerName: "Ordered Size2",
      field: "orderedSize2",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "orderedSize2",
          "Ordered Size2"
        );
        return params.data.orderedSize2;
      },
      width: isColumnWidth("orderedSize2", "MaterialsTable"),
      hide: isColumnHidden("orderedSize2"),
    },
    {
      headerName: "% of ordered2",
      field: "pctOfOrdered2",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedPctOfOrdered2(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "pctOfOrdered2",
          "% of ordered2"
        );
        return valueGetterPctOfOrdered2(params);
      },
      valueSetter: (params) => {
        return valueSetterPctOfOrdered2(params);
      },
      cellRenderer: (params) => {
        return cellRendererPctOfOrdered2(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("pctOfOrdered2", "MaterialsTable"),
      hide: isColumnHidden("pctOfOrdered2"),
    },
    {
      headerName: "Pcs/Pack",
      field: "pcsPerPack",
      suppressSizeToFit: true,
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedPcsPerPack(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "pcsPerPack",
          "Pcs/Pack"
        );
        return valueGetterPcsPerPack(params);
      },
      valueSetter: (params) => {
        return valueSetterPcsPerPack(params);
      },
      cellRenderer: (params) => {
        return cellRendererPcsPerPack(params);
      },
      hide: isColumnHidden("pcsPerPack"),
      width: isColumnWidth("pcsPerPack", "MaterialsTable"),
    },

    {
      headerName: "Width",
      field: "width",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedWidth(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "width", "Width");
        return valueGetterWidth(params);
      },
      valueSetter: (params) => {
        return valueSetterWidth(params);
      },
      cellRenderer: (params) => {
        return cellRendererWidth(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("width", "MaterialsTable"),
      hide: isColumnHidden("width"),
    },
    {
      headerName: "Depth",
      field: "depth",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedDepth(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "depth", "Depth");
        return valueGetterDepth(params);
      },
      valueSetter: (params) => {
        return valueSetterDepth(params);
      },
      cellRenderer: (params) => {
        return cellRendererDepth(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("depth", "MaterialsTable"),
      hide: isColumnHidden("depth"),
    },
    {
      headerName: "Length",
      field: "length",
      editable: true,

      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedLength(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "length", "Length");
        return valueGetterLength(params);
      },
      valueSetter: (params) => {
        return valueSetterLength(params);
      },
      cellRenderer: (params) => {
        return cellRendererLength(params);
      },
      suppressSizeToFit: true,
      width: isColumnWidth("length", "MaterialsTable"),
      hide: isColumnHidden("length"),
    },
    {
      headerName: "End of Section m2",
      field: "endOfSectionM2",
      suppressSizeToFit: true,
      // editable: true,
      // // type: "numericColumn",
      // cellEditor: "numericEditor",
      // onCellValueChanged: (params) => {
      //   return onCellValueChangedEndOfSectionM2(
      //     params,
      //     this.gridApi,
      //     this.props
      //   );
      // },
      // valueGetter: (params) => {
      //   getWidthColumnValue(
      //     params,
      //     this.componentName,
      //     "endOfSectionM2",
      //     "End of Section m2"
      //   );
      //   return valueGetterEndOfSectionM2(params)
      //     ? +setPrecision(valueGetterEndOfSectionM2(params), 5)
      //     : "";
      // },
      // valueSetter: (params) => {
      //   return valueSetterEndOfSectionM2(params);
      // },
      // cellRenderer: (params) => {
      //   return cellRendererEndOfSectionM2(params);
      // },
      width: isColumnWidth("endOfSectionM2", "MaterialsTable"),
      hide: isColumnHidden("endOfSectionM2"),
      cellRenderer: (params) =>
        params.value ? +setPrecision(params.value, 5) : "",
    },
    {
      headerName: "Category of Ordered Size",
      field: "categoryOfOrderedSize",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "categoryOfOrderedSize",
          "Category of Ordered Size"
        );
        return params.data.categoryOfOrderedSize;
      },
      width: isColumnWidth("categoryOfOrderedSize", "MaterialsTable"),
      hide: isColumnHidden("categoryOfOrderedSize"),
    },
    {
      headerName: "Dry time",
      suppressSizeToFit: true,
      width: isColumnWidth("dryTime", "MaterialsTable"),
      hide: isColumnHidden("dryTime"),
      field: "dryTime",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedAddDryTime(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "dryTime", "Dry time");
        return valueGetterAddDryTime(params);
      },
      valueSetter: (params) => {
        return valueSetterAddDryTime(params);
      },
      cellRenderer: (params) => {
        return cellRendererAddDryTime(params);
      },
    },
    {
      headerName: "Replenishment Time",
      suppressSizeToFit: true,
      width: isColumnWidth("replenishmentTime", "MaterialsTable"),
      hide: isColumnHidden("replenishmentTime"),
      field: "replenishmentTime",
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedReplenishTime(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "replenishmentTime",
          "Replenishment Time"
        );
        return valueGetterReplenishTime(params);
      },
      valueSetter: (params) => {
        return valueSetterReplenishTime(params);
      },
      cellRenderer: (params) => {
        return cellRendererReplenishTime(params);
      },
    },

    {
      headerName: "Reliability",
      field: "reliability",
      width: isColumnWidth("reliability", "MaterialsTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("reliability"),
      editable: true,
      // type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedReliability(params, this.gridApi, this.props);
      },
      valueGetter: (params) => {
        getWidthColumnValue(
          params,
          this.componentName,
          "reliability",
          "Reliability"
        );
        return valueGetterReliability(params);
      },
    },
    {
      headerName: "Total m3",
      field: "totalM3",
      suppressSizeToFit: true,
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "totalM3", "Total m3");
        return params.data.totalM3;
      },
      width: isColumnWidth("totalM3", "MaterialsTable"),
      hide: isColumnHidden("totalM3"),
      cellRenderer: (params) => params.value,
    },
  ];

  //rowMultiSelectWithClick = true;
  rowSelection = "multiple";

  constructor(props) {
    super(props);
    this.defaultStore = this.props.materials;
    this.componentName = "MaterialsTable";
    this.printHeader = "Materials Table";
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}

export default MaterialsTable;
