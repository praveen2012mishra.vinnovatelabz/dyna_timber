import React from "react";
import * as _ from "lodash";
import Datetime from "react-datetime/DateTime";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import CustomInput from "components/CustomInput/CustomInput";
import Button from "components/CustomButtons/Button";
import { Field, Formik } from "formik";
import { inject, observer } from "mobx-react";
import InventoryStocktakeClient from "api/inventoryStocktake";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import extendedFormsStyle from "assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.jsx";
import moment from "moment";
import IntegrationReactSelect from "components/Autocomplete/Autocomplete";

@inject("notifications", "inventoryStocktake", "inventory")
@observer
class InventoryStocktakeForm extends React.Component {
  componentDidMount() {
    this.props.inventory.fetchAll();
  }

  render() {
    const {
      notifications,
      inventoryStocktake,
      inventory,
      classes,
      initialData
    } = this.props;
    const skus = inventory.data.map(suggestion => ({
      value: suggestion,
      label: suggestion.sku
    }));
    let init;
    if (initialData) {
      init = _.clone(initialData, true);
      if (init.sku && skus) {
        init.sku = skus.find(item => item.label === init.sku);
      }
    }

    return (
      <Formik
        enableReinitialize
        initialValues={init ? init : { date: moment().format("YYYY-MM-DD") }}
        onSubmit={async (values, actions) => {
          try {
            console.log(values,'------------------>');
            
            if (_.get(values, "sku.label")) {
              values.sku = values.sku.label;
            }
            if (values.id) await InventoryStocktakeClient.update(values);
            else await InventoryStocktakeClient.save(values);
            notifications.enqueueSnackbar({ message: "Saved." });
            inventoryStocktake.fetchAll();
            inventoryStocktake.selectItem(null);
            inventoryStocktake.selectItemsTab();
          } catch (err) {
            console.error(err);
            notifications.enqueueSnackbar({
              message: "Saving failed.",
              options: { variant: "error" }
            });
          } finally {
            actions.setSubmitting(false);
          }
        }}
        render={({ handleSubmit, isSubmitting, setFieldValue }) => (
          <form onSubmit={handleSubmit}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <Field
                      name="sku"
                      render={({ field }) => (
                        <FormControl
                          fullWidth
                          className={classes.selectFormControl}
                        >
                          <IntegrationReactSelect
                            placeholder="Select SKU"
                            label="SKU"
                            suggestions={skus}
                            value={field.value || {}}
                            onChange={item => {
                              setFieldValue("sku", item);
                              setFieldValue("sku-item", item);
                              if (item.value) {
                                setFieldValue(
                                  "pcsPerPack",
                                  item.value.pcsPerPack
                                );
                              }
                            }}
                          ></IntegrationReactSelect>
                        </FormControl>
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <InputLabel></InputLabel>
                    <br />
                    <Field
                      name="date"
                      render={({ field }) => (
                        <FormControl fullWidth>
                          <Datetime
                            value={field.value}
                            inputProps={{
                              value: new moment(field.value).format(
                                "MMMM Do YYYY"
                              )
                            }}
                            onChange={item => {
                              setFieldValue("date", item.format("YYYY-MM-DD"));
                            }}
                            dateFormat={"MMMM Do YYYY"}
                            timeFormat={false}
                          />
                        </FormControl>
                      )}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="pcsPerPack"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Pcs/pack"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="finishedPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Finished Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Field
                      name="untreatedPacks"
                      render={({ field }) => (
                        <CustomInput
                          labelText="Untreated Packs"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            type: "number",
                            ...field,
                            value: field.value || ""
                          }}
                        />
                      )}
                    />
                  </GridItem>
                </GridContainer>
                {/* <button
                  type="submit"
                  disabled
                  style={{ display: "none" }}
                  aria-hidden="true"
                ></button> */}
                <Button color="primary" type="submit" disabled={isSubmitting}>
                  {initialData ? "Update Item" : "Save Item"}
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        )}
      />
    );
  }
}

export default withStyles(extendedFormsStyle)(InventoryStocktakeForm);
