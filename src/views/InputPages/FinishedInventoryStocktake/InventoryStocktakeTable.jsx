import { inject, observer } from "mobx-react";
import DefaultAgTable from "components/Table/DefaultAgTable";
import moment from "moment";
import inventoryStocktakeApi from "api/inventoryStocktake";
import InventoryStocktakeClient from "api/inventoryStocktake";
import * as _ from "lodash";
import { get } from "lodash";

function isColumnHidden(field) {
  let store = JSON.parse(localStorage.getItem("hiddenColumns")) || {
    InventoryStocktakeTable: {},
  };
  if (store.InventoryStocktakeTable) {
    return store.InventoryStocktakeTable[field] || false;
  }
  return false;
}

//give custom width to column
const isColumnWidth = (field, componentName) => {
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
	if (store[componentName]) {
	  let width = store[componentName][field];
	  width = parseInt(width, 10);
	  width = width+45;
	  //console.log(width,field,'============>');
	  return width;
	}
  };
  
  //take text width
  function textWidth(text, fontProp, type) {
	var tag = document.createElement("div");
	tag.style.position = "absolute";
	tag.style.left = "-999em";
	tag.style.whiteSpace = "nowrap";
	tag.style.font = fontProp;
	text=text?text.toString():text;
	tag.innerHTML = text;
  
	document.body.appendChild(tag);
  
	var result = tag.clientWidth;
	//console.log(result,'====>');
	result = result;
	//console.log(result);
  
	document.body.removeChild(tag);
  
	return result;
  }
  
  const getWidthColumnValue = (params, componentName, field,headerName) => {
	let data = params.data;
	//console.log(data,componentName,field);
	let store = JSON.parse(sessionStorage.getItem("isColumnWidth")) || {
	  [componentName]: {},
	};
  
	store[componentName] = {
	  ...store[componentName],
	};
	let storeWidth;
	let fieldWidth = textWidth(headerName, "normal 12px Roboto", "field");
	let valueWidth = textWidth(data[field], "normal 12px Roboto", "value");
  //console.log(fieldWidth,valueWidth,field);
	let arrayWidth = Object.assign([]);
	arrayWidth.push(fieldWidth);
	arrayWidth.push(valueWidth);
	//console.log(arrayWidth,Math.max(...arrayWidth),store[componentName][field],field);
	
	if (Object.keys(store[componentName]).length === 0) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (!store[componentName][field]) {
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else if (store[componentName][field] != Math.max(...arrayWidth)) {
	  arrayWidth.push(store[componentName][field]);
	  let finalWidth = Math.max(...arrayWidth);
	  store[componentName] = {
		...store[componentName],
		[field]: finalWidth,
	  };
	  sessionStorage.setItem("isColumnWidth", JSON.stringify(store));
	} else {
	  //console.log("No change in width resize");
	}
  };

const onCellValueChangedFinishedPacks = (params, gridApi, props) => {
  //debugger
  let store = JSON.parse(sessionStorage.getItem("finishedPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("finishedPacks", JSON.stringify(store));
  params.data.finishedPacks = params.newValue;
  updateFinishedPacks(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateFinishedPacks = (finishedPacks, props) => {
  //console.log("------------------------------->", reliability);
  InventoryStocktakeClient.update(finishedPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterFinishedPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("finishedPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.finishedPacks;
};

const valueSetterFinishedPacks = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.finishedPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("finishedPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("finishedPacks", JSON.stringify(store));
  params.data.finishedPacks = params.newValue;
  return true;
};

const cellRendererFinishedPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("finishedPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.finishedPacks;
};

const onCellValueChangedUntreatedPacks = (params, gridApi, props) => {
  //debugger
  let store = JSON.parse(sessionStorage.getItem("untreatedPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("untreatedPacks", JSON.stringify(store));
  params.data.untreatedPacks = params.newValue;
  updateUntreatedPacks(Object.assign({}, params.data), props);
  gridApi.refreshCells({ force: true });
};

const updateUntreatedPacks = (untreatedPacks, props) => {
  //console.log("------------------------------->", reliability);
  InventoryStocktakeClient.update(untreatedPacks)
    .then((res) => {
      props.materialsStocktake.setUpdateObject(res.data);
    })
    .catch((err) => console.log("error while updating inventory \n", err));
};

const valueGetterUntreatedPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("untreatedPacks")) || {};
  //console.log(parseInt(store[params.data.id]));

  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[params.data.id]);
  }
  return params.data.untreatedPacks;
};

const valueSetterUntreatedPacks = (params) => {
  if (parseInt(params.newValue) === parseInt(params.data.untreatedPacks))
    return false;
  let store = JSON.parse(sessionStorage.getItem("untreatedPacks")) || {};
  store = {
    ...store,
    [get(params, "data.id")]: params.newValue,
  };
  sessionStorage.setItem("untreatedPacks", JSON.stringify(store));
  params.data.untreatedPacks = params.newValue;
  return true;
};

const cellRendererUntreatedPacks = (params) => {
  let store = JSON.parse(sessionStorage.getItem("untreatedPacks")) || {};
  if (!isNaN(parseInt(store[[get(params, "data.id")]]))) {
    return parseInt(store[[get(params, "data.id")]]);
  }

  return params.data.untreatedPacks;
};

@inject("inventoryStocktake")
@observer
class InventoryStocktakeTable extends DefaultAgTable {
  columns = [
    {
      headerName: "SKU Code",
      field: "sku",
      width: isColumnWidth("sku", "InventoryStocktakeTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
				//console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
				getWidthColumnValue(params, this.componentName, "sku","SKU Code");
				return params.data.sku;
			},
      hide: isColumnHidden("sku"),
    },
    {
      headerName: "Description",
      field: "inventory.productName",
      width: isColumnWidth("productName", "InventoryStocktakeTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
				//console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
				getWidthColumnValue(params, this.componentName, "productName","Description");
				return params.data.productName;
			},
      //editable: true,
      hide: isColumnHidden("productName"),
    },
    {
      headerName: "Date",
      field: "date",
      cellRenderer: (params) =>{
        getWidthColumnValue(params, this.componentName, "date","Date");
        return params.value ? new moment(params.value).format("MMMM Do YYYY") : ""},
      chartDataType: "excluded",
      filter: "agDateColumnFilter",
      width:140,
      //width: isColumnWidth("date", "InventoryStocktakeTable"),
      suppressSizeToFit: true,
      hide: isColumnHidden("date"),
      filterParams: {
        comparator: function(filterLocalDateAtMidnight, cellValue) {
          var dateAsString = cellValue;
          if (dateAsString == null) return 0;

          // In the example application, dates are stored as dd/mm/yyyy
          // We create a Date object for comparison against the filter date
          let dateAsDate = new moment(dateAsString, "YYYY-MM-DD").toDate();
          // Now that both parameters are Date objects, we can compare
          if (dateAsDate < filterLocalDateAtMidnight) {
            return -1;
          } else if (dateAsDate > filterLocalDateAtMidnight) {
            return 1;
          } else {
            return 0;
          }
        },
      },
    },

    {
      headerName: "Pcs/pack",
      field: "pcsPerPack",
      editable: true,
      cellEditor: "numericEditor",
      width: isColumnWidth("pcsPerPack", "InventoryStocktakeTable"),
      suppressSizeToFit: true,
      valueGetter: params => {
				//console.log(isColumnWidth("chargeId", "TreatmentChargeTimesTable"));
				getWidthColumnValue(params, this.componentName, "pcsPerPack","Pcs/pack");
				return params.data.pcsPerPack;
			},
      hide: isColumnHidden("pcsPerPack"),
    },
    {
      headerName: "Finished Packs",
      field: "finishedPacks",
      //editable: true,
      //type: "numericColumn",
      //cellEditor: "numericEditor",
      width: isColumnWidth("finishedPacks", "InventoryStocktakeTable"),
      suppressSizeToFit: true,
      editable: true,
      //type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedFinishedPacks(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "finishedPacks","Finished Packs");
        return valueGetterFinishedPacks(params);
      },
      valueSetter: (params) => {
        return valueSetterFinishedPacks(params);
      },
      cellRenderer: (params) => {
        return cellRendererFinishedPacks(params);
      },
      hide: isColumnHidden("finishedPacks"),
    },
    {
      headerName: "Untreated Packs",
      field: "untreatedPacks",
      //editable: true,
      //cellEditor: "numericEditor",
      width: isColumnWidth("untreatedPacks", "InventoryStocktakeTable"),
      suppressSizeToFit: true,
      editable: true,
      //type: "numericColumn",
      cellEditor: "numericEditor",
      onCellValueChanged: (params) => {
        return onCellValueChangedUntreatedPacks(
          params,
          this.gridApi,
          this.props
        );
      },
      valueGetter: (params) => {
        getWidthColumnValue(params, this.componentName, "untreatedPacks","Untreated Packs");
        return valueGetterUntreatedPacks(params);
      },
      valueSetter: (params) => {
        return valueSetterUntreatedPacks(params);
      },
      cellRenderer: (params) => {
        return cellRendererUntreatedPacks(params);
      },
      hide: isColumnHidden("untreatedPacks"),
    },
  ];

  //rowMultiSelectWithClick = true;
  rowSelection = "multiple";

  onCellValueChanged = (event) => {
    const inventoryStocktake = event.data;
    console.log("Cell changed eventt: ", inventoryStocktake);
    inventoryStocktakeApi.update(inventoryStocktake);
  };

  constructor(props) {
    super(props);
    this.componentName = "InventoryStocktakeTable";
    this.printHeader = "Inventory Stocktake Table";
    this.defaultStore = this.props.inventoryStocktake;
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };
}

export default InventoryStocktakeTable;
