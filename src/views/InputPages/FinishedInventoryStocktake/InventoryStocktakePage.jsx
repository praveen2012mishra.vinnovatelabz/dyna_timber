import React from "react";
import { inject, observer } from "mobx-react";
import DefaultPage from "views/DefaultPage";
import InventoryStocktakeTable from "views/InputPages/FinishedInventoryStocktake/InventoryStocktakeTable";
import InventoryStocktakeForm from "views/InputPages/FinishedInventoryStocktake/InventoryStocktakeForm";
import ActionIcon from "@material-ui/icons/PlusOne";
import inventoryStocktake from "api/inventoryStocktake";
@inject("inventoryStocktake")
@observer
export default class InventoryStocktakePage extends DefaultPage {
  aditionalActions = [
    <ActionIcon
      onClick={async () => {
        await inventoryStocktake.newStocktake();
        this.props.inventoryStocktake.fetchAll();
      }}
    />
  ];

  constructor(props) {
    super(props);
    this.defaultStore = this.props.inventoryStocktake;
    this.components = {
      table: InventoryStocktakeTable,
      edit: InventoryStocktakeForm
    };
  }
}
