import React from "react";
// import Paper from '@material-ui/core/Paper';
// import {
//   Chart,
//   ArgumentAxis,
//   ValueAxis,
//   LineSeries,
//   Title,
//   //Legend,
// } from '@devexpress/dx-react-chart-material-ui';
// import { Animation } from '@devexpress/dx-react-chart';
//before add library linear graph
import PropTypes from "prop-types";
import * as _ from "lodash";
import { observer, inject } from "mobx-react";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// react plugin for creating vector maps
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
import List from "@material-ui/core/List";
import DialogTitle from "@material-ui/core/DialogTitle";
// @material-ui/icons
// import ContentCopy from "@material-ui/icons/ContentCopy";
import Store from "@material-ui/icons/Store";
// import InfoOutline from "@material-ui/icons/InfoOutline";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Refresh from "@material-ui/icons/Refresh";
import Edit from "@material-ui/icons/Edit";
import Language from "@material-ui/icons/Language";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Danger from "components/Typography/Danger.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import { action, flow, observable } from "mobx";
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  LabelList,
  CartesianGrid,
  Tooltip,
  Legend,
  BarChart,
  Bar,
  ComposedChart,
} from "recharts";
import Paper from "@material-ui/core/Paper";
import CustomTable from "components/Table/Table";

import {
  completedTasksChart,
  dailySalesChart,
  emailsSubscriptionChart,
} from "variables/charts";
import FinishedInventoryReportClient from "api/reports/finishedInventoryReport";

import dashboardStyle from "assets/jss/material-dashboard-pro-react/views/dashboardStyle";
//for date picker input
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import DashboardPdClient from "api/dashboardPd";

// var matrixify = function(arr, rows, cols) {
//   var matrix = [];
//   if (rows * cols === arr.length) {
//       for(var i = 0; i < arr.length; i+= cols) {
//           matrix.push(arr.slice(i, cols + i));
//       }
//   }

//   return matrix;
// };

// var a = [0, 1, 2, 3, 4, 5, 6, 7];
// matrixify(a, 2, 4);

const data = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

const colors = [
  "red",
  "green",
  "blue",
  "yellow",
  "pink",
  "gray",
  "orange",
  "white",
  "black",
];

function sortByMonth(a, b) {
  var as = a.name.split("-"),
    bs = b.name.split("-"),
    ad = new Date(as[0] + " 1," + as[1]),
    bd = new Date(bs[0] + " 1," + bs[1]);
  return ad.getTime() - bd.getTime();
}

let fir_data = [];
let fir_data_sku = [];
let fir_data_currentActCol = [];
const findSKUFir = (finishedInventoryReport) => {
  fir_data =
    finishedInventoryReport.data.length > 0 &&
    finishedInventoryReport.data.map((ele) => {
      let tempCurrentActCol = (ele.currentAct * 100) / ele.currentTargetInv;
      return { sku: ele.inventory.sku, currentActCol: tempCurrentActCol };
    });
  let fir_data_filter =
    fir_data.length > 0 &&
    fir_data.filter((ele) => {
      return ele.currentActCol >= 0 && ele.currentActCol <= 50;
    });
  fir_data_currentActCol =
    fir_data_filter.length > 0 &&
    fir_data_filter.map((ele) => {
      return Math.floor(ele.currentActCol);
    });
  fir_data_sku =
    fir_data_filter.length > 0 &&
    fir_data_filter.map((ele) => {
      return ele.sku;
    });
};

@inject("productionHoursWorked", "finishedInventoryReport", "dashboardPd")
@observer
class Dashboard extends React.Component {
  //show dashboard for productionWorkHours
  state = {
    value: 0,
    fromDate: "",
    fromDateDisplay: "",
    toDate: "",
    toDateDisplay: "",
    helperText1: "",
    error1: false,
    helperText2: "",
    error2: false,
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };
  handleChangeIndex = (index) => {
    this.setState({ value: index });
  };

  componentDidMount() {
    this.props.productionHoursWorked.fetchAll();
    this.props.finishedInventoryReport.fetchAll();
    this.props.dashboardPd.fetchAll();
    //this.fetchAll();
    this.setState({
      fromDateDisplay: this.getInitialMonthDate(),
      toDateDisplay: this.getCurrentDate(),
    });
  }

  //show the graph between production data
  getCurrentDate = () => {
    let store2 = JSON.parse(localStorage.getItem("toDate")) || {
      toDate: {},
    };
    let toDate =
      typeof store2.toDate !== "object"
        ? store2.toDate
        : moment(new Date()).format("YYYY-MM-DD");
    return toDate;
  };

  getInitialMonthDate = () => {
    var startdate = moment();
    startdate = startdate.subtract(30, "days");
    startdate = startdate.format("YYYY-MM-DD");
    let store1 = JSON.parse(localStorage.getItem("fromDate")) || {
      fromDate: {},
    };
    let fromDate =
      typeof store1.fromDate !== "object" ? store1.fromDate : startdate;
    return fromDate;
  };

  handleCurrentDateChange = (fromDate) => (evt) => {
    let selectedFromDate = evt.target.value;
    let store = JSON.parse(localStorage.getItem("fromDate")) || {
      fromDate: {},
    };
    if (
      moment
        .duration(moment(this.getCurrentDate()).diff(selectedFromDate))
        .asDays() >= 0
    ) {
      this.setState({
        helperText1: "",
        error1: false,
      });
      store.fromDate = evt.target.value;
      localStorage.setItem("fromDate", JSON.stringify(store));
    } else {
      this.setState({
        helperText1: "Please do not exceed from current date or To Date",
        error1: true,
      });
    }
  };

  handleToDateChange = (toDate) => (evt) => {
    let selectedFromDate = evt.target.value;
    let store = JSON.parse(localStorage.getItem("toDate")) || {
      toDate: {},
    };
    if (
      moment
        .duration(
          moment(new Date()).diff(moment(selectedFromDate).format("YYYY-MM-DD"))
        )
        .asDays() >= 0
    ) {
      this.setState({
        helperText2: "",
        error2: false,
      });
      store.toDate = evt.target.value;
      localStorage.setItem("toDate", JSON.stringify(store));
    } else {
      this.setState({
        helperText2: "Please do not exceed from current date",
        error2: true,
      });
    }
  };

  submitDate = () => {
    let { error1, error2 } = this.state;
    if (!error1 || !error2) {
      DashboardPdClient.all();

      window.location.reload();
    } else {
      error2 ? this.getCurrentDate() : this.getInitialMonthDate();
    }
  };

  render() {
    const {
      classes,
      productionHoursWorked,
      finishedInventoryReport,
      dashboardPd,
    } = this.props;
    let prodHoursChart = [];
    findSKUFir(finishedInventoryReport);
    //console.log(productionHoursWorked.data);
    let prodHoursMachines = [];
    let prodHours = _.forEach(
      _.groupBy(_.sortBy(productionHoursWorked.data, "date"), "date"),
      (value, key) => {
        let item = { name: key };
        _.forEach(value, (machine) => {
          //console.log(machine);
          item[machine.productionLine] = machine.m3perHour;
          prodHoursMachines = _.union(prodHoursMachines, [
            machine.productionLine,
          ]);
        });
        prodHoursChart.push(item);
      }
    );
    console.log("ProdHours", prodHoursChart);

    let totalM3HoursChart = [];
    let totalM3HoursMachines = [];
    let totalM3Hours = _.forEach(
      _.groupBy(_.sortBy(productionHoursWorked.data, "date"), "date"),
      (value, key) => {
        let item = { name: key };
        _.forEach(value, (machine) => {
          item[machine.productionLine] = machine.totalM3Produced;
          item[machine.productionLine + "Benchmark"] =
            machine.productionLineBenchmarkM3PerHour;
          totalM3HoursMachines = _.union(totalM3HoursMachines, [
            machine.productionLine,
          ]);
        });
        totalM3HoursChart.push(item);
      }
    );
    console.log("prodHoursMachines", prodHoursMachines);
    let numberOfpacks = [];
    let numberOfpacksArray = [];
    let machineName = [];
    let prodHours1 = _.forEach(
      _.groupBy(_.sortBy(dashboardPd.data, "date"), "date"),
      (value, key) => {
        let item = { name: key };
        _.forEach(value, (machine) => {
          //console.log(machine);
          item[machine.machine] = machine.yaxis;
          machineName = _.union(machineName, [machine.machine]);
        });
        numberOfpacks.push(item);
      }
    );

    var letters = /^[A-Za-z]+$/;
    let newArray = numberOfpacks[0]
      ? numberOfpacks[0]["name"][1].match(letters)
        ? numberOfpacks.sort(sortByMonth)
        : numberOfpacks
      : "";

    //Bar graph between actual and linear
    let listOfSkuInFIR = ["Actual", "Target"];
    let finshedInventoryData = [];
    finishedInventoryReport.data.forEach((ele) => {
      //console.log(ele.currentTargetInv,ele.currentAct,ele);
      if (ele.currentTargetInv !== 0) {
        finshedInventoryData.push({
          Target: ele.currentTargetInv,
          sku: ele.inventory.sku,
          Actual: ele.currentAct,
        });
      }
    });
    console.log(finshedInventoryData, listOfSkuInFIR,finishedInventoryReport.data);

    let listOfDifferentMachine = [];
    let arrayOfMachineInFir = [];
    let totalM3Hour = _.forEach(
      _.groupBy(_.sortBy(finishedInventoryReport.data, "date"), "date"),
      (value, key) => {
        let item = { name: key };
        _.forEach(value, (machine) => {
          //console.log(machine);
          item[machine.productionLine] = machine.totalM3Produced;
          item[machine.productionLine + "Benchmark"] =
            machine.productionLineBenchmarkM3PerHour;
          arrayOfMachineInFir = _.union(arrayOfMachineInFir, [
            machine.inventory.feedStock1Machine,
          ]);
        });
        listOfDifferentMachine.push(item);
      }
    );
    console.log(
      "finshedInventoryData",
      finshedInventoryData,
      arrayOfMachineInFir
    );

    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card chart className={classes.cardHover}>
              <CardHeader style={{ height: "300px", padding: "0.75rem 0rem" }}>
                <ResponsiveContainer height="100%" width="100%">
                  <BarChart
                    data={prodHoursChart}
                    margin={{
                      top: 5,
                      right: 30,
                      bottom: 5,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {prodHoursMachines.map((m, index) => (
                      <Bar
                        key={index}
                        type="monotone"
                        dataKey={m}
                        fill={colors[index]}
                      />
                    ))}
                  </BarChart>
                </ResponsiveContainer>
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>
                  m3/hr for each machine each day
                </h4>
              </CardBody>
              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> updated on refresh
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>
            <Card chart className={classes.cardHover}>
              <CardHeader style={{ height: "300px", padding: "0.75rem 0rem" }}>
                <ResponsiveContainer height="100%" width="100%">
                  <ComposedChart
                    data={totalM3HoursChart}
                    margin={{
                      top: 5,
                      right: 30,
                      bottom: 5,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {totalM3HoursMachines.map((m, index) => (
                      <Bar
                        key={index}
                        type="monotone"
                        dataKey={m}
                        fill={colors[index]}
                      />
                    ))}
                  </ComposedChart>
                </ResponsiveContainer>
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>
                  m3 produced for each machine each day
                </h4>
              </CardBody>
              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> updated on refresh
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        
          <GridItem xs={12} sm={12} md={12}>
            <Card chart className={classes.cardHover}>
            
              <LineChart
                width={1000}
                height={400}
                data={numberOfpacks}
                //margin={{ top: 5, right: 10, left: 10, bottom: 5 }}
                margin={{
                  top: 15,
                  right: 7,
                  left: 10,
                  bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 10" />
                <XAxis
                  dataKey="name"
                  interval={0}
                  padding={{ left: 40, right: 40 }}
                  tick={<CustomizedAxisTick />}
                ></XAxis>
                <YAxis />
                <Tooltip />
                <Legend
                  wrapperStyle={{ overflowX: "hidden", marginTop: "800px" }}
                />
                {machineName.map((m, index) => (
                  <Line
                    key={index}
                    padding={{ top: 60 }}
                    type="monotone"
                    dataKey={m}
                    stroke={colors[index]}
                  >
                    {/* <LabelList dataKey={m} position="bottom" angle="45"  /> */}
                  </Line>
                ))}
              </LineChart>
              <CardBody>
                <div className="display-date-box">
                  <div className="footer-box-pd">
                    <h4 className={classes.cardTitle}>
                      <div className="heading-display-date">
                      Graph Data is calculated from {this.state.fromDateDisplay}{" "}
                      to {this.state.toDateDisplay} .
                      </div>
                      
                    </h4>
                    <div className="date-selection-dashboard">
                      <div>
                        <form className={`${classes.container} date-selection-box`} noValidate>
                          <TextField
                            id="date"
                            label="Start Date"
                            type="date"
                            helperText={this.state.helperText1}
                            error={this.state.error1}
                            onChange={this.handleCurrentDateChange("fromDate")}
                            //defaultValue="2017-05-24"
                            defaultValue={`${this.getInitialMonthDate()}`}
                            className={classes.textField}
                            InputLabelProps={{
                              shrink: true,
                            }}
                          />
                          <TextField
                            id="date"
                            label="End Date"
                            type="date"
                            helperText={this.state.helperText2}
                            error={this.state.error2}
                            onChange={this.handleToDateChange("toDate")}
                            defaultValue={`${this.getCurrentDate()}`}
                            className={classes.textField}
                            InputLabelProps={{
                              shrink: true,
                            }}
                          />
                        </form>
                      </div>
                      <div>
                        <div className="container button-group">
                          <button
                            type="button"
                            className="btn dsr-button mr-4"
                            onClick={(evt) => {
                              this.submitDate(evt);
                            }}
                          >
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <h4 className={`${classes.cardTitle} display-date`}>
                    Packs for each machine in FIR
                  </h4>
                </div>
              </CardBody>
              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> updated on refresh
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>
            <Card chart className={classes.cardHover}>
              <CardHeader style={{ height: "auto", padding: "0.75rem .5rem" }}>
                <CardBody>
                  <h4 className={classes.cardTitle}>
                    List of SKU's which are red color in FI
                  </h4>
                </CardBody>
                {fir_data_sku.length > 0 && (
                  <div className={classes.paperSkuItem}>
                    {fir_data_sku.map((ele) => (
                      <Paper elevation={3}>
                        <div className="sku-name">{ele}</div>
                      </Paper>
                    ))}
                  </div>
                )}
              </CardHeader>

              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> updated on refresh
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={12} className="sku-actual-target">
            <Card chart className={classes.cardHover}>
              <CardHeader style={{ height: "450px", padding: "0.75rem 0rem" }}>
                <ResponsiveContainer height="100%" width="100%">
                  <BarChart
                    data={finshedInventoryData}
                    margin={{
                      top: 10,
                      right: 30,
                      bottom: 100,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis
                      dataKey="sku"
                      padding={{ left: 40, right: 20 }}
                      interval={0}
                      tick={<CustomizedAxisTickFir />}
                    />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {listOfSkuInFIR.map((m, index) => (
                      <Bar
                        key={index}
                        type="monotone"
                        padding={{ top: 60 }}
                        dataKey={m}
                        fill={colors[index]}
                      />
                    ))}
                  </BarChart>
                </ResponsiveContainer>
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>
                  Bar graph between Target Vs. Actual of SKU in FIR
                </h4>
              </CardBody>
              <CardFooter chart>
                <div className={classes.stats}>
                  <AccessTime /> updated on refresh
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(dashboardStyle)(Dashboard);

const CustomizedAxisTickFir = (props) => {
  const { x, y, stroke, payload } = props;

  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dy={0}
        textAnchor="end"
        fill="#666"
        transform="rotate(-60)"
      >
        {payload.value}
      </text>
    </g>
  );
};

const CustomizedAxisTick = (props) => {
  const { x, y, stroke, payload } = props;

  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dy={16}
        textAnchor="end"
        fill="#666"
        transform="rotate(-30)"
      >
        {payload.value}
      </text>
    </g>
  );
};
