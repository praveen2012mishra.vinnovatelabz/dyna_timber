import React, { Suspense } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';
// creates a beautiful scrollbar
import PerfectScrollbar from 'perfect-scrollbar';
import 'perfect-scrollbar/css/perfect-scrollbar.css';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
// core components
import AdminNavbar from 'components/Navbars/AdminNavbar.jsx';
import Footer from 'components/Footer/Footer.jsx';
import Sidebar from 'components/Sidebar/Sidebar.jsx';

import routes from 'routes.js';

import appStyle from 'assets/jss/material-dashboard-pro-react/layouts/adminStyle.jsx';

import image from 'assets/img/timber.jpg';
import logo from 'assets/img/dynatimber-logo-cmyk_2.png';
import Notifier from 'components/Snackbar/Notifier';
import CircularProgress from '@material-ui/core/CircularProgress';
import { inject, observer } from 'mobx-react';

let ps;

@inject('auth')
@observer
class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mobileOpen: false,
			miniActive: false,
			image: image,
			color: 'blue',
			bgColor: 'black',
			hasImage: true,
			fixedClasses: 'dropdown'
		};
		this.resizeFunction = this.resizeFunction.bind(this);
	}

	componentDidMount() {
		if (navigator.platform.indexOf('Win') > -1) {
			ps = new PerfectScrollbar(this.refs.mainPanel, {
				suppressScrollX: true,
				suppressScrollY: false
			});
			document.body.style.overflow = 'hidden';
		}
		window.addEventListener('resize', this.resizeFunction);
	}

	componentWillUnmount() {
		if (navigator.platform.indexOf('Win') > -1) {
			ps.destroy();
		}
		window.removeEventListener('resize', this.resizeFunction);
	}

	componentDidUpdate(e) {
		if (e.history.location.pathname !== e.location.pathname) {
			this.refs.mainPanel.scrollTop = 0;
			if (this.state.mobileOpen) {
				this.setState({ mobileOpen: false });
			}
		}
	}

	// handleImageClick = image => {
	//   this.setState({ image: image });
	// };
	// handleColorClick = color => {
	//   this.setState({ color: color });
	// };
	// handleBgColorClick = bgColor => {
	//   this.setState({ bgColor: bgColor });
	// };
	// handleFixedClick = () => {
	//   if (this.state.fixedClasses === 'dropdown') {
	//     this.setState({ fixedClasses: 'dropdown show' });
	//   } else {
	//     this.setState({ fixedClasses: 'dropdown' });
	//   }
	// };
	handleDrawerToggle = () => {
		this.setState({ mobileOpen: !this.state.mobileOpen });
	};

	getRoute() {
		return true;
		// return this.props.location.pathname !== "/admin/full-screen-maps";
	}

	getActiveRoute = routes => {
		let activeRoute = 'Default Brand Text';
		for (let i = 0; i < routes.length; i++) {
			if (routes[i].collapse) {
				let collapseActiveRoute = this.getActiveRoute(routes[i].views);
				if (collapseActiveRoute !== activeRoute) {
					return collapseActiveRoute;
				}
			} else {
				if (
					window.location.href.indexOf(
						routes[i].layout + '/' + routes[i].path.split('/')[1]
					) !== -1
				) {
					return routes[i].name;
				}
			}
		}
		return activeRoute;
	};

	getRoutes = routes => {
		return routes.map((route, key) => {
			if (route.collapse) {
				return this.getRoutes(route.views);
			}

			if (route.layout === '/admin') {
				let { component: Component } = route;
				return (
					<Route
						path={route.layout + route.path}
						key={key}
						render={props => {
							if (
								!route.access ||
								route.access.indexOf(this.props.auth.role) !== -1
							) {
								return <Component {...props} />;
							} else {
								return <Redirect from="/" to="/admin/dashboard" />;
							}
						}}
					/>
				);
			} else {
				return null;
			}
		});
	};

	sidebarMinimize() {
		this.setState({ miniActive: !this.state.miniActive });
	}

	resizeFunction() {
		if (window.innerWidth >= 960) {
			this.setState({ mobileOpen: false });
		}
	}

	render() {
		const { classes, ...rest } = this.props;
		const mainPanel =
			classes.mainPanel +
			' ' +
			cx({
				[classes.mainPanelSidebarMini]: this.state.miniActive,
				[classes.mainPanelWithPerfectScrollbar]:
					navigator.platform.indexOf('Win') > -1
			});
		return (
			<div className={classes.wrapper}>
				<Notifier />
				<Sidebar
					routes={routes}
					logoText={'Dyna Timber'}
					logo={logo}
					image={this.state.image}
					handleDrawerToggle={this.handleDrawerToggle}
					open={this.state.mobileOpen}
					color={this.state.color}
					bgColor={this.state.bgColor}
					miniActive={this.state.miniActive}
					{...rest}
				/>
				<div className={mainPanel + ' print-width'} ref="mainPanel">
					<AdminNavbar
						sidebarMinimize={this.sidebarMinimize.bind(this)}
						miniActive={this.state.miniActive}
						brandText={this.getActiveRoute(routes)}
						handleDrawerToggle={this.handleDrawerToggle}
						{...rest}
					/>
					{/* On the /maps/full-screen-maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
					{this.getRoute() ? (
						<div className={classes.content}>
							<div className={classes.container}>
								<Suspense fallback={<CircularProgress />}>
									<Switch>{this.getRoutes(routes)}</Switch>
								</Suspense>
							</div>
						</div>
					) : (
						<div className={classes.map}>
							<Suspense fallback={<CircularProgress />}>
								<Switch>{this.getRoutes(routes)}</Switch>
							</Suspense>
						</div>
					)}
					{this.getRoute() ? <Footer fluid /> : null}
					{/*<FixedPlugin*/}
					{/*  handleImageClick={this.handleImageClick}*/}
					{/*  handleColorClick={this.handleColorClick}*/}
					{/*  handleBgColorClick={this.handleBgColorClick}*/}
					{/*  handleHasImage={this.handleHasImage}*/}
					{/*  color={this.state["color"]}*/}
					{/*  bgColor={this.state["bgColor"]}*/}
					{/*  bgImage={this.state["image"]}*/}
					{/*  handleFixedClick={this.handleFixedClick}*/}
					{/*  fixedClasses={this.state.fixedClasses}*/}
					{/*  sidebarMinimize={this.sidebarMinimize.bind(this)}*/}
					{/*  miniActive={this.state.miniActive}*/}
					{/*/>*/}
				</div>
			</div>
		);
	}
}

Dashboard.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(appStyle)(Dashboard);
