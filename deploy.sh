#!/usr/bin/env sh

# abort on errors
set -e

PS3='Where would you like to deploy: '
options=("Production" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Production")
            echo "Building production version"
            yarn clean
            yarn build
            echo "Deploying to production"
            ssh root:dynatimber123!@128.199.214.209 "rm -rf /var/www/dyna/html/*"
            scp -r build/* root@128.199.214.209:/var/www/dyna/html
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

echo "Finished. Quiting. Bye."

