const {
    addDecoratorsLegacy,
    override,
    useEslintRc,
    addWebpackAlias
} = require("customize-cra");
const path = require('path');

module.exports = {
    webpack: override(
        addDecoratorsLegacy(),
        useEslintRc(),
        addWebpackAlias({
            ['@']: path.resolve(__dirname, 'src')
        })
    )
};